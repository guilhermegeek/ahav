﻿using System.Linq;
using Admin.ServiceModel.Controllers;
using Ahav.Infrastructure;
using Ahav.ServiceInterface.Business;
using Ahav.ServiceInterface.Filters;
using Ahav.ServiceInterface.Services;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Operations;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace Admin.ServiceInterface.Controllers
{
    public class TemplateController : Service
    {
        [LoggedIn]
        [RequestHasAppId]
        public object Get(TemplateEditController request)
        {
            using (var mediaService = base.ResolveService<MediaService>())
            {
                var template = TemplateBus.GetTemplate(request.Id);

                var response = new TemplateEditControllerResponse
                {
                    Id = template.Id,
                    Body = template.Body,
                    Name = template.Name,
                    Title = template.Title,
                    Less = template.Less
                };

                if (template.Files != null && template.Files.Any())
                {
                    var files = mediaService.Get(new GetMedias() { Ids = template.Files });
                    response.Files = files.Medias;
                }
                return new HttpResult(response)
                {
                    View = "EditTemplate",
                    Template = Request.IsAjax() ? "" : "_LayoutAdmin"
                };
            }
        }
        [LoggedIn]
        [RequestHasAppId]
        public object Get(ContentListController request)
        {
            var response = new ContentListControllerResponse();
            
            return new HttpResult(response)
                       {
                           View = "ContentList",
                           Template = Request.IsAjax()
                                          ? "" : "_LayoutAdmin"
                       };
        }
        [LoggedIn]
        [RequestHasAppId]
        public object Get(ContentCreateController request)
        {
            var response = new ContentCreateControllerResponse();
            return new HttpResult(response)
                       {
                           View = "ContentCreate",
                           Template = Request.IsAjax()
                                          ? ""
                                          : "_LayoutAdmin"
                       };
        }
        public TemplateBusiness TemplateBus { get; set; }
    }
}
