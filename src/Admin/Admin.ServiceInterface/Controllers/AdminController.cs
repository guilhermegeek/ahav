﻿using System.Linq;
using Admin.ServiceModel.Controllers;
using Admin.ServiceModel.Operations;
using Ahav.Infrastructure;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Filters;
using Ahav.ServiceInterface.Services;
using Ahav.ServiceModel.ApiDtos;
using Magazine.ServiceInterface.Data;
using Magazine.ServiceInterface.Services;
using Magazine.ServiceModel.ApiDtos;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace Admin.ServiceInterface.Controllers
{
    public class AdminController : Service
    {

        public HttpResult Any(UnauthorizedController request)
        {
            return new HttpResult { Template = Request.IsAjax() ? "" : "_LayoutAdmin", View = "Unauthorized"};
        }
     

        [Route("admin")]
        [LoggedIn]
        public object Get(AdminHomeController request)
        {
            var response = new AdminHomeControllerResponse();
            using (var appService = base.ResolveService<ApplicationAhavService>())
            {
                var appsResponse = appService.Get(new GetProjects {OwnersIds = new[] {Request.GetUserId()}});
                response.Apps = appsResponse.Projects;
            }
            return new HttpResult(response)
                       {
                           View = "Admin",
                           Template = Request.IsAjax() ? "" : "_LayoutAdmin"
                       };
        }
        public BlogService BlogService { get; set; }
        public ContentService ContentService { get; set; }
        public ContentRepository ContentRepository { get; set; }
        public TemplateRepository TemplateRepo { get; set; }
        public ArticleRepository BlogArticleRepo { get; set; }

        

        // Blog
        
        
        
        

       // Content
        //[LoggedIn]
        //[RequestHasAppId(Features = new[] { AhavFeature.Cms })]
        //public object Get(ViewContents request)
        //{
        //    var templateService = base.ResolveService<ContentTemplateService>();
        //    using (var contentService = base.ResolveService<ContentService>())
        //    {
        //        var response = new ViewContentsResponse();

        //        var res = contentService.Get(new GetContents {ProjectId = Request.GetProjectId()});
        //        response.Contents = res.Contents;
        //        //response.Contents = !Request.HasProject()
        //        //                        ? ContentRepository.Find(new QueryContent()).Content
        //        //                        : ContentRepository.GetByProject(Request.GetProjectId());

        //        //var tpls = TemplateRepo.GetTemplatesAvailable(Request.GetProjectId());
        //        var templates = templateService.Get(new GetTemplatesAvailable {});
        //        response.Templates = templates.Templates;

        //        //else
        //        //{
        //        //    ObjectId[] ids = TemplateRepo.GetTemplatesAvailable(Request.GetProjectId());
        //        //    var templates = TemplateRepo.GetTemplates(ids.ToArray());
        //        //    if (templates != null)
        //        //    {
        //        //        templates.ForEach(x => response.Templates.Add(x.ToInfo()));
        //        //    }
        //        //}
        //        return new HttpResult(response)
        //                   {
        //                       View = "ViewContents",
        //                       Template = Request.IsAjax() ? "" : "_LayoutAdmin"
        //                   };
        //    }
        //}


        //[LoggedIn]
        //[RequestHasAppId]
        //public object Get(ViewProjectVideoInit request)
        //{
        //    var response = new ViewProjectVideoInitResponse();
        //    using (var projectService = base.ResolveService<ApplicationAhavService>())
        //    {
        //        var res = projectService.Get(new GetProjectInitModal {ProjectId = Request.GetProjectId()});
        //        if (res.ResponseStatus != null)
        //            throw HttpError.Conflict(JsonSerializer.SerializeToString(res.ResponseStatus));
        //        if (res.HasVideo)
        //        {
        //            response.HasVideo = true;
        //            response.File = res.Video;
        //        }
        //    }
        //    using(var mediaService = base.ResolveService<MediaService>())
        //    {
        //        var res = mediaService.Get(new GetMedias { MediaType = MediaType.Video, ProjectId = Request.GetProjectId()});
        //        if (res.ResponseStatus != null)
        //            throw HttpError.Conflict(JsonSerializer.SerializeToString(res.ResponseStatus));
        //        response.Files = res.Medias;
        //    }
        //    return new HttpResult(response)
        //               {
        //                   View = "EditProjectVideoInit",
        //                   Template = Request.IsAjax()
        //                                  ? ""
        //                                  : "_LayoutAdmin"
        //               };
        //}

        
        //[LoggedIn]
        //[RequestHasAppId]
        //public object Get(ViewDomainSettings request)
        //{
        //    var response = new ViewDomainSettingsResponse();
        //    return new HttpResult(response)
        //               {
        //                   View = "DomainSettings",
        //                   Template = Request.IsAjax() ? "" : "_LayoutAdmin"
        //               };
        //}
    }
}