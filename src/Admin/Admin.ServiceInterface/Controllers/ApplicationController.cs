﻿using System;
using Admin.ServiceModel.Controllers;
using Ahav.Infrastructure;
using Ahav.ServiceInterface.Business;
using Ahav.ServiceInterface.Filters;
using Ahav.ServiceInterface.Services;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Common;
using ServiceStack.Common.Web;
using ServiceStack.ServiceInterface;

namespace Admin.ServiceInterface.Controllers
{
    public class ApplicationController : Service
    {
        [LoggedIn]
        public HttpResult Get(ApplicationEditController request)
        {
            var response = new ApplicationEditControllerResponse();
            return new HttpResult(response) {View = "ApplicationEdit", Template = Request.IsAjax() ? "" : "_LayoutAdmin"};
        }
        [LoggedIn]
        public HttpResult Get(ApplicationListController request)
        {
            var response = new ApplicationListControllerResponse();
            var apps = ApplicationBus.Query(Request.GetUserId());
            return new HttpResult(response) { View = "ApplicationList", Template = Request.IsAjax() ? "" : WebLayouts.LayoutAdmin };
        }
        [LoggedIn]
        public HttpResult Get(ApplicationCreateController request)
        {
            var response = new ApplicationCreateControllerResponse();
            
            // Public domains
            var domains = AppDomainBus.GetPublicDomains();
            response.AvailableDomains = domains;
            
            return new HttpResult(response) {View = "ApplicationCreate", Template = Request.IsAjax() ? "" : WebLayouts.LayoutAdmin};
        }

        [LoggedIn]
        [RequestHasAppId]
        public object Get(ApplicationViewController request)
        {
            var response = new ApplicationViewControllerResponse();

            var project = ApplicationBus.Get(request.Id);
            if (project == null)
            {
                throw new Exception();
            }

            response.ApplicationDescription = project.Description;
            response.ApplicationName = project.Name;
            response.ApplicationDomain = project.Description;
            response.Features = project.Features;

            return new HttpResult(response) {View = "ApplicationView", Template = Request.IsAjax() ? "" : "_LayoutAdmin"};
        }
        public ApplicationBusiness ApplicationBus { get; set; }
        public AppDomainBusiness AppDomainBus { get; set; }
    }
}
