﻿using Admin.ServiceModel.Controllers;
using Ahav.Infrastructure;
using Ahav.ServiceInterface.Business;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Filters;
using Ahav.ServiceInterface.Services;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Operations;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace Admin.ServiceInterface.Controllers
{
    public class ContentController  : Service
    {
        public TemplateBusiness TemplateBus { get; set; }
        public ContentBusiness ContentBus { get; set; }
        public ContentRepository ContentRepo { get; set; }

        [LoggedIn]
        [RequestHasAppId]
        public HttpResult Get(ContentListController request)
        {
            var response = new ContentListControllerResponse();
            using(var contentService = base.ResolveService<ContentService>())
            {
                var res = contentService.Get(new GetContents { ProjectId = Request.GetProjectId() });
                response.Contents = res.Contents;
            }
            using(var templateService = base.ResolveService<TemplateService>())
            {
                var tplRes = templateService.Get(new GetTemplatesAvailable() {});
                response.Templates = tplRes.Templates;
            }
            return new HttpResult(response)
                       {
                           View = "ContentList",
                           Template = Request.IsAjax()
                                          ? ""
                                          : "_LayoutAdmin"
                       };
        }

        public HttpResult Get(ContentEditController request)
        {
            var content = ContentRepo.Get(request.Id);
            var tpls = TemplateBus.GetTemplatesForApp(Request.GetProjectId());
            var response = new ContentEditControllerResponse
                           {
                               Id = content.Id,
                               Body = content.Body,
                               Css = content.Css,
                               //Files = content.Files,
                               Javascript = content.Javascript,
                               Slug = content.Slug,
                               Template = content.Template,
                               Title = content.Title,
                               Templates = tpls
                           };
            return new HttpResult(response)
                       {
                           View = "ContentEdit",
                           Template = Request.IsAjax()
                                          ? ""
                                          : "_LayoutAdmin"
                       };
        }
    }
}
