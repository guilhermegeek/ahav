﻿using System.Collections.Generic;
using Admin.ServiceModel.Controllers;
using Admin.ServiceModel.Operations;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Services;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Dtos;
using ServiceStack.Common.Extensions;
using ServiceStack.Common.Web;
using ServiceStack.ServiceInterface;

namespace Admin.ServiceInterface.Controllers
{
    public class GuestController : Service
    {
        public object Any(GuestController request)
        {
            // If authenticated, redirected to homepage
            var response = new GuestViewControllerResponse();
            var packages = PackageRepo.GetPackages(true, 0, 100);

            response.Packages = new List<AhavPackageInfo>();
            packages.ForEach(x => response.Packages.Add(x.ToInfo()));
            return new HttpResult(response)
                       {
                           View = "Welcome",
                           Template = "_LayoutGuest"
                       };
        }
        public HttpResult Any(ViewAccountConfirm request)
        {
            var response = new ViewAccountConfirmResponse();
            return new HttpResult(response)
                       {
                           View = "AccountConfirm",
                           Template = Request.IsAjax() ? "" : "_LayoutGuest"
                       };
        }

        public PackageRepository PackageRepo { get; set; } 
    }
}