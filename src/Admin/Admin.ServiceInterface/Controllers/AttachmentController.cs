﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Admin.ServiceModel.Controllers;
using Ahav.Infrastructure;
using Ahav.ServiceInterface.Filters;
using Ahav.ServiceInterface.Services;
using Ahav.ServiceModel.Operations;
using ServiceStack.Common.Web;
using ServiceStack.ServiceInterface;
using ServiceStack.Text;

namespace Admin.ServiceInterface.Controllers
{
    public class AttachmentController : Service
    {
        [LoggedIn]
        [RequestHasAppId]
        public object Get(AttachmentListController request)
        {
            var response = new AttachmentListControllerResponse();
            using (var mediaService = base.ResolveService<MediaService>())
            {
                var res = mediaService.Get(new GetMedias { ProjectId = Request.GetProjectId() });
                if (res.ResponseStatus != null)
                {
                    throw new Exception(JsonSerializer.SerializeToString(res.ResponseStatus));
                }
                response.Files = res.Medias;
            }
            return new HttpResult(response)
            {
                View = "Attachments",
                Template = Request.IsAjax()
                               ? ""
                               : "_LayoutAdmin"
            };
        }
    }
}
