﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Admin.ServiceModel.Controllers;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Filters;
using Ahav.ServiceInterface.Services;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Operations;
using Magazine.ServiceInterface.Data;
using Magazine.ServiceInterface.Services;
using Magazine.ServiceModel.ApiDtos;
using ServiceStack.Common.Web;
using ServiceStack.ServiceInterface;

namespace Admin.ServiceInterface.Controllers
{
    public class MagazineController : Service
    {
        public ArticleRepository BlogArticleRepo { get; set; }
        public TemplateRepository TemplateRepo { get; set; }

        //[LoggedIn]
        //[RequestHasAppId]
        public HttpResult Get(BlogCreateController request)
        {
            var response = new BlogCreateController();
            return new HttpResult(response)
                   {
                       Template = Request.IsAjax() ? "" : "_LayoutAdmin",
                       View = "BlogCreate"
                   };
        }

        [LoggedIn]
        [RequestHasAppId(Features = new[] { AhavFeature.Blog })]
        public object Get(BlogArticleEditController request)
        {
            var response = new BlogArticleEditControllerResponse();
            var article = BlogArticleRepo.Get(request.Id);
            var tplIds = TemplateRepo.GetTemplatesAvailable(Request.GetProjectId());
            var templates = TemplateRepo.GetTemplates(tplIds);
            templates.ForEach(x => response.Templates.Add(x.ToInfo()));
            if (article.Files.Any())
            {
                using (var mediaService = base.TryResolve<MediaService>())
                {
                    var files = mediaService.Get(new GetMedias { Ids = article.Files });
                    files.Medias.ToList().ForEach(x => response.Files.Add(x));
                }
                //article.Files.ToList().ForEach(x => response.Files.Add(new MediaDto {Id = x}));
            }

            if (article == null || article.Id.IsEmpty())
                throw HttpError.NotFound("article not found");

            response.Id = article.Id;
            response.Title = article.Title;
            response.Headline = article.Headline;
            response.Body = article.Body;
            response.Slug = article.Slug;
            response.TemplateId = article.TemplateId;

            return new HttpResult(response)
            {
                View = "BlogArticleEdit",
                Template = Request.IsAjax() ? "" : "_LayoutAdmin"
            };
        }

        [DefaultView("AddArticle")]
        [LoggedIn]
        public object Get(BlogArticleCreateController request)
        {
            var response = new BlogArticleCreateControllerResponse();
            using (var blogService = base.ResolveService<BlogService>())
            using (var blogArticleCategoryService = base.ResolveService<BlogArticleCategoryService>())
            {
                response.Blogs = blogService.Get(new GetBlogs()).Blogs;
                response.Categories = blogArticleCategoryService.Get(new GetBlogArticleCategory { }).Categories;
            }
            return response;
        }

        [LoggedIn]
        [RequestHasAppId(Features = new[] { AhavFeature.Blog })]
        public object Get(BlogViewController request)
        {
            using (var blogArticleService = base.ResolveService<ArticleService>())
            {
                var response = new BlogViewControllerResponse();
                var blogRes = blogArticleService.Get(new GetBlogArticles { BlogId = request.BlogId });
                response.Articles = blogRes.Articles;
                return new HttpResult(response)
                {
                    Template = Request.IsAjax() ? "" : "_LayoutAdmin",
                    View = "ViewBlog"
                };
            }
        }
        /// <summary>
        /// Shows all created blogs for this Application
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [LoggedIn]
        [RequestHasAppId(Features = new[] { AhavFeature.Blog })]
        public object Get(BlogListController request)
        {
            var response = new BlogListControllerResponse();
            using(var blogService = base.ResolveService<BlogService>())
            response.Blogs = blogService.Get(new GetBlogs()).Blogs.ToArray();
            return new HttpResult(response)
                   {
                       Template = Request.IsAjax() ? "" : "_LayoutAdmin",
                       View = "ViewBlogs"
                   };
        }
    }
}
