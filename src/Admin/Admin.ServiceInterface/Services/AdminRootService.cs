﻿using Admin.ServiceModel.Operations;
using Ahav.Infrastructure;
using ServiceStack.Common.Web;
using ServiceStack.ServiceInterface;

namespace Admin.ServiceInterface.Services
{
    public class AdminRootService : Service
    {
        public object Get(ViewAccounts request)
        {
            var response = new ViewAccountsResponse();
            return new HttpResult(response)
            {
                View = "ViewAccounts",
                Template = Request.IsAdmin()
                               ? ""
                               : "_LayoutAdmin"
            };
        }
    }
}