﻿using System.Linq;
using Admin.ServiceModel.Operations;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Filters;
using Ahav.ServiceInterface.Services;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Dtos;
using Magazine.ServiceModel.ApiDtos;
using ServiceStack.ServiceInterface;

namespace Admin.ServiceInterface.Services
{
    public class AhavService : Service
    {
        /// <summary>
        /// Called on the run of admin application at clientside
        /// Authenticate user, return his applications, notifications, etc
        /// Acts a boot method for AngularJS app
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [LoggedIn]
        public AdminConnectResponse Any(AdminConnect request)
        {
            var response = new AdminConnectResponse();
            //if (Request.GetProjectId().IsEmpty())
            //    return response;
            //using (var blogService = base.ResolveService<BlogService>())
            //{
            //    var blogs = blogService.Get(new GetBlogs { OwnersIds = new[] { Request.GetUserId() } });
            //    if (blogs.ResponseStatus == null && blogs.Blogs.Any())
            //    {
            //        response.Blogs = blogs.Blogs;
            //        var blogRes = blogService.Get(new GetBlog { Id = blogs.Blogs.First().Id });
            //        //response.Model.BlogDefault = blogRes.Blog;

            //    }
            //}
            var appsId = AppRepo.GetProjectsIdByUser(Request.GetUserId());
            var apps = AppRepo.Find(appsId);
            response.Apps = apps.ToArray();
            response.AppActive = AppRepo.Get(apps[0].Id).ToDto();
            return response;
        }
        [LoggedIn]
        public AhavConnectResponse Any(AhavConnect request)
        {
            var response = new AhavConnectResponse();
            //var appsId = AppRepo.GetProjectsIdByUser(Request.GetUserId());
            //var apps = AppRepo.Find(appsId);
            //response.Apps = apps.ToArray();
            //using (var projectService = base.ResolveService<ApplicationAhavService>())
            //{
            //    var videoRes = projectService.Get(new GetProjectInitModal { ProjectId = Request.GetProjectId() });
            //    if (videoRes.ResponseStatus != null)
            //    {
            //        response.VideoInit = new PopupStartDialog
            //        {
            //            FileId = videoRes.Video.Id,
            //            FileUrl = videoRes.Video.Url
            //        };
            //    }
            //}
            return response;
        }
        
        public ApplicationAhavRepository AppRepo { get; set; }
    }
}