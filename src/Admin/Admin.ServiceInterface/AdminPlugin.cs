﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Admin.ServiceInterface.Controllers;
using Admin.ServiceInterface.Services;
using Admin.ServiceModel.Common;
using Admin.ServiceModel.Controllers;
using Admin.ServiceModel.Operations;
using Ahav.ServiceModel;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Operations;
using ServiceStack.ServiceInterface;
using ServiceStack.WebHost.Endpoints;

namespace Admin.ServiceInterface
{
    public class AdminPlugin : IPlugin
    {
        public void Register(IAppHost appHost)
        {
            var container = appHost.GetContainer();
            appHost.RegisterService(typeof(AdminController));
            appHost.RegisterService(typeof(GuestController));
            appHost.RegisterService(typeof(ConnectService));
            appHost.RegisterService(typeof(AhavService));
            appHost.RegisterService<MagazineController>();
            appHost.RegisterService<ApplicationController>();
            appHost.RegisterService<ContentController>();
            appHost.RegisterService<TemplateController>();
            appHost.RegisterService<AttachmentController>();


#if DEBUG
            appHost.Routes.Add<GetTests>("/tests", ApplyTo.All);
#endif

            appHost.Routes
                .Add<AdminHomeController>(AdminRoutes.Admin)
                .Add<GuestController>(AdminRoutes.Guest)
                .Add<UnauthorizedController>(AdminRoutes.Unauthorized)

                // Application
                .Add<ApplicationListController>(AdminRoutes.ApplicationList)
                .Add<ApplicationCreateController>(AdminRoutes.ApplicationCreate)
                .Add<ApplicationEditController>(AdminRoutes.ApplicationEdit)
                .Add<ApplicationViewController>(AdminRoutes.ApplicationView)
                
                // Content
                .Add<ContentListController>(AdminRoutes.ContentList)
                .Add<ContentCreateController>(AdminRoutes.ContentCreate)
                .Add<ContentEditController>(AdminRoutes.ContentEdit, ApplyTo.Get)
                
                // Content Template
                .Add<TemplateListController>(AdminRoutes.TemplateList)
                .Add<TemplateEditController>(AdminRoutes.TemplateEdit, ApplyTo.Get)
                .Add<TemplateCreateController>(AdminRoutes.TemplateCreate)

                // Magazine
                .Add<BlogListController>(AdminRoutes.BlogList)
                .Add<BlogCreateController>(AdminRoutes.BlogCreate)
                .Add<BlogViewController>(AdminRoutes.BlogView)
                .Add<BlogEditController>(AdminRoutes.BlogEdit)
                .Add<BlogArticleListController>(AdminRoutes.BlogArticleList)
                .Add<BlogArticleCreateController>(AdminRoutes.BlogArticleCreate)
                .Add<BlogArticleEditController>(AdminRoutes.BlogArticleEdit)

                .Add<AdminConnect>(AhavRoute.AdminConnect, ApplyTo.All)
                .Add<AhavConnect>(AhavRoute.AhavConnect, ApplyTo.All);

                
                // Base
               
                

                // Ahav
                //.Add<AdminController>("/admin", ApplyTo.Get)
                //.Add<AttachmentControllerList>("/admin/attachments", ApplyTo.Get)
                ////.Add<ViewBlogs>("/admin/blog", ApplyTo.Get)
                //.Add<BlogViewController>("/admin/blog/{BlogId*}", ApplyTo.Get)
                //.Add<BlogTemplateController>("/admin/blog/template/articles")
                //.Add<BlogCreateController>("/admin/blog/new", ApplyTo.Post)
                //.Add<BlogArticleCreateController>("/admin/blog/article/new", ApplyTo.Get)
                //.Add<BlogArticleEditController>("/admin/blog/article/edit/{Id*}")
                //.Add<TemplateCreateController>("/admin/template/new", ApplyTo.Get)
                //.Add<ContentController>("/admin/content")
                //.Add<ViewEditTemplate>("/admin/template/edit/{Id*}", ApplyTo.Get)
                //.Add<ViewAddContent>("/admin/content/new", ApplyTo.Get)
                //.Add<ViewEditContent>("/admin/content/edit/{Id*}", ApplyTo.Get)

                // Admin
             

                // Projects
                
        }
    }
}
