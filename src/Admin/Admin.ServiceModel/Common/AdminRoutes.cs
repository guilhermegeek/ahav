﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.ServiceModel.Common
{
    public class AdminRoutes
    {
        public const string Unauthorized = "/unauthorized";
        public const string Admin = "/admin";
        public const string Guest = "/welcome";
        
        // Attachments
        public const string AttachmentList = "/admin/attachments";

        // Applications
        public const string ApplicationList = "/admin/apps";
        public const string ApplicationCreate = "/admin/apps/new";
        public const string ApplicationView = "/admin/apps/{Id}";
        public const string ApplicationEdit = "/admin/apps/{Id}/edit";
        
        // Content
        public const string ContentList = "/admin/content";
        public const string ContentCreate = "/admin/content/new";
        public const string ContentEdit = "/admin/content/{Id}/edit";
        
        // Content Template
        public const string TemplateCreate = "/admin/template/new";
        public const string TemplateEdit = "/admin/template/{Id}/edit";
        public const string TemplateList = "/admin/template";

        // Magazine
        public const string BlogList = "/admin/blog";
        public const string BlogCreate = "/admin/blog/new";
        public const string BlogView = "/admin/blog/{Id}";
        public const string BlogEdit = "/admin/blog/{Id}/edit";
        public const string BlogArticleList = "/admin/blog/{Id}/article";
        public const string BlogArticleCreate = "/admin/blog/{Id}/article/new";
        public const string BlogArticleEdit = "/admin/blog/{Id}/article/{ArticleId}";
    }
}
