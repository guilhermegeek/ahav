﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Controllers
{
    public class ApplicationViewController : IReturn<ApplicationViewControllerResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class ApplicationViewControllerResponse : IHasResponseStatus
    {
        public string ApplicationName { get; set; }
        public string ApplicationDescription { get; set; }
        public string ApplicationDomain { get; set; }
        public AhavFeature[] Features { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
