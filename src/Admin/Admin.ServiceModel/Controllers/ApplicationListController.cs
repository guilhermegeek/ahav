﻿using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Controllers
{
    public class ApplicationListController : IReturn<ApplicationListControllerResponse>
    {
        
    }
    public class ApplicationListControllerResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
