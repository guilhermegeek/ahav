﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Dtos;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Controllers
{
    public class AdminHomeController : IReturn<AdminHomeControllerResponse>
    {

    }
    public class AdminHomeControllerResponse : IHasResponseStatus
    {
        public IList<AhavProjectInfo> Apps { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
