﻿using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Controllers
{
    public class TemplateCreateController : IReturn<TemplateCreateControllerResponse>
    {
    }
    public class TemplateCreateControllerResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }       
    }
}
