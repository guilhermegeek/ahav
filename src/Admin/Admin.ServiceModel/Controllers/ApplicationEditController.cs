﻿using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Controllers
{
    public class ApplicationEditController : IReturn<ApplicationCreateControllerResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class ApplicationEditControllerResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
