﻿using System.Collections.Generic;
using Ahav.ServiceModel.Dtos;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Controllers
{
    public class ContentListController : IReturn<ContentListControllerResponse>
    {
        
    }
    public class ContentListControllerResponse : IHasResponseStatus
    {
        public IList<ContentInfo> Contents { get; set; }
        public IList<ContentTemplateInfo> Templates { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }

}
