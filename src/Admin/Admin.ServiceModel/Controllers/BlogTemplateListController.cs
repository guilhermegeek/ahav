﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Controllers
{
    public class BlogTemplateController : IReturn<BlogTemplateControllerResponses>
    {
        public ObjectId ObjectId { get; set; }
    }
    public class BlogTemplateControllerResponses : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
