﻿using System.Collections.Generic;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Controllers
{
    public class TemplateEditController : IReturn<TemplateEditControllerResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class TemplateEditControllerResponse : IHasResponseStatus
    {
        public TemplateEditControllerResponse()
        {
            this.Files = new List<MediaDto>();
        }
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public string Javascript { get; set; }
        public string Less { get; set; }
        public Dictionary<string, string> Body { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public IList<MediaDto> Files { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
