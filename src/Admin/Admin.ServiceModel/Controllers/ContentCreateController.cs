﻿using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Controllers
{
    public class ContentCreateController : IReturn<ContentCreateControllerResponse>
    {
        
    }
    public class ContentCreateControllerResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
