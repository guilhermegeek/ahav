﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Dtos;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Controllers
{
    public class GuestViewController : IReturn<GuestViewController>
    {

    }
    public class GuestViewControllerResponse: IHasResponseStatus
    {
        public GuestViewControllerResponse()
        {
            this.Packages = new List<AhavPackageInfo>();
        }

        public bool RegistrationIsAvailable { get; set; }
        public IList<AhavPackageInfo> Packages { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
