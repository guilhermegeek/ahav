﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Controllers
{
    public class BlogArticleListController : IReturn<BlogArticleListControllerResponse>
    {
        /// <summary>
        ///  Blog Id
        /// </summary>
        public ObjectId Id { get; set; }
    }
    public class BlogArticleListControllerResponse : IHasResponseStatus 
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
