﻿using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Controllers
{
    public class ApplicationCreateController : IReturn<ApplicationCreateControllerResponse>
    {
        
    }
    public class ApplicationCreateControllerResponse : IHasResponseStatus
    {
        public string[] AvailableDomains { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
