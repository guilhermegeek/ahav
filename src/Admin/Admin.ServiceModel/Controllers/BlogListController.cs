﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magazine.ServiceModel.Dtos;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Controllers
{
    public class BlogListController : IReturn<BlogListControllerResponse>
    {
        
    }
    public class BlogListControllerResponse : IHasResponseStatus
    {
        public BlogInfo[] Blogs { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
