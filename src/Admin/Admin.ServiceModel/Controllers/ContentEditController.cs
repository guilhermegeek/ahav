﻿using System.Collections.Generic;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Controllers
{
    public class ContentEditController : IReturn<ContentEditControllerResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class ContentEditControllerResponse : IHasResponseStatus
    {
        public ObjectId Id { get; set; }
        public string Slug { get; set; }
        /// <summary>
        /// Title element for <html>
        /// </summary>
        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Body { get; set; }
        public ObjectId? Template { get; set; }

        public string Javascript { get; set; }
        // Files
        public IList<MediaDto> Files { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public IList<ContentTemplateInfo> Templates { get; set; }
        public string Css { get; set; }
    }
}
