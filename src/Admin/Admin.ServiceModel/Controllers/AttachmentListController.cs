﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Dtos;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Controllers
{
    public class AttachmentListController : IReturn<AttachmentListController>
    {

    }
    public class AttachmentListControllerResponse : IHasResponseStatus
    {
        public AttachmentListControllerResponse()
        {
            this.Files = new List<MediaDto>();
        }
        public IList<MediaDto> Files { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
