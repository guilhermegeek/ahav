﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magazine.ServiceModel.Dtos;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Controllers
{
    public class BlogArticleCreateController : IReturn<BlogArticleCreateControllerResponse>
    {

    }
    public class BlogArticleCreateControllerResponse : IHasResponseStatus 
    {
        public IEnumerable<BlogArticleCategoryDto> Categories { get; set; }
        public IEnumerable<BlogInfo> Blogs { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
