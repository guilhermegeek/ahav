﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Dtos;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Controllers
{
    public class BlogCreateController : IReturn<BlogCreateControllerResponse>
    {

    }
    public class BlogCreateControllerResponse : IHasResponseStatus
    {
        public BlogCreateControllerResponse()
        {
            this.Templates = new List<ContentTemplateInfo>();
        }
        public ResponseStatus ResponseStatus { get; set; }
        public IList<ContentTemplateInfo> Templates { get; set; }
    }
}
