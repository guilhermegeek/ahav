﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Controllers
{
    public class BlogArticleEditController : IReturn<BlogArticleEditControllerResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class BlogArticleEditControllerResponse : IHasResponseStatus
    {
        public BlogArticleEditControllerResponse()
        {
            this.Files = new List<MediaDto>();
            this.Templates = new List<ContentTemplateInfo>();
        }
        public ObjectId Id { get; set; }
        public IList<ContentTemplateInfo> Templates { get; set; }
        public IList<MediaDto> Files { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Headline { get; set; }
        public Dictionary<string, string> Body { get; set; }
        public ObjectId? TemplateId { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
