﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magazine.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Controllers
{
    public class BlogViewController : IReturn<BlogViewControllerResponse>
    {
        public ObjectId BlogId { get; set; }
    }

    public class BlogViewControllerResponse : IHasResponseStatus
    {
        public BlogViewControllerResponse()
        {
            this.Articles = new List<BlogArticleInfo>();
        }
        public IList<BlogArticleInfo> Articles { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
