﻿using System.Collections.Generic;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Operations
{
    public class ViewAccounts : IReturn<ViewAccountsResponse>
    {
        public ViewAccounts()
        {
            this.Take = 20;
        }
        public int Skip { get; set; }
        public int Take { get; set; }
    }
    public class ViewAccountsResponse : IHasResponseStatus
    {
        public IList<UserInfo> Accounts { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class ViewAccount : IReturn<ViewAccountsResponse>
    {
        public ObjectId Id { get; set; }   
    }
    public class ViewAccountResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}