﻿using System.Collections.Generic;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Operations
{
    public class ViewAdminRegister : IReturn<ViewAdminRegisterResponse>
    {

    }
    public class ViewAdminRegisterResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
    
    public class WaitResponse
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
    
    public class ViewAccountConfirm : IReturn<ViewAccountConfirmResponse>
    {
        
    }
    public class ViewAccountConfirmResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class Wait
    {

    }
}