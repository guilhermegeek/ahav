﻿using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Operations
{
    public class GetTests : IReturn<GetTestsResponse>
    {
        
    }
    public class GetTestsResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}