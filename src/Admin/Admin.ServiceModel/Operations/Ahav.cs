﻿using System.Collections.Generic;
using Magazine.ServiceModel.Dtos;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Operations
{
    public class ViewArticle : IReturn<ViewArticle>
    {
        
    }
    public class ViewArticles : IReturn<ViewArticles>
    {
        
    }
    public class ViewArticlesResponse : IHasResponseStatus
    {
        public IList<BlogArticleInfo> Articles { get; set; }
        public string RenderBody { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}