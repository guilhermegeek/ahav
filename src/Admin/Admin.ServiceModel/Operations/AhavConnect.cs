﻿using System;
using System.Collections.Generic;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using Magazine.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Admin.ServiceModel.Operations
{
    public class AdminConnect : IReturn<AdminConnectResponse>
    {
        
    }

    public class AdminConnectResponse : IHasResponseStatus 
    {
        public AdminConnectResponse()
        {
            this.Apps = new List<AhavProjectInfo>();
            this.Blogs = new List<BlogInfo>();
        }

        public bool IsAuthenticated { get; set; }

        /// <summary>
        /// Projects that user own or has special permissions
        /// @TODO Own Projects, Others Projects
        /// </summary>
        public IEnumerable<AhavProjectInfo> Apps { get; set; }
        public AhavProjectDto AppActive { get; set; }
        /// <summary>
        /// Blogs
        /// </summary>
        public IEnumerable<BlogInfo> Blogs { get; set; }

        public AuthApi Auth { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class AhavConnect : IReturn<AhavConnectResponse>
    {
        /// <summary>
        /// Indicates when the database on device (smartphone, browser, etc) was last updated
        /// </summary>
        public DateTime LocalDbLastUpdated { get; set; }   
    }
    public class AhavConnectResponse : IHasResponseStatus
    {
        public AhavProjectInfo[] Apps { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public PopupStartDialog VideoInit { get; set; }
    }
    public class PopupStartDialog
    {
        //    public Dictionary<string, string> Body { get; set; }
        public ObjectId FileId { get; set; }
        public string FileUrl { get; set; }
    }
}