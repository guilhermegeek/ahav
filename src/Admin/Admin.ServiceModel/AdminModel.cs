﻿using System.Collections.Generic;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using Magazine.ServiceModel.Dtos;

namespace Admin.ServiceModel
{
    public class AdminModel
    {
        public AdminModel()
        {
            this.Projects = new List<AhavProjectInfo>();
            this.Blogs = new List<BlogInfo>();
        }

        public AhavProjectDto ProjectDefault { get; set; }
        /// <summary>
        /// Projects that user own or has special permissions
        /// @TODO Own Projects, Others Projects
        /// </summary>
        public IEnumerable<AhavProjectInfo> Projects { get; set; }
        /// <summary>
        /// Blogs
        /// </summary>
        public IEnumerable<BlogInfo> Blogs { get; set; }

        public BlogDto BlogDefault { get; set; }
        public AuthApi Auth { get; set; }
        public MediaDto ProjectVideoInit { get; set; }
    }
}