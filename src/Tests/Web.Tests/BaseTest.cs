﻿using System;
using Ahav.ServiceInterface.Infrasctructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ahav.Web.Tests
{
    [TestClass]
    public sealed class BaseTest
    {
        [TestMethod]
        [AssemblyInitializeAttribute]
        public static void AssemblyIni()
        {
            BootDatabases.Boot();
        }
    }
}
