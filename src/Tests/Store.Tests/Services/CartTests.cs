﻿using System.Collections.Generic;
using System.Linq;
using Ahav.ServiceInterface.Extensions;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.Tests;
using MongoDB.Bson;
using NUnit.Framework;
using ServiceStack.ServiceClient.Web;
using ServiceStack.WebHost.Endpoints;
using Store.ServiceInterface.Data;
using Store.ServiceModel.ApiDtos;
using Store.ServiceModel.Domain;
using Store.ServiceModel.Domain.Entities;

namespace Store.Tests.Services
{
    [Category(TestCategory.Cart)]
    [TestFixture]
    public class CartTests
    {
        [SetUp]
        public void SetUp()
        {
            client = new JsonServiceClient("http://localhost:1337");
            var container = EndpointHost.Config.ServiceManager.Container;
            auth = client.Login();
            cartRepo = container.TryResolve<ShoppingCartRepository>();
            productRepository = container.TryResolve<ProductRepository>();
        }

        private AhavAuth auth;
        private JsonServiceClient client;
        private ShoppingCartRepository cartRepo;
        private ProductRepository productRepository;

        private ObjectId[] productsIds;
        private Product CreateProduct()
        {
            return productRepository.CreateEntity(new Product
                                                  {
                                                      Title =
                                                          new Dictionary<string, string>
                                                          {{Culture.pt_PT, "Test"}},
                                                      Description =
                                                          new Dictionary<string, string>
                                                          {{Culture.pt_PT, "Test"}}
                                                  });
        }
    }
}
