﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ahav.Infrastructure;
using Ahav.ServiceInterface.Extensions;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.Tests;
using MongoDB.Bson;
using NUnit.Framework;
using ServiceStack.Common.Utils;
using ServiceStack.ServiceClient.Web;
using ServiceStack.WebHost.Endpoints;
using Store.ServiceInterface.Data;
using Store.ServiceModel.ApiDtos;
using Store.ServiceModel.Domain;
using Store.ServiceModel.Domain.Entities;

namespace Store.Tests.Services
{
    [Category(TestCategory.Product)]
    [TestFixture]
    public class ProductTests
    {
        private JsonServiceClient client;
        private ProductRepository productRepo;
        private ProductCategoryRepository productCategoryRepo;

        public ObjectId productId;
        public ObjectId categoryId;
        private AhavAuth auth;

        
        [SetUp]
        public void SetUp()
        {
            client = new JsonServiceClient("http://localhost:1337");
            client.Login();
            var container = EndpointHost.Config.ServiceManager.Container;
            productRepo = container.TryResolve<ProductRepository>();
            productCategoryRepo = container.TryResolve<ProductCategoryRepository>();
            productId = CreateProductWithCategory().Id;
        }

        private Product CreateProductWithCategory()
        {
            var category = productCategoryRepo.CreateEntity(new ProductCategory
                                       {
                                           Name = new Dictionary<string, string> {{Culture.pt_PT, "Test"}},

                                       });
            return productRepo.CreateEntity(new Product
            {
                Title =
                    new Dictionary<string, string> { { Culture.pt_PT, "Test" } },
                Description =
                    new Dictionary<string, string> { { Culture.pt_PT, "Test" } },
                CategoriesIds = new List<ObjectId> {  category.Id}
            });
        }
        [Test]
        public void Store_CreateProduct()
        {
            try
            {
                client.Post(new PostProduct());
                throw new Exception("not validated");
            }
            catch (WebServiceException ex)
            {
             
            }
            categoryId =
                client.Post(new PostProductCategory
                                {Name = new Dictionary<string, string> {{Culture.pt_PT, "Cartolinas"}}}).Category.Id;
            var addProduct = new PostProduct
            {
                Categories = new List<ObjectId> { categoryId },
                Price = 10.5m,
                Description = new Dictionary<string, string> {{Culture.pt_PT, "Testing"}},
                Title = new Dictionary<string, string> { { Culture.pt_PT, "Testing" } }
            };
            var result = client.Post(addProduct);
            Assert.IsFalse(result.Product.Id.IsEmpty());
            Assert.IsTrue(productCategoryRepo.Get(categoryId).ProductIds.Contains(result.Product.Id));
            Assert.IsTrue(productRepo.Get(result.Product.Id).CategoriesIds.Contains(categoryId));
        }
        [Test]
        public void Store_GetProducts()
        {
            CreateProductWithCategory();
            var products = client.Get(new GetProducts());
            Assert.IsTrue(products.Products.LongCount() > 0);
        }
        [Test]
        public void Store_GetProductsByCategory_HasResults()
        {
            var p = CreateProductWithCategory();
            var response = client.Get(new GetProducts {Categories = new[] {p.CategoriesIds.First()}});
            Assert.IsTrue(response.Products.LongCount() > 0);
        }
        [Test]
        public void Product_GetById()
        {
            var p = CreateProductWithCategory();
            var product = client.Get(new GetProduct { Id = p.Id });
            Assert.NotNull(product);       
        }

        [Test]
        public void Product_Follow()
        {
            throw new NotImplementedException();
        }
        [Test]
        public void Product_ChangePrice_NotifyUsersFollowing()
        {
            var result = client.Put(new PutProductPrice {Id = productId, Price = 100});
            Assert.IsNull(result.ResponseStatus);
            var product = productRepo.Get(productId);
            Assert.AreEqual(product.Price, 100);
        }
        [Test]
        public void UpdateProductInfo()
        {
            var p = CreateProductWithCategory();
            var request = new PutProductInfo { Id = p.Id, Title = new Dictionary<string, string> { { Culture.pt_PT, "Testing" } }, Description = new Dictionary<string, string> { { Culture.pt_PT, "Testing" } } };
            var result = client.Put(request);
            Assert.IsNull(result.ResponseStatus);
            var product = productRepo.Get(p.Id);
            Assert.AreEqual(product.Title, request.Title);
            Assert.AreEqual(product.Description, request.Description);
        }

        [Test]
        public void Store_CreateProductCategory()
        {
            var request = new PostProductCategory { Name = new Dictionary<string, string> { { Culture.pt_PT, "Test" } } };
            var result = client.Post(request);
            Assert.IsNull(result.ResponseStatus);
            var category = productCategoryRepo.Get(result.Category.Id);
            Assert.NotNull(category);
            Assert.AreEqual(request.Name, category.Name);

        }
        [Test]
        public void Store_UpdateProductCategory()
        {
            var category = productCategoryRepo.CreateEntity(new ProductCategory {Name = new Dictionary<string, string> {{Culture.pt_PT, "Test"}}});
            var resultUpdate =
                client.Put(new PutProductCategory
                               {
                                   Id = category.Id,
                                   Name = new Dictionary<string, string> {{Culture.pt_PT, "new name"}}
                               });
            Assert.IsNull(resultUpdate.ResponseStatus);
            category = productCategoryRepo.Get(category.Id);
            Assert.IsTrue(category.Name.Any(x => x.Value == "new name"));

            client.Put(new PutProductCategories {Categories = new [] { category.Id}, Id = productId});
            var product = productRepo.Get(productId);
            Assert.IsTrue(product.CategoriesIds.Contains(category.Id));
            category = productCategoryRepo.Get(category.Id);
            Assert.IsTrue(category.ProductIds.Contains(productId));
        }
    }
}
