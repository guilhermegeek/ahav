﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace ServiceStack.Expantion.Tests
{
    [TestFixture]
    public class HtmlBasicValidatorTests
    {
        [Test]
        public void Expantion_ValidateHtml_Invalid()
        {
            var validator = new HtmlBasicValidator();
            Assert.IsTrue(validator.ValidateHtml("asdasdasd"));
            Assert.IsTrue(validator.ValidateHtml("<a>asdasdasd"));
            Assert.IsFalse(validator.ValidateHtml("<a>asd</a>"));
        }
    }
}
