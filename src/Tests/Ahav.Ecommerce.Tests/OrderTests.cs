﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ahav.Ecommerce.ServiceInterface;
using Ahav.Ecommerce.ServiceInterface.Data;
using Ahav.ServiceModel.Domain;
using Ahav.Tests;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using NUnit.Framework;
using ServiceStack.ServiceClient.Web;
using ServiceStack.WebHost.Endpoints;

namespace Ecommerce.Tests
{
    [TestFixture]
    public class OrderTests
    {
        private Auth auth;
        private JsonServiceClient client;
        private OrderRepository orderRepo;

        private ObjectId[] productsIds;

        [SetUp]
        public void SetUp()
        {
            client = new JsonServiceClient("http://localhost:1337");
            var container = EndpointHost.Config.ServiceManager.Container;
            auth = client.Login();
            orderRepo = container.TryResolve<OrderRepository>();
            var productRepo = new ProductRepository();
            productsIds = productRepo.GetAll().Select(x => x.Id).ToArray();
            Assert.IsTrue(productsIds.Count() >= 1);
        }

        [Test]
        public void Test_Order()
        {
            
        }
    }
}
