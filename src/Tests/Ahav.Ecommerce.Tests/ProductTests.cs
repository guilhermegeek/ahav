﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ahav;
using Ahav.Ecommerce.ServiceInterface;
using Ahav.Ecommerce.ServiceInterface.Data;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Ecommerce;
using Ahav.Tests;
using MongoDB.Bson;
using NUnit.Framework;
using ServiceStack.ServiceClient.Web;
using ServiceStack.WebHost.Endpoints;

namespace Ecommerce.Tests
{
    [TestFixture]
    public class ProductTests
    {
        private JsonServiceClient client;
        private ProductRepository productRepo;
        private ProductCategoryRepository productCategoryRepo;

        public ObjectId productId;
        public ObjectId categoryId;
        private Auth auth;

        public ProductTests()
        {
            client = new JsonServiceClient("http://localhost:1337");
        }
        [SetUp]
        public void SetUp()
        {
            
            client.Login();
            var container = EndpointHost.Config.ServiceManager.Container;
            productRepo = container.TryResolve<ProductRepository>();
            productCategoryRepo = container.TryResolve<ProductCategoryRepository>();
            productId = productRepo.CreateEntity(new Product
                                                     {
                                                         Title =
                                                             new Dictionary<string, string> {{Culture.pt_PT, "Test"}},
                                                         Description =
                                                             new Dictionary<string, string> {{Culture.pt_PT, "Test"}}
                                                     }).Id;
        }

        [Test]
        public void AddProduct()
        {
            try
            {
                client.Post(new PostProduct());
                throw new Exception("not validated");
            }
            catch (WebServiceException ex)
            {
                Assert.IsTrue(ex.ErrorCode == "NotEmpty");
            }
            categoryId =
                client.Post(new PostProductCategory
                                {Name = new Dictionary<string, string> {{Culture.pt_PT, "Cartolinas"}}}).Category.Id;
            var addProduct = new PostProduct
            {
                Categories = new List<ObjectId> { categoryId },
                Price = 10.5m,
                Description = new Dictionary<string, string> {{Culture.pt_PT, "Testing"}},
                Title = new Dictionary<string, string> { { Culture.pt_PT, "Testing" } }
            };
            var result = client.Post(addProduct);
            Assert.IsFalse(result.Product.Id.IsEmpty());
            Assert.IsTrue(productCategoryRepo.Get(categoryId).ProductIds.Contains(result.Product.Id));
            Assert.IsTrue(productRepo.Get(result.Product.Id).CategoriesIds.Contains(categoryId));
        }
        [Test]
        public void GetProducts()
        {
            var products = client.Get(new GetProducts());
            Assert.IsTrue(products.Products.LongCount() > 0);
        }
        [Test]
        public void GetProductsByCategory()
        {
            var response = client.Get(new GetProducts {Categories = new[] {categoryId}});
            Assert.IsTrue(response.Products.LongCount() > 0);
        }
        [Test]
        public void GetProduct()
        {
            var product = client.Get(new GetProduct { Id = productId });
            Assert.NotNull(product);       
        }
        [Test]
        public void UpdateProductPrice()
        {
            var result = client.Put(new PutProductPrice {Id = productId, Price = 100});
            Assert.IsNull(result.ResponseStatus);
            var product = productRepo.Get(productId);
            Assert.AreEqual(product.Price, 100);
        }
        [Test]
        public void UpdateProductInfo()
        {
            var request = new PutProductInfo { Id = productId, Title = new Dictionary<string, string> { { Culture.pt_PT, "Testing" } }, Description = new Dictionary<string, string> { { Culture.pt_PT, "Testing" } } };
            var result = client.Put(request);
            Assert.IsNull(result.ResponseStatus);
            var product = productRepo.Get(productId);
            Assert.AreEqual(product.Title, request.Title);
            Assert.AreEqual(product.Description, request.Description);
        }

        [Test]
        public void ProductCategories()
        {
            var request = new PostProductCategory {Name = new Dictionary<string, string> {{Culture.pt_PT, "Test"}}};
            var result = client.Post(request);
            Assert.IsNull(result.ResponseStatus);
            var category = productCategoryRepo.Get(result.Category.Id);
            Assert.NotNull(category);
            Assert.AreEqual(request.Name, category.Name);

            var resultUpdate =
                client.Put(new PutProductCategory
                               {
                                   Id = result.Category.Id,
                                   Name = new Dictionary<string, string> {{Culture.pt_PT, "new name"}}
                               });
            Assert.IsNull(resultUpdate.ResponseStatus);
            category = productCategoryRepo.Get(result.Category.Id);
            Assert.IsTrue(category.Name.Any(x => x.Value == "new name"));

            client.Put(new PutProductCategories {Categories = new [] { result.Category.Id}, Id = productId});
            var product = productRepo.Get(productId);
            Assert.IsTrue(product.CategoriesIds.Contains(result.Category.Id));
            category = productCategoryRepo.Get(result.Category.Id);
            Assert.IsTrue(category.ProductIds.Contains(productId));

        }

    }
}
