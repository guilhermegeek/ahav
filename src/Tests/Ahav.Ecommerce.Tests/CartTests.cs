﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ahav;
using Ahav.Ecommerce.ServiceInterface;
using Ahav.Ecommerce.ServiceInterface.Data;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Ecommerce;
using Ahav.Tests;
using MongoDB.Bson;
using NUnit.Framework;
using ServiceStack.ServiceClient.Web;
using ServiceStack.WebHost.Endpoints;

namespace Ecommerce.Tests
{
    [TestFixture]
    public class CartTests
    {
        private Auth auth;
        private JsonServiceClient client;
        private CartRepository cartRepo;
        private ProductRepository productRepository;

        private ObjectId[] productsIds;

        [SetUp]
        public void SetUp()
        {
            client = new JsonServiceClient("http://localhost:1337");
            client.Login();
            var container = EndpointHost.Config.ServiceManager.Container;
            auth = client.Login();
            cartRepo = container.TryResolve<CartRepository>();
            productRepository = container.TryResolve<ProductRepository>();
            productsIds = new ObjectId[]
                              {
                                  productRepository.CreateEntity(new Product
                                                                     {
                                                                         Title =
                                                                             new Dictionary<string, string>
                                                                                 {{Culture.pt_PT, "Test"}},
                                                                         Description =
                                                                             new Dictionary<string, string>
                                                                                 {{Culture.pt_PT, "Test"}}
                                                                     }).Id,
                                  productRepository.CreateEntity(new Product
                                                                     {
                                                                         Title =
                                                                             new Dictionary<string, string>
                                                                                 {{Culture.pt_PT, "Test"}},
                                                                         Description =
                                                                             new Dictionary<string, string>
                                                                                 {{Culture.pt_PT, "Test"}}
                                                                     }).Id

                              };
        }

        [Test]
        public void Can_Add_And_Remove_Products() 
        {
            var cart = cartRepo.GetCart(auth.UserId);

            // Add
            var request = new AddCartProduct
            {
                Id = productsIds.First(),
                Quantity = 10
            };
            client.Post(request);
            cart = cartRepo.GetCart(auth.UserId);
            Assert.IsTrue(cart.Products.Any(x => x.Id == request.Id && x.Quantity == request.Quantity));
            // Get
            var cartResponse = client.Get(new GetCart());
            Assert.IsTrue(cart.Products.Count == cartResponse.Cart.Products.Count);
            client.Post(request);
            cart = cartRepo.GetCart(auth.UserId);
            Assert.IsTrue(cart.Products.Any(x => x.Id == request.Id && x.Quantity == 20));
            // Delete
            client.Post(new DeleteProductCart { Id = request.Id, Quantity = 18 });
            cart = cartRepo.GetCart(auth.UserId);
            Assert.IsTrue(cart.Products.Any(x => x.Id == request.Id && x.Quantity == 2));
        }
        [Test]
        public void Can_Apply_Coupon()
        {
            
        }
    }
}
