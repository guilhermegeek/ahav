﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ahav;
using Ahav.Ecommerce.ServiceInterface;
using Ahav.Ecommerce.ServiceInterface.Data;
using Ahav.Infrasctructure;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Extensions;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Operations;
using Ahav.Tests;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NUnit.Framework;
using ServiceStack.Common.Web;
using ServiceStack.Configuration;
using ServiceStack.Redis;
using ServiceStack.ServiceClient.Web;
using ServiceStack.WebHost.Endpoints;

namespace Ecommerce.Tests   
{
    [SetUpFixture]
    public class BaseTest
    {
        private AppHost appHost;
        private bool bulkDb = true;
        public JsonServiceClient Client;


        [SetUp]
        public void SetUp()
        {
            appHost = new AppHost();
            appHost.Init();
            appHost.Plugins.Add(new AhavPlugin());
            appHost.Plugins.Add(new EcommercePlugin());
            appHost.Start("http://*:1337/");
            Ahav.Tests.ServiceInterface.BootDatabases.Boot();
            Client = new JsonServiceClient("http://localhost:1337/");
            Client.AllowAutoRedirect = false;
            Client.Login();
        }

        [TearDown]
        public void Teardown()
        {
            appHost.Stop();
        }
    }
}