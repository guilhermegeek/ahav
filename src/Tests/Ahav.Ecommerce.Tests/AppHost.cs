﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ahav;
using Ahav.Ecommerce.ServiceInterface;
using Ahav.ServiceInterface.Extensions;
using MongoDB.Driver;
using ServiceStack.Redis;
using ServiceStack.ServiceInterface.Validation;
using ServiceStack.WebHost.Endpoints;
using ServiceStack.Configuration;

namespace Ecommerce.Tests
{
    public class AppHost : AppHostHttpListenerBase
    {
        public AppHost() : base("EcommerceTests", typeof(ProductService).Assembly)
        {
            
        }
        public override void Configure(Funq.Container container)
        {
            var appSettings = new AppSettings();
            SetConfig(new EndpointHostConfig
                          {
                              DebugMode = true
                          });
            container.Register<IRedisClientsManager>(x => new PooledRedisClientManager(appSettings.GetString("RedisHost")));
            container.Register<IRedisClient>(x => x.TryResolve<IRedisClientsManager>().GetClient());
            container.Register<MongoDatabase>(
                x =>
                new MongoClient(appSettings.GetString("MongoDbHost")).GetServer().GetDatabase(
                    appSettings.GetString("MongoDbDatabase")));
            PreRequestFilters.Add(AccountExtensions.ApiCredentialsAction);
            Plugins.Add(new ValidationFeature());
            Plugins.Add(new AhavPlugin());
            Plugins.Add(new EcommercePlugin());
        }
    }
}
