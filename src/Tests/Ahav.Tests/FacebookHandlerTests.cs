﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.ApiDtos;
using NUnit.Framework;
using ServiceStack.ServiceClient.Web;

namespace Ahav.Tests
{
    [TestFixture]
    public class FacebookHandlerTests
    {
        [Test]
        public void FacebookTab_Homepage()
        {
            var client = new JsonServiceClient("http://localhost:1337");
            var res = client.Get(new FacebookTabRequest {});
            Assert.IsNull(res.ResponseStatus);
        }
    }
}
