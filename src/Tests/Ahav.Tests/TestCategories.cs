﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ahav.Tests
{
    public static class TestCategory
    {
        public const string Blog = "Blog";
        public const string Newsletters = "Newsletters";
        public const string News = "News";
        public const string Ecommerce = "Ecommerce";
        public const string Content = "Content";
        public const string Project = "Project";
        public const string Package = "Package";
        public const string User = "User";
        public const string Product = "Product";
        public const string Coupon = "Coupon";
        public const string Cart = "Cart";
        public const string Order = "Order";
        public const string Account = "Account";
        public const string Event = "Event";
        public const string Login = "Login";
        public const string Register = "Register";
        public const string Media = "Media";
        public const string Social = "Social";
        public const string Support = "Support";
        public const string Mail = "Mail";
    }
}
