﻿using System;
using System.Diagnostics;
using System.Net;
using Ahav;
using Ahav.ServiceInterface;
using Ahav.ServiceInterface.Services;
using Ahav.ServiceInterface.Validators;
using Ahav.ServiceModel;
using Ahav.ServiceModel.Operations;
using Magazine.ServiceInterface;
using MongoDB.Driver;
using ServiceStack.Common.Web;
using ServiceStack.Configuration;
using ServiceStack.Razor;
using ServiceStack.Redis;
using ServiceStack;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.Validation;
using ServiceStack.WebHost.Endpoints;
using Store;
using Store.ServiceInterface;


namespace Ahav.Tests
{
    public class AppHost : AppHostHttpListenerBase
    {
        public AppHost()
            : base("Ahav",
                typeof(AccountService).Assembly)
        {
        }
        public override void Configure(Funq.Container container)
        {
            Console.WriteLine("Configure App Host");
            var appSettings = new AppSettings();
            SetConfig(new EndpointHostConfig
                          {
                              DebugMode = true
                          });
            container.Register<IRedisClientsManager>(x => new PooledRedisClientManager(appSettings.GetString("RedisHost")));
            container.Register<MongoDatabase>(x => new MongoClient(appSettings.GetString("MongoDbHost")).GetServer().GetDatabase(appSettings.GetString("MongoDbDatabase")));

            Plugins.RemoveAll(x => x is AuthFeature);
            Plugins.Add(new ValidationFeature());
            Plugins.Add(new RazorFormat());
            Plugins.Add(new AhavPlugin());
            Plugins.Add(new StorePlugin());
            Plugins.Add(new MagazinePlugin());
            Plugins.Add(new ValidationFeature());

            //container.RegisterValidators(typeof(ChangePasswordValidator).Assembly); // ServiceModel

            // PostMedia isn't routted in Core because MediaService is internal!
            Routes.Add<PostMedia>("/files");
            base.ServiceExceptionHandler = (req, request, exception) =>
            {
                Debug.WriteLine("ServiceStack Exception: " + exception.Message);
                throw new Exception(exception.Message);
            };
        }
    }
}
