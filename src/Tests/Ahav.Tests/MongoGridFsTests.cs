﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Extensions;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson;
using NUnit.Framework;
using ServiceStack.ServiceClient.Web;
using ServiceStack.WebHost.Endpoints;


namespace Ahav.Tests
{
    [TestFixture]
    public class MongoGridFsTests
    {
        private MongoGridFs gridFs;
        private JsonServiceClient client;
        private ObjectId projectId;
        private AhavAuth auth;

        [SetUp]
        public void SetUp()
        {
            client = new JsonServiceClient("http://localhost:1337/");
            var container = EndpointHost.Config.ServiceManager.Container;
            gridFs = container.TryResolve<MongoGridFs>();
            auth = client.Login();
            projectId = container.Resolve<ApplicationAhavRepository>().GetProjectsIdByUser(auth.UserId)[0];
        }

        [Test]
        public void GridFs_Add_File()
        {
            var id = ObjectId.Empty;
            
            string fileName = string.Format("{0}\\{1}.txt", AssemblyTest.FilesRootDir,
                                            RsaTokenProvider.Instance.RandomString(4));
            File.WriteAllText(fileName, "testing testing");

            using (var file = File.OpenRead(fileName))
            {
                var gridFile = gridFs.AddFile(file, fileName, projectId);
                id = (ObjectId) gridFile.Id;
            }
            Assert.IsFalse(id.IsEmpty());
            using (var file = gridFs.GetFile(id))
            {
                var buffer = new byte[file.Length];
                file.Read(buffer, 0, (int) file.Length);
            }
        }
    }
}
