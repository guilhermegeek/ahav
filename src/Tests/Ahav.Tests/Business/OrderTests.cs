﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ahav.Infrastructure;
using Ahav.ServiceInterface.Business;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Extensions;
using Ahav.ServiceModel.BusinessDtos;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson;
using NUnit.Framework;
using ServiceStack.ServiceClient.Web;
using ServiceStack.WebHost.Endpoints;
using Store.ServiceInterface.Data;
using Store.ServiceModel.Domain.Entities;

namespace Ahav.Tests.Business
{
    [Category(TestCategory.Order)]
    [TestFixture]
    public class OrderTests
    {
        [SetUp]
        public void SetUp()
        {
            client = new JsonServiceClient("http://localhost:1337");
            var container = EndpointHost.Config.ServiceManager.Container;
            auth = client.Login();
            productRepo = container.Resolve<ProductRepository>();
            productCategoryRepo = container.Resolve<ProductCategoryRepository>();
            orderBus = container.Resolve<OrderBusiness>();
            orderRepo = container.Resolve<OrderRepository>();
        }

        [Test]
        public void Order_CreateOrder()
        {
            var p = CreateProductWithCategory();

            // Create invalid order and return a ResponseStatus
            var error = orderBus.Create(new CreateOrder());
            Assert.IsNotNull(error.ResponseStatus);
            Assert.Greater(error.ResponseStatus.Errors.Count, 0);

            // Create a valid order
            var response = orderBus.Create(new CreateOrder
                            {
                                Offers = new[] {new OrderOffer {Id = p.Id, Pricing = p.Price, Quantity = 2, Title = p.Title.First().Value}},
                                OrderStatus = OrderStatus.OrderPaymentDue,
                                PaymentDue = DateTime.Now.AddMonths(1),
                                UserId = auth.UserId
                            });
            Assert.IsNull(response.ResponseStatus);
            Assert.IsNotNull(response.Order);
            Assert.Greater(response.Order.Offers.Length, 0);
            
            // Validate order state 
            var order = orderRepo.GetById(response.Order.Id);
            Assert.IsNotNull(order);
            Assert.AreEqual(order.Status, OrderStatus.OrderPaymentDue);
        }

        // Private
        private AhavAuth auth;
        private JsonServiceClient client;


        private ObjectId[] productsIds;
        private ProductRepository productRepo;
        private ProductCategoryRepository productCategoryRepo;
        private OrderRepository orderRepo;
        private OrderBusiness orderBus;

        private Product CreateProductWithCategory()
        {
            var category = productCategoryRepo.CreateEntity(new ProductCategory
            {
                Name = new Dictionary<string, string> { { Culture.pt_PT, "Test" } },

            });
            return productRepo.CreateEntity(new Product
            {
                Title =
                    new Dictionary<string, string> { { Culture.pt_PT, "Test" } },
                Description =
                    new Dictionary<string, string> { { Culture.pt_PT, "Test" } },
                CategoriesIds = new List<ObjectId> { category.Id }
            });
        }
    }
}
