﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceInterface.Business;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Infrasctructure;
using NUnit.Framework;
using ServiceStack.Common.Utils;
using ServiceStack.WebHost.Endpoints;

namespace Ahav.Tests.Business
{
    /// <summary>
    /// Tests for application public domains
    /// </summary>
    [TestFixture]
    public class AppDomainTests 
    {
        [SetUp]
        public void SetUp()
        {
            var container = EndpointHost.Config.ServiceManager.Container;
            appDomainRepo = container.TryResolve<AppDomainRepository>();
            appDomainBus = container.TryResolve<AppDomainBusiness>();
        }

        [Test]
        public void AppDomain_GetDomainsAvailable_Exists()
        {
            var randomDomain = CreateRandomDomain();
            var domains = appDomainBus.GetPublicDomains();
            Assert.IsTrue(domains.Contains(randomDomain));
        }

        [Test]
        public void AppDomain_CreatePublic()
        {
            string domain = RsaTokenProvider.Instance.RandomString(7) + ".com";
            appDomainBus.AddPublicDomain(domain);
            var domains = appDomainRepo.GetPublicDomains();
            Assert.IsTrue(domains.Contains(domain));
        }

        [Test]
        public void AppDomain_RemovePublicDomain()
        {
            var randomDomain = CreateRandomDomain();
            appDomainRepo.AddPublicDomain(randomDomain);
            string[] domains = appDomainRepo.GetPublicDomains();
            Assert.IsTrue(domains.Contains(randomDomain));
            
            // Remove
            appDomainBus.RemovePublicDomain(randomDomain);
            domains = appDomainRepo.GetPublicDomains();
            Assert.IsTrue(domains == null || !domains.Contains(randomDomain));
        }
        /// <summary>
        /// Creates a new random domain
        /// </summary>
        /// <returns>Domain created</returns>
        private string CreateRandomDomain()
        {
            string randomDomain = RsaTokenProvider.Instance.RandomString(10) + ".pt";
            appDomainBus.AddPublicDomain(randomDomain);
            return randomDomain;
        }
        private AppDomainBusiness appDomainBus;
        private AppDomainRepository appDomainRepo;
    }
}
