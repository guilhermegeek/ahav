﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Linq.Expressions;
//using System.Text;
//using System.Threading.Tasks;
//using Ahav.Infrastructure;
//using MongoDB.Bson;
//using MongoDB.Driver;
//using NUnit.Framework;
//using ServiceStack.WebHost.Endpoints;

//namespace Ahav.Tests.Infrastructure
//{
//    public class DumbEntity
//    {
//        public ObjectId Id { get; set; }
//        public string Name { get; set; }
//    }
//    public interface IRepository<TEntity> where TEntity : class
//    {
//        TEntity FindById(ObjectId id);
//        void InsertGraph(TEntity entity);
//        void Update(TEntity entity);
//        void Delete(ObjectId id);
//        void Delete(TEntity entity);
//        void Insert(TEntity entity);
//        RepositoryQuery<TEntity> Query();
//    }
//    public sealed class RepositoryQuery<TEntity> where TEntity : class
//    {
//        private readonly List<Expression<Func<TEntity, object>>>
//            _includeProperties;

//        private readonly Repository<TEntity> _repository;
//        private Expression<Func<TEntity, bool>> _filter;
//        private Func<IQueryable<TEntity>,
//            IOrderedQueryable<TEntity>> _orderByQuerable;
//        private int? _page;
//        private int? _pageSize;

//        public RepositoryQuery(Repository<TEntity> repository)
//        {
//            _repository = repository;
//            _includeProperties =
//                new List<Expression<Func<TEntity, object>>>();
//        }

//        public RepositoryQuery<TEntity> Filter(
//            Expression<Func<TEntity, bool>> filter)
//        {
//            _filter = filter;
//            return this;
//        }

//        public RepositoryQuery<TEntity> OrderBy(
//            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy)
//        {
//            _orderByQuerable = orderBy;
//            return this;
//        }

//        public RepositoryQuery<TEntity> Include(
//            Expression<Func<TEntity, object>> expression)
//        {
//            _includeProperties.Add(expression);
//            return this;
//        }

//        public IEnumerable<TEntity> GetPage(
//            int page, int pageSize, out int totalCount)
//        {
//            _page = page;
//            _pageSize = pageSize;
//            totalCount = _repository.Get(_filter).Count();

//            return _repository.Get(
//                _filter,
//                _orderByQuerable, _includeProperties, _page, _pageSize);
//        }

//        public IEnumerable<TEntity> Get()
//        {
//            return _repository.Get(
//                _filter,
//                _orderByQuerable, _includeProperties, _page, _pageSize);
//        }
//    }
//    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
//    {
//        private MongoCollection coll;
//        public Repository()
//        {
//            coll = MongoDbHelper.Instance.Database.GetCollection<DumbEntity>(typeof (DumbEntity).Name);
//        }

//        public virtual TEntity FindById(ObjectId id)
//        {
//            return coll.FindOneByIdAs<TEntity>(id);
//        }


//        public virtual void Delete(object id)
//        {
//            var entity = DbSet.Find(id);
//            var objectState = entity as IObjectState;
//            if (objectState != null)
//                objectState.State = ObjectState.Deleted;
//            Delete(entity);
//        }

//        public virtual void Insert(TEntity entity)
//        {
//            coll.Insert(entity);
//        }

//        public virtual RepositoryQuery<TEntity> Query()
//        {
//            var repositoryGetFluentHelper =
//                new RepositoryQuery<TEntity>(this);

//            return repositoryGetFluentHelper;
//        }

//        internal IQueryable<TEntity> Get(
//            Expression<Func<TEntity, bool>> filter = null,
//            Func<IQueryable<TEntity>,
//                IOrderedQueryable<TEntity>> orderBy = null,
//            List<Expression<Func<TEntity, object>>>
//                includeProperties = null,
//            int? page = null,
//            int? pageSize = null)
//        {
//            IQueryable<TEntity> query = DbSet;

//            if (includeProperties != null)
//                includeProperties.ForEach(i => { query = query.Include(i); });

//            if (filter != null)
//                query = query.Where(filter);

//            if (orderBy != null)
//                query = orderBy(query);

//            if (page != null && pageSize != null)
//                query = query
//                    .Skip((page.Value - 1) * pageSize.Value)
//                    .Take(pageSize.Value);

//            return query;
//        }
//    }

//    public class ARepository
//    {
        
//    }
//    [TestFixture]
//    public class RepositoryTests 
//    {
//       [Test]
//        public void Repository_UpdateFields()
//       {
//           var container = EndpointHost.Config.ServiceManager.Container;
//           var repo = container.TryResolve<Repository<>>();
//       }
//    }
//}
