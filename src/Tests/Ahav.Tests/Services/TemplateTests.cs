﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Extensions;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Operations;
using MongoDB.Bson;
using NUnit.Framework;
using ServiceStack.Configuration;
using ServiceStack.ServiceClient.Web;
using ServiceStack.WebHost.Endpoints;

namespace Ahav.Tests.Services
{
    [TestFixture]
    [Category(TestCategory.Content)]
    public class TemplateTests : DefaultTest
    {
        [SetUp]
        public void SetUp()
        {
            client = new JsonServiceClient("http://localhost:1337/");
            auth = client.Login();
            Assert.IsFalse(auth.UserId.IsEmpty());
            var container = EndpointHost.Config.ServiceManager.Container;
            gridFs = container.TryResolve<MongoGridFs>();
            projectId = container.Resolve<ApplicationAhavRepository>().GetProjectsIdByUser(auth.UserId)[0];
            templateRepo = container.Resolve<TemplateRepository>();
            Assert.IsFalse(projectId.IsEmpty());
        }

        [Test]
        public void Templates_PutViewPage()
        {
            throw new NotImplementedException();
        }
        [Test]
        public void Templates_Create_AvaiableForUser()
        {
            var response =
                client.Post(new PostContentTemplate { Name = "nt" });
            Assert.IsNull(response.ResponseStatus);
            var template = templateRepo.GetTemplate(response.Template.Id);
            Assert.AreEqual(response.Template.Name, template.Name);
            var projectTemplates = templateRepo.GetTemplatesAvailable(projectId);
            var templates = templateRepo.GetTemplates(projectTemplates.ToArray());
            Assert.IsTrue(templates.Any(x => x.Id == template.Id));
        }
        [Test]
        public void Templates_Search_FindResults()
        {
            var newTemplate = CreateTemplate();
            var response = client.Get(new GetTemplate { TemplateId = newTemplate.Id });
            Assert.IsNull(response.ResponseStatus);
            Assert.NotNull(response.Template);
            newTemplate = templateRepo.GetTemplate(newTemplate.Id);
            Assert.NotNull(newTemplate);

        }
        [Test]
        public void Templates_Get()
        {
            var template = CreateTemplate();
            var templateTest = CreateTemplate();
            var response = client.Get(new GetTemplates { Ids = new[] { template.Id } });
            Assert.IsNull(response.ResponseStatus);
            Assert.IsTrue(response.Templates.Any());
            Assert.IsTrue(response.Templates.All(x => x.Id != templateTest.Id));
        }
        [Test]
        public void Templates_UpdateInfo_UpdateSlug()
        {
            var template = CreateTemplate();
            var response = client.Put(new PutTemplate { Less = "@co: #000; body { color: @co; }", Css = "body { color: #000000; }",  TemplateId = template.Id, Name = "test", Body = new Dictionary<string, string> { { Culture.pt_PT, "putt" } }, Title = new Dictionary<string, string> { { Culture.pt_PT, "putt" } } });
            Assert.IsNull(response.ResponseStatus);
            template = templateRepo.GetTemplate(template.Id);
            Assert.NotNull(template);
            Assert.IsTrue(template.Body.Any(x => x.Value == "putt"));
            var rootPath = new AppSettings().Get("StaticCssPath", "");
            var fileName = string.Format("{0}{1}.css", rootPath, template.Id.ToString());
            Assert.IsTrue(File.Exists(fileName));
            File.Delete(fileName);
        }
        [Test]
        public void Temolate_Delete()
        {
            var template = CreateTemplate();
            var response = client.Delete(new DeleteTemplate { TemplateId = template.Id });
            Assert.IsNull(response.ResponseStatus);
            template = templateRepo.GetTemplate(template.Id);
            Assert.IsNull(template);

            // Test the migration proccess
            template = CreateTemplate();
            var templateMigration = CreateTemplate();
            //var content = templateRepo.Create(new Content { Template = template.Id });
            //response = client.Delete(new DeleteTemplate { TemplateId = template.Id, IdMigration = templateMigration.Id });
            //Assert.IsNull(response.ResponseStatus);
            //templateRepository.Get(content.Id);
            //Assert.AreEqual(content.Template, templateMigration.Id);
        }
        /// <summary>
        /// Return public templates
        /// </summary>
        [Test]
        public void Template_GetPublic()
        {
            var template = AddTemplate();
            templateRepo.SaveTemplateAvailable(template.Id);
            var response = client.Get(new GetTemplatesAvailable { Skip = 0, Take = 100 });
            Assert.IsNull(response.ResponseStatus);
            Assert.IsTrue(response.Templates.Any(x => x.Id == template.Id));
        }
        /// <summary>
        /// Set a template as pblic
        /// </summary>
        [Test]
        public void Template_SaveAsPublic()
        {
            var template = AddTemplate();
            var response = client.Put(new PutTemplatePublic { TemplateId = template.Id });
            Assert.IsNull(response.ResponseStatus);
        }

        [Test]
        public void Template_GetFiles()
        {
            var newTemplate = AddTemplate();
            var fileId = AssemblyTest.CreateFile(gridFs, projectId);
            templateRepo.AddFile(newTemplate.Id, fileId);
            var files = client.Get(new GetTemplateFiles { TemplateId = newTemplate.Id });
            Assert.IsNull(files.ResponseStatus);
            Assert.IsTrue(files.Files.Any());

        }
        [Test]
        public void Template_UploadFile_Ok()
        {
            var newTemplate = AddTemplate();
            string fileName = RsaTokenProvider.Instance.RandomString(5) + ".txt";
            string filePath = AssemblyTest.FilesPath + fileName;
            File.WriteAllText(filePath, RsaTokenProvider.Instance.RandomString(3000));
            using(var file = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                var stream = new MemoryStream();
                file.CopyTo(stream);
                var request = new PostTemplateFile { TemplateId = newTemplate.Id };
                var response = client.PostFileWithRequest<PostTemplateFileResponse>(AhavRoute.PostTemplateFile, stream, fileName, request);
                Assert.IsNull(response.ResponseStatus);
                Assert.IsTrue(gridFs.FileExists(response.Medias.First().Id));
                newTemplate = templateRepo.GetTemplate(newTemplate.Id);
                Assert.IsTrue(newTemplate.Files.Contains(response.Medias.First().Id));    
            }
        }
        [Test]
        public void Attahments_Remove()
        {
            var template = AddTemplate();
            var fileId = AssemblyTest.CreateFile(gridFs, projectId);
            templateRepo.AddFile(template.Id, fileId);
            
            var deleteResponse = client.Delete(new DeleteTemplateFile { TemplateId = template.Id, FileId = fileId});
            Assert.IsNull(deleteResponse.ResponseStatus);
            Assert.IsFalse(gridFs.FileExists(fileId));
            template = templateRepo.GetTemplate(template.Id);
            Assert.IsFalse(template.Files.Contains(fileId));
        }

        [Test]
        public void Navbar_Update()
        {
            var request = new PutTemplateNavbar
                              {

                              };
            var response = Client.Put(request);

        }
        private JsonServiceClient client;
        private TemplateRepository templateRepo;

        private AhavAuth auth;
        private ObjectId projectId;
        private MongoGridFs gridFs;


        private Template CreateTemplate()
        {
            return templateRepo.AddTemplate(projectId,
                new Template
                {
                    Name = "nt",
                    Title = new Dictionary<string, string> { { Culture.pt_PT, "testing" } },
                    Body = new Dictionary<string, string> { { Culture.pt_PT, "testing" } }
                });
        }
        private Template AddTemplate()
        {
            var c = templateRepo.AddTemplate(projectId, new Template
            {
                Body =
                    new Dictionary<string, string>
                                                                                {
                                                                                    {
                                                                                        Culture.pt_PT,
                                                                                        string.Format("<p>Header</p>{0}<p>footer</p>", TemplateExtensions.HtmlDivRenderId)
                                                                                    }
                                                                                },
                Title =
                    new Dictionary<string, string> { { Culture.pt_PT, "title" } },
                Name = "Bambora",
                Less = "js"
            });
            return c;
        }

    }
}
