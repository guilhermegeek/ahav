﻿using NUnit.Framework;

namespace Ahav.Tests.Services
{
    [TestFixture]
    public class CommentsTests : DefaultTest
    {
        [SetUp]
        public void SetUp()
        {
            
        }

        /// <summary>
        /// Should write a new message and increment the total counter of Comments for that bucket
        /// </summary>
        [SetUp]
        public void Comments_WriteMessage_IncrementTotalCount()
        {
            
        }
        /// <summary>
        /// Query more than 100 messages (the actua bucket limit). 
        /// Ensures the response indicates that the result is paginated
        /// </summary>
        [SetUp]
        public void Comments_ReadMoreThan100BucketLimits_PaginatedResult()
        {
            
        }
    }
}
