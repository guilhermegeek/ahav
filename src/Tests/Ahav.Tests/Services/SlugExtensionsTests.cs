﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Ahav.Infrastructure;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using NUnit.Framework;
using ServiceStack.Redis;
using ServiceStack.WebHost.Endpoints;
using System.Numerics;

namespace Ahav.Tests.Services
{
    [TestFixture]
    public class SlugExtensionsTests
    {
        private SlugRepository slugRepo;
        private IRedisClientsManager redisManager;
        

        [SetUp]
        public void Setup()
        {
            var container = EndpointHost.Config.ServiceManager.Container;
            slugRepo = container.TryResolve<SlugRepository>();
            redisManager = container.TryResolve<IRedisClientsManager>();
        }

        [Test]
        public void Slug_SplitInBuckets()
        {
            var coll = MongoDbHelper.Instance.Database.GetCollection<Content>(typeof(Content).Name);
            using (var redisClient = redisManager.GetClient())
            {
                //redisClient.FlushDb();
                for (int e = 0; e < 100; e++)
                {
                    for (int i = 0; i < 30; i++)
                    {
                        
                        var content = new Content
                                          {
                                              Title =
                                                  new Dictionary<string, string>
                                                      {
                                                          {
                                                              Culture.en_US,
                                                              "New Title for This awesome blog in and long one of course for tests!!"
                                                          },
                                                          {
                                                              Culture.pt_PT,
                                                              "New Title for This awesome blog in and long one of course for tests!!"
                                                          }
                                                      },
                                              Description =
                                                  new Dictionary<string, string>
                                                      {
                                                          {
                                                              Culture.en_US,
                                                              "<p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p>"
                                                          },
                                                          {
                                                              Culture.pt_PT,
                                                              "<p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p>"
                                                          }
                                                      },
                                              Body =
                                                  new Dictionary<string, string>
                                                      {
                                                          {
                                                              Culture.en_US,
                                                              "<p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p>"
                                                          },
                                                          {
                                                              Culture.pt_PT,
                                                              "<p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p><p>New Title for This awesome blog in and long one of course for tests!!</p>"
                                                          }
                                                      },
                                              Slug = RsaTokenProvider.Instance.RandomString(10),
                                              Css =
                                                  "asdasd asdasdasdasd asdasdasdasd asdasdasdasd asdasdasdasd asdasdasdasd asdasdasdasd asdasdasdasd asdasdasdasd asdasdasdasd asdasdasdasd asdasdasdasd asdasdasdasd asdasdasdasd asdasdasdasd asdasdasdasd asdasdasdasd asdasdasdasd asdasdasdasd asdasdasdasd asdasdasdasd asdasdasdasd asdasd"
                                          };
                        coll.Insert(content);
                        var bucketHex = BigInteger.Parse(content.Id.ToString(), NumberStyles.HexNumber);
                        int bucket = content.Id.Increment/1000;
                        string slug = RsaTokenProvider.Instance.RandomString(14);
                        redisClient.Hashes["hex::" + bucket.ToString()].AddIfNotExists(new KeyValuePair<string, string>(slug, content.Id.ToString()));
                        //redisClient.Hashes["incr::" + idInt.ToString()].AddIfNotExists(new KeyValuePair<string, string>(slug, content.Id.ToString()));
                    }
                }
            }
        }
    }
    public class SlugId
    {
        public Object Id { get; set; }
        public SlugType Type { get; set; }
    }
}
