﻿using System;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using NUnit.Framework;

namespace Ahav.Tests.Services
{
    [TestFixture]
    [Category(TestCategory.Login)]
    public class LoginTests : DefaultTest
    {
        [SetUp]
        public void SetUp()
        {
            Initialize();
        }

        /// <summary>
        /// Show throw invalid exception. Password is empty
        /// </summary>
        [Test]
        public void Login_EmailPw_ThrowInvalid()
        {
            try
            {
                var errorResponse = Client.Post(new PostLogin { Email = "asdasdasd" });
                throw new Exception("Not Validated");
            }
            catch (Exception ex)
            {
            }
        }
        [Test]
        public void Login_EmailPw_Success()
        {
            var user = Resolve<UserRepository>().SaveUser(new User
                {
                    PrimaryEmail = string.Format("{0}@msn.com", RsaTokenProvider.Instance.RandomString(4)),
                    Password = "123"
                });
            Resolve<AuthRepository>().CreateAuth(new AhavAuth { Token = RsaTokenProvider.Instance.RandomString(4), UserId = user.Id });
            var result = Client.Post(new PostLogin { Email = user.PrimaryEmail, Password = user.Password });
            Assert.NotNull(result.Auth);
            Assert.IsTrue(Resolve<AuthRepository>().GetAuth(result.Auth.Id).Token == result.Auth.Key);
        }
    }
}
