﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Operations;
using MongoDB.Driver.Builders;
using NUnit.Framework;

namespace Ahav.Tests.Services
{
    public class RecoverAccountTests : DefaultTest
    {
        [SetUp]
        public void SetUp()
        {
            Initialize();
        }

        [Test]
        public void Account_TryRecoverPassword()
        {
            
            var response = Client.Post(new PostRecoverPassword {Email = "email@guilhermecardoso.pt"});
            Assert.IsNull(response.ResponseStatus, "erro");
            var token = Resolve<AccountRepository>().GetRecoverPasswordToken("email@guilhermecardoso.pt");
            Assert.IsFalse(string.IsNullOrEmpty(token), "Token for recover password wasn't creatd");
            var confirmResponse = Client.Get(new GetConfirmRecoverPassword { Email = "email@guilhermecardoso.pt", Token = token });
            Assert.IsTrue(Resolve<AccountRepository>().ValidatePassord(TestAuth.UserId, confirmResponse.NewPassword));
            // reset password for others tests
            Resolve<AccountRepository>().Update(TestAuth.UserId, new[] { Update<User>.Set(x => x.Password, "123") });
        }
    }
}
