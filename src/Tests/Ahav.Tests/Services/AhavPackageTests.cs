﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.DataDtos;
using NUnit.Framework;
using ServiceStack.ServiceClient.Web;

namespace Ahav.Tests.Services
{
    [TestFixture]
    [Category(TestCategory.Package)]
    public class AhavPackageTests: DefaultTest
    {
        [SetUp]
        public void SetUp()
        {
            Initialize();
        }

        [Test]
        public void Package_CreateInvalid_Throw()
        {
            try
            {
                Client.Post(new PostAhavPackage {});
                throw new Exception("");
            }
            catch(WebServiceException ex)
            {
                
            }
        }
        [Test]
        public void Package_CreateFree_Available()
        {
            var res = Client.Post(new PostAhavPackage
                                      {
                                          Activate = true,
                                          Start = DateTime.Now,
                                          Ammount = 0,
                                          Title = new Dictionary<string, string> {{Culture.pt_PT, "Free package"}},
                                          Description = new Dictionary<string, string> {{Culture.pt_PT, "Free package"}},
                                          Duration = TimeSpan.FromDays(365),
                                          Features = new AhavFeature[] {AhavFeature.Blog}
                                      });
            Assert.IsNull(res.ResponseStatus);
            var package = Resolve<PackageRepository>().ReadPackages(new ReadPackages {Unactive = false});
            Assert.IsNotNull(package);
            Assert.IsTrue(package.Packages.Any(x => x.Id == res.Package.Id));

        }
    }
}
