﻿using System;
using NUnit.Framework;

namespace Ahav.Tests.Services
{
    [TestFixture]
    public class NotificationTests : DefaultTest
    {
        [SetUp]
        public void SetUp()
        {
            Initialize();
        }
        /// <summary>
        /// Creates a new notification when the user as zero notifications
        /// </summary>
        [Test]
        public void Notification_CreateFirstUnreaded()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates a new notification when the user already has notifications unreaded
        /// </summary>
        [Test]
        public void Notification_CreateWithMoreUnreaded()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Reads notifications and must return unreade notifications
        /// </summary>
        [Test]
        public void Notification_GetNotifications_ReturnUnreaded()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Marks a notification as readed
        /// </summary>
        [Test]
        public void Notification_MarkAsReaded()
        {
            throw new NotImplementedException();
        }
    }
}
