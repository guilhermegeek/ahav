﻿using System.Collections.Generic;
using Ahav.Infrastructure;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Extensions;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Social;
using Ahav.ServiceModel.Social.Domain;
using MongoDB.Bson;
using NUnit.Framework;
using ServiceStack.ServiceClient.Web;
using ServiceStack.WebHost.Endpoints;

namespace Ahav.Tests.Services
{
    [TestFixture]
    [Category(TestCategory.Register)]
    public class SocialTests
    {
        private AhavAuth _auth;
        private JsonServiceClient _jsonClient;
        private SocialRepository _socialRepository;
        private ContentRepository contentRepo;
        private SlugRepository slugRepo;
        private string projectSubDomain;
        private ObjectId projectId;
        [SetUp]
        public void SetUp()
        {
            _jsonClient = new JsonServiceClient("http://localhost:1337/");
            var container = EndpointHost.Config.ServiceManager.Container;
            _socialRepository = container.TryResolve<SocialRepository>();
            contentRepo = container.TryResolve<ContentRepository>();
            slugRepo = container.TryResolve<SlugRepository>();
            _auth = _jsonClient.Login();
            projectId = container.Resolve<ApplicationAhavRepository>().GetProjectsIdByUser(_auth.UserId)[0];
            projectSubDomain = container.TryResolve<ApplicationAhavRepository>().GetSubdomain(projectId);
            
        }
        private Content CreateContent()
        {
            var c =  contentRepo.Add(new Content
            {
                Title = new Dictionary<string, string> { { Culture.pt_PT, "Test" } },
                Body = new Dictionary<string, string> { { Culture.pt_PT, "Test" } },
                Slug = "/fbtab"
            }, _auth.UserId);
            slugRepo.AddSlug(projectId, SlugType.Content, c.Id, RsaTokenProvider.Instance.RandomString(5));
            return c;
        }
        [Test]
        public void Test_SaveFacebookTab()
        {
            var content = CreateContent(); 
            var result = _jsonClient.Post(new PostFacebookTab {ContentId = content.Id, SubDomain = projectSubDomain});
            Assert.IsNull(result.ResponseStatus);
            var tab = _socialRepository.GetFacebookTab(projectSubDomain);
            Assert.AreEqual(tab, content.Id);
        }
        [Test]
        public void Test_GetFacebookTab()
        {
            var content = CreateContent();
            // Save Facebook Tab
            _socialRepository.SaveFacebookTab(projectSubDomain, content.Id);
            var result = _jsonClient.Get(new GetFacebookTab {SubDomain = projectSubDomain});
            Assert.IsNotEmpty(result.RenderBody);
            Assert.AreEqual(result.RenderBody, "Test");
        }
        [Test]
        public void Social_RegisterAccount()
        {
            var response = _jsonClient.Post(new RegisterSocial { Provider = SocialProvider.Facebook, Token = "" });
            Assert.IsFalse(response.UserId.IsEmpty());
        }
        [Test]
        public void Can_Like_Object()
        {
            var likeResponse =
                _jsonClient.Post(new PostSocialLike { ObjectId = 123123, Provider = SocialProvider.Facebook });
            
        }
    }
}
