﻿using System.Collections.Generic;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Extensions;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.DataDtos;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Resources;
using Magazine.ServiceInterface.Data;
using Magazine.ServiceModel.ApiDtos;
using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using Magazine.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceClient.Web;
using ServiceStack.WebHost.Endpoints;

namespace Ahav.Tests.Services
{
    public abstract class DefaultTest
    {
        public AhavAuth TestAuth;
        public ObjectId ProjectId;
        public JsonServiceClient Client;
        public DefaultTest()
        {
            // Setup is called once classe is inherited
            Initialize();
        }
        public void Initialize()
        {
            var container = EndpointHost.Config.ServiceManager.Container;
            Client = new JsonServiceClient("http://localhost:1337");
            TestAuth = Client.Login();
            ProjectId = container.Resolve<ApplicationAhavRepository>().GetProjectsIdByUser(TestAuth.UserId)[0];
            Client.Headers[ProjectExtensions.HttpHeaderProjectId] = ProjectId.ToString();
        }
        public T Resolve<T>()
        {
            var container = EndpointHost.Config.ServiceManager.Container;
            return container.Resolve<T>();
        }

        public Blog CreateBlog()
        {
            var blog = new Blog()
            {
                Description = new Dictionary<string, string> { { Culture.pt_PT, "Testing!" } },
                Title = new Dictionary<string, string> { { Culture.pt_PT, "Blog de test" } },
                Owner = TestAuth.UserId,
                SubDomain = RsaTokenProvider.Instance.RandomString(10),
            };
            Resolve<BlogRepository>().Create(blog);
            Resolve<SlugRepository>().AddSlug(ProjectId, SlugType.Blog, blog.Id, blog.SubDomain);
            return blog;
        }
         // Private Helper
        public BlogTemplate CreateBlogTemplate()
        {
            return Resolve<BlogTemplateRepository>().AddTemplate(new CreateBlogTemplate
                                                                     {
                                                                         Name = RsaTokenProvider.Instance.RandomString(5),
                                                                         Public = true,
                                                                         ProjectId = ProjectId,
                                                                         Body =
                                                                             new Dictionary<string, string>
                                                                                 {
                                                                                     {
                                                                                         Culture.pt_PT,
                                                                                         BlogDefaultTemplateResources.Body
                                                                                     }
                                                                                 },
                                                                         BodyViewArticle =
                                                                             new Dictionary<string, string>
                                                                                 {
                                                                                     {
                                                                                         Culture.pt_PT,
                                                                                         BlogDefaultTemplateResources.
                                                                                         BodyViewArticle
                                                                                     }
                                                                                 },
                                                                         BodyViewIndex =
                                                                             new Dictionary<string, string>
                                                                                 {
                                                                                     {
                                                                                         Culture.pt_PT,
                                                                                         BlogDefaultTemplateResources.
                                                                                         BodyViewIndex
                                                                                     }
                                                                                 }
                                                                     }).BlogTemplate;
        }

        /// <summary>
        /// Creates a new article without publishing
        /// </summary>
        /// <returns></returns>
        public Article CreateArticleWithTemplate(ObjectId blogId)
        {
            var blog = Resolve<BlogRepository>().Get(blogId);
            var tpl = CreateBlogTemplate();
            var category = new BlogArticleCategory
            {
                Name = "Test",
                Title = new Dictionary<string, string> { { Culture.pt_PT, "Test" } }
            };
            Resolve<BlogPostCategoryRepository>().Create(category);
            var a = new Article
            {
                Categories = new[] { category.Id },
                BlogId = blog.Id,
                Title = new Dictionary<string, string> { { Culture.pt_PT, "Test" } },
                Body = new Dictionary<string, string> { { Culture.pt_PT, "Test" } },
                CreatedBy = TestAuth.UserId,
                Headline = new Dictionary<string, string> { { Culture.pt_PT, "Test" } },
                Excerpt = new Dictionary<string, string> { { Culture.pt_PT, "Test" } },
                Slug = RsaTokenProvider.Instance.RandomString(10),
                State = BlogArticleState.Created,
                TemplateId = tpl.Id
            };
            Resolve<ArticleRepository>().Create(a);
            Resolve<BlogPostCategoryRepository>().AddPost(category.Id, new BlogArticleRef { Id = a.Id, State = BlogArticleState.Created });

            Resolve<BlogTemplateRepository>().SetDefaultTemplate(new UpdateBlogTemplateDefault { BlogId = blog.Id, BlogTemplateId = tpl.Id });
            Resolve<SlugRepository>().AddSlug(ProjectId, SlugType.Article, a.Id, a.Slug);
            return a;
        }
        public Article CreateArticleWithTemplate()
        {
            var blog = CreateBlog();
            return CreateArticleWithTemplate(blog.Id);
        }
        /// <summary>
        /// Creates a new package. Use ammoun 0 for free packages
        /// </summary>
        /// <param name="ammount"></param>
        /// <returns></returns>
        public AhavPackage CreatePackage(int ammount = 20)
        {
            return Resolve<PackageRepository>().Add(new CreatePackage
            {
                Ammount = ammount,
                Title =
                    new Dictionary<string, string> { { Culture.pt_PT, "Title" } },
                Description =
                    new Dictionary<string, string> { { Culture.pt_PT, "Desc" } },
                Features =
                    new AhavFeature[] { AhavFeature.Blog, AhavFeature.Cms },
                IsActive = true
            }).Package;
        }
    }
}
