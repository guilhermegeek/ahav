﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Common;
using NUnit.Framework;
using ServiceStack.ServiceClient.Web;

namespace Ahav.Tests.Services
{
    [TestFixture]
    [Category(TestCategory.Package)]
    public class PackageTests : DefaultTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
        }
        [Test]
        public void Package_CreateInvalid_Throw()
        {
            try
            {
                Client.Post(new PostAhavPackage());
                throw new Exception("Request not validated");
            }
            catch (WebServiceException ex)
            {
                Assert.AreEqual(ex.ErrorCode, "NotEmpty");
            }
        }
        [Test]
        public void Packages_CreateNew()
        {
            var request = new PostAhavPackage
                              {
                                  Title = new Dictionary<string, string> {{Culture.pt_PT, "Pt test"}},
                                  Description = new Dictionary<string, string> {{Culture.pt_PT, "Pt test"}},
                                  Activate = true,
                                  Ammount = 0,
                                  Duration = new TimeSpan(30, 0, 0, 0),
                                  Start = DateTime.Now,
                                  Features = new[]
                                                 {
                                                     AhavFeature.Blog, AhavFeature.Newsletters,
                                                     AhavFeature.Support,
                                                     AhavFeature.Ecommerce, AhavFeature.Cms,
                                                 }
                              };
            var response = Client.Post(request);
            Assert.IsNull(response.ResponseStatus);
            var packages = Resolve<PackageRepository>().GetPackages(true);
            Assert.IsTrue(packages.Any(x => x.Id == response.Package.Id));

            request.Activate = false;
            response = Client.Post(request);
            packages = Resolve<PackageRepository>().GetPackages(true);
            Assert.IsTrue(packages.All(x => x.Id != response.Package.Id));
            packages = Resolve<PackageRepository>().GetPackages(false);
            Assert.IsTrue(packages.Any(x => x.Id == response.Package.Id));
        }

        [Test]
        public void Packages_Remove()
        {
            throw new NotImplementedException();
        }
        [Test]
        public void PackageS_GetAvailable()
        {
            var package = CreatePackage();
            var response = Client.Get(new GetAhavPackagesAvailable());
            Assert.IsNull(response.ResponseStatus);
            Assert.IsTrue(response.Packages.Any());
        }
    }
}
