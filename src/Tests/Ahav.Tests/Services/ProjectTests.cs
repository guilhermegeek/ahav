﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Ahav.Infrastructure;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Extensions;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.DataDtos;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using NUnit.Framework;
using ServiceStack.ServiceClient.Web;
using ServiceStack.WebHost.Endpoints;

namespace Ahav.Tests.Services
{
    [TestFixture]
    [Category(TestCategory.Project)]
    public class ProjectTests
    {
        private ApplicationAhavRepository _projectRepository;
        private PackageRepository packageRepo;
        private JsonServiceClient _client;
        private AhavAuth _auth;
        private MongoGridFs gridFs;
        private ObjectId projectId;

        /// <summary>
        /// Creates a new Project with the state Enabled
        /// </summary>
        /// <returns></returns>
        private ApplicationAhav Create()
        {
            var p = this.CreatePackage();
            return
                _projectRepository.Add(_auth.UserId, "Testing", "Testing", RsaTokenProvider.Instance.RandomString(5),
                    new[] {AhavFeature.Cms, AhavFeature.Ecommerce}, AhavProjectState.Enabled);
        }
        /// <summary>
        /// Creates a new package. Use ammoun 0 for free packages
        /// </summary>
        /// <param name="ammount"></param>
        /// <returns></returns>
        private AhavPackage CreatePackage(int ammount = 20)
        {
            return packageRepo.Add(new CreatePackage
                                                        {
                                                            Ammount = ammount,
                                                            Title =
                                                                new Dictionary<string, string>
                                                                    {{Culture.pt_PT, "Title"}},
                                                            Description =
                                                                new Dictionary<string, string> {{Culture.pt_PT, "Desc"}},
                                                            Features =
                                                                new AhavFeature[]
                                                                    {AhavFeature.Blog, AhavFeature.Cms},
                                                                    IsActive = true
                                                        }).Package;
        }
        [SetUp]
        public void SetUp()
        {
            _client=new JsonServiceClient("http://localhost:1337/");
            var container = EndpointHost.Config.ServiceManager.Container;
            _projectRepository = container.Resolve<ApplicationAhavRepository>();
            packageRepo = container.Resolve<PackageRepository>();
            gridFs = container.Resolve<MongoGridFs>();
            _auth = _client.Login();
            projectId = _projectRepository.GetProjectsIdByUser(_auth.UserId)[0];
        }

        [Test]
        public void Apps_Delete()
        {
            var app = Create();
            var res = _projectRepository.Get(app.Id);
            Assert.IsNotNull(res);
            Assert.AreEqual(app.Name, res.Name);
            var resDel = _client.Delete(new DeleteApplication {Id = app.Id});
            Assert.IsNull(resDel.ResponseStatus);
            res = _projectRepository.Get(app.Id);
            Assert.IsNull(res);
        }
        /// <summary>
        /// Tests the validations for creating a new Project
        /// </summary>
        [Test]
        public void Apps_CreateValidation_Throw()
        {
            try
            {
                _client.Post(new PostApplication());
                throw new Exception("Request not validated");
            }
            catch (WebServiceException ex)
            {

            }
        }
        /// <summary>
        /// Projects for Packages with 0 ammount should be created without any order 
        /// </summary>
        [Test]
        public void App_CreatesWithFreePackage_Complete()
        {
            var pack = this.CreatePackage(0); // 0 in ammount for a free package
            var response = _client.Post(new PostApplication
                                            {
                                                Name = "Test Project", 
                                                Description = "Test description",
                                                PackageId = pack.Id,
                                                Domain = RsaTokenProvider.Instance.RandomString(3)
                                            });
            Assert.IsNull(response.ResponseStatus);
            Assert.NotNull(response.Project);
            var project = _projectRepository.Get(response.Project.Id);
            Assert.AreEqual(project.Name, response.Project.Name);
            Assert.AreEqual(project.ProjectState, AhavProjectState.Enabled);
            Assert.IsTrue(_projectRepository.GetProjectsIdByUser(_auth.UserId).Contains(response.Project.Id));
        }
        /// <summary>
        /// Creates a new project using a paid package
        /// Should save the project, create a new invoice
        /// </summary>
        [Test]
        public void App_CreateWithPaidPackage()
        {
            var package = CreatePackage(100);
            var response = _client.Post(new PostApplication
                                            {
                                                Name = "Test",
                                                Description = "Test description",
                                                PackageId = package.Id,
                                                Domain = RsaTokenProvider.Instance.RandomString(6)
                                            });
            Assert.IsNull(response.ResponseStatus);
            var project = _projectRepository.Get(response.Project.Id);
            Assert.AreEqual(project.ProjectState, AhavProjectState.Waiting);
        }
        [Test]
        public void Project_GetById()
        {
            var project = _projectRepository.Get(projectId);
            var response = _client.Get(new GetProject {Id = projectId});
            Assert.IsNull(response.ResponseStatus);
            Assert.AreEqual(response.Project.Name, project.Name);
        }

        [Test]
        public void App_GetByOwnerAndAppId()
        {
            var project = this.Create();
            var response = _client.Get(new GetProjects
                                           {
                                               OwnersIds = new[] {_auth.UserId}
                                           });
            Assert.IsNull(response.ResponseStatus);
            Assert.IsTrue(response.Projects.Any());

            response = _client.Get(new GetProjects
                                       {
                                           ProjectsIds = new[] {project.Id}
                                       });
            Assert.IsNull(response.ResponseStatus);
            Assert.IsTrue(response.Projects.Any());
        }
      
        [Test]
        public void Test_PrderPackage()
        {
            throw new NotImplementedException();
        }
        [Test]
        public void App_UpdateBasic()
        {
            var project = this.Create();
            var response = _client.Put(new PutApplicationBasic {Id = project.Id, Name = "1", Description = "2"});
            Assert.IsNull(response.ResponseStatus);
            var p = _projectRepository.Get(project.Id);
            Assert.IsTrue(p.Name == "1");
        }
        
        /// <summary>
        /// Get the Initial Video 
        /// </summary>
        [Test]
        public void TesT_GetVideoInit()
        {
            var file = AssemblyTest.CreateFile(gridFs, projectId);
            _projectRepository.SaveVideoInit(projectId, file);
            var response = _client.Get(new GetProjectInitModal { ProjectId = projectId });
            Assert.IsNull(response.ResponseStatus);
            Assert.IsTrue(response.HasVideo);
            Assert.AreEqual(response.Video.Id, file); 
        }
        [Test]
        public void Test_SetVideoInit()
        {
            var file = AssemblyTest.CreateFile(gridFs, projectId);
            var response = _client.Post(new PostProjectInitModal {FileId = file});
            Assert.IsNull(response.ResponseStatus);
            var video = _projectRepository.GetVideoInit(projectId);
            Assert.IsTrue(video.HasValue);
            Assert.AreEqual(video.Value, file);
        }

        [Test]
        public void Test_RemoveVideoInit()
        {
            var file = AssemblyTest.CreateFile(gridFs, projectId);
            _projectRepository.SaveVideoInit(projectId, file);
            var response = _client.Delete(new DeleteProjectInitModal {});
            Assert.IsNull(response.ResponseStatus);
            var video = _projectRepository.GetVideoInit(projectId);
            Assert.IsFalse(video.HasValue);
        }
        [Test]
        public void Application_UplodateAttachment()
        {
            string file = AssemblyTest.FilesRootDir + "\\TESTS.txt";
            File.WriteAllText(file, "asdasd");
            var filesToUpload = new FileInfo(file);
            var response = _client.PostFileWithRequest<PostAttachmentResponse>(AhavRoute.PostAttachment, filesToUpload,
                                                                               new PostAttachment());
            Assert.IsNull(response.ResponseStatus);
            Assert.IsNotNull(response.File);
            Assert.IsFalse(response.File.Id.IsEmpty());
        }
        [TearDown]
        public void TearDown()
        {
            
        }
    }
}
