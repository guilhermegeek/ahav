﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Ahav.Infrastructure;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Extensions;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Operations;
using MongoDB.Bson;
using NUnit.Framework;
using ServiceStack.ServiceClient.Web;
using ServiceStack.WebHost.Endpoints;

namespace Ahav.Tests.Services
{
    [TestFixture]
    [Category(TestCategory.Newsletters)]
    public class NewslettersTests
    {
        private NewslettersRepository newslettersRepo;
        private AhavAuth auth;
        private MongoGridFs gridFs;
        private JsonServiceClient client;
        private ObjectId projectId;

        private Newsletter CreateNewsletter()
        {
            var newsletter = new Newsletter
                                 {
                                     Content = new Dictionary<string, string> {{Culture.pt_PT, "Testing"}},
                                     Subject = new Dictionary<string, string> {{Culture.pt_PT, "Testing"}},
                                     IsHtml = true,
                                     State = NewsletterState.Draft
                                 };
            newslettersRepo.Create(newsletter);
            return newsletter;
        }

        [SetUp]
        public void Setup()
        {
            client = new JsonServiceClient("http://localhost:1337/");
            auth = client.Login();
            var container = EndpointHost.Config.ServiceManager.Container;
            gridFs = container.TryResolve<MongoGridFs>();
            newslettersRepo = container.TryResolve<NewslettersRepository>();
            projectId = container.Resolve<ApplicationAhavRepository>().GetProjectsIdByUser(auth.UserId)[0];
            
            client.AllowAutoRedirect = false;
        }
        [Test]
        public void Test_Post_Newsletter()
        {
            var postReq = new PostNewsletter {Name = "Testing" + RsaTokenProvider.Instance.RandomString(4)};
            var addResponse = client.Post(postReq);
            Assert.IsFalse(addResponse.Result.Id.IsEmpty());
            var newsletter = newslettersRepo.Get(addResponse.Result.Id);
            Assert.AreEqual(postReq.Name, newsletter.Name);
        }
        [Test]
        public void Test_Put_Newsletter()
        {
            var newsletter = CreateNewsletter();
            var putReq = new PutNewsletter {Id = newsletter.Id, Subject = newsletter.Subject, Body = newsletter.Content, IsHtml = newsletter.IsHtml, Name = RsaTokenProvider.Instance.RandomString(4)};
            var saveResponse = client.Put(putReq);
            Assert.IsNull(saveResponse.ResponseStatus);
            newsletter = newslettersRepo.Get(newsletter.Id);
            Assert.AreEqual(newsletter.Name, putReq.Name);
        }
        [Test]
        public void Test_Get_Newsletter()
        {
            var newsletter = CreateNewsletter();
            var result = client.Get(new GetNewsletter {Id = newsletter.Id});
            Assert.IsNull(result.ResponseStatus);
          Assert.AreEqual(newsletter.Name, result.Newsletter.Name);
        }
        [Test]
        public void Test_Query_Newsletters()
        {
            for (int i = 0; i < 5; i++)
            {
                CreateNewsletter();
            }
            var result = client.Get(new GetNewsletters {});
            Assert.IsTrue(result.Newsletters.Any());
        }

        [Test]
        public void Test_Upload_NewsletterFile()
        {
            var newsletter = CreateNewsletter();
            var file = AssemblyTest.FilesRootDir + "\\Tasdsa.txt";
            var filesToUpload = new FileInfo(file);
            File.WriteAllText(file, "test test");
            var req = new PostNewsletterFiles {Id = newsletter.Id};
            var res = client.PostFileWithRequest<PostNewsletterFilesResponse>(AhavRoute.PostNewsletterFiles,
                                                                              filesToUpload, req);
            Assert.IsNull(res.ResponseStatus);
            Assert.IsTrue(res.Files.Any());
            Assert.IsTrue(gridFs.FileExists(res.Files.First().Id));
            
        }

        [Test]
        public void Test_Remove_NewsletterFile()
        {
            var newsletter = CreateNewsletter();
            var fileId = AssemblyTest.CreateFile(gridFs, projectId);
            Assert.IsFalse(fileId.IsEmpty());
            Assert.IsTrue(gridFs.FileExists(fileId));
            newslettersRepo.AddFile(newsletter.Id, fileId);
            
            client.Delete(new DeleteNewsletterFile {FileId = fileId, NewsletterId = newsletter.Id});
            Assert.IsFalse(gridFs.FileExists(fileId));
            newsletter = newslettersRepo.Get(newsletter.Id);
            Assert.IsFalse(newsletter.Files.Contains(fileId));
        }
    }
}
