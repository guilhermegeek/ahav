﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Domain.Entities;
using NUnit.Framework;
using ServiceStack.ServiceClient.Web;

namespace Ahav.Tests.Services
{
    [TestFixture]
    public class CompanyTests : DefaultTest
    {
        [SetUp]
        public void SetUp()
        {
            Initialize();
        }

        [Test]
        public void Company_Get()
        {
            var c = CreateCompant();
            var res = Client.Get(new GetCompany {Id = c.Id});
            Assert.IsNull(res.ResponseStatus);
            Assert.IsNotNull(res.Company);
            Assert.AreEqual(c.Name, res.Company.Name);
        }
        [Test]
        public void Company_Create()
        {
            // Validate
            try
            {
                Client.Post(new PostCompany());
                throw new Exception("Not validated");
            }
            catch(WebServiceException ex)
            {
                Assert.IsTrue(ex.GetFieldErrors().Any(x => x.ErrorCode == "NotEmpty"));
            }

            // Create a valid company
            var res = Client.Post(new PostCompany
                                  {
                                      Name = "Teste!"
                                  });
            Assert.IsNull(res.ResponseStatus);
            Assert.IsNotNull(res.Company);

            // Company exists on bd
            var company = Resolve<CompanyRepository>().Get(res.Company.Id);
            Assert.AreEqual(company.Name, "Teste!");

        }

        [Test]
        public void Company_Update()
        {
            var c = CreateCompant();
            // Validate request
            try
            {
                Client.Put(new PutCompany());
                throw new Exception("request not validated");
            }
            catch (WebServiceException ex)
            {
                Assert.IsTrue(ex.GetFieldErrors().Any(x => x.ErrorCode == "NotEmpty"));
            }

            // Update 
            var res = Client.Put(new PutCompany
                                 {
                                     Id = c.Id,
                                     Name = "New Name"
                                 });
            Assert.IsNull(res.ResponseStatus);
            var company = Resolve<CompanyRepository>().Get(c.Id);
            Assert.AreEqual(company.Name, "New Name");
        }

        private Company CreateCompant()
        {
            return Resolve<CompanyRepository>().CreateEntity(new Company {Name = "Teste!!!"});
        }
    }
}
