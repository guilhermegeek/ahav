﻿using System;
using System.Linq;
using Ahav.Infrastructure;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Extensions;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.DataDtos;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Operations;
using MongoDB.Bson;
using NUnit.Framework;
using ServiceStack.ServiceClient.Web;
using ServiceStack.WebHost.Endpoints;

namespace Ahav.Tests.Services
{
    [TestFixture]
    [Category(TestCategory.Support)]
    public class SupportATests
    {
        private ObjectId blogId;
        private JsonServiceClient client;
        private SupportTicketRepository TicketRepo { get; set; }
        private SupportARepository supportRepo;
        private SupportContactDto CreateContact()
        {
            return supportRepo.AddContact(new CreateSupportContact
                                              {
                                                  Contact = "asd",
                                                  Email = "asdasd@msn.com",
                                                  Message = "asdasd",
                                                  Subject = "asdasdasdsad",
                                                  Name = "asdsad"
                                              }).Contact;
        }

        [SetUp]
        public void Setup()
        {
            client = new JsonServiceClient("http://localhost:1337/");
            var container = EndpointHost.Config.ServiceManager.Container;
            TicketRepo = container.TryResolve<SupportTicketRepository>();
            supportRepo = container.TryResolve<SupportARepository>();
            client.Login();
        }
        [Test]
        public void Test_CreateSupportContact()
        {
            var response = client.Post(new PostSupportContact
                            {
                                Contact = "Test",
                                Email = "test@mail.com",
                                Message = "asdasd",
                                Subject = "asdasd",
                                Name = "asdasd"
                            });
            Assert.IsNull(response.ResponseStatus);
            Assert.IsFalse(response.Contact.Id.IsEmpty());
            Assert.IsFalse(supportRepo.ReadContact(new ReadSupportContact { Id = response.Contact.Id}).Contact.Id.IsEmpty());
        }
        [Test]
        public void Test_GetSupportContact()
        {
            var contact = CreateContact();
            var response = client.Get(new GetSupportContact {Id = contact.Id});
            Assert.IsNull(response.ResponseStatus);
            Assert.IsFalse(response.Contact.Id.IsEmpty());
        }
        [Test]
        public void Tickets()
        {
            
            var addTicket = new PostSupportTicket
                                {
                                    Title = "",
                                    Content =
                                        "Sempre que tento criar um produto, dá-me um erro. Não tenho permissão para criar posts?!?!"
                                };
            try
            {
                client.Post(addTicket);
                ThrowNew();
            }
            catch (WebServiceException ex)
            {
                var a = ex.ErrorMessage;
            }
            addTicket.Title = "Problema a criar produto";
            var addTicketResponse = client.Post(addTicket);
            Assert.NotNull(addTicketResponse.Result);
            var ticket = TicketRepo.Get(addTicketResponse.Result.Id);
            Assert.IsTrue(ticket.State == SupportTicketState.Created);
            Assert.IsFalse(ticket.Id.IsEmpty());
            Assert.AreEqual(addTicketResponse.Result.Id, ticket.Id);
            
            // comments
            var addComment = client.Post(new PostSupportTicketComment {Comment = "Test", Id = ticket.Id});
            Assert.NotNull(addTicketResponse.Result);
            ticket = TicketRepo.Get(ticket.Id);
            Assert.IsTrue(ticket.Comments.Any(x => x.Comment == "Test"));
            var comments = client.Get(new GetSupportTicketComments {Id = ticket.Id});
            Assert.IsTrue(comments.Result.CommentsCount == ticket.CommentsCount);
            var deleteComments = client.Post(new DeleteSupportTicketComments {TicketId = ticket.Id, CommentId = addComment.Result.Id});
            // edit
            client.Post(new EditSupportTicket
                            {
                                Id = ticket.Id,
                                Content = "updated",
                                Priority = SupportTicketPriority.Normal,
                                Title = "updated"
                            });
            ticket = TicketRepo.Get(ticket.Id);
            Assert.IsTrue(ticket.Title == "updated");

            // state
            client.Post(new EditSupportTicketState { Id = ticket.Id, State = SupportTicketState.Closed});
            ticket = TicketRepo.Get(ticket.Id);
            Assert.IsTrue(ticket.State == SupportTicketState.Closed);
        }
        public static void ThrowNew()
        {
            throw new Exception("should be throwed before");
        }
    }
}
