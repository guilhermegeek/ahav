﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Extensions;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Operations;
using MongoDB.Driver.Builders;
using NUnit.Framework;
using ServiceStack.Common.Web;
using ServiceStack.ServiceClient.Web;
using ServiceStack.Text;
using ServiceStack.WebHost.Endpoints;

namespace Ahav.Tests.Services
{
    [TestFixture]
    [Category(TestCategory.Account)]
    public class AccountTests
    {
        private JsonServiceClient client;
        public AccountRepository accountRepo { get; set; }
        private UserRepository userRepo;
        private AuthRepository authRepo;
        private AhavAuth auth;
        private string currentPassword = "123";

        [SetUp]
        public void Setup()
        {
            var container = EndpointHost.Config.ServiceManager.Container;
            accountRepo = container.Resolve<AccountRepository>();
            userRepo = container.Resolve<UserRepository>();
            authRepo = container.Resolve<AuthRepository>();
            client = new JsonServiceClient("http://localhost:1337/");
            client.AllowAutoRedirect = false;
            auth = client.Login();
        }
        /// <summary>
        /// This registratio is only available to AdminRoots
        /// </summary>
        [Test]
        public void Account_CreateNewUser()
        {
            var request = new PostUser
                              {
                                  Email = string.Format("as{0}d@asd.com", DateTime.Now.Millisecond.ToString()),
                                  FirstName = "gui",
                                  LastName = "asdasd",
                                  AutoConfirmEmail = false
                              };
            var response = client.Post(request);
            Assert.IsFalse(response.UserId.IsEmpty());
            Assert.IsFalse(string.IsNullOrEmpty(response.ConfirmationToken));
            Assert.IsFalse(string.IsNullOrEmpty(response.Password));
            var userResponse = accountRepo.Get(response.UserId);
            Assert.AreEqual(request.Email, userResponse.PrimaryEmail);
            Assert.AreEqual(response.Password, userResponse.Password);

            // Test with auto confirm account
            request.AutoConfirmEmail = true;
            response = client.Post(request);
            Assert.IsTrue(string.IsNullOrEmpty(response.ConfirmationToken));
            userResponse = accountRepo.Get(response.UserId);
            Assert.AreEqual(userResponse.AccountState, AccountState.Confirmed);
        }
       

      
        [Test]
        public void Roles()
        {
            var newUser = client.Post(new PostRegisterBasic
                                          {
                                              Email = DateTime.Now.Second + "guilhermeasd@msn.com",
                                              FirstName = "José",
                                              LastName = "Nunes",
                                              Password = "123"
                                          });
            client.Post(new PermissionRoles {UserId = newUser.UserId, Role = AccountRole.NewsPublisher.ToString()});
            var roles = client.Get(new PermissionRoles {UserId = newUser.UserId});
            Assert.Contains(AccountRole.NewsPublisher.ToString(), roles.Roles.ToList());

            client.Delete(new PermissionRoles {UserId = newUser.UserId, Role = AccountRole.NewsPublisher.ToString()});
            roles = client.Get(new PermissionRoles {UserId = newUser.UserId});
            Assert.IsFalse(roles.Roles.Contains(AccountRole.NewsPublisher.ToString()));

        }
        
        [Test]
        public void Client_Logout()
        {
            var nclient = new JsonServiceClient("http://localhost:1337/");
            nclient.AllowAutoRedirect = false;
            var login = nclient.Login();
            var nauth = authRepo.GetAuth(login.UserId);
            Assert.IsNotNull(nauth);
            nclient.Get(new AnyLogout());
            auth = authRepo.GetAuth(login.UserId);
            Assert.IsNull(auth);
        }
        [Test]
        public void Account_ChangePassword()
        {
            Assert.IsTrue(accountRepo.ValidatePassord(auth.UserId, "123"));
            PostAccountPasswordResponse changeResponse;
            try
            {
                changeResponse =
                    client.Post(new PostAccountPassword { ActualPassword = "123", Password = "1", PasswordConfirm = "2" });
                thrownew();
            }
            catch (WebServiceException ex)
            {
             
                Assert.IsTrue(ex.GetFieldErrors().Any(x => x.ErrorCode == "PwNotMatching"));
            }
            changeResponse = client.Post(new PostAccountPassword { ActualPassword = "1", Password = "22", PasswordConfirm = "22" });
            Assert.IsTrue(changeResponse.ResponseStatus.ErrorCode == HttpStatusCode.InternalServerError.ToString());
            Assert.IsTrue(changeResponse.ResponseStatus.Errors.Any(x => x.ErrorCode == "PwWrong"));

            try
            {
                client.Post(new PostAccountPassword {ActualPassword = "123", Password = "123", PasswordConfirm = "123"});
                thrownew();
            }
            catch (WebServiceException ex)
            {
                Assert.IsTrue(ex.ErrorCode == "PwIsSame");
            }
            client.Post(new PostAccountPassword {ActualPassword = "123", Password = "1234", PasswordConfirm = "1234"});
            client.Post(new PostAccountPassword {ActualPassword = "1234", Password = "123", PasswordConfirm = "123"});
            Assert.IsTrue(accountRepo.ValidatePassord(auth.UserId, "123"));
        }
        [Test]
        public void User_UpdateInfo()
        {
            try
            {
                client.Post(new BasicInfo {FirstName = "Luis"});
                thrownew();
            }
            catch(WebServiceException ex)
            {
                Assert.IsTrue(ex.ErrorCode == "NotEmpty");
            }
            client.Post(new BasicInfo { FirstName = "Luis", LastName = "Nunes" });
            var user = client.Get(new BasicInfo()).Result;
            Assert.IsTrue(user.FirstName == "Luis");
        }
        [Test]
        public void User_UpdateContactDetails()
        {
         
            var contactDetails = client.Get(new Contact());
            IList<string> websites = contactDetails.Websites;
            websites.Add("http://testing.com");
            client.Post(new Contact
                            {
                                Websites = Enumerable.ToArray(websites),
                                Mobiles = contactDetails.Mobiles,
                                PublicEmail = contactDetails.PublicEmail
                            });
            var updatedContactDetails = client.Get(new Contact());
            Assert.IsTrue(updatedContactDetails.Websites.Contains("http://testing.com"));
        }

        private void thrownew()
        {
            throw new Exception("should be invalid at this point");
        }
    }
}
