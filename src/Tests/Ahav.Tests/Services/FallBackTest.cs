﻿using System.Collections.Generic;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Extensions;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson;
using NUnit.Framework;
using ServiceStack.ServiceClient.Web;
using ServiceStack.WebHost.Endpoints;

namespace Ahav.Tests.Services
{
    [TestFixture]
    public class FallBackTest 
    {
        private ContentRepository contentRepo;
        private TemplateRepository templateRepo;
        private AhavAuth auth;
        private ObjectId projectId;
        private ApplicationAhavRepository projectRepo;
        private JsonServiceClient client;
        private SlugRepository slugRepo;

        [SetUp]
        public void SetUp()
        {
            client = new JsonServiceClient("http://localhost:1337/");
            auth = client.Login();
            var container = EndpointHost.Config.ServiceManager.Container;
            projectRepo = container.Resolve<ApplicationAhavRepository>();
            contentRepo = container.Resolve<ContentRepository>();
            templateRepo = container.Resolve<TemplateRepository>();
            projectId = projectRepo.GetProjectsIdByUser(auth.UserId)[0];
        }
        private Template CreateTemplate()
        {
            return templateRepo.AddTemplate(projectId,
                new Template
                {
                    Name = "nt",
                    Body = new Dictionary<string, string> { { Culture.pt_PT, string.Format("<div>Header</div>{0}<div>Footer</div>", TemplateExtensions.HtmlDivRenderId) } },
                    Title = new Dictionary<string, string> { { Culture.pt_PT, "testing" } }
                });
        }
        private Content CreateContent(ObjectId templateId)
        {
            var content = contentRepo.Create(new Content
                                   {
                                       Title = new Dictionary<string, string> {{Culture.pt_PT, "Welcome Page"}},
                                       Body =
                                           new Dictionary<string, string>
                                               {
                                                   {
                                                       Culture.pt_PT,
                                                       @"<div class=""jumbotron""><h1>Welcomepage</h1></div><p>Welcome!<br>Edit this page."
                                                   }
                                               },
                                       Owner = auth.UserId,
                                       Template = templateId,
                                       Slug = RsaTokenProvider.Instance.RandomString(5)
                                   });
            slugRepo.AddSlug(projectId, SlugType.Content, content.Id, content.Slug);
            return content;
        }
       
       
    }
}
