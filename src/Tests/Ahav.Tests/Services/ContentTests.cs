﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Extensions;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson;
using NUnit.Framework;
using ServiceStack.ServiceClient.Web;
using ServiceStack.WebHost.Endpoints;

namespace Ahav.Tests.Services
{
    [TestFixture]
    [Category(TestCategory.Content)]
    public class ContentTests
    {
        private AhavAuth _auth;
        private ObjectId projectId;
        private JsonServiceClient client;
        private ContentRepository _contentRepository;
        
        private AccountRepository _accountRepository;
        private ApplicationAhavRepository projectRepo;
        private SlugRepository slugRepo;
        private MongoGridFs gridFs;
        private TemplateRepository templateRepo;
        
        private Content AddContent()
        {
            var tpl = CreateTemplate();
            var c = _contentRepository.Create(new Content
            {
                Title = new Dictionary<string, string> { { Culture.pt_PT, "test" } },
                Body = new Dictionary<string, string> { { Culture.pt_PT, "test" } },
                Javascript = "ola",
                Owner = _auth.UserId,
                Slug = RsaTokenProvider.Instance.RandomString(10),
                ProjectId = projectId,
                Template = tpl.Id
            });
            slugRepo.AddSlug(projectId, SlugType.Content, c.Id, c.Slug);
            projectRepo.AddContent(projectId, c.Id);
            return c;
        }
        private Template CreateTemplate()
        {
            return templateRepo.AddTemplate(projectId,
                new Template
                {
                    Name = "nt",
                    Body = new Dictionary<string, string> { { Culture.pt_PT, string.Format("<div>Header</div><div>testing</div>{0}<div>Footer</div>", TemplateExtensions.HtmlDivRenderId) } },
                    Title = new Dictionary<string, string> { { Culture.pt_PT, "testing" } }
                });
        }

        [SetUp]
        public void SetUp()
        {
            client = new JsonServiceClient("http://localhost:1337/");
            _auth = client.Login();
            //_jsonService.Post(new PostAccountContent {AccountId = _auth.UserId});
            var container = EndpointHost.Config.ServiceManager.Container;
            gridFs = container.TryResolve<MongoGridFs>();
            templateRepo = container.TryResolve<TemplateRepository>();
            _contentRepository = container.Resolve<ContentRepository>();
            projectRepo = container.Resolve<ApplicationAhavRepository>();
            _accountRepository = container.Resolve<AccountRepository>();
            projectId = container.Resolve<ApplicationAhavRepository>().GetProjectIdBySubdomain("localhost");
            if(projectId.IsEmpty()) throw new Exception("Project localhost doesnt exist at Setup");
            slugRepo = container.Resolve<SlugRepository>();
            client.SetProjectCredentials(projectId);
        }
        
        /// <summary>
        /// When the request contains a Template id, checks if the Content uses that Template
        /// If has the same template, return a
        /// </summary>
        [Test]
        public void Test_ReuseTemplateIfCached()
        {
            var content = AddContent();
            string url = string.Format("/{0}/{1}?template={2}", "gui", content.Slug, content.Template);
            var response = client.Get<FallbackResponse>(url);
            Assert.IsTrue(response.ReuseTemplate);
            Assert.IsFalse(response.RenderBody.Contains("Header"));
        }
        /// <summary>
        /// Tests if a authorized user can post a new Content
        /// </summary>
        /// It's created a test Template for the Content
        [Test]
        public void Content_Create()
        {
            var tpl = CreateTemplate();
            var response = client.Post(new PostContent
                                                 {
                                                     Slug = RsaTokenProvider.Instance.RandomString(10),
                                                     Template = tpl.Id
                                                 });
            Assert.IsNull(response.ResponseStatus, "ResponseStatus was returned");
            Assert.IsFalse(response.Content.Id.IsEmpty());
            var content = _contentRepository.Get(response.Content.Id);
            Assert.IsNotNull(content);
            // Confirms is ContentTemplate has content id
            var ids = templateRepo.GetContentIdsFromTemplate(tpl.Id);
            Assert.IsNotNull(ids);
            Assert.IsTrue(ids.Any(x => x == response.Content.Id));

        }
        /// <summary>
        /// Get a Content 
        /// </summary>
        [Test]
        public void Content_GetById()
        {
            var newContent = AddContent();
            var getResponse = client.Get(new GetContent {ContentId = newContent.Id});
            
            Assert.AreEqual(newContent.Slug, getResponse.Content.Slug);
        }
        [Test]
        public void Content_Query()
        { 
            var newContent = AddContent();
            var response = client.Get(new GetContents {Skip = 0, Take = 2000, ProjectId = projectId});
            Assert.IsNull(response.ResponseStatus);

            Assert.IsTrue(response.Contents.Any(x => x.Id == newContent.Id));
            Assert.IsTrue(response.Contents.Any(x => x.Slug == newContent.Slug));
        }
        [Test]
        public void Content_Update()
        {
            var newContent = AddContent();
            var response = client.Put(new PutContent
                                                {
                                                    ContentId = newContent.Id,
                                                    Title = newContent.Title,
                                                    Body = newContent.Body,
                                                    Javascript = "adeus",
                                                    Slug = newContent.Slug
                                                });
            Assert.IsNull(response.ResponseStatus);
            var content = _contentRepository.Get(newContent.Id);
            Assert.AreEqual("adeus", content.Javascript);
            Assert.IsFalse(slugRepo.SlugIsAvailable(projectId, SlugType.Content,  newContent.Slug));
            // updateSlug
            response = client.Put(new PutContent
                                            {
                                                ContentId = newContent.Id,
                                                Title = newContent.Title,
                                                Body = newContent.Body,
                                                Javascript = "adeus",
                                                Slug = newContent.Slug + "a"
                                            });
            Assert.IsNull(response.ResponseStatus);
            Assert.IsTrue(slugRepo.GetId(projectId, SlugType.Content,  newContent.Slug).IsEmpty());
            Assert.AreEqual(newContent.Id, slugRepo.GetId(projectId, SlugType.Content, newContent.Slug + "a"));
        }
        [Test]
        public void Content_Remove()
        {
            var newContent = AddContent();

            var deleteResponse = client.Delete(new DeleteContent
            {
                ContentId = newContent.Id
            });
            Assert.IsNull(deleteResponse.ResponseStatus);
            var deletedContent = _contentRepository.Get(newContent.Id);
            Assert.IsNull(deletedContent);
        }
        [Test]
        public void Content_GetFiles()
        {
            var fileId = AssemblyTest.CreateFile(gridFs, projectId);
            var content = AddContent();
            _contentRepository.Addfile(content.Id, fileId);
            var files = client.Get(new GetContentFiles {ContentId = content.Id});
            Assert.IsNull(files.ResponseStatus);
            Assert.IsTrue(files.Files.Any());

        }
        [Test]
        public void Content_UploadFile()
        {
            var newContent = AddContent();
            
            string file = AssemblyTest.FilesRootDir + "\\TESTS.txt";
            var filesToUpload = new FileInfo(file);
            File.WriteAllText(file, "test");
            var request = new PostContentFile {ContentId = newContent.Id};
            var response = client.PostFileWithRequest<PostContentFileResponse>(AhavRoute.PostContentFile, filesToUpload, request);
            Assert.IsNull(response.ResponseStatus);
            foreach(var media in response.Medias)
            {
                Assert.IsTrue(gridFs.FileExists(media.Id));
            }
            var content = _contentRepository.Get(newContent.Id);
            Assert.IsTrue(content.Files.Contains(response.Medias.First().Id));
        }

        [Test]
        public void Content_DeleteFile()
        {
            var content= AddContent();
            var fileId = AssemblyTest.CreateFile(gridFs, projectId);
            
            _contentRepository.Addfile(content.Id, fileId);
            var deleteResponse = client.Delete(new DeleteContentFile {ContentId = content.Id, FileId = fileId});
            Assert.IsNull(deleteResponse.ResponseStatus);
            Assert.IsFalse(gridFs.FileExists(fileId));
            content = _contentRepository.Get(content.Id);
            Assert.IsTrue(content.Files.All(x => x != fileId));
        }
        /// <summary>
        /// Sets a page as root, acessible from "/"
        /// </summary>
        [Test]
        public void Content_PutDefault()
        {
            var c = AddContent();
            var response = client.Put(new PutContentDefault {ContentId = c.Id});
            Assert.IsNull(response.ResponseStatus);
            var contentDefault = _contentRepository.GetContentDefault(projectId);
            Assert.AreEqual(contentDefault, c.Id);
        }
        /// <summary>
        /// Gets the default content
        /// </summary>
        [Test]
        public void Content_GetDefault()
        {
            var c = AddContent();
            _contentRepository.SetContentDefault(projectId, c.Id);
            var response = client.Get(new GetContentDefault {ContentId = c.Id});
            Assert.IsNull(response.ResponseStatus);
            Assert.AreEqual(response.Content.Id, c.Id);
        }
    }
}
