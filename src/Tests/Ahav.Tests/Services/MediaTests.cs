﻿using System.IO;
using System.Linq;
using Ahav.Infrastructure;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Extensions;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Enums;
using Ahav.ServiceModel.Operations;
using MongoDB.Bson;
using NUnit.Framework;
using ServiceStack.Configuration;
using ServiceStack.ServiceClient.Web;
using ServiceStack.ServiceHost;
using ServiceStack.WebHost.Endpoints;

namespace Ahav.Tests.Services
{
    [TestFixture]
    [Category(TestCategory.Media)]
    public class MediaTests
    {
        private JsonServiceClient _jsonClient;
        private AhavAuth auth;
        private MongoGridFs gridFs;
        private ObjectId projectId;
        private string path;

        [SetUp]
        public void SetUp()
        {
            _jsonClient = new JsonServiceClient("http://localhost:1337/");
            auth = _jsonClient.Login();
            File.WriteAllText(Path.Combine(AssemblyTest.FilesRootDir, RsaTokenProvider.Instance.RandomString(3) + ".txt"), "TESTS");
            var container = EndpointHost.Config.ServiceManager.Container;
            projectId = container.Resolve<ApplicationAhavRepository>().GetProjectsIdByUser(auth.UserId)[0];
            gridFs = container.TryResolve<MongoGridFs>();
            var appSettings = new AppSettings();
            path = appSettings.Get("FilesUploadPath", "c:\\temp\\");

        }
        [Test]
        public void Medias_UploadNewFile_FileExist()
        {
            
            string fileName = RsaTokenProvider.Instance.RandomString(5) + ".txt";
            string filePath = AssemblyTest.FilesPath + fileName;
            File.WriteAllText(filePath, RsaTokenProvider.Instance.RandomString(30000));

            using(var file = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                var stream = new MemoryStream();
                file.CopyStream(stream);

                //var response = _jsonClient.PostFileWithRequest<PostMediaResponse>(AhavRoute.PostMedia, stream, fileName, new PostMedia
                //                                                                                                             {
                //                                                                                                                 MediaType = MediaObjType.Attachment
                //                   
                var req = new PostMedia
                              {
                                  MediaType = MediaObjType.Attachment
                              };
                req.Files.Add(new HttpFile
                {
                    InputStream = stream
                });
                var response =
                    _jsonClient.Post(req);
                Assert.IsNull(response.ResponseStatus);
                var fileId = response.Medias.First().Id;
                Assert.IsTrue(gridFs.FileExists(fileId));
            }
        }

        [Test]
        public void Media_CreateFile()
        {
            var fileId = AssemblyTest.CreateFile(gridFs, projectId);
            var files = _jsonClient.Get(new GetMedias {Ids = new[] {fileId}});
            Assert.IsTrue(files.Medias.Any());
        }
        [Test]
        public void Media_RemoveFile()
        {
            var fileId = AssemblyTest.CreateFile(gridFs, projectId);
            var deleteResponse = _jsonClient.Delete(new DeleteMedia {Id = fileId});
            Assert.IsNull(deleteResponse.ResponseStatus);
            Assert.IsFalse(gridFs.FileExists(fileId));
        }
    }
}
