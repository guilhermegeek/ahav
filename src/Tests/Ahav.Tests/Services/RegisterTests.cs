﻿using System;
using System.Linq;
using Ahav.Infrastructure;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceInterface.Validators;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Operations;
using NUnit.Framework;
using ServiceStack.ServiceClient.Web;
using ServiceStack.WebHost.Endpoints;

namespace Ahav.Tests.Services
{
    [TestFixture]
    [Category(TestCategory.Register)]
    public class RegisterTests
    {
        private JsonServiceClient client = new JsonServiceClient("http://localhost:1337/");
        private AccountRepository accountRepo;
        
        [SetUp]
        public void SetUp()
        {
            var container = EndpointHost.Config.ServiceManager.Container;
            accountRepo = container.Resolve<AccountRepository>();
            client.AllowAutoRedirect = false;
        }
        /// <summary>
        /// Test if a user can register a new account with an already registered email
        /// </summary>
        [Test]
        public void Register_Basic_ValidatorEmailInUse()
        {
            var req = new PostRegisterBasic
                          {
                              Email = RsaTokenProvider.Instance.RandomString(5) + "@msn.com",
                              FirstName = "José",
                              LastName = "Nunes",
                              Password = "123"
                          };
            var res = client.Post(req);
            Assert.IsNull(res.ResponseStatus);
            
                var resError = client.Post(req);
                Assert.AreEqual(resError.ResponseStatus.ErrorCode, AccountValidationError.EmailInUse.ToString());   
            
        }

        /// <summary>
        /// Ensure the state of new accounts is NotConfirmed
        /// Only AdminRoot can create new accounts without any confirmation
        /// </summary>
        [Test]
        public void Register_Basic_WaitForConfirmation()
        {
            var req = new PostRegisterBasic
            {
                Email = RsaTokenProvider.Instance.RandomString(5) + "@msn.com",
                FirstName = "José",
                LastName = "Nunes",
                Password = "123"
            };
            var res = client.Post(req);
            Assert.IsNull(res.ResponseStatus);
            var account = accountRepo.Get(res.UserId);
            Assert.IsNotNull(account);
            Assert.AreEqual(account.AccountState, AccountState.NotConfirmed);
        }
        /// <summary>
        /// End to end test for Registration and Confirmation
        /// </summary>
        [Test]
        public void Register_Basic_E2E()
        {
            try
            {
                client.Post(new PostRegisterBasic { });
                throw new Exception("ValidationFeature should throw an exception");
            }
            catch (WebServiceException ex)
            {
                Assert.IsTrue(ex.GetFieldErrors().Any());
            }
            var request = new PostRegisterBasic
            {
                Email = RsaTokenProvider.Instance.RandomString(4).ToLower() + "guilherme@msn.com",
                FirstName = "José",
                LastName = "Nunes",
                Password = "123"
            };
            var response = client.Post(request);

            Assert.IsFalse(response.UserId.IsEmpty());
            try
            {
                var errorResponse = client.Post(request);
                throw new Exception("Email already registered wasn't validated");
            }
            catch (WebServiceException ex)
            {
                Assert.IsTrue(ex.GetFieldErrors().Any(x => x.ErrorCode == AccountValidationError.EmailInUse.ToString()));
            }

            Assert.IsFalse(accountRepo.CheckEmailAvailability(request.Email));

            var responseConfirm = client.Post(new PostRegistrationConfirm { Token = response.ConfirmationToken, UserId = response.UserId });
            Assert.IsTrue(responseConfirm.IsConfirmed);
            Assert.IsTrue(accountRepo.IsConfirmed(response.UserId));
            var responseLogin = client.Post(new PostLogin { Email = request.Email, Password = request.Password });
            Assert.IsNull(responseLogin.ResponseStatus);
        }
    }
}
