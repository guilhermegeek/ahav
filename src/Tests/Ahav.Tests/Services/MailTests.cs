﻿using System.Collections.Generic;
using System.Linq;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Extensions;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Operations;
using Magazine.ServiceInterface.Data;
using MongoDB.Bson;
using NUnit.Framework;
using ServiceStack.ServiceClient.Web;
using ServiceStack.WebHost.Endpoints;

namespace Ahav.Tests.Services
{
    [TestFixture]
    [Category(TestCategory.Mail)]
    public class MailTests
    {
        private ObjectId blogId;
        private ObjectId postId;
        private JsonServiceClient client;
        private MailRepository mailRepo;
        private ArticleRepository blogPostRepo;
        private BlogCategoryRepository blogCategoryRepo;
        private BlogPostCategoryRepository blogPostCategoryRepo;
        private AhavAuth auth;

        [SetUp]
        public void Setup()
        {
            client = new JsonServiceClient("http://localhost:1337/");
            var container = EndpointHost.Config.ServiceManager.Container;
            mailRepo = container.TryResolve<MailRepository>();
            auth = client.Login();
        }
         [Test]
        public void Send()
        {
            var response = client.Post(new SendEmailI
                            {
                                Content = "<p>Testing <b>bold</b></p><p>Thank you!</p>",
                                IsHtml = true,
                                Recipients = new[] {"guilherme-cardoso@msn.com", "guilhermegeek@gmail.com"},
                                Subject = "Test Eamail"
                            });
            Assert.IsFalse(response.EmailsNotSented.Any());
        }

        [Test]
        public void Test_SendEmailsQueue()
        {
            var mail = mailRepo.Add(new Mail
                                       {
                                           Content = new Dictionary<string, string> {{Culture.pt_PT, "test"}},
                                           Subject = new Dictionary<string, string> {{Culture.pt_PT, "testcontent"}},
                                           IsHtml = true
                                       });
        
            mailRepo.AddToQueue(mail.Id, new MailQueue { Email = "test@msn.com"});
            var queues = mailRepo.GetQueue(mail.Id);
            Assert.IsTrue(queues.Any());
            client.Post(new SendQueueEmails {EmailId = mail.Id});
            queues = mailRepo.GetQueue(mail.Id);
            Assert.IsTrue(queues == null || !queues.Any());
        }
    }
}
