﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Ahav.ServiceInterface;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel;
using Magazine.ServiceInterface;
using MongoDB.Bson;
using NUnit.Framework;
using ServiceStack.Configuration;

namespace Ahav.Tests
{

    [SetUpFixture]
    public class AssemblyTest
    {
        public static string FilesRootDir;
        public static string FilesPath;
        private AppHost appHost;

        [SetUp]
        public void SetUp()
        {
            appHost = new AppHost();

            appHost.Init();

            appHost.Plugins.Add(new AhavPlugin());
            appHost.Plugins.Add(new MagazinePlugin());
            FilesRootDir = appHost.Config.WebHostPhysicalPath;
            FilesPath = new AppSettings().Get("FilesUploadPath", @"c:\tmp\");
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-PT");
            appHost.Start("http://*:1337/");
            BootDatabases.Boot();
        }
        [TearDown]
        public void Teardown()
        {
            appHost.Stop();
        }
        /// <summary>
        /// Helper method to create a file using Mongo GridFS
        /// </summary>
        /// <param name="gridFs"></param>
        /// <returns></returns>
        public static ObjectId CreateFile(MongoGridFs gridFs, ObjectId projectId )
        {
            var file = AssemblyTest.FilesRootDir + "\\asdasd.txt";
            var filesToUpload = new FileInfo(file);
            File.WriteAllText(file, "asdasdasd");
            ObjectId fileId = ObjectId.Empty;
            using (var stream = filesToUpload.OpenRead())
            {
                fileId = (ObjectId)gridFs.AddFile(stream, "Test file", projectId).Id;
            }
            return fileId;
        }
    }
}
