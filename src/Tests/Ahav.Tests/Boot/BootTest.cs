﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using Ahav.Infrastructure;
using Ahav.ServiceInterface;
using Ahav.ServiceInterface.Extensions;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel;
using Ahav.ServiceModel;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Operations;
using MongoDB.Bson;
using NUnit.Framework;
using ServiceStack.ServiceClient.Web;


namespace Ahav.Tests.Boot
{
    [TestFixture]
    public class BootTest
    {
        [Test]
        [Ignore]
        public void Boot()
        {
            var appHost = new AppHost();
            appHost.Init();
            appHost.Plugins.Add(new AhavPlugin());
            appHost.Start("http://*:1337/");
            var client = new JsonServiceClient("http://localhost:1337/");
            BootDatabases.Boot();
            client.Login();
            var contentTplRes = client.Post(new PostContentTemplate
                        {
                            Name = "Evento 7 Dezembro"
                        });
            client.Put(new PutTemplate
                       {
                           Body = new Dictionary<string, string> { { Culture.pt_PT, BootResources.TemplateBody } },
                           Less = BootResources.TemplateLess,
                           Title = new Dictionary<string, string> { { Culture.en_US, "Adegas do Dão" } },
                           Name = "Evento 7 Dezembro",
                           TemplateId = contentTplRes.Template.Id
                       });

            var pages = new Dictionary<string, string>
                        {
                            {
                                "/adegasododao.pt/home", BootResources.HomeHtml
                            },
                            {
                                "/adegasdodao.pt/where", BootResources.WhereHtml
                            },
                            {
                                "/adegasdodao.pt/sponsors", BootResources.SponsorsHtml
                            },
                            {
                                "/adegasdodao.pt/supporters", BootResources.SupportersHtml
                            }
                        };
            ObjectId homeId;
            foreach (var page in pages)
            {
                var c1 = client.Post(new PostContent
                {
                    Slug = page.Key,
                    Template = contentTplRes.Template.Id
                });

                client.Put(new PutContent
                {
                    Body = new Dictionary<string, string> { { Culture.pt_PT, BootResources.HomeHtml } },
                    Title = new Dictionary<string, string> { { Culture.pt_PT, "Adegas Do Dão" } },
                    ContentId = c1.Content.Id,
                    Slug = c1.Content.Slug,
                    Template = c1.Content.TemplateId
                });
            }
            //client.Put(new PutContentDefault{ ContentId = })
        }
    }
}
