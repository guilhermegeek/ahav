﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Infrastructure;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using NUnit.Framework;
using ServiceStack.WebHost.Endpoints;
using DayOfWeek = Ahav.ServiceModel.Common.DayOfWeek;

namespace Ahav.Tests
{
    [TestFixture]
    public class RandomsTests
    {
        [Test]
        public void R()
        {
            var container = EndpointHost.Config.ServiceManager.Container;
            var busCol = MongoDbHelper.Instance.Database.GetCollection<LocalBusiness>(typeof(LocalBusiness).Name);

            var entity = new LocalBusiness
                         {
                             Address = "Prolongamento da Rua São João de Deus, lote 16 - 3510-174 Abraveses - Viseu",
                             BusinessType = LocalBusinessType.ChildCare,
                             Description = new Dictionary<string, string> {{Culture.pt_PT, "Casa de cantinho de animais. Aqui guardamos gatos e cães perdidos das ruas e tentamos encontrar-lhe um novo lar."}},
                             Name = "shelter3",
                             Title = new Dictionary<string, string> {{Culture.pt_PT, "Cantinho dos animais"}},
                             OpeningHours = new[] {new OpeningHoursSpecification {DayOfWeek = DayOfWeek.Monday, Closes = DateTime.Now,}},
                             Location = new[]{40.656863,  -7.913952}
                         };
            busCol.Insert(entity);
            Assert.IsFalse(entity.Id.IsEmpty());
        }
    }
}
