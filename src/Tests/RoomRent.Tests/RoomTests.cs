﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace RoomRent.Tests
{
    [TestFixture]
    public class RoomTests
    {
        /// <summary>
        /// Creates a new room
        /// </summary>
        [Test]
        public void Landlord_CreateRoom()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Creates a new room and show him on featured
        /// </summary>
        [Test]
        public void Landlord_CreateRoom_Vip_Featured()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Accepts a room offer
        /// </summary>
        [Test]
        public void Landlord_AcceptRoomOffer()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Search rooms near by user position (ie: AR clients)
        /// </summary>
        [Test]
        public void Rooms_SearchNearBy()
        {
            throw new NotImplementedException();
        }
    }
}
