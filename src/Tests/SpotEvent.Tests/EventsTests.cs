﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace SpotEvent.Tests
{
    /// <summary>
    /// Events for tests
    /// </summary>
    [TestFixture]
    public class aEventsTests
    {
        /// <summary>
        /// Create a new event
        /// </summary>
        [Test]
        public void Event_Create_Authenticated()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Publishes an event and publishe to facebook, twitter, etc
        /// </summary>
        [Test]
        public void Event_Publish_StreamSocial()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Subscribe a restrict event but still waiting for confirmation
        /// </summary>
        [Test]
        public void Event_SubscribeRestricted_WaitForConfirmation()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Subscribe a free event, result is ok and event is added to agenda
        /// </summary>
        [Test]
        public void Event_SusbcribeFree_AddedToAgenda()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Find the near events
        /// </summary>
        [Test]
        public void Events_FindNear_P2P()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Removes a confirmation from a event
        /// </summary>
        [Test]
        public void Event_Unsubscribe()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Cancel a event, notify all users
        /// </summary>
        [Test]
        public void Event_Cancel()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Update basic event information
        /// </summary>
        [Test]
        public void Event_Update()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Get a list of users attending to a event
        /// </summary>
        [Test]
        public void Event_GetWhoIsGoing()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Get a list of users friends attending to a event
        /// </summary>
        [Test]
        public void Event_GetFriendsAttending()
        {
            throw new NotImplementedException();
        }
    }
}
