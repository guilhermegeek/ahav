﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.Tests.Services;
using Magazine.ServiceInterface.Data;
using Magazine.ServiceModel.ApiDtos;
using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using NUnit.Framework;

namespace Magazine.Tests.Services
{
    [TestFixture]
    public class ArticleCategoryTests : DefaultTest
    {
        [SetUp]
        public void SetUp()
        {
            Initialize();
        }

        [Test]
        public void ArticleCategory_Remove_RemoveCategoriesFromArticles()
        {
            CreateBlog();
            var article = CreateArticleWithTemplate();
            var category = Resolve<BlogPostCategoryRepository>().Get(article.Categories.First());
            Assert.IsTrue(category.Posts.Any());
            Client.Delete(new DeleteBlogArticleCategory {Id = article.Categories.First()});
            var res = Resolve<BlogPostCategoryRepository>().Get(article.Categories.First());
            Assert.IsNull(res);
        }
        [Test]
        public void Article_GetArticleCategories()
        {
            if (!Resolve<BlogPostCategoryRepository>().GetCategories().Any())
                Resolve<BlogPostCategoryRepository>().Create(new BlogArticleCategory { Title = new Dictionary<string, string> { { Culture.pt_PT, "CategoryTest" } } });
            var getCategories = Client.Get(new GetBlogArticleCategory { });
            Assert.IsTrue(getCategories.Categories.Any());
        }
    }
}
