﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.Tests;
using Ahav.Tests.Services;
using Magazine.ServiceInterface;
using Magazine.ServiceInterface.Data;
using Magazine.ServiceModel.ApiDtos;
using NUnit.Framework;
using MagazineRoutes = Magazine.ServiceModel.MagazineRoutes;

namespace Magazine.Tests.Services
{
    [TestFixture]
    public class ArticleAttachments : DefaultTest
    {

        [Test]
        public void Test_UploadArticleFile()
        {
            var article = CreateArticleWithTemplate();
            string file = AssemblyTest.FilesRootDir + "\\TESTS.txt";
            var files = new FileInfo(file);
            File.WriteAllText(file, "test");
            var request = new PostBlogArticleFile { ArticleId = article.Id };
            var res = Client.PostFileWithRequest<PostBlogArticleFileResponse>(MagazineRoutes.PostBlogArticleFile, files, request);
            Assert.IsNull(res.ResponseStatus);
            Assert.IsTrue(Resolve<MongoGridFs>().FileExists(res.Medias.First().Id));
            var medias = Resolve<ArticleRepository>().Get(article.Id).Files;
            Assert.IsTrue(medias.Any());
        }
        
    }
}
