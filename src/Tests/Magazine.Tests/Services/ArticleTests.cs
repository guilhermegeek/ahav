﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel;
using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Resources;
using Ahav.Tests;
using Ahav.Tests.Services;
using Magazine.ServiceInterface.Data;
using Magazine.ServiceModel.ApiDtos;
using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Dtos;
using MongoDB.Bson;
using NUnit.Framework;
using ServiceStack.ServiceClient.Web;

namespace Magazine.Tests.Services
{
    [TestFixture]
    [Category(TestCategory.Blog)]
    public class ArticleTests : DefaultTest
    {
        [SetUp]
        public void SetUp()
        {
            Initialize();
        }
        /// <summary>
        /// Remove an existing Article with success
        /// </summary>
        [Test]
        public void Blog_RemoveArticle()
        {
            var article = CreateArticleWithTemplate();
            var delRes = Client.Delete(new DeleteBlogArticle { ArticleId = article.Id });
            Assert.IsNull(delRes.ResponseStatus);
            Assert.IsTrue(Resolve<SlugRepository>().SlugIsAvailable(ProjectId, SlugType.Article, article.Slug));
            article = Resolve<ArticleRepository>().Get(article.Id);
            Assert.IsTrue(article == null || article.Id.IsEmpty());
        }
        [Test]
        public void Test_Update_Article_Basic()
        {
            var article = CreateArticleWithTemplate();
            var req = new PutBlogArticle
            {
                Id = article.Id,
                Slug = RsaTokenProvider.Instance.RandomString(10),
                //    Categories = article.Categories,
                Body = article.Body,
                //        Slideshow = article.Slideshow,
                //         Tags = article.Tags,
                Title = article.Title
            };
            var postResponse = Client.Put(req);
            Assert.IsNull(postResponse.ResponseStatus);
            var uArticle = Resolve<ArticleRepository>().Get(article.Id);
            Assert.AreEqual(uArticle.Slug, req.Slug);
            // Check the new slug create
            Assert.IsFalse(Resolve<SlugRepository>().SlugIsAvailable(ProjectId, SlugType.Article, req.Slug));
            // Confirm that the old slug is available for use
            Assert.IsTrue(Resolve<SlugRepository>().SlugIsAvailable(ProjectId, SlugType.Article, article.Slug));
        }
        /// <summary>
        /// Creates a new article without publishing 
        /// </summary>
        [Test]
        public void Blog_CreateArticle_StateCreated()
        {

            var blog = CreateBlog();
            var article = Client.Post(new PostBlogArticle
            {
                BlogId = blog.Id,
                SubDomain = RsaTokenProvider.Instance.RandomString(4)
            });
            Assert.IsNull(article.ResponseStatus);
            Assert.IsFalse(article.Article.Id.IsEmpty());
            var a = Resolve<ArticleRepository>().Get(article.Article.Id);
            Assert.AreEqual(a.Id, article.Article.Id);
            Assert.AreEqual(a.State, BlogArticleState.Created);
        }

        /// <summary>
        /// Creates a new article and publish
        /// </summary>
        [Test]
        public void Blog_CreateArticle_StatePublish()
        {
            var blog = CreateBlog();
            var res =
                Client.Post(new PostBlogArticle
                {
                    BlogId = blog.Id,
                    SubDomain = RsaTokenProvider.Instance.RandomString(4)
                });
            Assert.IsNull(res.ResponseStatus);
            var article = Resolve<ArticleRepository>().Get(res.Article.Id);
            Assert.AreEqual(article.State, BlogArticleState.Published);
        }
        [Test]
        public void Blog_Article_UpdateState()
        {
            var blog = CreateBlog();
            var article = CreateArticleWithTemplate();
            Assert.AreEqual(article.State, BlogArticleState.Created);
            var response =
                Client.Put(new PutArticleState { ArticleId = article.Id, ArticleState = BlogArticleState.Suspended });
            Assert.IsNull(response.ResponseStatus);
            var updateArticle = Resolve<ArticleRepository>().Get(article.Id);
            Assert.AreEqual(updateArticle.State, BlogArticleState.Suspended);
        }
        [Test]
        public void Blog_ArticleCategoryCreate()
        {
            var postCategory =
                Client.Post(new PostBlogArticleCategory
                {
                    Title = new Dictionary<string, string> { { Culture.pt_PT, "CategoryTest" } },
                    Description = new Dictionary<string, string> { { Culture.pt_PT, "CategoryTest" } }
                });
            Assert.IsNull(postCategory.ResponseStatus);
            Assert.IsFalse(postCategory.Category.Id.IsEmpty());
            var categories = Resolve<BlogPostCategoryRepository>().GetCategories();
            Assert.IsTrue(categories.Any(x => x.Title == postCategory.Category.Title));
        }

        [Test]
        public void Blog_ArticleComments()
        {
            var article = CreateArticleWithTemplate();
            var res = Client.Post(new PostBlogArticleComment { ArticleId = article.Id, Message = "testing!" });
            var comments = Client.Get(new GetBlogArticleComments { ArticleId = article.Id, Take = 1000 });
            Assert.IsTrue(comments.Results.Any());
        }
    }
}