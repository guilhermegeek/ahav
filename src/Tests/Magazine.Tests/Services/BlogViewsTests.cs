﻿using System.Linq;
using Ahav.ServiceModel.ApiDtos;
using Ahav.Tests;
using Ahav.Tests.Services;
using Magazine.ServiceInterface.Data;
using Magazine.ServiceModel.ApiDtos;
using Magazine.ServiceModel.Controllers;
using NUnit.Framework;

namespace Magazine.Tests.Services
{
    [TestFixture]
    [Category(TestCategory.Blog)]
    public class BlogViewsTests : DefaultTest
    {
        [SetUp]
        public void SetUp()
        {
            Initialize();
        }
        /// <summary>
        /// Loads the /blog as html
        /// </summary>
        [Test]
        public void Magazine_RenderHtml()
        {
        }
        /// <summary>
        /// Test the default view at /blog 
        /// </summary>
        [Test]
        public void Blog_ViewIndex()
        {
            var blog = CreateBlog();
            var article = CreateArticleWithTemplate(blog.Id);
            CreateArticleWithTemplate(blog.Id); // Aditional article for Articles on response
            Resolve<BlogTemplateRepository>().SetDefaultTemplate(new UpdateBlogTemplateDefault { BlogId = blog.Id, BlogTemplateId = article.TemplateId });
            Resolve<BlogRepository>().SetBlogDefault(ProjectId, blog.Id);
            var res = Client.Get(new GetBlogIndexView());
            Assert.IsNull(res.ResponseStatus);
            Assert.IsFalse(string.IsNullOrEmpty(res.RenderBody));
            Assert.IsTrue(res.Articles.Any(x => x.Id == article.Id));
        }
        [Test]
        public void Blog_ViewArticle()
        {
            var blog = CreateBlog();
            var article = CreateArticleWithTemplate();
            Resolve<BlogTemplateRepository>().SetDefaultTemplate(new UpdateBlogTemplateDefault { BlogId = blog.Id, BlogTemplateId = article.TemplateId });
            Resolve<BlogRepository>().SetBlogDefault(ProjectId, blog.Id);
            var res = Client.Get(new ArticleViewController { Id = article.Id});
            Assert.IsNull(res.ResponseStatus);
            Assert.AreEqual(article.Id, res.Article.Id);
        }
        
    }
}
