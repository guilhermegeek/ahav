﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.Tests.Services;
using Magazine.ServiceInterface.Data;
using Magazine.ServiceModel.ApiDtos;
using NUnit.Framework;

namespace Magazine.Tests.Services
{
    [TestFixture]
    public class BlogCategoryTests : DefaultTest
    {
        [Test]
        public void Blog_CreateCategory()
        {
            var category = Client.Post(new PostBlogCategory
            {
                Name = new Dictionary<string, string> { { Culture.pt_PT, "CategoryTest" } },
                Description = new Dictionary<string, string> { { Culture.pt_PT, "Category Test Description" } }
            }).Category;
            Assert.IsFalse(category.Id.IsEmpty());
            Assert.IsNull(Resolve<BlogPostCategoryRepository>().Get(category.Id));
        }
    }
}
