﻿using System.Collections.Generic;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Resources;
using Ahav.Tests;
using Ahav.Tests.Services;
using Magazine.ServiceInterface.Data;
using Magazine.ServiceModel.ApiDtos;
using NUnit.Framework;

namespace Magazine.Tests.Services
{
    [TestFixture]
    [Category(TestCategory.Blog)]
    public class BlogTemplateTests : DefaultTest
    {
        [SetUp]
        public void SetUp()
        {
            Initialize();
        }
        [Test]
        public void Blog_CreateBlankTemplate()
        {
            var blog = CreateBlog();
            var res = Client.Post(new PostBlogTemplateBlank { Name = "Test template", Public = false });
            Assert.IsNull(res.ResponseStatus);
        }
        [Test]
        public void Blog_CloneTemplate()
        {
            var blog = CreateBlog();
            var tplRes = Resolve<BlogTemplateRepository>().AddTemplate(new CreateBlogTemplate
            {
               
                Body =
                    new Dictionary<string, string>
                                                                      {
                                                                          {
                                                                              Culture.pt_PT,
                                                                              BlogDefaultTemplateResources.Body
                                                                          }
                                                                      },
                BodyViewArticle =
                    new Dictionary<string, string>
                                                                      {
                                                                          {
                                                                              Culture.pt_PT,
                                                                              BlogDefaultTemplateResources.BodyViewArticle
                                                                          }
                                                                      },
                BodyViewIndex =
                    new Dictionary<string, string>
                                                                      {
                                                                          {
                                                                              Culture.pt_PT,
                                                                              BlogDefaultTemplateResources.BodyViewIndex
                                                                          }
                                                                      },
                Name = "Test template",
                ProjectId = ProjectId
            });
            var res = Client.Post(new PostBlogTemplateClone { BlogTemplateId = tplRes.BlogTemplate.Id });
            Assert.IsNull(res.ResponseStatus);
            var tpl = Resolve<BlogTemplateRepository>().ReadTemplate(new ReadBlogTemplate { Id = res.BlogTemplateId });
            Assert.IsNull(tpl.ResponseStatus);
            Assert.AreEqual(tplRes.BlogTemplate.Body, tpl.Template.Body);
        }
        [Test]
        public void Blog_SaveDefaultTemplate()
        {
            var template = CreateBlogTemplate();
            var blog = CreateBlog();
            var response = Client.Put(new PutBlogTemplateDefault { BlogId = blog.Id, TemplateId = template.Id });
            Assert.IsNull(response.ResponseStatus);
            var id = Resolve<BlogTemplateRepository>().GetDefaultTemplateId(new ReadBlogTemplateDefaultId {BlogId = blog.Id});
            Assert.AreEqual(template.Id, id.TemplateId);
        }
        [Test]
        public void Blog_GetDefaultTemplate()
        {
            var blog = CreateBlog();
            var tpl = CreateBlogTemplate();
            Resolve<BlogTemplateRepository>().SetDefaultTemplate(new UpdateBlogTemplateDefault
                                                                     {
                                                                         BlogId = blog.Id,
                                                                         BlogTemplateId = tpl.Id
                                                                     });
            var res = Client.Get(new GetBlogTemplateDefault { BlogId = blog.Id });
            Assert.IsNull(res.ResponseStatus);
            Assert.AreEqual(res.TemplateId, tpl.Id);

        }

        [Test]
        public void Blog_UpdateTemplate()
        {
            var tpl = CreateBlogTemplate();
            string randomName = RsaTokenProvider.Instance.RandomString(15);
            var response = Client.Put(new PutBlogTemplate
                                          {
                                              Id = tpl.Id,
                                              Name = randomName,
                                              Body = tpl.Body,
                                              BodyViewArticle = tpl.BodyViewArticle,
                                              BodyViewIndex = tpl.BodyViewIndex,
                                              Metadata = tpl.Metadata
                                          });
            Assert.IsNull(response.ResponseStatus);
            var tplDb = Resolve<BlogTemplateRepository>().ReadTemplate(new ReadBlogTemplate {Id = tpl.Id});
            Assert.AreEqual(randomName, tplDb.Template.Name);

        }
        
        //[Test]
        //public void Blog_SaveDefaultTemplate()
        //{
        //    var tpl = CreateTemplate();
        //    var blog = CreateBlog();
        //    var res = client.Put(new PutBlogTemplateDefault { BlogId = blog.Id, TemplateId = tpl.Id });
        //    Assert.IsNull(res.ResponseStatus);
        //    var defaultTemplate = blogTemplateRepo.GetDefaultTemplate(new ReadBlogTemplateDefault { BlogId = blog.Id });
        //    Assert.AreEqual(defaultTemplate.BlogTemplate.Id, tpl.Id);
        //}
    }
}
