﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel;
using Ahav.ServiceModel;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Resources;
using Ahav.Tests;
using Ahav.Tests.Services;
using Magazine.ServiceInterface;
using Magazine.ServiceInterface.Data;
using Magazine.ServiceModel.ApiDtos;
using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using Magazine.ServiceModel.Dtos;
using MongoDB.Bson;
using NUnit.Framework;
using ServiceStack.ServiceClient.Web;

namespace Magazine.Tests.Services
{
    /// <summary>
    /// Setup ensures that all tests can be runned agains an existing blog and post
    /// </summary>
    [TestFixture]
    [Category(TestCategory.Blog)]
    public class BlogTests : DefaultTest
    {
        
        [SetUp]
        public void Setup()
        {
            Initialize();
        }
        
        // Private Helper
        private BlogTemplate CreateTemplate()
        {
            return Resolve<BlogTemplateRepository>().AddTemplate(new CreateBlogTemplate
                                                    {
                                                        Body =
                                                            new Dictionary<string, string>
                                                                {{Culture.pt_PT, BlogDefaultTemplateResources.Body}},
                                                        BodyViewArticle =
                                                            new Dictionary<string, string>
                                                                {
                                                                    {
                                                                        Culture.pt_PT,
                                                                        BlogDefaultTemplateResources.BodyViewArticle
                                                                    }
                                                                },
                                                        BodyViewIndex =
                                                            new Dictionary<string, string>
                                                                {
                                                                    {
                                                                        Culture.pt_PT,
                                                                        BlogDefaultTemplateResources.BodyViewIndex
                                                                    }
                                                                }
                                                    }).BlogTemplate;

        }
        private BlogCategory CreateCategory()
        {
            var category = new BlogCategory
            {
                Owner = TestAuth.UserId,
                Description = new Dictionary<string, string> { { Culture.pt_PT, "Testing!" } },
                Name = new Dictionary<string, string> { { Culture.pt_PT, "Testing!" } }
            };
            Resolve<BlogCategoryRepository>().Create(category);
            return category;
        }
        private Blog CreateBlog()
        {
            var category = CreateCategory();
            var blog = new Blog()
                           {
                               Description = new Dictionary<string, string> {{Culture.pt_PT, "Testing!"}},
                               Title = new Dictionary<string, string> {{Culture.pt_PT, "Blog de test"}},
                               CategoriesIds = new[] {category.Id},
                               Owner = TestAuth.UserId,
                               SubDomain = RsaTokenProvider.Instance.RandomString(10),
                           };
            Resolve<BlogRepository>().Create(blog);
            Resolve<SlugRepository>().AddSlug(ProjectId, SlugType.Blog, blog.Id, blog.SubDomain);
            return blog;
        }
        /// <summary>
        /// Creates a new article without publishing
        /// </summary>
        /// <returns></returns>
        private Article CreateArticleWithTemplate()
        {
            var blog = CreateBlog();
            var tpl = CreateTemplate();
            var a = new Article
                        {
                            BlogId = blog.Id,
                            Title = new Dictionary<string, string> {{Culture.pt_PT, "Test"}},
                            Body = new Dictionary<string, string> {{Culture.pt_PT, "Test"}},
                            CreatedBy = TestAuth.UserId,
                            Headline = new Dictionary<string, string> {{Culture.pt_PT, "Test"}},
                            Excerpt = new Dictionary<string, string> {{Culture.pt_PT, "Test"}},
                            Slug = RsaTokenProvider.Instance.RandomString(10),
                            State = BlogArticleState.Created
                        };
            Resolve<ArticleRepository>().Create(a);
            Resolve<BlogTemplateRepository>().SetDefaultTemplate(new UpdateBlogTemplateDefault
                                                    {BlogId = blog.Id, BlogTemplateId = tpl.Id});
            Resolve<SlugRepository>().AddSlug(ProjectId, SlugType.Article, a.Id, a.Slug);
            return a;
        } 

        //[Test]
        //public void Blog_SetIdForArticlesViewTemplate()
        //{
        //    var blog = CreateBlog();
        //    var tpl = CreateTemplate();
        //    var res = client.Post(new PutBlogTemplateDefault {BlogId = blog.Id, TemplateId = tpl.Id});
        //    Assert.IsNull(res.ResponseStatus);
        //    var id = blogRepo.GetBlogTemplateDefault(blog.Id);
        //    Assert.AreEqual(tpl.Id, id);
        //}
        
        [Test]
        public void Blog_GetById_Exists()
        {
            var blog = CreateBlog();
            var response = Client.Get(new GetBlog {Id = blog.Id});
            Assert.IsNull(response.ResponseStatus);
            Assert.AreEqual(response.Blog.Name, blog.Name);

        }
        [Test]
        public void Blog_GetBySlug_Exists()
        {
            var blog = CreateBlog();
            var respose = Client.Get(new GetBlog {Slug = blog.SubDomain});
            Assert.IsNull(respose.ResponseStatus);
            Assert.AreEqual(respose.Blog.Name, blog.Name);
        }
        [Test]
        public void Blog_Create()
        {
            //var category = CreateCategory();
            //Assert.IsFalse(category.Id.IsEmpty());
            var blogId =
                Client.Post(new PostBlog
                                {
                                    Name = "Test",
                                    //Categories = new [] {category.Id},
                                    OwnerId = TestAuth.UserId,
                                    SubDomain = "ta" + RsaTokenProvider.Instance.RandomString(3)
                                }).Id;
            Assert.IsFalse(blogId.IsEmpty());
            var blog = Resolve<BlogRepository>().Get(blogId);
            Assert.IsNotNull(blog);
            Assert.IsTrue(blog.Name == "Test");
            //Assert.IsTrue(Resolve<BlogCategoryRepository>().Get(category.Id).Blogs.Any(x => x.Id == blogId));

            var blogs = Client.Get(new GetBlogs {});
            Assert.IsTrue(blogs.Blogs.Any(x => x.Id == blogId));
            blogs = Client.Get(new GetBlogs {OwnersIds = new[] {TestAuth.UserId}});
            Assert.IsTrue(blogs.Blogs.Any());

            // Validate Slug
            var slugId = Resolve<SlugRepository>().GetId(ProjectId, SlugType.Blog, blog.SubDomain);
            Assert.IsFalse(slugId.IsEmpty());
            Assert.AreEqual(slugId, blog.Id);
        }
        [Test]
        public void Blog_Update_TitleAndName()

        {
            var blog = CreateBlog();
            string updatedS = "updatedtest";
            string updateN = "new-name";

            var response = Client.Put(new PutBlog
                           {
                               BlogId = blog.Id,
                               Description = blog.Description,
                               Title = new Dictionary<string, string> { { Culture.pt_PT, updatedS } },
                               SubDomain = blog.SubDomain,
                               Categories = blog.CategoriesIds,
                               Name = updateN
                           });
            Assert.IsNull(response.ResponseStatus);
            var b = Resolve<BlogRepository>().Get(blog.Id);
            Assert.IsTrue(b.Title.Any(x => x.Value == updatedS));
            Assert.AreEqual(b.Name, updateN);
        }
        
        /// <summary>
        /// Post a new article, but return invalid ResponseStatus 
        /// </summary>
        [Test]
        public void Blog_Create_Invalid_ThrowInvalid()
        {
            try
            {
                Client.Post(new PostBlog());
                throw new Exception("request not validated");
            }
            catch (WebServiceException ex)
            {

            }
        }
        
        [Test]
        public void Blog_E2E()
        {
            ObjectId categoryId;
            // Add
            var category = new BlogCategory { Description = new Dictionary<string, string> { { Culture.pt_PT, "test" } }, Name = new Dictionary<string, string> { { Culture.pt_PT, "Test" } } };
            var result = Resolve<BlogCategoryRepository>().Create(category);
            categoryId = Resolve<BlogCategoryRepository>().GetAll().First().Id;

            var blog = new Blog {Name = "Asd"};
            Resolve<BlogRepository>().Create(blog);


            var req = new PostBlogArticle()
                          {
                              SubDomain = RsaTokenProvider.Instance.RandomString(4),
                              Categories = new[] {categoryId},
                              BlogId = blog.Id
                          };
            var addPost = Client.Post(req);
            Assert.IsFalse(addPost.Article.Id.IsEmpty());
            Assert.IsTrue(addPost.Article.Slug== req.SubDomain);
            var postId = addPost.Article.Id;
            var post = Resolve<ArticleRepository>().Get(addPost.Article.Id);
            Assert.AreEqual(post.Slug, addPost.Article.Slug);
            Assert.IsTrue(addPost.Article.Categories.Contains(categoryId));

       
            var posts = Client.Get(new GetBlogArticles());
            Assert.IsTrue(posts.Articles.Any());

            posts = Client.Get(new GetBlogArticles {BlogId = blog.Id});
            Assert.IsTrue(posts.Articles.Any());

            post = Resolve<ArticleRepository>().Get(postId);
            post.Title[Culture.pt_PT] = "testtttt";

            
            //var updatePost = client.Get(new UpdateBlogPost {Id = postId});
        }

        
        
        


        
        /// <summary>
        /// Should set a Blog as default blog
        /// </summary>
        [Test]
        public void Blog_SetDefault()
        {
            var blog = CreateBlog();
            var res = Client.Post(new PostBlogDefault {BlogId = blog.Id});
            Assert.IsNull(res.ResponseStatus);
            var id = Resolve<BlogRepository>().GetBlogDefault(ProjectId);
            Assert.AreEqual(blog.Id, id);
        }
        
    }
}
