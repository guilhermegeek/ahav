﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace City.Tests
{
    [TestFixture]
    public class SportsMatchTests
    {
        /// <summary>
        /// Creates a new match, and the result must appear on scope (redis)
        /// </summary>
        [Test]
        public void Sports_SaveMatch_AppearsOnScore()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Removes a match
        /// </summary>
        [Test]
        public void Sports_RemoveMatch()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Returns matches
        /// </summary>
        [Test]
        public void Sports_GetMatches()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Updates the result from a match
        /// </summary>
        [Test]
        public void Sports_UpdateMatch()
        {
            throw new NotImplementedException();
        }
    }
}
