﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace City.Tests
{
    [TestFixture]
    public class SportsViewTests
    {
        /// <summary>
        /// Home page for sports.
        /// </summary>
        [Test]
        public void Sports_ViewHome()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// View page for a league, ie: Football Second League, Basket, etc
        /// </summary>
        [Test]
        public void Sports_ViewLeague()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// View page for a season journeys
        /// </summary>
        [Test]
        public void Sports_ViewJourneys()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// View page for a match, ie: AC Viseu x Tondela 
        /// </summary>
        [Test]
        public void Sports_ViewMatch()
        {
            throw new NotImplementedException();
        }
        [Test]
        public void Sports_ViewTeam()
        {
            throw new NotImplementedException();
        }
    }
}
