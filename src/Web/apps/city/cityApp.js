﻿angular.module("cityApp", ["commonApp"])
    .config(function ($stateProvider) {
        $stateProvider.state('home', {
            url: "/home",
            templateUrl: '/home.html',
            controller: 'homeController'
        });
    }).run(function () {
        
    });