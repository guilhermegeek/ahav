﻿angular.module("guestApp", ['commonApp'])
    .config(['$httpProvider', '$locationProvider', '$stateProvider', function($httpProvider, $locationProvider, $stateProvider) {
        $stateProvider
            .state("welcome", {
                url: "/welcome",
                templateUrl: "/welcome.html?format=bare",
                controller: "welcomeController"
            });
    }]).run(['$rootScope', function($rootScope) {

    }]);