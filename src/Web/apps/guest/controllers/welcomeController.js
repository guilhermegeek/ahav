﻿angular.module("guestApp").controller("welcomeController", function ($scope, $state, $rootScope, registerService, dialogService, socialService, accountService) {
    $scope.user = {};
    $scope.login = function() {
        accountService.authenticate($scope.user.email, $scope.user.password)
            .then(function(res) {
                window.location = "/admin";
            });
    };


    $scope.templates = { };
    $scope.account = { };
    $scope.basicRegister = function() {
        registerService.postRegister($scope.account)
            .then(function() {
                dialogService.ok(response.data.modal);
                $state.transitionTo("confirm");
            }, function(resDto) {
                if (!angular.isUndefined(resDto.responseStatus)) {
                    dialogService.validation(resDto.responseStatus);
                } else {
                    dialogService.fail(resDto);
                }
            });
    };
    $scope.registerFb = function(id, token) {
        socialService.register(id, token, 'facebook');
    };
});