﻿var cultures = ['pt-PT', 'en-US'];
describe("Test the culture functions", function () {
    var $scope = null;
    var utilsSvc;
    var entity;
    
    beforeEach(function () {
        module('commonApp');
        inject(function (utilsService) {
            utilsSvc = utilsService;
        });
        entity = {
            id: 1,
            name: { 'en-US': 'pristine', 'pt-PT': 'pristine' },
            body: { 'en-US': 'pristine', 'pt-PT': 'pristine' }
        };
    });

    it("Should save localized properties in entity", function () {
        var result = utilsSvc.saveLocale(entity, { name: 'dirty' }, 'en-US');
        expect(result.name["en-US"]).toBe("dirty");
        expect(result.name["pt-PT"]).toBe("pristine");
    });
    it("Should return a DTO with a single locale", function() {
        
        var properties = ['name'];
        var result = utilsSvc.getDto(entity, ['name'], 'pt-PT');
        expect(result.name = 'pt');
        expect(result.id).toBeUndefined();
    });
    it("Should create a new DTO for API requests", function() {
        entity.another = "1";
        var requestDto = utilsSvc.createRequest(entity, ['id']);
        expect(requestDto.name).toBeUndefined();
        expect(requestDto.id).toBe(entity.id);
        requestDto = utilsSvc.createRequest(entity, ['id', 'name']);
        expect(requestDto.name).toBe(entity.name);
        expect(requestDto.another).toBeUndefined();
    });
    it("Should return a localized DTO", function() {
        var dto = utilsSvc.getDtoModel(entity, ['id'], ['name'], null, 'pt-PT');
        expect(dto.id).toBe(entity.id);
        expect(dto.name).toBe("pristine");
        expect(dto.body).toBeUndefined();
    });
});