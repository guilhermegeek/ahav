﻿describe("Test the conversion of LESS to CSS", function() {
    var scope,
        element,
        lessSvc,
        lessCode, 
        timer;

    beforeEach(function () {
        module("ahav.common");
        inject(function(lessService, $timeout) {
            lessSvc = lessService;
            timer = $timeout;
        });
    });

    lessCode = '@import "/Content/bootstrap/bootstrap.less"; @body-bg: #CCC;';
    it("Should convert LESS to CSS", function () {
        var css = "";
        lessSvc.parse(lessCode)
            .then(function(res) {
                css = res;
            });
        // Flush is need at this promise for some reason...
        timer.flush();
        expect(css).toContain("Bootstrap");
        console.log(css);
    });
    
    // PATHS NOT WORKING
    it("Should convert LESS using template to CSs", function() {
        var css = "";
        lessSvc.parseTemplate('@import "bootstrap.less"; @body-bg: #FFF;', ['/Content/bootstrap'])
            .then(function(res) {
                css = res;
            });
        timer.flush();
        expect(css).toContain("Bootstrap");
        console.log(css);
    });
});