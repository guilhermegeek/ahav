﻿// Template and Content rendering tests
// All requests for Ahav projects excepect to be /subdomain/content_slug
// Projects with self domain or subdomain, are redirected in web service from domain.com/content_slug to domain.com/domain/content_slug

var contentMock = {
    getByPath: function($timeout, $q, subDomain, slug) {
        var deferred = $q.defer();
        $timeout(function() {
            deferred.resolve({
                data: {
                    content: {
                        title: "New Content",
                        body: "<p>Content Body!</p>"
                    },
                    template: {
                        id: 1,
                        title: "Testing",
                        body: '<div>Header</div><div id="templateContent">content</div><div>Footer</div>'
                    },
                    contentWithTemplate: '<div>Header</div><div ui-view="subContent"></div><div>Footer</div>',
                    reuseTemplate: true
                }
            });
        });
        return deferred.promise;
    }
};

describe("Should Get Template and Content", function () {
    var scope,
        contentSvc,
        timer;

    beforeEach(function() {
        module("ahavApp");
        inject(function(contentService, $timeout) {
            timer = $timeout;
            contentSvc = contentService;
        });
    });
    

})