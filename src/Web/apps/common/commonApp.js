var commonModules = [
    'angular-servicestack',
    "ui.bootstrap", "ui.bootstrap.transition", "ui.bootstrap.modal", "ui.router", "ui.bootstrap.tabs",
    "angularFileUpload",
    "ui.map",
    "colorpicker.module"];
var commonModule = angular.module('commonApp', commonModules);
commonModule.config(function ($provide, $locationProvider, $httpProvider, $stateProvider) {
    // Constansts
    $provide.constant("CONTENT_ERRORS", {
        slugInUse: "slugInUse",
        blogArticlesClose: "blogArticlesClose"
    });
    $httpProvider.defaults.headers.common['X-Requested-With'] = "XMLHttpRequest";
    // Use Html5Mode only if browser supports pushState
    var html5Mode = !!(window.history && history.pushState);
    $locationProvider.html5Mode(html5Mode);

    // Available from admin, city, web, etc
    $stateProvider
        .state("unauthorized", {
            url: "/unauthorized",
            templateUrl: "/unauthorized.html?format=html",
            controller: "unauthorizedCtrl"
        })
        .state("logout", {
            url: "/logout",
            controller: function() { window.location = "/logout"; }
        });
});
commonModule.run(function($rootScope, accountService) {
    // init the FB JS SDK

    FB.init({
        appId: '175461699313531',                        // App ID from the app dashboard
        channelUrl: 'http://www.guilhermecardoso.pt/fbchannel.html', // Channel file for x-domain comms
        status: true,                                 // Check Facebook Login status
        xfbml: true                                  // Look for social plugins on the page
    });


    // This flag indicates if the app is running under PhoneGap.
    // Others components uses this to render appropiate content for mobile. 
    $rootScope.phoneGap = (document.URL.indexOf("http://") == -1); // Phonegap doesn't run under http
});



commonModule.directive('modalComponent', function (modalService) {
    return {
        scope: {},
        replace: true,
        link: function ($scope, element, attributes) {
            $scope.modalApi = modalService;
            $scope.close = function (index) {
                modalService.close(index);
            };
        }
        //templateUrl: 'modalSidebar.html'
    };
});
commonModule.factory('modalService', function ($rootScope) {
    return {
        dto: { title: '', message: '' },
        cindex: 0,
        list: [],
        hasUnread: function () {
            return this.list.length > 0;
        },
        open: function (index) {
            this.dto = this.list[index];
            this.cindex = index;
        },
        close: function () {
            var position = this.index >= 2
                ? this.cindex - 1
                : 0;
            this.list.splice(position, 1);
            if (this.list.length > 0) {
                this.open(position - 1 > 0 ? position - 1 : 0);
            }
        },
        success: function (dto) {
            this.list.push(dto);
            if (this.list.length == 1) {
                this.open(0);
            }
        },
        error: function (response) {
            this.list.push({ title: 'ups', message: response });
            if (this.list.length == 1) {
                this.open(0);
            }
        },
        validation: function (response) {
            this.list.push(dto);
            if (this.list.length == 1) {
                this.open(0);
            }
        }
    };
});


commonModule.factory('notifyService', function ($rootScope, serviceStackRestClient) {

});


// view-source:http://bennadel.github.io/JavaScript-Demos/demos/image-cross-fade-angularjs/
commonModule.factory("gSlideService", function ($rootScope, $timeout) {
    return {
        // Init model bay be received by attributes or js function 
        init: function (model) {
            var slid;
            slid = $timeout(function () {
                $scope.image = model.images[_index];
                _index++;
                $scope.showImage();
            }, 4000);
        }
    };
});
commonModule.directive("gSlide", function ($rootScope, gSlideService) {
    var fader;
    var primary;
    var viewPort;

    function initFade(fadeSource) {
        fader
			.prop("src", fadeSource)
			.addClass("show");

        // Don't actually start the fade until the 
        // primary image has loaded the new source.
        primary.one("load", startFade);
    }
    // I determine if the fader is currently fading
    // out of view (that is currently animated).
    function isFading() {
        return (
			fader.hasClass("show") ||
			fader.hasClass("fadeOut")
		);
    }
    // I start the fade-out process.
    function startFade() {
        // The .width() call is here to ensure that 
        // the browser repaints before applying the
        // fade-out class (so as to make sure the
        // opacity doesn't kick in immediately).
        fader.width();
        fader.addClass("fadeOut");
        setHeight();
        setTimeout(teardownFade, 250);
    }
    // I clean up the fader after the fade-out has
    // completed its animation.
    function teardownFade() {
        fader.removeClass("show fadeOut");
    }

    var maxHeight = null;
    var heightExtraHeight = 20;
    function setHeight() {
        var height = primary.height() + heightExtraHeight;
        if (height > maxHeight) maxHeight = height;
        fader.height = height;
        viewPort.height = height;
    }

    return {
        restrict: "EACM",
        template: "<img class='primary' ng-src='{{image}}' />",
        transclude: true,
        scope: false,

        link: function ($scope, element, attributes, $timeout) {
            element.prepend("<img class='autofade' />");
            fader = element.find("img.autofade");
            primary = element.find("img.primary");
            viewPort = element.find(".viewport");
            $scope.$watch("image", function (newValue, oldValue) {
                if (newValue === oldValue) return;
                if (isFading()) return;
                initFade(oldValue);
            });
            // I prepare the fader to show the previous image
            // while fading out of view.

        }
    };
});
// https://github.com/angular-ui/ui-tinymce
commonModule.value('uiTinymceConfig', {}).directive('uiTinymce', ['uiTinymceConfig', function (uiTinymceConfig) {
    uiTinymceConfig = uiTinymceConfig || {};
    var generatedIds = 0;
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ngModel) {
            var expression, options, tinyInstance,
              updateView = function () {
                  ngModel.$setViewValue(elm.val());
                  if (!scope.$$phase) {
                      scope.$apply();
                  }
              };
            // generate an ID if not present
            if (!attrs.id) {
                attrs.$set('id', 'uiTinymce' + generatedIds++);
            }
            if (attrs.uiTinymce) {
                expression = scope.$eval(attrs.uiTinymce);
            } else {
                expression = {};
            }
            options = {
                // Update model when calling setContent (such as from the source editor popup)
                setup: function (ed) {
                    var args;
                    ed.on('init', function (args) {
                        ngModel.$render();
                    });
                    // Update model on button click
                    ed.on('ExecCommand', function (e) {
                        ed.save();
                        updateView();
                    });
                    // Update model on keypress
                    ed.on('KeyUp', function (e) {
                        ed.save();
                        updateView();
                    });
                    // Update model on change, i.e. copy/pasted text, plugins altering content
                    ed.on('SetContent', function (e) {
                        if (!e.initial) {
                            ed.save();
                            updateView();
                        }

                    });
                    if (expression.setup) {
                        scope.$eval(expression.setup);
                        delete expression.setup;
                    }
                },
                mode: 'exact',
                elements: attrs.id,
                plugins: "code table",
                toolbar: "undo redo || styleselect | bold italic | code table",
                menubar: false,
                relative_urls: false,
                content_css: "/assets/bootstrap/bootstrap.min.css",
                valid_elements: "@[template|id|class|style|title|dir<ltr?rtl|lang|xml::lang],"
    + "a[rel|rev|charset|hreflang|tabindex|accesskey|type|"
    + "name|href|target|title|class],strong/b,em/i,strike,u,"
    + "#p[style],-ol[type|compact],-ul[type|compact],-li,br,img[longdesc|usemap|"
    + "src|border|alt=|title|hspace|vspace|width|height|align],-sub,-sup,"
    + "-blockquote,-table[border=0|cellspacing|cellpadding|width|frame|rules|"
    + "height|align|summary|bgcolor|background|bordercolor],-tr[rowspan|width|"
    + "height|align|valign|bgcolor|background|bordercolor],tbody,thead,tfoot,"
    + "#td[colspan|rowspan|width|height|align|valign|bgcolor|background|bordercolor"
    + "|scope],#th[colspan|rowspan|width|height|align|valign|scope],caption,-div,"
    + "-span,-code,-pre,address,-h1,-h2,-h3,-h4,-h5,-h6,hr[size|noshade],-font[face"
    + "|size|color],dd,dl,dt,cite,abbr,acronym,del[datetime|cite],ins[datetime|cite],"
    + "object[classid|width|height|codebase|*],param[name|value|_value],embed[type|width"
    + "|height|src|*],map[name],area[shape|coords|href|alt|target],bdo,"
    + "button,col[align|char|charoff|span|valign|width],colgroup[align|char|charoff|span|"
    + "valign|width],dfn,fieldset,form[action|accept|accept-charset|enctype|method],"
    + "input[accept|alt|checked|disabled|maxlength|name|readonly|size|src|type|value],"
    + "kbd,label[for],legend,noscript,optgroup[label|disabled],option[disabled|label|selected|value],"
    + "q[cite],samp,select[disabled|multiple|name|size],small,"
    + "textarea[cols|rows|disabled|name|readonly],tt,var,big,iframe[src|width|heigtht|frameborder]",
                extended_valid_elements: "script[type|src],div[data-ng-controller|ng-controller|data-ng-include|ng-include|data-ui-sref|ui-sref|data-ui-view|ui-view|ui-map|data-ui-map|ui-options|data-ui-options|backstretch|data-backstretch|backstretch-bg|data-backstretch-bg|ahav-contact|data-ahav-contact]"
            };
            // extend options with initial uiTinymceConfig and options from directive attribute value
            angular.extend(options, uiTinymceConfig, expression);
            setTimeout(function () {
                tinymce.init(options);
            });


            ngModel.$render = function () {
                if (!tinyInstance) {
                    tinyInstance = tinymce.get(attrs.id);
                }
                if (tinyInstance) {
                    tinyInstance.setContent(ngModel.$viewValue || '');
                }
            };
        }
    };
}]);


// This directive allow to compile new DOM with bindings 
commonModule.directive('compile', function ($compile) {
    // directive factory creates a link function
    return {
        transclude: true,
        link: function ($scope, element, attrs) {
            $scope.$watch(
                function (scope) {
                    // watch the 'compile' expression for changes
                    return scope.$eval(attrs.compile);
                },
                function (value) {
                    // when the 'compile' expression changes
                    // assign it into the current DOM
                    element.html(value);

                    // compile the new DOM and link it to the current
                    // scope.
                    // NOTE: we only compile .childNodes so that
                    // we don't get into infinite loop compiling ourselves
                    $compile(element.contents())(scope);
                }
            );
        }
    };
});