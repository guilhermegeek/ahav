﻿angular.module("commonApp")
    .factory('templateService', function($rootScope, modalService, serviceStackRestClient) {
        return {
            routeGetFiles: function(templateId) {
                return "/api/template/" + templateId + "/file/";
            }
        };
    });