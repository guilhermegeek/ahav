﻿angular.module("commonApp").factory("lessService", function($q, $timeout) {
        return {
            parseTemplate: function (lessCode, includePaths) {
                var deferred = $q.defer();
                var parser = new (less.Parser)({
                    paths: includePaths
                });
                parser.parse(lessCode, function(err, tree) {
                    if (err) {
                        deferred.reject(err);
                    }
                    var css = tree.toCSS();
                    deferred.resolve(css);
                });
        
                return deferred.promise;
            },
            parse: function (lessCode) {
                var deferred = $q.defer();
                var parser = new (less.Parser);
                parser.parse(lessCode, function(err, tree) {
                    if (err) {
                        deferred.reject(err);
                    }
                    var css = tree.toCSS();
                    deferred.resolve(css);
                });
                
                return deferred.promise;
            }
        };
    });