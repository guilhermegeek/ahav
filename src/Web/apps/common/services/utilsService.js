﻿angular.module("commonApp").factory('utilsService', function () {
    return {
        // Creates a request DTO 
        createRequest: function (res, properties, propertiesLocale, propLocaleObj) {
            var dto = {};
            // Properties
            if (!angular.isUndefined(properties) && properties != null && properties.length > 0) {
                for (var j = 0; j < properties.length; j++) {
                    dto[properties[j]] = angular.isUndefined(res) || res == null || angular.isUndefined(res[properties[j]])
                        ? ""
                        : res[properties[j]];
                }
            }
            // Locale Properties: <culture, string>
            if (!angular.isUndefined(propertiesLocale) && propertiesLocale != null && propertiesLocale.length > 0) {
                for (var i = 0; i < propertiesLocale.length; i++) {
                    if (angular.isUndefined(dto[propertiesLocale[i]])) {
                        dto[propertiesLocale[i]] = {};
                    }
                    dto[propertiesLocale[i]] = angular.isUndefined(res) || res == null || angular.isUndefined(res[propertiesLocale[i]])
                        ? {}
                        : res[propertiesLocale[i]];
                }
            }
            // Locale Object <culture, object>
            if (!angular.isUndefined(propLocaleObj) && propLocaleObj != null && propLocaleObj.length > 0) {
                for (var k = 0; k < propLocaleObj.length; k++) {
                    if (angular.isUndefined(dto[propLocaleObj[k]])) {
                        dto[propLocaleObj[k]] = {};
                    }
                    dto[propLocaleObj[k]] = {};
                }
            }
            return dto;
        },
        getDtoModel: function (entity, prop, propLocale, propLocaleObj, locale) {
            var dto = {};
            if(!angular.isUndefined(prop) && prop != null && prop.length > 0)
            for (var i = 0; i < prop.length; i++) {
                dto[prop[i]] = !angular.isUndefined(entity) && entity != null && !angular.isUndefined(entity[prop[i]])
                    ? entity[prop[i]]
                    : "";
            }
            if (!angular.isUndefined(propLocaleObj) && propLocaleObj != null && propLocaleObj.length > 0) {
                for (var j = 0; j < propLocaleObj.length; j++) {
                    dto[propLocaleObj[j]] = !angular.isUndefined(entity[propLocaleObj[j]]) && !angular.isUndefined(entity[propLocaleObj[j]][locale])
                        ? entity[propLocaleObj][locale]
                        : [];
                }
            }
            if (!angular.isUndefined(propLocale) && propLocale != null && propLocale.length > 0) {
                for (var i = 0; i < propLocale.length; i++) {
                    if (angular.isUndefined(dto[propLocale[i]])) {
                        dto[propLocale[i]] = {};
                    }
                    dto[propLocale[i]] = !angular.isUndefined(entity[propLocale[i]]) && !angular.isUndefined(entity[propLocale[i]][locale])
                        ? entity[propLocale[i]][locale]
                        : "";
                }
            }
            return dto;
        },
        localizeRequest: function (originalObj, properties, cultures) {
            var object = angular.copy(originalObj);
            for (var i = 0; i < cultures.length; i++) {
                for (var j = 0; j < properties.length; j++) {
                    var property = properties[j];
                    var obj = {};
                    var culture = cultures[i];
                    obj[culture] = originalObj[property][i];
                    if (i == 0)
                        object[property] = {}; // reset 
                    object[property][culture] = originalObj[property][i];
                }
            }
            return object;
        },
        saveLocale: function (entity, localeDto, locale) {
            var dto = angular.copy(entity);
            for (var property in localeDto) {
                //if(entity.hasOwnProperty(property)) {
                dto[property][locale] = localeDto[property];
                //}
            }
            return dto;
        },
        getDto: function (entity, prop, locale) {
            var dto = {};
            for (var i = 0; i < prop.length; i++) {
                if (angular.isUndefined(dto[prop[i]])) {
                    dto[prop[i]] = "";
                }
                else {
                    dto[prop[i]] = !angular.isUndefined(entity[prop[i]]) && !angular.isUndefined(entity[prop[i]][locale])
                                ? entity[prop[i]][locale]
                                : "";
                }
            }
            return dto;
        }
    };
});