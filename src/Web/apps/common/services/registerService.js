﻿angular.module("commonApp").factory("registerService", function($q, serviceStackRestClient) {
    return {
        postRegister: function (dto) {
            var deferred = $q;
            serviceStackRestClient.post("/api/register", dto)
                .success(function(res) {
                    deferred.resolve(res.data);
                }).validation(function(res) {
                    deferred.reject(res.data);
                }).error(function(res) {
                    deferred.reject(res);
                });
            return deferred.promise;
        }
    };
});