;angular.module("commonApp")
    .factory("ahavService", function($rootScope, serviceStackRestClient) {
        return {
            init: function() {
                return serviceStackRestClient.get("/api/ahav/connect");
            }
        };
    });
;angular.module("commonApp")
    .factory("authService", function($rootScope, dialogService, serviceStackRestClient) {
        var _a = {
            id: _auth.id,
            key: _auth.key
        };
        var _p = null;
        return {
            getDefaultProject: function() {
                return serviceStackRestClient.get("/api/project/")
                    .success(function(response) {
                        return response.response.data.projects[0];
                        _p = response.response.data.projects[0];
                    }).error(function() {
                        dialogService.error(response.response.responseStatus);
                    });
            },
            getAuth: function() {
                return _a;
            },
            getProjectId: function() {
                return _p;
            }
        };
    });
;angular.module("commonApp")
    .factory("accountService", function(dialogService, serviceStackRestClient, $q) {
        return {
            postLogin: function(requestDto) {
                var deferred = $q.defer();
                serviceStackRestClient.post("/login", requestDto)
                    .success(function(response) {
                        deferred.resolve(response.data);
                    }).validation(function(response) {
                        dialogService.validation(response.response.data.responseStatus);
                        deferred.reject();
                    }).error(function(response) {
                        deferred.reject(response);
                        dialogService.fail(response);
                    });
                return deferred.promise;
            }
        };
    });
;angular.module("commonApp")
    .factory("projectService", function($q,  $modal, $rootScope, serviceStackRestClient, dialogService) {
        return {
            getAvailable: function() {
                return serviceStackRestClient.get("/api/project").error(function(response) {
                    dialogService.error(response.responseStatus);
                });
            },
            get: function(projectId) {
                return serviceStackRestClient.get("/api/project/" + projectId);
            },
            getPackagesAvailable: function() {
                return serviceStackRestClient.get("/api/package/available");
            },
            post: function(projectDto) {
                return serviceStackRestClient.post("/api/project/", projectDto);
            },
            put: function (id, request) {
                return serviceStackRestClient.put("/api/project/" + id, request);
            },
            // Returns a Promise
            // Resolve - DTO video, files
            viewSetVideoInit: function () {
                var deferred = $q.defer();
                serviceStackRestClient.get("/admin/project/video-init")
                    .success(function (res) {
                        var dto = {
                            files: res.data.files || [],
                            hasVideo: false
                        };
                        if(res.data.hasVideo) {
                            dto.hasVideo = true;
                            dto.video = res.data.video;
                        }
                        return deferred.resolve(dto);
                    }).error(function(res) {
                        dialogService.fail(res);
                    });
                return deferred.promise;
            },
            // Request DTO: projectId, fileId
            // Returns a Promise
            // Resolves - DTO video, files, hasVideo
            // Rejects: Http response obect
            postVideoInit: function (requestDto) {
                var deferred = $q.defer();
                serviceStackRestClient.post("/api/project/init-video", requestDto)
                    .success(function(res) {
                        dialogService.ok(res.data.modal);
                        deferred.resolve();
                    }).error(function(res) {
                        dialogService.fail(res);
                        deferred.reject(res);
                    });
                return deferred.promise;
            },
            deleteVideoInit: function (projectId) {
                return serviceStackRestClient.del("/api/project/init-video", projectId);
            }
        };
    });
;angular.module("commonApp").factory('contentService', function ($rootScope, serviceStackRestClient, $q, dialogService) {
    return {
        validateSlug: function(slug, contentId) {
            return serviceStackRestClient.post("/api/content/slug", { slug: slug, contentId: contentId });
        },
        getByPath: function(path) {
            return serviceStackRestClient.get('/' + path);
        },
        viewContents: function() {
            return serviceStackRestClient.get('/admin/content');
        },
        getAdminEditView: function(contentId) {
            return serviceStackRestClient.get("/admin/content/edit/" + contentId);
        },
        viewEditTemplate: function (templateId) {
            var deferrer = $q.defer();
            serviceStackRestClient.get("/admin/template/edit/" + templateId)
                .success(function(res) {
                    return deferrer.resolve(res);
                }).error(function(res) {
                    dialogService.fail(res);
                    return deferrer.reject(res);
                });
            return deferrer.promise;
        },
        postTemplate: function(dto) {
            return serviceStackRestClient.post("/api/template", dto);
        },
        saveTemplate: function(dto) {
            return serviceStackRestClient.put("/api/template/" + dto.id, dto);
        },
        post: function(dto) {
            return serviceStackRestClient.post('/api/content/', dto);
        },
        put: function(dto) {
            return serviceStackRestClient.put('/api/content/' + dto.id, dto);
        },
        del: function(contentId) {
            return serviceStackRestClient.del("/api/content/" + contentId);
        },
        delTemplate: function(templateId) {
            return serviceStackRestClient.del("/api/template/" + templateId);
        },
        getFiles: function(contentId, queries) {
            return serviceStackRestClient.get("/api/content/" + contentId + "/file", queries);
        },
        delFile: function(contentId, fileId) {
            //return serviceStackRestClient.delete("/api/content/" + contentId + "/file/" + fileId);
        },
        putContentDefault: function(contentId) {
            return serviceStackRestClient.put("/api/content/default", { contentId: contentId });
        },
        routeGetFiles: function(contentId) {
            return "/api/content/" + contentId + "/file";
        }
    };
});
;angular.module("commonApp")
    .factory('templateService', function($rootScope, modalService, serviceStackRestClient) {
        return {
            routeGetFiles: function(templateId) {
                return "/api/template/" + templateId + "/file/";
            }
        };
    });
;angular.module("commonApp")
    .factory("projectService", function($q,  $modal, $rootScope, serviceStackRestClient, dialogService) {
        return {
            getAvailable: function() {
                return serviceStackRestClient.get("/api/project").error(function(response) {
                    dialogService.error(response.responseStatus);
                });
            },
            get: function(projectId) {
                return serviceStackRestClient.get("/api/project/" + projectId);
            },
            getPackagesAvailable: function() {
                return serviceStackRestClient.get("/api/package/available");
            },
            post: function(projectDto) {
                return serviceStackRestClient.post("/api/project/", projectDto);
            },
            put: function (id, request) {
                return serviceStackRestClient.put("/api/project/" + id, request);
            },
            // Returns a Promise
            // Resolve - DTO video, files
            viewSetVideoInit: function () {
                var deferred = $q.defer();
                serviceStackRestClient.get("/admin/project/video-init")
                    .success(function (res) {
                        var dto = {
                            files: res.data.files || [],
                            hasVideo: false
                        };
                        if(res.data.hasVideo) {
                            dto.hasVideo = true;
                            dto.video = res.data.video;
                        }
                        return deferred.resolve(dto);
                    }).error(function(res) {
                        dialogService.fail(res);
                    });
                return deferred.promise;
            },
            // Request DTO: projectId, fileId
            // Returns a Promise
            // Resolves - DTO video, files, hasVideo
            // Rejects: Http response obect
            postVideoInit: function (requestDto) {
                var deferred = $q.defer();
                serviceStackRestClient.post("/api/project/init-video", requestDto)
                    .success(function(res) {
                        dialogService.ok(res.data.modal);
                        deferred.resolve();
                    }).error(function(res) {
                        dialogService.fail(res);
                        deferred.reject(res);
                    });
                return deferred.promise;
            },
            deleteVideoInit: function (projectId) {
                return serviceStackRestClient.del("/api/project/init-video", projectId);
            }
        };
    });
;angular.module("commonApp")
    .factory("socialService", function(serviceStackRestClient, $rootScope) {
        window.fbAsyncInit = function() {

        };
        return {
            login: function() {

            },
            register: function(id, token, provider) {
                var p = serviceStackRestClient.post("/api/social/register", { id: id, token: token, provider: provider })
                    .success(function(response) {
                        return response.response.data;
                    });
                return p;
            },
            validateToken: function(provider, token) {
                return serviceStackRestClient
                    .post("");
            },
            share: function(absoluteUri, message) {
                return serviceStackRestClient
                    .post('/social/share', { absoluteUri: absoluteUri, message: message });
            },
            like: function(provider, objectId) {
                return serviceStackRestClient
                    .post("/api/social/" + provider + "/" + objectId + "/like");
            }
        };
    });
;angular.module("commonApp")
    .factory('dialogService', function ($modal) {
        return {
            success: function (_title, _body) {
                var buttonName = (typeof buttonName === "undefined") ? "Ok" : buttonName;
                if (navigator.notification) {
                    notification.alert(message, alertCallback, title, buttonName);
                }
                else {
                    return $modal.open({
                        templateUrl: "modalSuccess.html",
                        controller: "modalSuccessController",
                        resolve: {
                            title: function() {
                                return _title;
                            },
                            body: function() {
                                return _body;
                            }
                        }
                    });
                }
            },
            // Receives a modal object with title and message
            ok: function (modal) {
                if (angular.isUndefined(modal))
                    modal = { title: "Success", message: "Done!" };
                return $modal.open({
                    templateUrl: "modalSuccess.html",
                    controller: "modalSuccessController",
                    resolve: {
                        title: function() {
                            return modal.title;
                        },
                        body: function() {
                            return modal.message;
                        },
                        buttons: function() {
                            return modal.buttons || [];
                        }
                    }
                });
            },
            validation: function(responseStatus) {
                if (angular.isUndefined(responseStatus)) {
                    return this.error("Ups", "Ups algo correu mal");
                }
                return $modal.open({
                    templateUrl: "modalValidation.html",
                    controller: "modalValidationController",
                    resolve: {
                        title: function() {
                            return "Ups";
                        },
                        body: function() {
                            return responseStatus.message || "Invalid";
                        },
                        errors: function() {
                            return responseStatus.errors || [];
                        }
                    }
                });
            },
            error: function(_title, _body) {
                return $modal.open({
                    templateUrl: "modalError.html",
                    controller: "modalErrorController",
                    resolve: {
                        title: function() {
                            return _title;
                        },
                        body: function() {
                            return _body;
                        }
                    }
                });
            },
            fail: function(response) {
                return $modal.open({
                    templateUrl: "modalError.html",
                    controller: "modalErrorController",
                    resolve: {
                        title: function() {
                            return "Ups";
                        },
                        body: function() {
                            return response.data.responseStatus.message;
                        }
                    }
                });
            },
            confirm: function(_title, _body) {
                return $modal.open({
                    templateUrl: "modalConfirm.html",
                    controller: "modalConfirmController",
                    resolve: {
                        title: function() {
                            return _title;
                        },
                        body: function() {
                            return _body;
                        }
                    }
                });
            }
        };
    }).controller("modalSuccessController", function($sce, $scope, $modalInstance, title, body) {
        $scope.title = title;
        $scope.body = $sce.trustAsHtml(body);
        $scope.close = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.ok = function() {
            $modalInstance.close();
        };
    }).controller("modalErrorController", function($sce, $scope, $modalInstance, title, body) {
        $scope.title = title;
        $scope.body = $sce.trustAsHtml(body);
        $scope.close = function() {
            $modalInstance.close();
        };
    }).controller("modalValidationController", function($sce, $scope, $modalInstance, title, body, errors) {
        $scope.title = title;
        $scope.body = $sce.trustAsHtml(body);
        $scope.errors = errors || [];
        $scope.close = function() {
            $modalInstance.close();
        };
    }).controller("modalConfirmController", function($sce, $scope, $modalInstance, title, body) {
        $scope.title = title;
        $scope.body = $sce.trustAsHtml(body);
        $scope.cancel = function() {
            $modalInstance.dismiss("cancel");
        };
        $scope.confirm = function() {
            $modalInstance.close();
        };
    });
;angular.module("ahav.ui").factory("lessService", function($q, $timeout) {
        return {
            parseTemplate: function (lessCode, includePaths) {
                var deferred = $q.defer();
                var parser = new (less.Parser)({
                    paths: includePaths
                });
                parser.parse(lessCode, function(err, tree) {
                    if (err) {
                        deferred.reject(err);
                    }
                    var css = tree.toCSS();
                    deferred.resolve(css);
                });
        
                return deferred.promise;
            },
            parse: function (lessCode) {
                var deferred = $q.defer();
                var parser = new (less.Parser);
                parser.parse(lessCode, function(err, tree) {
                    if (err) {
                        deferred.reject(err);
                    }
                    var css = tree.toCSS();
                    deferred.resolve(css);
                });
                
                return deferred.promise;
            }
        };
    });
;angular.module("commonApp").factory("browserDetectService", function($rootScope) {
    var browserDetect =
        {
            init: function() {
                this.browser = this.searchString(this.dataBrowser) || "Other";
                this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
            },

            searchString: function(data) {
                for (var i = 0; i < data.length; i++) {
                    var dataString = data[i].string;
                    this.versionSearchString = data[i].subString;

                    if (dataString.indexOf(data[i].subString) != -1) {
                        return data[i].identity;
                    }
                }
            },

            searchVersion: function(dataString) {
                var index = dataString.indexOf(this.versionSearchString);
                if (index == -1) return;
                return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
            },

            dataBrowser:
                [
                    { string: navigator.userAgent, subString: "Chrome", identity: "Chrome" },
                    { string: navigator.userAgent, subString: "MSIE", identity: "Explorer" },
                    { string: navigator.userAgent, subString: "Firefox", identity: "Firefox" },
                    { string: navigator.userAgent, subString: "Safari", identity: "Safari" },
                    { string: navigator.userAgent, subString: "Opera", identity: "Opera" }
                ]
        };
    browserDetect.init();
    return {
        browser: function() {
            return browserDetect.dataBrowser;
        },
        version: function() {
            return browserDetect.version;
        }
    };
});
;angular.module("commonApp").factory('supportService', function ($rootScope, serviceStackRestClient) {
    return {
        sendContact: function(requestDto) {
            return serviceStackRestClient.post("/api/support/contact", requestDto);
        }
    };
});
;angular.module("commonApp").factory('blogService', function ($q, $rootScope, modalService, serviceStackRestClient, dialogService) {
    return {
        postBlog: function(requestDto) {
            return serviceStackRestClient.post("/api/blog", requestDto);
        },
        postBlogArticle: function (requestDto) {
            return serviceStackRestClient.post('/api/blog/article', requestDto); 
        },
        putBlogArticle: function (requestDto, id) {
            return serviceStackRestClient.put("/api/blog/article/" + id, requestDto);
        },
        getBlogArticle: function (articleId) {
            return serviceStackRestClient.get("/api/blog/article/" + articleId);
        },
        getBlogArticles: function (blogId, skip, take) {
            return serviceStackRestClient.get("/admin/blog/" + blogId + "/article");
        },
        deletefile: function (articleId, fileId) {
            return serviceStackRestClient.del("/api/blog/article/" + articleId + "/file" + fileId);
        },
        deleteArticle: function (articleId) {
            return serviceStackRestClient.del("/api/blog/article/" + articleId);
        },
        getViewArticle: function(articleId) {
            return serviceStackRestClient.get("/blog/" + articleId);
        },
        // Blog Id
        // ReturnTemplates - If true, response has a list of Templates 
        getArticlesViewTemplate: function (blogId, returnTemplates) {
            var deferred = $q.defer();
            serviceStackRestClient.get("/api/blog/" + blogId + "/template/articles")
                .success(function(res) {
                    dialogService.ok(res.data.modal);
                    deferred.resolve(res.data);
                }).validation(function (res) {
                    dialogService.validation(res.data.responseStatus);
                    deferred.reject(res);
                }).error(function (res) {
                    dialogService.fail(res);
                    deferred.reject(res);
                });
            return deferred.promise;
        },
        postArticlesViewtemplate: function (requestDto) {
            var deferred = $q.defer();
            serviceStackRestClient.post("/api/blog/" + requestDto.blogId + "/template/articles", requestDto)
                .success(function(res) {
                    dialogService.ok(res.data.modal);
                    deferred.resolve(res.data);
                }).validation(function(res) {
                    dialogService.validation(res.data.responseStatus);
                    deferred.reject(res);
                }).error(function(res) {
                    dialogService.fail(res);
                    deferred.reject(res);
                });
            return deferred.promise();
        },
        postBlogDefault: function (blogId) {
            var deferred = $q.defer();
            serviceStackRestClient.post("/api/blog/" + blogId + "/default")
                .success(function(res) {
                    dialogService.ok(res.data.modal);
                    deferred.resolve(res.data);
                }).validation(function(res) {
                    dialogService.validation(res.data.responseStatus);
                }).error(function(res) {
                    dialogService.fail(res);
                });
            return deferred.promise;
        },
        routeGetFiles: function(articleId) {
            return "/api/blog/article/" + articleId + "/file";
        }
    };
});
;angular.module("commonApp").factory('utilsService', function () {
    return {
        // Creates a request DTO 
        createRequest: function (res, properties, propertiesLocale, propLocaleObj) {
            var dto = {};
            // Properties
            if (!angular.isUndefined(properties) && properties != null && properties.length > 0) {
                for (var j = 0; j < properties.length; j++) {
                    dto[properties[j]] = angular.isUndefined(res) || res == null || angular.isUndefined(res[properties[j]])
                        ? ""
                        : res[properties[j]];
                }
            }
            // Locale Properties: <culture, string>
            if (!angular.isUndefined(propertiesLocale) && propertiesLocale != null && propertiesLocale.length > 0) {
                for (var i = 0; i < propertiesLocale.length; i++) {
                    if (angular.isUndefined(dto[propertiesLocale[i]])) {
                        dto[propertiesLocale[i]] = {};
                    }
                    dto[propertiesLocale[i]] = angular.isUndefined(res) || res == null || angular.isUndefined(res[propertiesLocale[i]])
                        ? {}
                        : res[propertiesLocale[i]];
                }
            }
            // Locale Object <culture, object>
            if (!angular.isUndefined(propLocaleObj) && propLocaleObj != null && propLocaleObj.length > 0) {
                for (var k = 0; k < propLocaleObj.length; k++) {
                    if (angular.isUndefined(dto[propLocaleObj[k]])) {
                        dto[propLocaleObj[k]] = {};
                    }
                    dto[propLocaleObj[k]] = {};
                }
            }
            return dto;
        },
        getDtoModel: function (entity, prop, propLocale, propLocaleObj, locale) {
            var dto = {};
            if(!angular.isUndefined(prop) && prop != null && prop.length > 0)
            for (var i = 0; i < prop.length; i++) {
                dto[prop[i]] = !angular.isUndefined(entity) && entity != null && !angular.isUndefined(entity[prop[i]])
                    ? entity[prop[i]]
                    : "";
            }
            if (!angular.isUndefined(propLocaleObj) && propLocaleObj != null && propLocaleObj.length > 0) {
                for (var j = 0; j < propLocaleObj.length; j++) {
                    dto[propLocaleObj[j]] = !angular.isUndefined(entity[propLocaleObj[j]]) && !angular.isUndefined(entity[propLocaleObj[j]][locale])
                        ? entity[propLocaleObj][locale]
                        : [];
                }
            }
            if (!angular.isUndefined(propLocale) && propLocale != null && propLocale.length > 0) {
                for (var i = 0; i < propLocale.length; i++) {
                    if (angular.isUndefined(dto[propLocale[i]])) {
                        dto[propLocale[i]] = {};
                    }
                    dto[propLocale[i]] = !angular.isUndefined(entity[propLocale[i]]) && !angular.isUndefined(entity[propLocale[i]][locale])
                        ? entity[propLocale[i]][locale]
                        : "";
                }
            }
            return dto;
        },
        localizeRequest: function (originalObj, properties, cultures) {
            var object = angular.copy(originalObj);
            for (var i = 0; i < cultures.length; i++) {
                for (var j = 0; j < properties.length; j++) {
                    var property = properties[j];
                    var obj = {};
                    var culture = cultures[i];
                    obj[culture] = originalObj[property][i];
                    if (i == 0)
                        object[property] = {}; // reset 
                    object[property][culture] = originalObj[property][i];
                }
            }
            return object;
        },
        saveLocale: function (entity, localeDto, locale) {
            var dto = angular.copy(entity);
            for (var property in localeDto) {
                //if(entity.hasOwnProperty(property)) {
                dto[property][locale] = localeDto[property];
                //}
            }
            return dto;
        },
        getDto: function (entity, prop, locale) {
            var dto = {};
            for (var i = 0; i < prop.length; i++) {
                if (angular.isUndefined(dto[prop[i]])) {
                    dto[prop[i]] = "";
                }
                else {
                    dto[prop[i]] = !angular.isUndefined(entity[prop[i]]) && !angular.isUndefined(entity[prop[i]][locale])
                                ? entity[prop[i]][locale]
                                : "";
                }
            }
            return dto;
        }
    };
});
