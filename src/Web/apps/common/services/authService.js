﻿angular.module("commonApp")
    .factory("authService", function($rootScope, dialogService, serviceStackRestClient) {
        var _a = {
            id: _auth.id,
            key: _auth.key
        };
        var _p = null;
        return {
            getDefaultProject: function() {
                return serviceStackRestClient.get("/api/project/")
                    .success(function(response) {
                        return response.response.data.projects[0];
                        _p = response.response.data.projects[0];
                    }).error(function() {
                        dialogService.error(response.response.responseStatus);
                    });
            },
            getAuth: function() {
                return _a;
            },
            getProjectId: function() {
                return _p;
            }
        };
    });