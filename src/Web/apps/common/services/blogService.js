﻿angular.module("commonApp").factory('blogService', function ($q, $rootScope, modalService, serviceStackRestClient, dialogService) {
    return {
        postBlog: function(requestDto) {
            return serviceStackRestClient.post("/api/blog", requestDto);
        },
        postBlogArticle: function (requestDto) {
            return serviceStackRestClient.post('/api/blog/article', requestDto); 
        },
        putBlogArticle: function (requestDto, id) {
            return serviceStackRestClient.put("/api/blog/article/" + id, requestDto);
        },
        getBlogArticle: function (articleId) {
            return serviceStackRestClient.get("/api/blog/article/" + articleId);
        },
        getBlogArticles: function (blogId, skip, take) {
            return serviceStackRestClient.get("/admin/blog/" + blogId + "/article");
        },
        deletefile: function (articleId, fileId) {
            return serviceStackRestClient.del("/api/blog/article/" + articleId + "/file" + fileId);
        },
        deleteArticle: function (articleId) {
            return serviceStackRestClient.del("/api/blog/article/" + articleId);
        },
        getViewArticle: function(articleId) {
            return serviceStackRestClient.get("/blog/" + articleId);
        },
        // Blog Id
        // ReturnTemplates - If true, response has a list of Templates 
        getArticlesViewTemplate: function (blogId, returnTemplates) {
            var deferred = $q.defer();
            serviceStackRestClient.get("/api/blog/" + blogId + "/template/articles")
                .success(function(res) {
                    dialogService.ok(res.data.modal);
                    deferred.resolve(res.data);
                }).validation(function (res) {
                    dialogService.validation(res.data.responseStatus);
                    deferred.reject(res);
                }).error(function (res) {
                    dialogService.fail(res);
                    deferred.reject(res);
                });
            return deferred.promise;
        },
        postArticlesViewtemplate: function (requestDto) {
            var deferred = $q.defer();
            serviceStackRestClient.post("/api/blog/" + requestDto.blogId + "/template/articles", requestDto)
                .success(function(res) {
                    dialogService.ok(res.data.modal);
                    deferred.resolve(res.data);
                }).validation(function(res) {
                    dialogService.validation(res.data.responseStatus);
                    deferred.reject(res);
                }).error(function(res) {
                    dialogService.fail(res);
                    deferred.reject(res);
                });
            return deferred.promise();
        },
        postBlogDefault: function (blogId) {
            var deferred = $q.defer();
            serviceStackRestClient.post("/api/blog/" + blogId + "/default")
                .success(function(res) {
                    dialogService.ok(res.data.modal);
                    deferred.resolve(res.data);
                }).validation(function(res) {
                    dialogService.validation(res.data.responseStatus);
                }).error(function(res) {
                    dialogService.fail(res);
                });
            return deferred.promise;
        },
        routeGetFiles: function(articleId) {
            return "/api/blog/article/" + articleId + "/file";
        }
    };
});