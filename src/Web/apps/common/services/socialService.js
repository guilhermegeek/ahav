﻿angular.module("commonApp")
    .factory("socialService", function(serviceStackRestClient, $rootScope) {
        window.fbAsyncInit = function() {

        };
        return {
            login: function() {

            },
            register: function(id, token, provider) {
                var p = serviceStackRestClient.post("/api/social/register", { id: id, token: token, provider: provider })
                    .success(function(response) {
                        return response.response.data;
                    });
                return p;
            },
            validateToken: function(provider, token) {
                return serviceStackRestClient
                    .post("");
            },
            share: function(absoluteUri, message) {
                return serviceStackRestClient
                    .post('/social/share', { absoluteUri: absoluteUri, message: message });
            },
            like: function(provider, objectId) {
                return serviceStackRestClient
                    .post("/api/social/" + provider + "/" + objectId + "/like");
            }
        };
    });