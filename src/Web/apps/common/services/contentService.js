﻿angular.module("commonApp").factory('contentService', function ($rootScope, serviceStackRestClient, $q, dialogService) {
    return {
        validateSlug: function(slug, contentId) {
            return serviceStackRestClient.post("/api/content/slug", { slug: slug, contentId: contentId });
        },
        getByPath: function(path) {
            return serviceStackRestClient.get('/' + path);
        },
        viewContents: function() {
            return serviceStackRestClient.get('/admin/content');
        },
        getAdminEditView: function(contentId) {
            return serviceStackRestClient.get("/admin/content/edit/" + contentId);
        },
        viewEditTemplate: function (templateId) {
            var deferrer = $q.defer();
            serviceStackRestClient.get("/admin/template/edit/" + templateId)
                .success(function(res) {
                    return deferrer.resolve(res);
                }).error(function(res) {
                    dialogService.fail(res);
                    return deferrer.reject(res);
                });
            return deferrer.promise;
        },
        postTemplate: function(dto) {
            return serviceStackRestClient.post("/api/template", dto);
        },
        saveTemplate: function(dto) {
            return serviceStackRestClient.put("/api/template/" + dto.id, dto);
        },
        post: function(dto) {
            return serviceStackRestClient.post('/api/content/', dto);
        },
        put: function(dto) {
            return serviceStackRestClient.put('/api/content/' + dto.id, dto);
        },
        del: function(contentId) {
            return serviceStackRestClient.del("/api/content/" + contentId);
        },
        delTemplate: function(templateId) {
            return serviceStackRestClient.del("/api/template/" + templateId);
        },
        getFiles: function(contentId, queries) {
            return serviceStackRestClient.get("/api/content/" + contentId + "/file", queries);
        },
        delFile: function(contentId, fileId) {
            //return serviceStackRestClient.delete("/api/content/" + contentId + "/file/" + fileId);
        },
        putContentDefault: function(contentId) {
            return serviceStackRestClient.put("/api/content/default", { contentId: contentId });
        },
        routeGetFiles: function(contentId) {
            return "/api/content/" + contentId + "/file";
        }
    };
});