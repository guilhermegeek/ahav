﻿angular.module("commonApp")
    .factory("accountService", function(dialogService, serviceStackRestClient, $q) {
        return {
            authenticate: function(email, password) {
                var deferred = $q.defer();
                serviceStackRestClient.post("/login", { email: email, password: password})
                    .success(function(response) {
                        deferred.resolve(response.data);
                    }).validation(function(response) {
                        dialogService.validation(response.response.data.responseStatus);
                        deferred.reject();
                    }).error(function(response) {
                        deferred.reject(response);
                        dialogService.fail(response);
                    });
                return deferred.promise;
            }
        };
    });