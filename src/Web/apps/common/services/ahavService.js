﻿angular.module("commonApp")
    .factory("ahavService", function($rootScope, serviceStackRestClient) {
        return {
            init: function() {
                return serviceStackRestClient.get("/api/ahav/connect");
            }
        };
    });