﻿angular.module("commonApp").factory('supportService', function ($rootScope, serviceStackRestClient) {
    return {
        sendContact: function(requestDto) {
            return serviceStackRestClient.post("/api/support/contact", requestDto);
        }
    };
});