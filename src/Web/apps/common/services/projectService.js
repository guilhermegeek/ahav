﻿angular.module("commonApp")
    .factory("projectService", function($q,  $modal, $rootScope, serviceStackRestClient, dialogService) {
        return {
            getAvailable: function() {
                return serviceStackRestClient.get("/api/project").error(function(response) {
                    dialogService.error(response.responseStatus);
                });
            },
            get: function(projectId) {
                return serviceStackRestClient.get("/api/project/" + projectId);
            },
            getPackagesAvailable: function() {
                return serviceStackRestClient.get("/api/package/available");
            },
            post: function(projectDto) {
                return serviceStackRestClient.post("/api/project/", projectDto);
            },
            put: function (id, request) {
                return serviceStackRestClient.put("/api/project/" + id, request);
            },
            // Returns a Promise
            // Resolve - DTO video, files
            viewSetVideoInit: function () {
                var deferred = $q.defer();
                serviceStackRestClient.get("/admin/project/video-init")
                    .success(function (res) {
                        var dto = {
                            files: res.data.files || [],
                            hasVideo: false
                        };
                        if(res.data.hasVideo) {
                            dto.hasVideo = true;
                            dto.video = res.data.video;
                        }
                        return deferred.resolve(dto);
                    }).error(function(res) {
                        dialogService.fail(res);
                    });
                return deferred.promise;
            },
            // Request DTO: projectId, fileId
            // Returns a Promise
            // Resolves - DTO video, files, hasVideo
            // Rejects: Http response obect
            postVideoInit: function (requestDto) {
                var deferred = $q.defer();
                serviceStackRestClient.post("/api/project/init-video", requestDto)
                    .success(function(res) {
                        dialogService.ok(res.data.modal);
                        deferred.resolve();
                    }).error(function(res) {
                        dialogService.fail(res);
                        deferred.reject(res);
                    });
                return deferred.promise;
            },
            deleteVideoInit: function (projectId) {
                return serviceStackRestClient.del("/api/project/init-video", projectId);
            }
        };
    });