﻿angular.module("commonApp")
    .factory('dialogService', function ($modal  ) {
        return {
            success: function (_title, _body) {
                // In mobile devices (ie: apps from Cordova/Phonegap) it's preferred to work with natives buttons, styled of cource
                // Here it's
                var buttonName = (typeof buttonName === "undefined") ? "Ok" : buttonName;
                if (navigator.notification) {
                    notification.alert(message, alertCallback, title, buttonName);
                }
                else {
                    // .setErrorDetails("Server Error 500", "Some exception text"); // not working
                    var dialog = $modal.open({
                        backdrop: true,
                        keyboard: true,
                        backdropClick: true,
                        templateUrl: "modalSuccess.html",
                        controller: "modalSuccessController",
                        resolve: {
                            title: function() {
                                return _title;
                            },
                            body: function() {
                                return _body;
                            }
                        }
                    });
                    
                    //return $modal.open({
                    //    templateUrl: "modalSuccess.html",
                    //    controller: "modalSuccessController",
                    //    resolve: {
                    //        title: function() {
                    //            return _title;
                    //        },
                    //        body: function() {
                    //            return _body;
                    //        }
                    //    }
                    //});
                }
            },
            // Receives a modal object with title and message
            ok: function (modal) {
                if (angular.isUndefined(modal))
                    modal = { title: "Success", message: "Done!" };
                return $modal.open({
                    templateUrl: "modalSuccess.html",
                    controller: "modalSuccessController",
                    resolve: {
                        title: function() {
                            return modal.title;
                        },
                        body: function() {
                            return modal.message;
                        },
                        buttons: function() {
                            return modal.buttons || [];
                        }
                    }
                });
            },
            validation: function(responseStatus) {
                if (angular.isUndefined(responseStatus)) {
                    return this.error("Ups", "Ups algo correu mal");
                }
                return $modal.open({
                    templateUrl: "modalValidation.html",
                    controller: "modalValidationController",
                    resolve: {
                        title: function() {
                            return "Ups";
                        },
                        body: function() {
                            return responseStatus.message || "Invalid";
                        },
                        errors: function() {
                            return responseStatus.errors || [];
                        }
                    }
                });
            },
            error: function(_title, _body) {
                return $modal.open({
                    templateUrl: "modalError.html",
                    controller: "modalErrorController",
                    resolve: {
                        title: function() {
                            return _title;
                        },
                        body: function() {
                            return _body;
                        }
                    }
                });
            },
            fail: function(response) {
                return $modal.open({
                    templateUrl: "modalError.html",
                    controller: "modalErrorController",
                    resolve: {
                        title: function() {
                            return "Ups";
                        },
                        body: function() {
                            return response.data.responseStatus.message;
                        }
                    }
                });
            },
            confirm: function(_title, _body) {
                return $modal.open({
                    templateUrl: "modalConfirm.html",
                    controller: "modalConfirmController",
                    resolve: {
                        title: function() {
                            return _title;
                        },
                        body: function() {
                            return _body;
                        }
                    }
                });
            }
        };
    }).controller("modalSuccessController", function($sce, $scope, $modalInstance, title, body) {
        $scope.title = title;
        $scope.body = $sce.trustAsHtml(body);
        $scope.close = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.ok = function() {
            $modalInstance.close();
        };
    }).controller("modalErrorController", function($sce, $scope, $modalInstance, title, body) {
        $scope.title = title;
        $scope.body = $sce.trustAsHtml(body);
        $scope.close = function() {
            $modalInstance.close();
        };
    }).controller("modalValidationController", function($sce, $scope, $modalInstance, title, body, errors) {
        $scope.title = title;
        $scope.body = $sce.trustAsHtml(body);
        $scope.errors = errors || [];
        $scope.close = function() {
            $modalInstance.close();
        };
    }).controller("modalConfirmController", function($sce, $scope, $modalInstance, title, body) {
        $scope.title = title;
        $scope.body = $sce.trustAsHtml(body);
        $scope.cancel = function() {
            $modalInstance.dismiss("cancel");
        };
        $scope.confirm = function() {
            $modalInstance.close();
        };
    });