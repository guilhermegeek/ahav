﻿angular.module("commonApp")
    .directive('contenteditable', function() {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                elm.prepend("<div ></div>");
                // view -> model
                elm.bind('blur', function() {
                    scope.$apply(function() {
                        if (!angular.isUndefined(attrs["editor"]) && !angular.isUndefined(tinyMCE.get(attrs["editor"]))) {
                            tinyMCE.get(attrs["editor"]).setContent(elm.html());
                        }
                        ctrl.$setViewValue(elm.html());
                    });
                });

                // model -> view
                ctrl.$render = function() {
                    elm.html(ctrl.$viewValue);
                };

                // load init value from DOM
                ctrl.$setViewValue(elm.html());
            }
        };
    });