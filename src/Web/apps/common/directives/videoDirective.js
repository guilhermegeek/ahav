﻿angular.module("commonApp")
.service("videoUtils", function () {
    return {
        isMobile: function() {
            return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf("IEMobile") !== -1);
        }
    };
}).directive("videoPlayer", function($timeout, videoUtils) {
    return {
        restrict: 'A',
        scope: {
            autoPlay: "=autoPlay",
            stretch: "=stretch"
        },
        controller: function($scope) {
            var isVideoReady = false,
                isElementReady = false,
                isPlayerReady = false;

            $scope.onElementReady = function() {
                isElementReady = true;
                if(isVideoReady) {
                    $scope.setPlayerReady();
                }
            };
            $scope.onVideoReady = function() {
                isVideoReady = true;
                if(isElementReady) {
                    $scope.setPlayerReady();
                }
            };
            $scope.setPlayerReady = function() {
                this.play();
            };

        },
        link: {
            pre: function (scope, element, attr, controller) {
                $controller.videoLinkElement = element;
                $controller.elementScope = scope;
                $controller.videoElement = $controller.elementScope.find("video");
                $controller.elementScope.ready($scope.onElementReady);
                $controller.videoElement.ready($scope.onVideoReady);
            }
        }
    };
    })