﻿// https://github.com/arnaudbreton/angular-backstrech

angular.module("commonApp").directive('backstretch', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            var bg = $parse(attr.backstretchBg)(scope);
            $(element).backstretch(bg);
        }
    };
});