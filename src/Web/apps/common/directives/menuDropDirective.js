﻿angular.module("commonApp").directive("menuDrop", function ($parse, $log) {
    return {
        restrict: "ECMA",
        templateUrl: "menu-drop.html",
        scope: {
            menuValues: "=menuValues"
        },
        link: function (scope, element, attr) {
            $log.info(scope.menuValues.length);
        }
    };
});