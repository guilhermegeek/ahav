﻿angular.module("commonApp")
    .directive('draggable', function() {
        return function(scope, element) {
            var el = element[0];
            el.draggable = true;
            el.addEventListener('dragstart', function(e) {
                e.dataTransfer.effectAllowed = true;
                e.classList.add('drag');
                return false;
            }, false);
            el.addEventListener('dragend', function(e) {
                e.classList.remove('drag');
                return false;
            }, false);
        };
    }).directive('dropabble', function() {
        return {
            scope: { },//onDrop: '=', // parent's method to call on drop event
            link: function(scope, element) {

                var el = element[0];

                el.addEventListener('dragover', function(e) {
                    e.dataTransfer.dropEffect = 'move';
                    if (e.preventDefaults) e.preventDefaults();
                    this.classList.add('over');
                    return false;
                }, false);

                el.addEventListener('dragenter', function(e) {
                    this.classList.add("over");
                    return false;
                }, false);

                el.addEventListener('dragleave', function(e) {
                    this.classList.remove("over");
                    return false;
                }, false);

                el.addEventListener("drop", function(e) {
                    // Stop some browsers from redirecting
                    if (e.stopPropagation) e.stopPropagation();

                    this.classList.remove("over");
                });
            }
        };
    });