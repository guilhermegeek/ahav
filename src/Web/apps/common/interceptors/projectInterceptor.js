﻿angular.module("commonApp").factory("projectInterceptor", function($q, $rootScope, $injector) {
    return {
        request: function(config) {
            if ($rootScope.appCurrent != null) {
                config.headers["project"] = $rootScope.appCurrent.id;
            }
            return config || $q.when(config);
        }
    };
})