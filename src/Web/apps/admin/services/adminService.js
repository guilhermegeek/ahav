﻿// Admin API Service
angular.module("adminApp")
    .factory("adminService", function ($q, $modal, $rootScope, serviceStackRestClient, dialogService) {
        return {
            connect: function () {
                var deferred = $q.defer();
                serviceStackRestClient.get("/api/admin/connect")
                    .success(function (res) {
                        deferred.resolve(res.data);
                    }).error(function(res) {
                        dialogService.fail(res);
                    });
                return deferred.promise;
            },
            contentEditController: function (id) {
                var deferred = $q.defer();
                serviceStackRestClient.get("/admin/content/" + id + "/edit")
                    .success(function(res) {
                        deferred.resolve(res.data);
                    }).error(function (res) { dialogService.fail(res); });
                return deferred.promise;
            },
            applicationsController: function() {
                var deferred = $q.defer();
                serviceStackRestClient.get("/admin/application")
                    .success(function(res) {
                        deferred.resolve(res);
                    }).error(function() {

                    });
                return deferred.promise;
            },
            applicationViewController:function(appId) {
                var deferred = $q.defer();
                serviceStackRestClient.get("/admin/application/" + appId)
                    .success(function(res) {
                        deferred.resolve(res);
                    }).error(function(res) {
                    
                    });
            },
            viewBlog: function(blogId) {
                return serviceStackRestClient.get("/admin/blog/" + blogId);
            },
            viewBlogArticleEdit: function(articleId) {
                return serviceStackRestClient.get("/admin/blog/article/edit/" + articleId);
            },
            viewAttachments: function () {
                var deferred = $q.defer();
                
                serviceStackRestClient.get("/admin/attachments")
                    .success(function (res) {
                        var resDto = {
                            files: res.data.files || []
                        };
                        deferred.resolve(resDto);
                        
                        //    $modal.open({
                        //        templateUrl: "template/modal/content/attachs.html",
                        //        controller: "modalAttachController",
                        //        resolve: {
                        //            queue: function () {
                        //                return res.data.files || [];
                        //            },
                        //            options: function () {
                        //                return {
                        //                    url: "/api/attachment",
                        //                    autoUpload: true
                        //                };
                        //            }
                        //        }
                        
                    }).error(function(res) {
                        dialogService.fail(res);
                        deferred.reject(res);
                });
                return deferred.promise;
                
            },
            modalAttachs: function (url, queue) {
                var d1 = $q.defer();
                $modal.open({
                    templateUrl: "template/modal/content/attachs.html",
                    controller: "modalAttachController",
                    resolve: {
                        queue: function() {
                            return queue;
                        },
                        options: function() {
                            return {
                                autoUpload: true,
                                url: url,
                                showThumbnails: true
                            };
                        }
                    }
                });
                return d1.promise;
            }
        };
    });