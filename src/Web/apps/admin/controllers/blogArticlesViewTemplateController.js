﻿angular.module("adminApp").controller("blogArticlesViewTemplateController", function ($modalInstance, blogService, $stateParams, dialogService, $scope) {
    var blogId = $stateParams.blogId;
    blogService.getArticlesViewTemplate(blogId, true)
        .then(function(res) {
            $scope.template = res.data.templateId;
            $scope.templates = res.data.templates;
        });
    $scope.cancel = function() {
        $modalInstance.close();
    };
    $scope.save = function() {
        var requestDto = {
            templateId: template,
            blogId: blogId
        };
        blogService.postArticlesViewtemplate(requestDto)
            .then(function(res) {
                $modalInstance.close();
            });
    };
});