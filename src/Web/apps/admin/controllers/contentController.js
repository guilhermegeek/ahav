﻿angular.module("adminApp").
    controller('contentController', function (utilsService, lessService, $rootScope, $scope, serviceStackRestClient, contentService, $location, $modal) {
        $scope.templatesAvailable = [];
        $rootScope.activeMenu = 2;
        contentService.viewContents()
            .success(function(response) {
                $scope.contents = response.data.contents || [];
                $scope.templates = response.data.templates || [];
            }).error(function(response) {
                alert("erro");
            });
        $scope.editContent = function(id) {
            $location.path("/admin/content/edit/" + id);
        };
        $scope.addContentModal = function() {
            var dialog = $modal.open({
                templateUrl: "addContentModal.html",
                controller: "contentAddModalController",
                resolve: {
                    templates: function () {
                        return $scope.templates;
                    }
                }
            });
            dialog.result.then(function(c) {
                if(!angular.isUndefined(c))
                    $scope.contents.push(c);
            }, function () {
                console.log("Add Content - Modal dismiss");
            });
        };
        $scope.addTemplateModal = function() {
            var dialog = $modal.open({
                templateUrl: "addTemplateModal.html",
                controller: "templateAddModalController",
                resolve: {
                    templatesAvailable: function() {
                        return $scope.templatesAvailable;
                    }
                }
            });
            dialog.result.then(function(template) {
                if (!angular.isUndefined(template))
                    $scope.templates.push(template);
            });
        };
    }).controller("contentAddModalController", function($scope, $modalInstance, contentService, dialogService, templates) {
        $scope.content = {
            slug: "",
            template: null
        };
        $scope.templates = templates || [];

        $scope.close = function () {
            $modalInstance.dismiss();
        };
        
        $scope.ok = function() {
            contentService.post($scope.content)
                .success(function (response) {
                    $modalInstance.close(response.data.content);
                    dialogService.ok(response.data.modal);
                }).validation(function (response) {
                    dialogService.validation(response.response.data.responseStatus)
                })
                .error(function (response) {
                     dialogService.fail(response);
                });
        };
    })
    .controller("templateAddModalController", function ($scope, $modalInstance, contentService, dialogService, templatesAvailable) {
        $scope.templatesAvailable = templatesAvailable || [];
        $scope.template = {
            name: "",
            fromId: null
        };
        $scope.close = function() {
            $modalInstance.dismiss();
        };
        $scope.ok = function() {
            contentService.postTemplate($scope.template)
                .success(function(response) {
                    $modalInstance.close(response.data.template);
                    dialogService.ok(response.data.modal);
                }).error(function(response) {
                    dialogService.fail(response);
                });
        };
    });
adminModule.controller("templateNewController", function ($scope, utilsService, dialogService, contentService, $modal) {
    var lp = ['body', 'title'],
        prop = ['name', 'javascript'],
        requestDto = utilsService.createRequest({ }, prop, lp);
    $scope.template = utilsService.getDto(requestDto, prop, lp);
    $scope.subView = 0;
    $scope.editLocale = 0;
    var saveDto = function() {
        var requestProp = { };
        for (var i = 0; i < lp.length; i++) {
            requestProp[lp[i]][$rootScope.cultures[$scope.editLocale]] = $scope.template[lp[i]];
        }
        requestDto = utilsService.saveLocale(requestDto, requestProp, $scope.cultures[$scope.editLocale]);
        for (var j = 0; j < prop.length; j++) {
            requestDto[prop[j]] = $scope.template[prop[j]];
        }
    };
    var setCulture = function() {
        var dto = utilsService.getDto(requestDto, lp, $scope.cultures[$scope.editLocale]);
        for (var i = 0; i < lp.length; i++) {
            $scope.template[lp[i]] = dto[lp[i]];
        }
        //tinyMCE.get('templateBodyEditor').setContent($scope.template.body);
        $scope.addTemplateForm.$setPristine(true);
        $scope.addTemplateForm.body.$setPristine(true);
    };
    $scope.viewCulture = function(index) {
        $scope.editLocale = index;
        setCulture();
    };
    $scope.saveTemplate = function() {
        if ($scope.addTemplateForm.$dirty)
            saveDto();
        contentService.createTemplate(requestDto)
            .success(function(response) {
                dialogService.success(response.data.modal.title, response.data.modal.message);
                ;
            }).error(function(response) {
                dialogService.error("Ups", response.data.responseStatus.message);
            });
    };
});

//adminModule.controller('newContentController', function ($rootScope, $scope, contentService) {
//    var contentLocale = ["title", "body"], saveDtoLocale = function () {
//        for (var i = 0, p; p = contentLocale[i]; i++) {
//            $scope.request[p][$scope.cultures[$scope.editLocale]] = $scope.content[p];
//        }
//        $scope.request.slug = $scope.content.slug;
//        $scope.request.javascript = $scope.request.javascript;
//    };
//    $rootScope.activeMenu = 2;
//    $scope.subView = 0;
//    $scope.editLocale = 0;
//    $scope.content = {
//        title: "",
//        body: "",
//        javascript: "",
//        slug: ""
//    };
//    $scope.request = {
//        title: {},
//        body: {},
//        javascript: "",
//        slug: ""
//    };
//    // Actions
//    $scope.validateSlug = function () {
//        contentService.validateSlug($scope.content.slug);
//    };
//    $scope.addContent = function () {
//        saveDtoLocale();
//        contentService.post($scope.request);
//    };
//    $scope.viewCulture = function (index) {
//        // Save current localized model properties
//        if ($scope.content.title != "" || $scope.body != "") {
//            saveDtoLocale();
//        }
//        $scope.editLocale = index;
//        for (var i = 0, p; p = contentLocale[i]; i++) {
//            var current = $scope.request[p][$scope.cultures[$scope.editLocale]];
//            if (angular.isUndefined(current)) {
//                $scope.content[p] = "";
//            } else {
//                $scope.content[p] = current;
//            }
//        }
//        tinyMCE.get('contentBodyEditor').execCommand("mceInsertContent", false, "<p>teste</p>");
//    };
//});
