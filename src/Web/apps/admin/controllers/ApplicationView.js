﻿angular.module("adminApp").__defineGetter__("applicationViewController", ['adminService', '$scope', function(adminService, $scope) {
    adminService.applicationViewController
        .then(function(res) {
            $scope.applicationName = res.applicationName;
            $scope.applicationDescription = res.applicationDescription;
            $scope.applicationDomain = res.applicationDomain;
        });
}]);