﻿angular.module("adminApp")
    .controller('adminHomeController', function ($scope, $rootScope, serviceStackRestClient, dialogService, modalService, $modal) {
        
        
        $scope.addAccountModal = function () {
            var dialog = $modal.open({
                templateUrl: "addAccountModal.html",
                controller: "AddAccountModalController"
            });
        };
        $scope.editProjectModal = function() {

                    var dialog = $modal.open({
                        templateUrl: "/Content/partials/project-edit.html",
                        controller: "projectEditController",
                        resolve: {
                            projectId: function() {
                                return $rootScope.project.id;
                            }
                        }
                    });
                    dialog.result.then(function(project) {
                        $rootScope.setProject($rootScope.project.id);
                    });
               

        };
    }).controller("AddAccountModalController", function ($scope, $modalInstance, accountService, dialogService, projectService) {
        $scope.addAccount = function () {
            accountService.post($scope.account)
                .success(function (response) {

                }).error(function (response) {

                });
        };
    });