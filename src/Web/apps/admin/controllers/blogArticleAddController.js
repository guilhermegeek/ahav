﻿angular.module("adminApp")
    .controller('blogArticleAddController', function ($modalInstance, dialogService, $rootScope, $scope, blogService, serviceStackRestClient, utilsService, modalService) {
    var prop = ["subDomain", "name"],
        requestDto = utilsService.createRequest(null, prop, null);
    $scope.article = utilsService.getDto(requestDto, prop, null);
    $scope.subView = 0;
    $scope.editLocale = $rootScope.locale;

    var saveDto = function() {
        for (var j = 0; j < prop.length; j++) {
            requestDto[prop[j]] = $scope.article[prop[j]];
        }
        // Set blog id
        requestDto["blogId"] = $rootScope.blogActive;
    };
    $scope.addArticle = function() {
        saveDto();
        blogService.postBlogArticle(requestDto)
            .success(function (response) {
                $modalInstance.close(response.data.articleInfo);
                dialogService.ok(response.data.modal);
            }).error(function(response) {
                dialogService.fail(response);
            });
    };
});