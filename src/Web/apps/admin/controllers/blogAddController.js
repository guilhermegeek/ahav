﻿angular.module("adminApp")
    .controller("blogAddController", function($scope, $rootScope, utilsService, dialogService, $modal, $modalInstance, blogService) {
        var prop = ['name', 'subDomain'],
            requestDto = utilsService.createRequest(null, prop, null, null);
        $scope.blog = utilsService.getDto(requestDto, prop, null);
        // Blog was created (used to hide button)
        $scope.fs = false;
        // Save DTO request
        var saveDto = function() {
            for (var i = 0; i < prop.length; i++) {
                requestDto[prop[i]] = $scope.blog[prop[i]];
            }
        };

        $scope.addBlog = function() {
            saveDto();
            blogService.postBlog(requestDto)
                .success(function(res) {
                    $scope.fs = true;
                    $modalInstance.close();
                    dialogService.ok(res.data.modal);
                }).error(function(res) {
                    dialogService.fail(res);
                });
        };
        $scope.close = function() {
            $modalInstance.close();
        };
    });