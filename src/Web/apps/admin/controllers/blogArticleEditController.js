﻿angular.module("adminApp")
    .controller("blogArticleEditController", function ($rootScope, $timeout, $stateParams, $scope,adminService, blogService, dialogService, utilsService) {
        var prop = ["id", "name", "slug"],
            propLocale = ["headline", "title", "body"],
            blogId = $rootScope.blogActive,
            articleId = $stateParams.articleId,
            requestDto = utilsService.createRequest(null, prop, propLocale, null); //@TODO this extra null is wrong?!?!
        alert(blogId);
        $scope.article = utilsService.getDto(requestDto, prop, propLocale);
        $scope.editLocale = 0; // Portuguese by default
        $scope.tags = [];
        $scope.categories = [];
        $scope.queue = []; // Attachements
        
        var saveDto = function() {
            for (var i = 0; i < prop.length; i++) {
                requestDto[prop[i]] = $scope.article[prop[i]];
            }
            for (var j = 0; j < propLocale.length; j++) {
                requestDto[propLocale[j]][$rootScope.cultures[$scope.editLocale]] = $scope.article[propLocale[j]];
            }
        };
        var setCulture = function() {
            var dto = utilsService.getDtoModel(requestDto, prop, propLocale, null, $scope.cultures[$scope.editLocale]);
            for (var i = 0; i < prop.length; i++) {
                $scope.article[prop[i]] = dto[prop[i]];
            }
            for (var j = 0; j < propLocale.length; j++) {
                $scope.article[propLocale[j]] = dto[propLocale[j]];
            }
            if (angular.isUndefined(tinyMCE.get("articleBody")))
                $timeout(function () {
                    setTinymce();
                }, 4000);
            else
                setTinymce();
        };
        var setTinymce = function() {
            tinyMCE.get("articleBody").setContent($scope.article.body || "");
        };
        $scope.saveArticle = function () {
            saveDto();
            blogService.putBlogArticle(requestDto, articleId)
                .success(function(res) {
                    dialogService.ok(res.data.modal);
                }).error(function(res) {
                    dialogService.fail(res);
                });
        };
        $scope.deleteArticle = function() {
            dialogService.confirm("Apagar?", "<p>Deseja mesmo apagar o artigo</p><p>Após apagado, não pode ser restaurado!</p>")
                .result.then(function() {
                    blogService.deleteArticle(articleId)
                        .success(function(res) {
                            dialogService.ok(res.data.modal);
                            // @TODO REMOVE ARTICLE FROM SCOPE
                        }).error(function(res) {
                            dialogService.fail(res);
                        });
                });
        };
        $scope.modalAttachs = function () {
            var url = blogService.routeGetFiles(articleId);
            adminService.modalAttachs(url, $scope.queue);
        }
        $scope.viewCulture = function(index) {
            saveDto();
            $scope.editLocale = index;
            setCulture();
        };
        // Init
        adminService.viewBlogArticleEdit(articleId)
            .success(function (response) {
                requestDto = utilsService.createRequest(response.data, prop, propLocale, null);
                if(!angular.isUndefined(response.data.files) && response.data.files != null && response.data.files.length > 0) {
                    $scope.queue = response.data.files;
                }
                if(!angular.isUndefined(response.data.templates) && response.data.templates !=null && response.data.templates.length > 0) {
                    
                }
                setCulture();
            }).error(function (res) {

            });
    }).controller("blogEditController", function ($scope, blogService) {
        $scope.templates = [];
        //$scope.templates = response.data.templates;
    });