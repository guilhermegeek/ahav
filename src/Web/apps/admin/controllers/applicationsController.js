﻿angular.module("adminApp").controller("applicationsController", ['$scope', 'adminService', function($scope, adminService) {
    $scope.appsAvailable = [];

    var res = adminService.applicationsController();
    res.then(function(res) {
        if (res.appsAvailable.length > 0)
            $scope.appsAvailable = res.appsAvailable;
    });
}]);