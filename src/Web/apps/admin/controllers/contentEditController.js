﻿adminModule
    .controller("contentEditController", function(adminService, utilsService, $modal, $rootScope, dialogService, $scope, contentService, $stateParams, $sce) {
        var contentId = $stateParams.id,
            prop = ["id", "template", "slug"],
            propLocale = ["body", "title"],
            requestDto = utilsService.createRequest({ }, prop, propLocale),
            contentErrors = {
                slugInUse: "slugInUse"
            };
        $scope.navType = 'pills';
        requestDto.id = contentId;
        $scope.content = utilsService.getDtoModel(requestDto, prop, propLocale, $scope.cultures[$scope.editLocale]);
        $rootScope.activeMenu = 2;
        $scope.editLocale = 0;

        var saveDto = function() {
            var requestProp = { };
            for (var i = 0; i < propLocale.length; i++) {
                requestDto[propLocale[i]][$rootScope.cultures[$scope.editLocale]] = $scope.content[propLocale[i]];
            }
            requestDto = utilsService.saveLocale(requestDto, requestProp, $scope.cultures[$scope.editLocale]);
            for (var j = 0; j < prop.length; j++) {
                requestDto[prop[j]] = $scope.content[prop[j]];
            }
        };
        var setCulture = function() {
            var dto = utilsService.getDtoModel(requestDto, prop, propLocale, [], $scope.cultures[$scope.editLocale]);
            for (var i = 0; i < propLocale.length; i++) {
                $scope.content[propLocale[i]] = dto[propLocale[i]];
            }
            for (var j = 0; j < prop.length; j++) {
                $scope.content[prop[j]] = requestDto[prop[j]];
            }
            if (angular.isUndefined(tinyMCE.get("contentBodyEditor"))) {
                setTimeout(4000, function() {
                    tinyMCE.get('contentBodyEditor').setContent($scope.content.body);
                    $scope.$apply();
                });
            } else {
                tinyMCE.get('contentBodyEditor').setContent($scope.content.body);
            }

            //$scope.editContentForm.$setPristine(true);
            //$scope.editContentForm.body.$setPristine(true);
        };

        // Init 
        adminService.contentEditController(contentId)
            .then(function(res) {
                // Files
                $scope.queue = res.files || [];
                requestDto = utilsService.createRequest(res, prop, propLocale);
                $scope.templates = res.templates || [];
                setCulture();
            });

        // Attachments
        $scope.modalAttachs = function () {
            var url = contentService.routeGetFiles(contentId);
            adminService.modalAttachs(url, $scope.queue);
        };
        $scope.modalDeleteContent = function() {
            dialogService.confirm('Delete?', "<p>Once you delete this you can't restore it back.</p><p>Are you sure?")
                .result.then(function() {
                    contentService.del(contentId)
                        .success(function(response) {
                            dialogService.ok(response.data.modal);
                            // @TODO REMOVE CONTENT FROM SCOPE
                        }).error(function(response) { dialogService.fail(response); });
                });
        };
        $scope.setDefaultContent = function() {
            contentService.putContentDefault(contentId)
                .success(function(response) {
                    dialogService.ok(response.data.modal);
                }).error(function(response) { dialogService.fail(response) });
        };
        // Actions
        $scope.validateSlug = function() {
            contentService.validateSlug($scope.content.slug, contentId)
                .success(function(response) {
                    if ($scope.editContentForm["slug"].$dirty && $scope.editContentForm["slug"].$invalid) {
                        $scope.editContentForm["slug"].$setValidity(contentErrors.slugInUse, true);
                    }
                })
                .validation(function(response) {
                    $scope.editContentForm["slug"].$setValidity(contentErrors.slugInUse, false);
                    //dialogService.error("Ups", response.response.responseStatus.message);
                }).error(function(response) {
                    dialogService.error("Ups", response.response.responseStatus.message);
                });
        };
        $scope.saveContent = function() {
            saveDto();
            contentService.put(requestDto)
                .success(function(response, headers, config) {
                    dialogService.success(response.data.modal.title, response.data.modal.message);
                }).error(function(response) {
                    dialogService.error("Error", response.response.data.responseStatus.message);
                });
        };
        $scope.viewCulture = function(index) {
            saveDto();
            $scope.editLocale = index;
            setCulture();
        };
        $scope.appendImage = function(imageUrl) {
            tinyMCE.get("contentBodyEditor").execCommand("mceInsertContent", false, "<img src='" + imageUrl + "' alt='image' />");
        };
        $scope.previewHtml = function() {
            $scope.previewBody = $sce.trustAsHtml($scope.content.body);
        };

        setTimeout(4000, function() {
            tinymce.activeEditor.dom.setAttrib(tinymce.activeEditor.dom.select('body'), 'droppable', 'droppable');
            $scope.$apply();
        });
    });