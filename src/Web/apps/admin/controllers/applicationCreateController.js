﻿angular.module("adminApp")
    .controller("applicationCreateController", function ($rootScope, $scope, $modalInstance, projectService, dialogService, utilsService) {
        // Project was created (used to hide the button) 
        $scope.fs = false;
        $scope.project = { name: "", description: "", domain:"", packageId: null  };
        projectService.getPackagesAvailable()
            .success(function(response) {
                $scope.packages = response.data.packages;
            });
        $scope.close = function() {
            $modalInstance.close();
        };
        $scope.addProject = function() {
            var request = angular.copy($scope.project);
            projectService.post(request)
                .success(function(response) {
                    $scope.fs = true;
                    $modalInstance.close();
                    $rootScope.projects.push(response.data.project);
                    if ($rootScope.projects.length == 1) {
                        $rootScope.projectActive = response.data.project.id;
                    }
                    dialogService.ok(response.data.modal);
                }).validation(function(res) {
                    dialogService.validation(res.responseStatus);
                });
        };
    });