﻿adminModule.controller("templateEditController", function (lessService, $rootScope, $scope, utilsService, contentService, $modal, dialogService, $stateParams, $sce, $timeout, templateService, adminService) {
    $scope.editor = {
        fonts: [{name: "PT Comic Sans", value: "PTCS"}, {name: "Arial", value:"A"}]
    };

    var init = true,
        templateId = $stateParams.templateId,
        prop = ['id', 'name', 'css', 'less'],
        propLocale = ['body', 'title'],
        propLocaleObj = ["menu"],
        requestDto = utilsService.createRequest({}, prop, propLocale, propLocaleObj);
    requestDto.id = templateId;
    //$scope.template = utilsService.getDto(requestDto, prop, propLocale);//$rootScope.cultures[$scope.editLocale]
    $scope.template = { };
    //  $scope.template.menu     = [];
    $scope.editLocale = 0;
    $scope.showAddMenu = false;
    $scope.textColor = {
        color: ""
    };
  
    var saveDto = function () {
        var requestProp = {};
        // Save localized properties
        for (var i = 0; i < propLocale.length; i++) {
            requestDto[propLocale[i]][$rootScope.cultures[$scope.editLocale]] = $scope.template[propLocale[i]];
        }
        // Save localized objects
        for (var k = 0; k < propLocaleObj.length; k++) {
            requestDto[propLocaleObj[k]][$rootScope.cultures[$scope.editLocale]] = $scope.template[propLocaleObj[k]];
        }
        // Save properties
        for (var j = 0; j < prop.length; j++) {
            requestDto[prop[j]] = $scope.template[prop[j]];
        }
        lessService.parse(requestDto.less)
            .then(function(css) {
                requestDto.css = css;
            }, function(err) {
                alert("Error in less :(");
            });
    };
    var setCulture = function () {
        var dto = utilsService.getDtoModel(requestDto, prop, propLocale, propLocaleObj, $scope.cultures[$scope.editLocale]);
        for (var j = 0; j < prop.length; j++) {
            $scope.template[prop[j]] = requestDto[prop[j]];
        }
        for (var i = 0; i < propLocale.length; i++) {
            $scope.template[propLocale[i]] = dto[propLocale[i]];
        }
        for (var v = 0; v < propLocaleObj.length; v++) {
            $scope.template[propLocaleObj[v]] = dto[propLocaleObj[v]];
        }
        //$scope.editTemplateForm.$setPristine(true);
        //$scope.editTemplateForm.body.$setPristine(true);
    };
    // Load initial content
    contentService.viewEditTemplate(templateId)
        .then(function (response) {
            var res = response.data;
            requestDto = utilsService.createRequest(res, prop, propLocale, propLocaleObj);
            setCulture();
            $scope.queue = res.files || [];
            init = false;
            // SAVE CSS
            //$scope.previewHtml();
            
        });
    // Attachments
    var u = templateService.routeGetFiles(templateId);
    
    $scope.modalAttachs = function () {
        var d1 = adminService.modalAttachs(u, $scope.queue);       
    };

    // Change edit language
    $scope.viewCulture = function (index) {
        saveDto();
        $scope.editLocale = index;
        setCulture();
    };
    $scope.preview = { };
    $scope.previewHtml = function () {
        
    };
    $scope.saveTemplate = function () {
        saveDto();
        contentService.saveTemplate(requestDto)
            .success(function (response) {
                dialogService.success(response.data.modal.title, response.data.modal.message);
            }).error(function (response) {
                dialogService.error("Ups", response.data.responseStatus.message);
            });
    };
    $scope.modalDelete = function() {
        dialogService.confirm("Delete?", "<p>Once you delete this you can't restore it back.</p><p>Are you sure?")
            .result.then(function() {
                contentService.delTemplate(templateId)
                    .success(function(response) {
                        dialogService.ok(response.data.modal);
                    }).error(function(response) {
                        dialogService.error("Error", response.response.data.responseStatus.message);
                    });
            });
    };
    $scope.addMenuModal = function () {
        var m = $modal.open({
            templateUrl: "addMenuModal.html",
            controller: "addMenuModalController"
        });
        m.result.then(function(menu) {
            $scope.template.menu.push(menu);
            saveDto();
        });
    };
    $scope.modalHtml = function() {
        var m = $modal.open({
            templateUrl: "templateBodyHtml.html",
            resolve: {
                template: function () {
                    return $scope.template;
                },
            },
            controller: "contentModalHtmlController"
        });
    };
}).controller("contentModalHtmlController", function ($scope, template, $sce) {
    alert(template);
    $scope.templateCss = template.css;
    $scope.templateBody = $sce.trustAsHtml(template.body);
}).controller("addMenuModalController", function ($modalInstance, $scope, modalService) {
    $scope.menu = { name: "", href: "" };
    $scope.close = function() {
        $modalInstance.dismiss();
    };
    $scope.addMenu = function() {
        $modalInstance.close($scope.menu);
    };
});