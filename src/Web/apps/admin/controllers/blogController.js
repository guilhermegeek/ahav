﻿angular.module("adminApp")
    .controller('blogController', function($stateParams, $rootScope, $modal, $scope, blogService, adminService, modalService) {
        var blogId = $stateParams.blogId;

        // Init
        adminService.viewBlog(blogId)
            .success(function (response, headers, config) {
                $rootScope.setBlog(blogId);
                $scope.blogs = response.data.blogs || [];
                $scope.articles = response.data.articles || [];
            })
            .error(function(response, headers, config) {
                modalService.error(response.data.modal);
            });
        $scope.setDefault = function () {
            blogService.postBlogDefault(blogId)
                .then(function (res) {

                });
        };
        // Add Article Modal
        $scope.addArticleModal = function() {
            var dialog = $modal.open({
                templateUrl: "/Content/partials/blog-article-add.html",
                controller: "blogArticleAddController",
                resolve: {
                    blogId: function() { return $rootScope.blogActive; }
                }
            });
            dialog.result.then(function(c) {
                if (!angular.isUndefined(c)) {
                    $scope.articles.push(c);
                }
            }, function() {

            });
        };
    });