﻿angular.module("adminApp")
    .controller("modalAttachController", function($scope, $modalInstance, queue, options) {
        $scope.queue = queue;
        // We have to make sure that the Url ends with /, to add file id without having to check it on nested controllers like delete and update
        if (!options.url.endsWith("/")) {
            options.url = options.url + "/";
        }
        $scope.options = options;
        
        $scope.close = function() {
            $modalInstance.close();
        };

    }).controller('modalAttachDestroyController', function($scope, $http, serviceStackRestClient) {
        //var file = $scope.file,
        //    state;
        //if (file.url) {
        //    file.$state = function() {
        //        return state;
        //    };
        //    file.$destroy = function() {
        //        state = 'pending';
        //        return serviceStackRestClient["delete"]($scope.options.url + file.id)
        //            .success(function(response) {
        //                state = "resolved";
        //                $scope.clear(file);
        //            }).error(function(response) {
        //                state = "rejected";
        //            });

        //    };
        //} else if (!file.$cancel && !file._index) {
        //    file.$cancel = function() {
        //        $scope.clear(file);
        //    };
        //}
    });