﻿angular.module("adminApp").controller("projectEditController", function(projectId, $rootScope, $scope, $modalInstance, projectService, dialogService, utilsService) {
    var prop = ['name', 'description', 'subDomain', 'id'],
        requestDto = utilsService.createRequest(null, prop, null, null);
        
    $scope.fs = false;
    $scope.projectEdit = {};

    var saveDto = function() {
        for (var i = 0; i < prop.length; i++)
            requestDto[prop[i]] = $scope.projectEdit[prop[i]];
    };
    var setView = function () {
        var dto = utilsService.getDtoModel(requestDto, prop, null, null, null);
        for (var j = 0; j < prop.length; j++)
            $scope.projectEdit[prop[j]] = dto[prop[j]];
    };
    projectService.get(projectId)
        .success(function (res) {
            requestDto = utilsService.createRequest(res.data.project, prop, null, null);
            //var project = res.data.project;
            //$scope.projectEdit = project;
            setView();
        }).error(function(res) {
            dialogService.fail(res);
        });
        $scope.close = function() {
            $modalInstance.close();
        };
        $scope.saveProject = function() {
            saveDto();
            projectService.put($scope.projectEdit.id, requestDto)
                .success(function(res) {
                    $scope.fs = true;
                    $modalInstance.close();
                    dialogService.ok(res.data.modal);
                });
        };
    });