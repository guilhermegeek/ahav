﻿angular.module("adminApp")
.controller("projectSetVideoController", function ($rootScope, $q, $log, $scope, utilsService, dialogService, $modalInstance, serviceStackRestClient, projectService) {
    var prop = ["fileId"],
        requestDto = utilsService.getDto(null, prop, null),
        projectId = $rootScope.projectRequest;
    
    $scope.videos = [];
    $scope.video = {};
    $scope.hasVideo = false;
    $scope.videoInit = { };
    requestDto["projectId"] = projectId;
    
    // Load modal
    var p1 = projectService.viewSetVideoInit();
    p1.then(function (dto) {
        //$scope.menuValues = dto.files; // update dropdown menu
        $scope.menuValues = dto.files;
        $log.info(dto.files.length);
        $scope.videos = dto.files || [];
        $log.info("menu values: " + $scope.menuValues);
        $scope.hasVideo = dto.hasVideo;
        if($scope.hasVideo)
            $scope.video = dto.video;
    });

    var saveDto = function () {
        for (var i = 0; i < prop.length; i++) {
            requestDto[prop[i]] = $scope.videoInit[prop[i]];
        }
    };
    
    $scope.saveVideo = function (fileId) {
        saveDto();
        var p2 = projectService.postVideoInit(requestDto);
        p2.then(function(res) {
            $modalInstance.close(res.data);
        }, function(res) {

        });
    };
    $scope.close = function() {
        $modalInstance.dismiss();
    };
})