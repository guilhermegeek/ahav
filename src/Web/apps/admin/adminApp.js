var adminModule = angular.module('adminApp', ['commonApp']);
adminModule.config(function ($httpProvider, $locationProvider, $stateProvider) {
    
    // API default credentials (injected on every get response content-type:text/html)
    $httpProvider.defaults.headers.common["auth-id"] = _auth.id;
    $httpProvider.defaults.headers.common["auth-key"] = _auth.key;
    
    // Intercepts every http request to append the current project id, if any
    $httpProvider.interceptors.push("projectInterceptor");

    $stateProvider
        .state("admin", {
            url: "/admin",
            templateUrl: "/admin.html?format=bare",
            controller: "adminHomeController"
        }).state("settingsDomain", {
            url: "/admin/settings/domain",
            templateUrl: "/admin/settings/domain?format=html",
            controller: "settingsDomainController"
        }).state("apps", {
            url: "/admin/apps",
            templateUrl: "/admin/apps.html?format=html",
            controller: "applicationsController"
        }).state("appsCreate", {
            url: "/admin/apps/new",
            templateUrl: "/admin/apps/new.html",
            controller: "applicationCreateController"
        }).state("admin.project", {
            url: "/apps"
        }).state("admin.project.add", {
            url: "/new",
            onEnter: function($rootScope, $state, $modal, dialogService) {
                $modal.open({
                    templateUrl: "/admin/apps/new.html",
                    controller: "applicationCreateController"
                }).result.then(function(project) {
                    $state.transitionTo("admin");
                });
            }
        }).state("content", {
            url: "/admin/content",
            templateUrl: "/admin/content.html?format=html",
            controller: "contentController"
        }).state("contentEdit", {
            url: "/admin/content/:id/edit",
            templateUrl: function(stateParams) {
                 return "/admin/content/" + stateParams.id + "/edit.html?format=html";
            },
            controller: "contentEditController"
        }).state("blogCreate", {
            url: "/admin/blog/new",
            onEnter: function($state, $stateParams, $modal) {
                var dialog = $modal.open({
                    templateUrl: "/admin/blog/new.html?format=html",
                    controller: "blogCreateCtrl"
                });
                dialog.result.then(function(project) {
                    $state.transitionTo("admin");
                });
            }
        }).state("blogArticleEdit", {
            url: "/admin/blog/article/edit/:articleId",
            templateUrl: function(stateParams) { return "/admin/blog/article/edit/?format=html&id=" + stateParams.articleId; },
            controller: "blogArticleEditController"
        }).state("templateEdit", {
            url: "/admin/template/:templateId/edit",
            templateUrl: function (stateParam) { return "/admin/template/" + stateParam.templateId + "/edit.html?format=html"; },
            controller: "templateEditController"
        }).state("adminBlog", {
            url: "/admin/blog/:blogId",
            templateUrl: function(stateParams) { return "/admin/blog/" + stateParams.blogId + "?format=html"; },
            controller: "blogController"
        }).state("adminBlog.articlesTemplate", {
            url: "/template/articles",
            onEnter: function($state, $stateParams, $modal) {
                var dialog = $modal.open({
                    templateUrl: "/admin/blog/template/articles.html",
                    controller: "blogArticlesViewTemplateController"
                });
                dialog.result.then(function(project) {
                    $state.transitionTo("admin");
                });
            }
        }).state("attachments", {
            url: "/admin/attachments",
            views: {
                "": {
                    templateUrl: "/admin/attachments?format=html",
                    controller: "attachmentsController",
                },
                "menu": {
                    templateUrl: "admin/attachments/top-menu.html",
                    //abstract: false
                }
            },
            //abstract: false
        });
});
adminModule.run(function($rootScope, $modal, projectService, authService, dialogService, adminService, $modalStack) {
    $rootScope.$on('$stateChangeStart', function() {
        var top = $modalStack.getTop();
        if (top) {
            $modalStack.dismiss(top.key);
        }
    });
    $rootScope.projects = [];
    $rootScope.projectActive = null;
    $rootScope.blogs = [];
    $rootScope.blogActive = null;

    $rootScope.changeProject = function(projectId) {
        // watch on projectActive will change the project ui and config
        $rootScope.projectActive = projectId;
    };
    // Everytime projectActive is changed, the request headers are changed as so others dtos
    $rootScope.$watch("projectActive", function(newVal, oldVal) {
        if (newVal == oldVal) return;
        $rootScope.setProject(newVal);
    });
    $rootScope.setProject = function(id) {
        projectService.get(id)
            .success(function(response) {
                // For project interceptor
                $rootScope.appCurrent = response.data.project;
            }).error(function(response) {
                dialogService.error(response.data.responseStatus);
            });
    };
    $rootScope.setBlog = function(id) {
        $rootScope.blogRequest = id;
    };


    // Setup method for Startup Admin with remote data
    var d1 = adminService.connect();
    d1.then(function(resDto) {
        // Applications
        $rootScope.apps = resDto.apps || []; // The default apps
        if ($rootScope.apps.length > 0) {
            // Default project
            $rootScope.appCurrent = { id: $rootScope.apps[0].id };
            $rootScope.setProject($rootScope.apps[0].id);
        }
        //$rootScope.blogs = resDto.model.blogs || [];
        //if ($rootScope.blogs.length > 0) {
        //    $rootScope.rBlog = angular.isUndefined(resDto.blogDefault)
        //        ? $rootScope.blogs[0]
        //        : resDto.model.blogDefault;
        //    $rootScope.setBlog($rootScope.rBlog.id);
        //}
    });

    $rootScope.layout = {
        showLeft: false,
        showRight: false,
        rTemplate: "'rSidebar.html'",
        types: ['content', 'blog', 'article', 'newsletter'],
        single: function() {
            $rootScope.layout.showLeft = false;
            $rootScope.layout.showRight = false;
        }
    };
    // Culture Settings
    $rootScope.cultures = ['pt-PT', 'en-US'];
    $rootScope.locale = 0;
    $rootScope.activeMenu = 0;
    $rootScope.socialProviders = ['facebook', 'gplus', 'twitter', 'youtube', 'pinterest'];
    $rootScope.headTitle = "testingww";
    $rootScope.changeCulture = function(index) {
        $rootScope.locale = index;
    };
    //$rootScope.template = "";

    $rootScope.addBlogModal = function() {
        var modal = $modal.open({
            templateUrl: "/Content/partials/blog-add.html",
            controller: "blogAddController"
        });
        modal.result.then(function(blog) {
            $rootScope.blogs.push(blog);
        });
    };
    $rootScope.addProjectModal = function() {
        //var dialog = $modal.open({
        //    templateUrl: "/Content/partials/project-add.html",
        //    controller: "addProjectModalController"
        //});
        //dialog.result.then(function(project) {
        //    $rootScope.projects.push(project);
        //});
    };
    //$('.nav-tabs').tabdrop();
    //$(".filestyle").filestyle();
});