﻿// Helper directive to show available languages (ie: for dropdown in content transation)
angular.module("adminApp")
    .directive("locales", function($rootScope) {
        return {
            restrict: "EACM",
            templateUrl: 'templates/locales.html',
            link: function(scope, element, attrs) {

            }
        };
    });