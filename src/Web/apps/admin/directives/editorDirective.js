﻿// Editor directive, used by content, templates, events. It's designed to be a tab content in a tab set element
angular.module("adminApp")
    .directive("editor", function(editorService, $compile) {
        return {
            restrict: "EACM",
            //template: "<iframe>{{template}}</iframe>",
            //templateUrl: "editor-0.1.html",
            template: "<iframe id='frameEditor' src='javascript:''' frameborder='0' allowtransparency='true' style='width:100%; display:block;'></iframe>",
            scope: {
                body: "@"
            },
            controller: "editorController"
        };
    }).factory("editorService", function($compile) {
        return {
            insertImage: function(parent) {

            },
            insertRow: function (columns) {
                
            }
        };
    }).controller("editorController", function($scope, $compile, editorService, utilsService, $log) {
        $log.info("editorController");
    
        $scope.insertImage = function() {
            editorService.insertImage();
            var el = $compile("<img alt='' />")();
            element.parent().append(el);
        };

    });