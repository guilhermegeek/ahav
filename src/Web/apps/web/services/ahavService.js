﻿angular.module("ahavApp").factory("ahavService", function($q, $modal, serviceStackRestClient, $rootScope, dialogService) {
    return {
        connect: function() {
            var deferred = $q.defer();
            serviceStackRestClient.get("/api/ahav/connect")
                .success(function(res) {
                    deferred.resolve(res.data);
                }).error(function(res) {
                    dialogService.failt(res);
                });
            return deferred.promise;
        },
        getArticlesController: function() {
            var deferred = $q.defere();
            serviceStackRestClient.get("/blog")
                .success(function(res) {
                    deferred.resolve(res.data);
                }).error(function(res) {
                    deferred.reject(res);
                    dialogService.fail(res);
                });
            return deferred.promise;
        },
        getArticleController: function ($state) {
            var deferred = $q.defere();
            serviceStackRestClient.get("/blog/" + $state.articleId)
                .success(function(res) {
                    deferred.resolve(res.data);
                }).error(function(res) {
                    deferred.reject(res);
                });
            return deferred.promise;
        }
    };
});