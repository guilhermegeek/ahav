angular.module('angular-servicestack').config(function (serviceStackRestConfigProvider, $httpProvider) {

});
angular
    .module('ahavApp', ['analytics', 'angular-servicestack', 'commonApp'])
    .value('$anchorScroll', angular.noop)
    .config(function ($httpProvider, $locationProvider, $stateProvider, $urlRouterProvider) {
        $httpProvider.interceptors.push("projectInterceptor");
        $httpProvider.defaults.headers.common['X-Requested-With'] = "XMLHttpRequest";
        $urlRouterProvider.otherwise("/home");
        
        $stateProvider
        .state("content", {
            url: "/:path",
            templateUrl: function (stateParams) {
                return "/" + stateParams.path + "?format=html";
            },
            controller: "contentViewController"
        }).state("article", {
            url: "/blog",
            templateUrl: "/blog?format=html",
            controller: "articlesController"
        }).state("article.view", {
            url: "/article/:articleId",
            templateUrl: function (stateParams) {
                return "/blog/" + stateParams.articleId + "?format=html";
            },
            controller: "viewArticleController"
        });
    }).run(function ($rootScope, $timeout, ahavService) {
        // Set project id for subdomain (test porpuses)
        $rootScope.projectActive = null;
        if (!angular.isUndefined(project) && project != null) {
            $rootScope.projectActive = project;
        }
        // Set project subdomain for mocked requests
        if(!angular.isUndefined(subDomain) && subDomain != null) {
            $rootScope.subDomain = subDomain;
        }
        ahavService.connect()
            .then(function(resDto) {
                if(resDto.hasVideo) {
                    alert(resDto.hasVideo.id);
                }
            });
    }).controller('mapController', function ($scope, $timeout) {
        
        var mylat = new google.maps.LatLng(40.654776, -7.912467);
        $scope.mapOptions = {
            center: mylat,
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        $timeout(function () {
            var marker = new google.maps.Marker({
                position: mylat,
                map: $scope.mapDao,
                title: 'Hello World!'
            });
            var infowindow = new google.maps.InfoWindow({
                content: '<div style="color:#000;"><h4>Pousada de Viseu</h4><p>Morada: Rua Hospital, 3500-161 Viseu<br>Coordenadas: 40.654776, -7.912467</p></div>',
                maxWidth: '800px'
            });
            infowindow.open($scope.mapDao, marker);
        }, 3000);
    }).controller('viewTemplateController', function ($timeout, $compile, $rootScope, $route, $scope, $stateParams, contentService, $sce) {
        //var templateId = null;
        // TemplateId is sended every request. If the response content use the same template, the template dto isn't returned in response
        // In order to avoid more traffic
        contentService.getByPath($rootScope.subDomain + "/" + $stateParams.path + "?format=json")
            .success(function (response) {
                if (!response.data.reuseTemplate) {
                    $scope.renderBody = response.data.renderBody;
                    $rootScope.metaTags = response.data.metaTags;
                } else {
                    alert("reuse!");
                }
                if(!angular.isUndefined(response.data.videoInit) && response.data.videoInit != null) {
                    alert("show video!");
                }
                $scope.editLocale = 1;
            }).error(function () {
                alert('error');
            });
    }).directive("ahavContact", function ($modal, $rootScope, $modalInstance, dialogService, supportService) {
        return {
            restrict: "ECMA",
            template: "<input type=\"button\" class=\"btn\" value=\"Contacto\" >",
            link: function ($scope) {
                $scope.modalSendContact = function() {
                    $modal.open({
                        templateUrl: "sendContactModal.html",
                        controller: "sendContactModalController",
                        resolve: {
                            project: function() {
                                return project;
                            }
                        }
                    });
                };
            }
        };
    }).controller("sendContactModalController", function (dialogService, supportService) {
        var requestDto = {};
        supportService.sendContact(requestDto)
            .success(function (response) {
                dialogService.ok(response.response.data);
            }).fail(function (response) { dialogService.fail(response); });
    });       