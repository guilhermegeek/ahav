﻿angular.module("ahavApp").controller("articlesController", function($scope, $rootScope, ahavService) {
    ahavService.getArticlesController()
        .then(function(resDto) {
            $scope.title = resDto.title;
            $scope.body = resDto.body;
        });
});