﻿angular.module("ahavApp").controller('contentViewController', function ($timeout, $compile, $rootScope, $route, $scope, $stateParams, contentService, $sce) {
    contentService.getByPath($stateParams.path + "?format=json")
        .success(function (response) {
            if (!response.data.reuseTemplate) {
                $rootScope.renderBody = response.data.renderBody;
            } else {
                alert("reuse!");
            }
            $scope.editLocale = 1;
        }).error(function () {
            alert('error');
        });
})