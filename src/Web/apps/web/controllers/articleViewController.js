﻿angular.module("ahavApp").controller("viewArticleController", function($stateParams, $rootScope, $scope, blogService, dialogService) {
    var articleId = $stateParams.articleId;
    blogService.getViewArticle(articleId)
        .success(function(res) {
            $rootScope.title = res.data.title;
            $rootScope.body = res.data.body;
        }).error(function(res) {
            dialogService.fail(res);
        });
});