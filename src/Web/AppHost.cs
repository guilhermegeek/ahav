﻿using System;
using System.Net;
using System.Web;
using System.Web.Routing;
using Admin.ServiceInterface;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Validators;
using Ahav.ServiceModel;
using Ahav.ServiceModel;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Operations;
using Ahav;
using City;
using Elmah;
using Magazine.ServiceInterface;
using Magazine.ServiceModel;
using MongoDB.Bson;
using ServiceStack.Common.Web;
using ServiceStack.Razor;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.Validation;
using ServiceStack.Text;
using ServiceStack.WebHost.Endpoints;
using Store;
using Store.ServiceInterface;

namespace Web
{
    public class DumbA {}
    public class AppHost : AppHostBase
    {
        public AppHost()
            : base("Web", typeof (DumbA).Assembly)
        {

        }

        public override void Configure(Funq.Container container)
        {
            JsConfig.EmitCamelCaseNames = true;
            var config = new EndpointHostConfig
                              {
                                  DebugMode = true,
                                  AllowFileExtensions = {"less", "cshtml", "css", "map"},
                                  GlobalResponseHeaders =
                                      {
                                          //{"Access-Control-Allow-Origin", "http://localhost"},
                                          {"Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS"},
                                          {"Access-Control-Allow-Headers", "Content-Type"}
                                      },
                              };
            config.RawHttpHandlers.Add(req =>
                                           {
                                               var uri = new Uri(req.AbsoluteUri);
                                               // Base App
                                               if (uri.Host == "ahav.pt")
                                               {
                                                   req.Items[AhavExtensions.HttpItemIsBaseApp] = JsonSerializer.SerializeToString(true);
                                               }
                                               // Others Apps

                                               //if (!req.IsAuthenticated())
                                               //    throw HttpError.Unauthorized(req.AbsoluteUri);

                                               var projectRepo = req.TryResolve<ApplicationAhavRepository>();

                                               ObjectId id = GetCredentialProjectFromRequest(req);
                                               if (!id.IsEmpty())
                                               {
                                                   // @TODO CHECK IF IS OWNER
                                                   req.SetProjectId(id);
                                               }
                                               else
                                               {
                                                   id = projectRepo.GetProjectIdBySubdomain(uri.Host);
                                                   if (!id.IsEmpty())
                                                       req.SetProjectId(id);
                                               }

                                               // Resolve default root
                                               if (string.IsNullOrEmpty(req.PathInfo) || req.PathInfo == "/")
                                               {
                                                   var repo = TryResolve<ApplicationAhavRepository>();
                                                   var model = repo.GetRouteModel(id);
                                                   switch (model.Default)
                                                   {
                                                           case RouteA.BlogIndex:
                                                           HttpContext.Current.RewritePath(string.Format(MagazineRoutes.GetArticlesView));
                                                           break;
                                                       case RouteA.Content:
                                                          HttpContext.Current.RewritePath(string.Format("/blog/{0}", model.DefaultId));
                                                           break;
                                                   }
                                                   return null;
                                               }
                                               var crawl = req.QueryString["_escaped_fragment_"];
                                               if(crawl != null)
                                               {
                                                   HttpContext.Current.RewritePath(string.Format("/snapshots/{0}", crawl));
                                               }

                                               return null;
                                           });
            SetConfig(config);
            Plugins.RemoveAll(x => x is AuthFeature);
            Plugins.Add(new ValidationFeature());
            Plugins.Add(new RazorFormat());
            Plugins.Add(new AhavPlugin());
            Plugins.Add(new CityPlugin());
            Plugins.Add(new MagazinePlugin());
            Plugins.Add(new StorePlugin());
            Plugins.Add(new AdminPlugin());
            container.RegisterValidators(typeof(ChangePasswordValidator).Assembly, typeof(BasicInfoValidator).Assembly); // ServiceModel
            Plugins.Add(new ValidationFeature());
        }
        private static ObjectId GetCredentialProjectFromRequest(IHttpRequest req)
        {
            ObjectId projectId;
            if (req.Headers[ProjectExtensions.HttpHeaderProjectId] != null &&
                !String.IsNullOrEmpty(req.Headers[ProjectExtensions.HttpHeaderProjectId]))
            {
                if (ObjectId.TryParse(req.Headers[ProjectExtensions.HttpHeaderProjectId].ToString(), out projectId))
                {
                    return projectId;
                }
            }
            if (req.QueryString[ProjectExtensions.HttpQueryProjectId] != null &&
                !String.IsNullOrEmpty(req.QueryString[ProjectExtensions.HttpQueryProjectId].ToString()))
            {
                if (ObjectId.TryParse(req.QueryString[ProjectExtensions.HttpQueryProjectId].ToString(), out projectId))
                {
                    return projectId;
                }
            }
            return ObjectId.Empty;
        }
    }
}