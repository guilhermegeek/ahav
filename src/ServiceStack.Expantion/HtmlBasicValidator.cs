﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.Expantion.Interfaces;

namespace ServiceStack.Expantion
{
    /// <summary>
    /// Basic validation for <tags></tags> 
    /// Checks for opening tags, then the closure
    /// </summary>
    /// http://www.dotnetperls.com/html-brackets
    public class HtmlBasicValidator : IHtmlBasicValidator
    {
        public bool ValidateHtml(string html)
        {
            // False = SmallerThan
            // True = GreaterThan

            bool expected = false; // Must start with < [Smaller Than]
            for (int i = 0; i < html.Length; i++) // Loop
            {
                // Letter.
                char letter = html[i];

                // Common case.
                if (letter != '>' &&
                    letter != '<')
                {
                    continue;
                }

                // False = SmallerThan [<]
                // True = GreaterThan [>]
                bool found = letter == '>';

                // If we found what we expected, expect the opposite next.
                if (found == expected)
                {
                    expected = !expected;
                }
                else
                {
                    // Disallow.
                    return false;
                }
            }
            // Return true if expected is false [we expect < SmallerThan]
            return !expected;
        }
    }
}
