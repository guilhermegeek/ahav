﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceStack.Expantion.Interfaces
{
    public interface IHtmlBasicValidator
    {
        bool ValidateHtml(string html);
    }
}
