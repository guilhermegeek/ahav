﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;
using Store.ServiceModel.Domain.Entities;

namespace Store.ServiceModel.BusinessDtos
{
    public class CreateProduct : IReturn<CreateProductResponse>
    {
        public string Name { get; set; }
        public ObjectId CategoriesId { get; set; }
    }
    public class CreateProductResponse : IHasResponseStatus
    {
        public ProductDto Product { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
