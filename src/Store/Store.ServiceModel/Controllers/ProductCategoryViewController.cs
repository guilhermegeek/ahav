﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;
using Store.ServiceModel.Domain;
using Store.ServiceModel.Domain.Entities;

namespace Store.ServiceModel.Controllers
{
    public class ProductCategoryViewController
    {
        public ObjectId CategoryId { get; set; }
    }

    public class ProductCategoryViewControllerResponse : IHasResponseStatus
    {
        public ProductInfo[] Products { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
