﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;
using Store.ServiceModel.Domain;
using Store.ServiceModel.Domain.Entities;

namespace Store.ServiceModel.Controllers
{
    public class ProductViewController : IReturn<ProductViewControllerResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class ProductViewControllerResponse : IHasResponseStatus
    {
        public ProductDto Product { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
