﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson;
using Store.ServiceModel.Domain;

namespace Store.ServiceModel.Domain.Entities
{
    public class ProductDto
    {
        public ProductDto()
        {
            Comments = new List<NComment>();
            CategoriesIds = new List<ObjectId>();
        }
        public ObjectId Id { get; set; }
        public List<ObjectId> CategoriesIds { get; set; }
        public decimal Price { get; set; }
        public Iventory Iventory { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public List<NComment> Comments { get; set; }
        public string[] Tags { get; set; }
        public ProductVariation[] Variations { get; set; }
        public TimeSpan ShippingIn { get; set; }
        public Dictionary<string, decimal> ShipsFrom { get; set; }

        /// <summary>
        /// The object id of the post reference in social networks like Facebook 
        /// </summary>
        //public Dictionary<SocialProvider, object> SocialObjectId { get; set; } 
    }
}
