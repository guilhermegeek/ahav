﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using Store.ServiceModel.Domain;

namespace Store.ServiceModel.Dtos
{
    public class ShoppingCartDto
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public float Price { get; set; }
        public ShoppingCartItem[] Items { get; set; }
        
    }
}
