﻿using System;

namespace Store.ServiceModel.Dtos
{
    public class CouponDto
    {
        public string Name { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public decimal Disccount { get; set; }
        public bool CanBeReused { get; set; }
        public bool IsActive { get; set; }
    }
}
