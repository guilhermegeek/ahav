﻿using MongoDB.Bson;

namespace Store.ServiceModel.Dtos
{
    
    public class ProductCategoryDto
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
    }
    public class ProductCategoryInfo
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
    }
}
