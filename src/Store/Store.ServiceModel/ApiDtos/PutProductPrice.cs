﻿using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Store.ServiceModel.ApiDtos
{
    public class PutProductPrice : IReturn<PutProductPriceResponse>
    {
        public ObjectId Id { get; set; }
        public decimal Price { get; set; }
    }
    public class PutProductPriceResponse
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
