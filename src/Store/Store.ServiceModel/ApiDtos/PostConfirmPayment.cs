﻿using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Store.ServiceModel.ApiDtos
{
    public class PostConfirmPayment : IReturn<PostConfirmPaymentResponse>
    {
        public ObjectId OrderId { get; set; }
        public string Observations { get; set; }
    }
    public class PostConfirmPaymentResponse : IHasResponseStatus, IHasResponseModal
    {
        public bool Result { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
