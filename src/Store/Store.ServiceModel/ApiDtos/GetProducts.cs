﻿using System.Collections.Generic;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;
using Store.ServiceModel.Domain;
using Store.ServiceModel.Domain.Entities;

namespace Store.ServiceModel.ApiDtos
{
    public class GetProducts : IReturn<GetProductsResponse>
    {
        public int Skip { get; set; }
        public int Take { get; set; }
        public ProductOrderBy? OrderBy { get; set; }
        public ObjectId[] Categories { get; set; }
        public decimal? MinimumPrice { get; set; }
        public decimal? MaximumPrice { get; set; }
    }

    public class GetProductsResponse : IHasResponseStatus
    {
        public GetProductsResponse()
        {
            Products = new List<ProductInfo>();
        }
        public IList<ProductInfo> Products { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
