﻿using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;
using Store.ServiceModel.Domain;
using Store.ServiceModel.Domain.Entities;

namespace Store.ServiceModel.ApiDtos
{
    public class GetProduct : IReturn<GetProductResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class GetProductResponse : IHasResponseStatus
    {
        public ProductDto Product { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
