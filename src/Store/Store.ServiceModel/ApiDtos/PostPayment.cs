﻿using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Store.ServiceModel.ApiDtos
{
    /// <summary>
    /// Restricted to certain users
    /// Enables a order paymment to be confirmed, observation should be required
    /// </summary>
    public class PostPayment : IReturn<PostPaymentResponse>
    {
        public string Observations { get; set; }
        public ObjectId OrderId { get; set; }
    }
    public class PostPaymentResponse : IHasResponseStatus, IHasResponseModal
    {
        public bool Result { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
