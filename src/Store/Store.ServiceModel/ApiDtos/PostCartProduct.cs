﻿using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Store.ServiceModel.ApiDtos
{
    public class PostCartProduct : IReturn<PostProductResponse>
    {
        /// <summary>
        /// Product Id
        /// </summary>
        public ObjectId ProductId { get; set; }
        public int Quantity { get; set; }
    }

    public class PostCartProductResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
