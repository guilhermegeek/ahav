﻿using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Store.ServiceModel.ApiDtos
{
    public class DeleteCouponCart : IReturn<DeleteCouponResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class DeleteCouponCartResponse
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
