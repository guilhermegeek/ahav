﻿using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Store.ServiceModel.ApiDtos
{
    public class DeleteProductCart : IReturn<DeleteProductCartResponse>
    {
        public ObjectId Id { get; set; }
        /// <summary>
        /// </summary>
        public int Quantity { get; set; }
    }

    public class DeleteProductCartResponse
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
