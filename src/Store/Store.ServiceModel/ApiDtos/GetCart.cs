﻿using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;
using Store.ServiceModel.Domain;
using Store.ServiceModel.Domain.Entities;

namespace Store.ServiceModel.ApiDtos
{
    public class GetCart : IReturn<GetCartResponse>
    {

    }
    public class GetCartResponse : IHasResponseStatus
    {
        /// <summary>
        /// Product, Quantity
        /// </summary>
        public ShoppingCart Cart { get; set; }

        public ResponseStatus ResponseStatus { get; set; }
    }
}
