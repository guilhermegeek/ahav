﻿using System.Collections.Generic;
using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Store.ServiceModel.ApiDtos
{
    public class PostOrderFromCart : IReturn<PostOrderFromCartResponse>
    {
        public ObjectId CartId { get; set; }
        /// <summary>
        /// ProductId, Quantity
        /// </summary>
        public Dictionary<ObjectId, int> Products { get; set; }

        public string[] Coupons { get; set; }
    }
    public class PostOrderFromCartResponse : IHasResponseStatus, IHasResponseModal
    {
        public ObjectId OrderId { get; set; }
        public Payment[] PaymmentsAvailable { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}