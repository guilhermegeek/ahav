﻿using System;
using System.Collections.Generic;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;
using Store.ServiceModel.Domain;
using Store.ServiceModel.Dtos;

namespace Store.ServiceModel.ApiDtos
{
    public class GetCoupons : IReturn<GetCouponsResponse>
    {
        public string CodeSearch { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
    }
    public class GetCouponsResponse
    {
        public IEnumerable<CouponDto> Results { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
