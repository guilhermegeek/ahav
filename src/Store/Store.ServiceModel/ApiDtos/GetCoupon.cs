﻿using System.Collections.Generic;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using Store.ServiceModel.Domain;
using Store.ServiceModel.Dtos;

namespace Store.ServiceModel.ApiDtos
{
    public class GetCoupon
    {
        public ObjectId Id { get; set; }
    }
    public class GetCouponResponse
    {
        public IEnumerable<UserInfo> UsersThatUsedCoupon { get; set; }
        public CouponDto Coupon { get; set; }
    }
}
