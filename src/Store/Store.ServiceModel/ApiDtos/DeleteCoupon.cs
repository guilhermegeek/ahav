﻿using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Store.ServiceModel.ApiDtos
{
    public class DeleteCoupon : IReturn<DeleteCouponResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class DeleteCouponResponse
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
