﻿using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Store.ServiceModel.ApiDtos
{
    /// <summary>
    /// Confirm if order is available to Paypal first
    /// </summary>
    public class GetPaymmentPaypal : IReturn<GetPaymmentPaypalResponse>
    {
        public ObjectId OrderId { get; set; }
    }
    public class GetPaymmentPaypalResponse : IHasResponseStatus
    {
        public decimal Ammount { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
