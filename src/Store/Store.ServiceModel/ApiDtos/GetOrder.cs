﻿using System.Collections.Generic;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;
using Store.ServiceModel.Domain;
using Store.ServiceModel.Domain.Entities;

namespace Store.ServiceModel.ApiDtos
{
    public class GetOrder : IReturn<GetOrderResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class GetOrderResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public IList<ProductInfo> Products { get; set; }
        public decimal Total { get; set; }
    }
}
