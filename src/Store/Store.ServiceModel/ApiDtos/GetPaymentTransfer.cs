﻿using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Store.ServiceModel.ApiDtos
{
    public class GetPaymentTransfer : IReturn<GetPaymentTransferResponse>
    {
        public ObjectId OrderId { get; set; }
    }
    public class GetPaymentTransferResponse : IHasResponseStatus
    {
        public string Bank { get; set; }
        public string Nib { get; set; }
        public string Iban { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
