﻿using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;
using Store.ServiceModel.Domain;
using Store.ServiceModel.Dtos;

namespace Store.ServiceModel.ApiDtos
{
    public class PostCouponCart : IReturn<PostCouponCartResponse>
    {
        public string Coupon { get; set; }
    }
    public class PostCouponCartResponse : IHasResponseStatus, IHasResponseModal
    {
        public CouponDto Coupon { get; set; }
        /// <summary>
        /// Updated cart with coupon applied
        /// </summary>
        public ShoppingCartDto Cart { get; set; }

        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
