﻿using System.Collections.Generic;
using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using ServiceStack.FluentValidation;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;
using Store.ServiceModel.Domain;
using Store.ServiceModel.Dtos;

namespace Store.ServiceModel.ApiDtos
{
    public class PostProductCategory : IReturn<PostProductCategoryResponse>
    {
        public Dictionary<string, string> Name { get; set; }
    }
    public class PostProductCategoryValidator : AbstractValidator<PostProductCategory>
    {
        public PostProductCategoryValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
        }
    }
    public class PostProductCategoryResponse : IHasResponseStatus, IHasResponseModal
    {
        public ProductCategoryDto Category { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
