﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;
using Store.ServiceModel.Domain;
using Store.ServiceModel.Domain.Entities;

namespace Store.ServiceModel.ApiDtos
{
    public class PostProduct : IReturn<PostProductResponse>
    {

        public PostProduct()
        {
            Categories = new List<ObjectId>();
        }
        public List<ObjectId> Categories { get; set; }
        /// <summary>
        /// Descriptive titles are best. Try to describe your item the way a shopper would.
        /// </summary>
        public Dictionary<string, string> Title { get; set; }
        /// <summary>
        /// Try to answer the questions shoppers will have. Tell the item's story and explain why it's special.
        /// </summary>
        public Dictionary<string, string> Description { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        /// <summary>
        /// What keywords will shoppers type into search to find your item?
        /// Tags help shoppers find your item when they search on Etsy. Add 13 tags to reach as many Etsy shoppers as possible.
        /// </summary>
        public string[] Tags { get; set; }
        /// <summary>
        /// Add Variations to your listing to highlight available options for shoppers. 
        /// Once you add Variations, a buyer will be required to select an option before purchase. 
        /// You'll be able to view the options they've selected on your Sold Orders, Receipts, and transaction emails. 
        /// Properties and options are not incorporated into Search.
        /// </summary>
        public ProductVariation[] Variations { get; set; }
        /// <summary>
        /// 1 business day, 2, etc on select (convert to timespan)
        /// Processing time is the estimated time between an item's purchase date and when it will be shipped. If the item is made-to-order, include the time required to make it. Processing times can be changed at any time for current listings. Note that if an order is running late, you can edit the processing time once, and the buyer will be notified.
        /// </summary>
        public TimeSpan ShippingIn { get; set; }
        /// <summary>
        /// Country, Cost
        /// </summary>
        public Dictionary<string, decimal> ShipsFrom { get; set; }
    }
    public class PostProductResponse : IHasResponseStatus
    {
        public ProductDto Product { get; set; }
        public string ProductUri { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
