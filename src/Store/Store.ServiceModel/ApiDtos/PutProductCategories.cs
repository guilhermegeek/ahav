﻿using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Store.ServiceModel.ApiDtos
{
    /// <summary>
    /// Update product categories
    /// </summary>
    public class PutProductCategories : IReturn<PutProductCategoriesResponse>
    {
        public ObjectId Id { get; set; }
        public ObjectId[] Categories { get; set; }
    }
    
    public class PutProductCategoriesResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
