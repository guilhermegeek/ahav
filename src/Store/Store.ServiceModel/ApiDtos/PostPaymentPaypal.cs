﻿using MongoDB.Bson;
using ServiceStack.ServiceHost;

namespace Store.ServiceModel.ApiDtos
{
    public class PostPaymentPaypal : IReturn<PostPaymentPaypalResponse>
    {
        public ObjectId OrderId { get; set; }
        public string PaypalAccount { get; set; }
        public string PaypalToken { get; set; }
        public decimal Ammount { get; set; }
    }
    public class PostPaymentPaypalResponse
    {
        public bool Result { get; set; }
    }

}
