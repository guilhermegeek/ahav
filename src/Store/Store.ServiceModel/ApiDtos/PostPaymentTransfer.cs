﻿using System;
using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Store.ServiceModel.ApiDtos
{
    public class PostPaymentTransfer : IReturn<PostPaymentTransferResponse>
    {
        public ObjectId OrderId { get; set; }
        public decimal Ammount { get; set; }
        public DateTime TransferedAt { get; set; }
        public string Nib { get; set; }
        public string AccountOwner { get; set; }
    }
    public class PostPaymentTransferResponse : IHasResponseStatus, IHasResponseModal
    {
        public bool Result { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
