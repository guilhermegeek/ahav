﻿using System.Collections.Generic;
using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Store.ServiceModel.ApiDtos
{
    /// <summary>
    /// Updates a category
    /// </summary>
    public class PutProductCategory : IReturn<PutProductCategoryResponse>
    {
        public ObjectId Id { get; set; }
        public Dictionary<string, string> Name { get; set; }
    }
    public class PutProductCategoryResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
