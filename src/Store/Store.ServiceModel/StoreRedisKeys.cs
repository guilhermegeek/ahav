﻿using MongoDB.Bson;

namespace Store.ServiceModel
{
    public static class StoreRedisKeys
    {
        public const string RecentProductsPriceLowed = "urn:Products:PriceLowed";
        public const string AllCategories = "urn:Categories";

        public static string ProductInfo(ObjectId id)
        {
            return string.Format("product::{0}::info", id);
        }
        public static string AllProductsInCategory(ObjectId id)
        {
            return string.Format("urn:Category:{0}:products", id);
        }

        
    }
}
