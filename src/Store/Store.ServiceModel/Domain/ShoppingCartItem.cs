﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Domain;
using MongoDB.Bson;

namespace Store.ServiceModel.Domain
{
    public class ShoppingCartItem
    {
        public ObjectId Id { get; set; }
        public int Quantity { get; set; }
        public string Title { get; set; }
        public Pricing Pricing { get; set; }
    }
}
