﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.ServiceModel.Domain
{
    /// <summary>
    /// Ship dimensions aren't the same as Product variation. A product hay have 30cm cubic, but the box may be 1 meter cuic
    /// </summary>
    public class ProductShippingDimensions
    {
        public decimal Width { get; set; }
        public decimal Height { get; set; }
        public decimal Depth { get; set; }
    }
}
