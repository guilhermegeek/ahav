﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using Store.ServiceModel.Dtos;

namespace Store.ServiceModel.Domain
{
    public class IventoryItemCarted 
    {
        
        public int Quantity { get; set; }
        public ObjectId CartId { get; set; }
        public DateTime Timespan { get; set; }
        public ObjectId Id { get; set; }
        public decimal Price { get; set; }
        public string Title { get; set; }

    }
}
