﻿namespace Store.ServiceModel.Domain
{
    public enum IventoryHistoryEvent
    {
        Added = 1,
        Selled = 2,
        ReceivedFromOrderReturned = 3,
        SentedForMaitanence = 4,
        ReceivedFromMaitence = 5,
        Destroyed = 6
    }
}
