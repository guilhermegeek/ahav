﻿using System;
using MongoDB.Bson;

namespace Store.ServiceModel.Domain.Entities
{
    public class ShoppingCart
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public float Price { get; set; }
        public DateTime LastModified { get; set; }
        public ShoppingCartStatus Status { get; set; }
        public ShoppingCartItem[] Items { get; set; }
    }
}
