﻿using System.Collections.Generic;
using MongoDB.Bson;

namespace Store.ServiceModel.Domain.Entities
{
    public class ProductCategory
    {
        public ProductCategory()
        {
            ProductIds = new List<ObjectId>();
        }
        public ObjectId Id { get; set; }
        public Dictionary<string, string> Name { get; set; }
        public IList<ObjectId> ProductIds { get; set; }
        ///public ProductInfo[] Products { get; set; } 
    }
}
