﻿using System;
using System.Collections.Generic;
using Ahav.Interfaces;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson;

namespace Store.ServiceModel.Domain.Entities
{
    /// <summary>
    /// Represents an Offer
    /// </summary>
    public class Product : IThink
    {
        public Product()
        {
            Comments = new List<NComment>();
            CategoriesIds = new List<ObjectId>();
        }
        public ObjectId Id { get; set; }
        public Pricing Price { get; set; }

        public List<ObjectId> CategoriesIds { get; set; }
        public string Name { get; set; }
        
        /// <summary>
        ///  Title
        /// </summary>
        public Dictionary<string, string> Title { get; set; }
        /// <summary>
        /// Product main information
        /// </summary>
        public Dictionary<string, string> Description { get; set; }
        public List<NComment> Comments { get; set; }
        /// <summary>
        /// Tags for the product
        /// </summary>
        public string[] Tags { get; set; }
        /// <summary>
        /// Varitations for product [width : 100, color: "red"]
        /// </summary>
        public Dictionary<ProductVariation, object> Variations { get; set; }
        /// <summary>
        /// TimeSpan indicating how many time the store needs to ship the product
        /// </summary>
        public TimeSpan ShippingIn { get; set; }
        /// <summary>
        /// Ships From
        /// </summary>
        public Dictionary<string, decimal> ShipsFrom { get; set; }
        public string[] Audience { get; set; }
    }
}
