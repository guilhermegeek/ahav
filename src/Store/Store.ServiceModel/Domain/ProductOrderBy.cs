﻿namespace Store.ServiceModel.Domain
{
    public enum ProductOrderBy
    {
        TitleAsc = 1, 
        TitleDesc = 2, 
        PriceAsc = 3, 
        PriceDesc = 4
    }
}
