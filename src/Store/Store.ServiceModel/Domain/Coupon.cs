﻿using System;
using MongoDB.Bson;

namespace Store.ServiceModel.Domain
{
    public class Coupon
    {
        public ObjectId Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public decimal Disccount { get; set; }
        public bool CanBeReused { get; set; }
        public bool IsActive { get; set; }
    }
}
