﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;

namespace Store.ServiceModel.Domain
{
    public class Iventory
    {
        public Iventory()
        {
            History = new List<IventoryHistory>();
        }
        public ObjectId Id { get; set; }
        public ObjectId ProductId { get; set; }
        public long Quantity { get; set; }
        public IventoryItemCarted[] Carted { get; set; }
        public List<IventoryHistory> History { get; set; }
    }
}
