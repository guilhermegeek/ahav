﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace Store.ServiceModel.Domain
{
    /// <summary>
    /// Product information is implement on general arrays of embed documments, like ShoppingCartItems, ProductCategory
    /// </summary>
    public interface IProductInfo
    {
        ObjectId Id { get; set; }
        string Title { get; set; }
        float Price { get; set; }
    }
    public class ProductInfo : IProductInfo
    {
        public ObjectId Id { get; set; }
        public string Title { get; set; }
        public float Price { get; set; }
    }
}
