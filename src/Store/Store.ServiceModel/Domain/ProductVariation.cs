﻿namespace Store.ServiceModel.Domain
{
    public enum ProductVariation
    {
        Weight = 1,
        WeightGrams = 2,
        WeightPounds = 3,
        WeightOunces = 4,
        WeightKilograms = 5
    }
}
