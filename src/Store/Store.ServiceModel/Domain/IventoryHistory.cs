﻿using System;

namespace Store.ServiceModel.Domain
{
    public class IventoryHistory
    {
        public object ProductId { get; set; }
        public IventoryHistoryEvent Event { get; set; }
        public string Observation { get; set; }
        public DateTime CreatedAt { get; set; }
        public object CreatedBy { get; set; }
    }
}
