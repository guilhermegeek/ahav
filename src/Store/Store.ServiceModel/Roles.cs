﻿namespace Store.ServiceModel
{
    public static class EcommerceRoles
    {
        public const string ShopOwner = "shopowner";
        public const string ShopSupport = "shopsupport";
        public const string ShopSales = "shopsales";
        public const string ProductManager = "productmanager";
        public const string OrderManager = "ordermanager";
    }
}
