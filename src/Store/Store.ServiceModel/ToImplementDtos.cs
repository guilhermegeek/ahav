﻿using Ahav.ServiceModel.Common;

namespace Store.ServiceModel
{
    public class ShippingRate
    {
        public string Description { get; set; }
        public decimal FromKg { get; set; }
        public decimal ToKg { get; set; }
    }
    public class Taxes
    {
        public Country Country { get; set; }
        public decimal TaxRate { get; set; }
    }
}
