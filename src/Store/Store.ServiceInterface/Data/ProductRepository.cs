﻿using System.Collections.Generic;
using System.Linq;
using Ahav.Infrastructure;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using Store.ServiceInterface.Extensions;
using Store.ServiceModel;
using Store.ServiceModel.Domain;
using Store.ServiceModel.Domain.Entities;
using Store.ServiceModel.Dtos;


namespace Store.ServiceInterface.Data
{
    /// <summary>
    /// Repository implementing Redis and MongoDB
    /// Product and Product Category
    /// </summary>
    public class ProductRepository : Repository<Product>
    {
        public static IMongoFields ProductInfoFields = Fields<Product>.Include(x => x.Id, x => x.Title, x => x.Price);
        public static IMongoFields ProductDtoFields = Fields<Product>.Include(x => x.Id, x => x.Title, x => x.Description,
                                                                              x => x.CategoriesIds, x => x.Price, x => x.Tags);
        public ProductCategoryRepository ProductCategoryRepo { get; set; }

        private MongoCollection productCollection;
        private MongoCollection productCategoryCollection;

        public ProductRepository()
        {
            this.productCollection = MongoDbHelper.Instance.Database.GetCollection(typeof(Product).Name);
            this.productCategoryCollection = MongoDbHelper.Instance.Database.GetCollection(typeof(ProductCategory).Name);
        }

        
        public void AddIdsToCategory(ObjectId categoryId, ObjectId[] produtsId)
        {
            ProductCategoryRepo.Save(categoryId,
                Update<ProductCategory>.PushAll(x => x.ProductIds, produtsId));
        }
        public void StoreProduct(Product product, ProductInfo productInfo)
        {
            collection.Save(product);
            productInfo.Id = product.Id;
            using(var client = RedisManager.GetClient())
            {
                client.Set(StoreRedisKeys.ProductInfo(product.Id), productInfo);
            }
            foreach (var category in product.CategoriesIds)
            {
            
            }
        }
        public IEnumerable<ProductInfo> GetProducts(ObjectId[] categoriesIds, decimal? minimumPrice, decimal? maximumPrice, int skip, int take)
        {
            var queries = new List<IMongoQuery>();
            List<ProductInfo> productsInfo = new List<ProductInfo>();
            List<Product> products;
            if (categoriesIds != null && categoriesIds.Count() > 0)
            {
                var ids = new List<ObjectId>();
                categoriesIds.ToList().ForEach(x => ids.AddRange(ProductCategoryRepo.Get(x).ProductIds));
                queries.Add(Query<Product>.In(x => x.Id, ids));
            }
            if (minimumPrice.HasValue)
                queries.Add(Query<Product>.GT(x => x.Price.List, minimumPrice.Value));
            if (maximumPrice.HasValue)
                queries.Add(Query<Product>.LT(x => x.Price.List, maximumPrice.Value));

            if (queries.Any())

                products = collection.FindAs<Product>(Query.And(queries))
                    .SetSkip(skip)
                    .SetLimit(take)
                    .SetFields(ProductInfoFields).ToList();
            else
                products = collection.FindAllAs<Product>()
                    .SetSkip(skip)
                    .SetLimit(take)
                    .SetFields(ProductInfoFields).ToList();
            products.ForEach(x => productsInfo.Add(x.ToInfo()));
            return productsInfo;
        }
        public ProductDto GetProduct(ObjectId id)
        {
            var product = collection.FindAs<Product>(Query<Product>.EQ(x => x.Id, id)).SetFields(ProductDtoFields).FirstOrDefault();
            return product.ToDto();
        }
        public ProductCategoryDto GetCategory(ObjectId categoryId)
        {
            return null;
            //return productCategoryCollection.findo
        }

        public static void RegisterMappings()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(Product)))
            {
                BsonClassMap.RegisterClassMap<Product>(cm =>
                {
                    cm.MapIdField(x => x.Id).SetIdGenerator(ObjectIdGenerator.Instance);
                    cm.MapMember(x => x.Price).SetIsRequired(true);
                    cm.MapMember(x => x.CategoriesIds);
                    cm.MapMember(x => x.Title);
                    cm.MapMember(x => x.Description);
                    cm.MapProperty(x => x.Tags);
                });
            }
            if (!BsonClassMap.IsClassMapRegistered(typeof(ProductCategory)))
            {
                BsonClassMap.RegisterClassMap<ProductCategory>(cm =>
                {
                    cm.MapIdProperty(x => x.Id).SetIdGenerator(ObjectIdGenerator.Instance);
                    cm.MapMember(x => x.Name).SetIsRequired(true);
                    cm.MapMember(x => x.ProductIds);
                });
            }
            if (!BsonClassMap.IsClassMapRegistered(typeof(Iventory)))
            {
                BsonClassMap.RegisterClassMap<Iventory>(cm =>
                {
                    cm.MapIdProperty(x => x.ProductId).SetIdGenerator(ObjectIdGenerator.Instance);
                    cm.MapMember(x => x.Quantity);
                    cm.MapMember(x => x.History);
                });
            }
            if (!BsonClassMap.IsClassMapRegistered(typeof(Coupon)))
            {
                BsonClassMap.RegisterClassMap<Coupon>(cm =>
                {
                    cm.MapIdProperty(x => x.Id).SetIdGenerator(ObjectIdGenerator.Instance);
                    cm.MapProperty(x => x.CanBeReused);
                    cm.MapProperty(x => x.Disccount);
                    cm.MapProperty(x => x.Start);
                    cm.MapProperty(x => x.End);
                    cm.MapProperty(x => x.Name);
                    cm.MapProperty(x => x.IsActive);
                });
            }

        }
    }
    public class ProductCategoryRepository : Repository<ProductCategory>
    {
    }
}
