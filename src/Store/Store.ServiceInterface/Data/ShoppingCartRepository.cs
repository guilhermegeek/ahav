﻿using System;
using Ahav.Infrastructure;
using MongoDB.Bson;
using MongoDB.Driver;
using Store.ServiceModel.Domain;
using Store.ServiceModel.Domain.Entities;

namespace Store.ServiceInterface.Data
{
    public class ShoppingCartRepository : Repository<ShoppingCart>
    {
        private MongoCollection cartColl = MongoDbHelper.Instance.Database.GetCollection<ShoppingCart>(typeof (ShoppingCart).Name);

        public WriteConcernResult AddProductCart(ObjectId userId, ObjectId productId, int quantity)
        {
            var cartProduct = new ShoppingCartItem
            {
                Id = productId,
                Quantity = quantity,
            };
            //var user = Get(userId);
            //if (user.Products.Any(x => x.Id == cartProduct.Id))
            //    return
            //        Save(Query.And(Query.EQ("_id", userId), Query.EQ("Products._id", productId)),
            //             Update.Inc("Products.$.Quantity", quantity));
            //return Save(userId, Update<User>.Push(x => x.Products, cartProduct));
            throw new NotImplementedException();
        }
        public WriteConcernResult DeleteProductCart(ObjectId userId, ObjectId productId, int quantity)
        {
            //var product = collection.FindOneByIdAs<User>(userId).Products.SingleOrDefault(x => x.Id == productId);
            //if (product == null)
            //{
            //    throw new KeyNotFoundException();
            //}

            //if (product.Quantity <= quantity) // delete
            //    return collection.Update(Query.And(Query.EQ("_id", userId), Query.EQ("Products._id", productId)),
            //                             Update.Pull("Products.$._id", productId)
            //        );
            //return collection.Update(Query.And(Query.EQ("_id", userId), Query.EQ("Products._id", productId)),
            //                         Update.Inc("Products.$.Quantity", -quantity)
            //    );
            throw new NotImplementedException();
        }
        public ShoppingCart GetCart(ObjectId id)
        {
            return cartColl.FindOneByIdAs<ShoppingCart>(id);
        }
    }
}
