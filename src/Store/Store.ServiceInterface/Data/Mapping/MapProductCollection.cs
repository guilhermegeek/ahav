﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using Store.ServiceModel.Domain.Entities;

namespace Store.ServiceInterface.Data.Mapping
{
    public static class MapProductCollection
    {
        public static void Register()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof (Product)))
            {
                BsonClassMap.RegisterClassMap<Product>(m =>
                                                       {
                                                           m.MapIdProperty(x => x.Id).SetIdGenerator(ObjectIdGenerator.Instance);
                                                           m.MapProperty(x => x.Title);
                                                           m.MapProperty(x => x.Name);
                                                           m.MapProperty(x => x.Description);
                                                           m.MapProperty(x => x.Price).SetIsRequired(true);
                                                           m.MapProperty(x => x.ShippingIn);
                                                           m.MapProperty(x => x.ShipsFrom);
                                                           m.MapProperty(x => x.Tags);
                                                           m.MapProperty(x => x.Variations);
                                                           m.MapProperty(x => x.CategoriesIds);
                                                           m.MapProperty(x => x.Audience);
                                                       });
            }
        }
    }
}
