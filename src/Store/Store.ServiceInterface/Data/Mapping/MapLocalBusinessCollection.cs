﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson.Serialization;

namespace Store.ServiceInterface.Data.Mapping
{
    public static class MapLocalBusinessCollection 
    {
        public static void Register()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof (LocalBusiness)))
            {
                BsonClassMap.RegisterClassMap<LocalBusiness>(cm =>
                                                             {
                                                                 cm.MapProperty(x => x.Organization);
                                                                 cm.MapProperty(x => x.BusinessType);

                                                             });
            }
        }
    }
}
