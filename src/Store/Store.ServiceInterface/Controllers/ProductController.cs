﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Infrastructure;
using ServiceStack.Common.Web;
using ServiceStack.ServiceInterface;
using Store.ServiceModel.Controllers;

namespace Store.ServiceInterface.Controllers
{
    public class ProductController : Service
    {
        public object Get(ProductViewController request)
        {
            var response = new ProductViewControllerResponse();
            return new HttpResult(response) {View = "WebProductView", Template = ""};
        }
    }
}
