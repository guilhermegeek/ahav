﻿using System.Linq;
using Ahav.Infrastructure;
using Store.ServiceModel.Domain;
using Store.ServiceModel.Domain.Entities;
using Store.ServiceModel.Dtos;

namespace Store.ServiceInterface.Extensions
{
    public static class EcommerceExtensions
    {
        public static CouponDto ToDto(this Coupon coupon)
        {
            return new CouponDto
                       {
                           CanBeReused = coupon.CanBeReused,
                           Disccount = coupon.Disccount,
                           End = coupon.End,
                           IsActive = coupon.IsActive,
                           Name = coupon.Name,
                           Start = coupon.Start
                       };
        }
        public static ProductDto ToDto(this Product product)
        {
            var culture = ServiceStackExtensions.GetCulture();
            return new ProductDto
                       {
                           Id = product.Id,
                           CategoriesIds = product.CategoriesIds,
                           Comments = product.Comments,
                           Title =
                               product.Title.Any(x => x.Key == culture)
                                   ? product.Title.First(x => x.Key == culture).Value
                                   : product.Title.FirstOrDefault().Value,
                           Description =
                               product.Description.Any(x => x.Key == culture)
                                   ? product.Description.First(x => x.Key == culture).Value
                                   : product.Description.FirstOrDefault().Value
                       };
        }
        public static ProductInfo ToInfo(this Product product)
        {
            var culture = ServiceStackExtensions.GetCulture();
            return new ProductInfo
                       {
                           Id = product.Id,
                           //Price = product.Price.List,
                           Title =
                               product.Title.Any(x => x.Key == culture)
                                   ? product.Title.First(x => x.Key == culture).Value
                                   : product.Title.FirstOrDefault().Value
                       };
        }
        public static ProductCategoryDto ToDto(this ProductCategory category)
        {
            var culture = ServiceStackExtensions.GetCulture();
            return new ProductCategoryDto
            {
                Id = category.Id,
                Name = category.Name.Any(x => x.Key == culture) ? category.Name.First(x => x.Key == culture).Value : category.Name.FirstOrDefault().Value
                
            };
        }
    }
}
