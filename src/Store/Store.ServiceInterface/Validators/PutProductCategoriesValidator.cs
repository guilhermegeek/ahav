﻿using ServiceStack.FluentValidation;
using Store.ServiceModel.ApiDtos;

namespace Store.ServiceInterface.Validators
{
    public class PutProductCategoriesValidator : AbstractValidator<PutProductCategories>
    {
        public PutProductCategoriesValidator()
        {
            RuleFor(x => x.Categories.Length).GreaterThan(0);
        }
    }
}
