﻿using ServiceStack.FluentValidation;
using Store.ServiceModel.ApiDtos;

namespace Store.ServiceInterface.Validators
{
    public class PostProductValidator : AbstractValidator<PostProduct>
    {
        public PostProductValidator()
        {
            RuleFor(x => x.Description).NotEmpty().WithErrorCode("NotEmpty");
            RuleFor(x => x.Price).NotEmpty().WithErrorCode("NotEmpty");
            RuleFor(x => x.Title).NotEmpty().WithErrorCode("NotEmpty");
            //RuleFor(x => x.Categories.Length).GreaterThan(0).WithErrorCode("NotEmpty");
        }
    }
}
