﻿using ServiceStack.FluentValidation;
using Store.ServiceModel.ApiDtos;

namespace Store.ServiceInterface.Validators
{
    public class PostCartProductValidator : AbstractValidator<PostCartProduct>
    {
        public PostCartProductValidator()
        {
            RuleFor(x => x.Quantity).GreaterThanOrEqualTo(1);
        }
    }
}
