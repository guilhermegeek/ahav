﻿using ServiceStack.FluentValidation;
using Store.ServiceModel.ApiDtos;

namespace Store.ServiceInterface.Validators
{
    public class PutProductPriceValidator : AbstractValidator<PutProductPrice>
    {
        public PutProductPriceValidator()
        {
            RuleFor(x => x.Price).GreaterThan(0);
        }
    }
}
