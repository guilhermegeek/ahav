﻿using ServiceStack.FluentValidation;
using Store.ServiceModel.ApiDtos;

namespace Store.ServiceInterface.Validators
{
    public class PutProductInfoValidator : AbstractValidator<PutProductInfo>
    {
        public PutProductInfoValidator()
        {
            RuleFor(x => x.Title).NotEmpty();
        }
    }
}
