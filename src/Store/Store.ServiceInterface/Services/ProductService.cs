﻿using System.Linq;
using Ahav.ServiceInterface.Filters;
using MongoDB.Driver.Builders;
using ServiceStack.ServiceInterface;
using Store.ServiceInterface.Data;
using Store.ServiceInterface.Extensions;
using Store.ServiceModel;
using Store.ServiceModel.ApiDtos;
using Store.ServiceModel.Domain;
using Store.ServiceModel.Domain.Entities;

namespace Store.ServiceInterface.Services
{
    public class ProductService : Service
    {
        public ProductRepository ProductRepo { get; set; }
        public ProductCategoryRepository ProductCategoryRepo { get; set; }

        public GetProductsResponse Get(GetProducts request)
        {
            var response = new GetProductsResponse();
            response.Products = ProductRepo.GetProducts(request.Categories, request.MinimumPrice, request.MaximumPrice,
                                                       request.Skip, request.Take == 0 ? 20 : request.Take).ToList();

            return response;
        }

        public GetProductResponse Get(GetProduct request)
        {
            var response = new GetProductResponse();
            response.Product = ProductRepo.GetProduct(request.Id);
            return response;
        }
        [LoggedIn(Roles = new[]{ EcommerceRoles.ProductManager})]
        [RequestHasAppId]
        public PostProductResponse Post(PostProduct request)
        {
            var response = new PostProductResponse();
            var product = new Product
                              {
                                  CategoriesIds = request.Categories,
                                  //Price = request.Price,
                                  Title = request.Title,
                                  Description = request.Description,
                                  ShippingIn = request.ShippingIn,
                                  ShipsFrom = request.ShipsFrom,
                                  //Variations = request.Variations
                              };
            ProductRepo.StoreProduct(product, product.ToInfo());
            response.Product = product.ToDto();
            return response;
        }
        [LoggedIn]
        [RequestHasAppId]
        public PutProductPriceResponse Put(PutProductPrice request)
        {
            var response = new PutProductPriceResponse();
            var result = ProductRepo.Save(request.Id, 
                Update<Product>
                .Set(x => x.Price.List, request.Price));
            return response;
        }
        [LoggedIn]
        [RequestHasAppId]
        public PutProductInfoResponse Put(PutProductInfo request)
        {
            var response = new PutProductInfoResponse();
            var result = ProductRepo.Save(request.Id, 
                Update<Product>.Set(x => x.Title, request.Title)
                .Set(x => x.Description, request.Description));
            return response;
        }
        [LoggedIn]
        [RequestHasAppId]
        public PostProductCategoryResponse Post(PostProductCategory request)
        {
            var response = new PostProductCategoryResponse();
            var category = new ProductCategory
                               { 
                                   Name = request.Name
                               };
            ProductCategoryRepo.Create(category);
            response.Category = category.ToDto();
            return response;
        }
        /// <summary>
        /// Updated a product category
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [LoggedIn]
        [RequestHasAppId]
        public PutProductCategoryResponse Put(PutProductCategory request)
        {
            var response = new PutProductCategoryResponse();
            var updateResult = ProductCategoryRepo.Save(Query<ProductCategory>.EQ(x => x.Id, request.Id),
                                                Update<ProductCategory>.Set(x => x.Name, request.Name));
            return response;
        }
        /// <summary>
        /// Adds categories to a product
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [LoggedIn]
        [RequestHasAppId]
        public PutProductCategoriesResponse Put(PutProductCategories request)
        {
            var response = new PutProductCategoriesResponse();
            foreach (var productCategory in request.Categories)
            {
                ProductRepo.Save(request.Id, Update<Product>.Push(x => x.CategoriesIds, productCategory));
                ProductCategoryRepo.Save(productCategory, Update<ProductCategory>.Push(x => x.ProductIds, request.Id));
            }
            return response;
        }
    }
}