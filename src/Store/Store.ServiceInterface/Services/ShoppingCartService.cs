﻿using System.Net;
using Ahav.Infrastructure;
using Ahav.ServiceInterface.Filters;
using ServiceStack.Common.Utils;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.ServiceModel;
using Store.ServiceInterface.Business;
using Store.ServiceInterface.Data;
using Store.ServiceModel.ApiDtos;

namespace Store.ServiceInterface.Services
{
    public class ShoppingCartService : Service
    {
        public ShoppingCartRepository CartRepo { get; set; }
        public CouponRepository CouponRepo { get; set; }
        public ShoppingCartBusiness CartBus { get; set; }

        /// <summary>
        /// Return the shopping cart object 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [LoggedIn]
        public GetCartResponse Get(GetCart request)
        {
            var response = new GetCartResponse();
            var cart = CartBus.Get();
            response.Cart = cart;
            //cart.Products.ForEach(x => response.Products.Add(ProductRepo.GetProductInfo(x.Id), x.Quantity));
            return response;
        }
        /// <summary>
        /// Adds a product to shopping cart.
        /// If a product already exists, the quantity is updated.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [LoggedIn]
        public PostCartProductResponse Post(PostCartProduct request)
        {
            var response = new PostCartProductResponse();
            CartBus.AddProductToCart(Request.GetUserId(), request.ProductId, request.Quantity);
            return response;
        }
        /// <summary>
        /// Removes an product from the shopping cart indicating the quantity of items to be removed
        /// If the final quantity is zero, the product is removed from the cart
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public DeleteProductCartResponse Post(DeleteProductCart request)
        {
            var response = new DeleteProductCartResponse();
            var r = CartRepo.DeleteProductCart(Request.GetUserId(), request.Id, request.Quantity);
            return response;
        }
        /// <summary>
        /// Validate a coupon and add it to the shopping cart
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public PostCouponCartResponse Any(PostCouponCart request)
        {
            var response = new PostCouponCartResponse();
            if(CouponRepo.CouponIsValid(request.Coupon, Request.GetUserId()))
            {
                
            }
            return response;
        }
    }
}
