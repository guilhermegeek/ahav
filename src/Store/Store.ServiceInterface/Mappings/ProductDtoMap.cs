﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using Ahav.Infrastructure;
using AutoMapper;
using Store.ServiceModel.Domain;
using Store.ServiceModel.Domain.Entities;

namespace Store.ServiceInterface.Mappings
{
    public static class ProductDtoMap
    {
        public static void Register()
        {
            Mapper.CreateMap<Product, ProductDto>().ConvertUsing(m =>
                                                                 {
                                                                     var culture = ServiceStackExtensions.GetCulture();
                                                                     return new ProductDto
                                                                            {
                                                                                Id = m.Id,
                                                                                CategoriesIds = m.CategoriesIds,
                                                                                Comments = m.Comments,
                                                                                Description = m.Description.Any(x => x.Key == culture)
                                                                                    ? m.Description.First(x => x.Key == culture).Value
                                                                                    : m.Description.FirstOrDefault().Value,
                                                                                Title = m.Title.Any(x => x.Key == culture)
                                                                                    ? m.Description.First(x => x.Key == culture).Value
                                                                                    : m.Description.FirstOrDefault().Value
                                                                            };
                                                                 });
        }
    }
}
