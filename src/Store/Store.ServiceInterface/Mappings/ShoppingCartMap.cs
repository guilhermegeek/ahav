﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Store.ServiceModel.Domain;
using Store.ServiceModel.Domain.Entities;
using Store.ServiceModel.Dtos;

namespace Store.ServiceInterface.Mappings
{
    public static class ShoppingCartMap
    {
        public static void Register()
        {
            Mapper.CreateMap<ShoppingCart, ShoppingCartDto>().ConvertUsing(x =>
                                                                           {
                                                                               return new ShoppingCartDto
                                                                                      {
                                                                                          Id = x.Id,
                                                                                          Name = x.Name,
                                                                                          Price = x.Price,
                                                                                          Items = x.Items
                                                                                      };
                                                                           });
        }
    }
}
