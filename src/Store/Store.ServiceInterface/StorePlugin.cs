﻿using Ahav.ServiceInterface.Mappings;
using ServiceStack.WebHost.Endpoints;
using Store.ServiceInterface.Business;
using Store.ServiceInterface.Controllers;
using Store.ServiceInterface.Data;
using Store.ServiceInterface.Data.Mapping;
using Store.ServiceInterface.Mappings;
using Store.ServiceInterface.Services;


namespace Store.ServiceInterface
{
    public class StorePlugin : IPlugin
    {
        public void Register(IAppHost appHost)
        {
            var container = appHost.GetContainer();
            
            // Product
            appHost.RegisterService<ProductService>();
            appHost.RegisterService<ProductController>();
            container.RegisterAutoWired<ProductBusiness>();
            container.RegisterAutoWired<ProductRepository>();

            // Product Category
            container.RegisterAutoWired<ProductCategoryRepository>();

            // Coupons
            appHost.RegisterService<CouponService>();
            
            // Shopping Cart
            appHost.RegisterService<ShoppingCartService>();
            appHost.RegisterService<ShoppingCartController>();
            container.RegisterAutoWired<ShoppingCartBusiness>();
            container.RegisterAutoWired<ShoppingCartRepository>();

            container.RegisterAutoWired<CouponRepository>();

            // Mappings
            MapProductCollection.Register();
            MapLocalBusinessCollection.Register();
            ShoppingCartMap.Register();
            OrderDtoMap.Register();
            ProductDtoMap.Register();
        }
    }
}
