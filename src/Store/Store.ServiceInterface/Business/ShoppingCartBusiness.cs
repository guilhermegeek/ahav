﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using Store.ServiceInterface.Data;
using Store.ServiceModel.Domain;
using Store.ServiceModel.Domain.Entities;
using Store.ServiceModel.Dtos;

namespace Store.ServiceInterface.Business
{
    public class ShoppingCartBusiness
    {
        public ShoppingCartRepository ShoppingCartRepo { get; set; }

        public ShoppingCart Get()
        {
            var id = ObjectId.Empty;
            return ShoppingCartRepo.Get(id);
        }

        public void AddProductToCart(ObjectId userId, ObjectId productId, int quantity)
        {
            ObjectId cartId = ObjectId.Empty;
            ShoppingCartRepo.AddProductCart(userId, productId, quantity);
        }
    }
}
