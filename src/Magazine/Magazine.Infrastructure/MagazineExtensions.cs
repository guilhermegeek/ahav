﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.Text;

namespace Magazine.Infrastructure
{
    public static class MagazineExtensions
    {
        public const string HttpItemBlogI = "itemblogid";

        public static ObjectId GetBlogId(this IHttpRequest req)
        {
            return req.Items.ContainsKey(HttpItemBlogI) &&
                   !String.IsNullOrEmpty(req.Items[HttpItemBlogI].ToString())
                       ? JsonSerializer.DeserializeFromString<ObjectId>(
                           req.Items[HttpItemBlogI].ToString())
                       : ObjectId.Empty;
        }
    }
}
