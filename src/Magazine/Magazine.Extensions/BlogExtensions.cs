﻿using System.Linq;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceModel.Dtos;
using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using Magazine.ServiceModel.Dtos;

namespace Magazine.Extensions
{
    public static class BlogRole
    {
        /// <summary>
        /// This user can make all actions on blog content
        /// </summary>
        public const string Administrator = "blogadmin";

        /// <summary>
        /// This user can write new articles and edit is own
        /// </summary>
        public const string Writter = "blogwritter";

        /// <summary>
        /// This user can edit articles, in order to improve their quality
        /// </summary>
        public const string Editor = "blogeditor";
    }
    public static class BlogExtensions
    {
        public static BlogInfo ToInfo(this Blog blog)
        {
            var culture = ServiceStackExtensions.GetCulture();
            return new BlogInfo
                       {
                           Id = blog.Id,
                           SubDomain = blog.SubDomain,
                           Name = blog.Name
                       };
        }
        public static BlogDto ToDto(this Blog blog)
        {
            var culture = ServiceStackExtensions.GetCulture();
            return new BlogDto
                       {
                           Id = blog.Id,
                           SubDomain = blog.SubDomain,
                           Categories = blog.CategoriesIds,
                           Description =
                               blog.Description.Any(x => x.Key == culture)
                                   ? blog.Description.First(x => x.Key == culture).Value
                                   : blog.Description.FirstOrDefault().Value,
                           Title = blog.Title.Any(x => x.Key == culture)
                                       ? blog.Description.First(x => x.Key == culture).Value
                                       : blog.Description.FirstOrDefault().Value,
                           Name = blog.Name,
                           Owner = blog.Owner
                       };
        }
        public static BlogArticleInfo ToInfo(this Article post)
        {
            var culture = ServiceStackExtensions.GetCulture();
            return new BlogArticleInfo
                       {
                           Slug = post.Slug,
                           Content =
                               post.Body.Any(x => x.Key == culture)
                                   ? post.Body.First(x => x.Key == culture).Value
                                   : post.Body.FirstOrDefault().Value,
                           CreatedBy = new AuthorInfo {Id = post.CreatedBy},
                           Id = post.Id,
                           Title =
                               post.Title.Any(x => x.Key == culture)
                                   ? post.Title.First(x => x.Key == culture).Value
                                   : post.Title.FirstOrDefault().Value
                       };
        }
        public static BlogArticleDto ToDto(this Article post)
        {
            var culture = ServiceStackExtensions.GetCulture();
            var article = new BlogArticleDto
                       {
                           Slug = post.Slug,
                           Id = post.Id,
                           Categories = post.Categories,
                           Content =
                               post.Body.Any(x => x.Key == culture)
                                   ? post.Body.First(x => x.Key == culture).Value
                                   : post.Body.FirstOrDefault().Value,
                           CreatedBy = post.CreatedBy,
                           Tags = post.Tags,
                           Title =
                               post.Title.Any(x => x.Key == culture)
                                   ? post.Title.First(x => x.Key == culture).Value
                                   : post.Title.FirstOrDefault().Value
                       };
            article.Excerpt = post.Excerpt != null && post.Excerpt.Any()
                                  ? post.Excerpt.FirstOrDefault(x => x.Key == culture).Value
                                  : article.Title;
            return article;
        }
        public static BlogArticleRef ToRef(this Article post)
        {
            var culture = ServiceStackExtensions.GetCulture();
            return new BlogArticleRef
                       {
                           Id = post.Id,
                           State = BlogArticleState.Suspended,
                           Title =
                               post.Title.Any(x => x.Key == culture)
                                   ? post.Title.First(x => x.Key == culture).Value
                                   : post.Title.FirstOrDefault().Value
                       };
        }
        public static BlogArticleRef ToRef(this BlogArticleDto post)
        {

            return new BlogArticleRef
            {
                Id = post.Id,
                State = BlogArticleState.Suspended,
                Title = post.Title
            };
        }
        public static BlogArticleCategoryDto ToDto(this BlogArticleCategory request)
        {
            var culture = ServiceStackExtensions.GetCulture();
            return new BlogArticleCategoryDto
                       {
                           Id = request.Id,
                           Title =
                               request.Title.Any(x => x.Key == culture)
                                   ? request.Title.First(x => x.Key == culture).Value
                                   : request.Title.FirstOrDefault().Value
                       };
        }
    }
}
