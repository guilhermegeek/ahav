﻿using System.Linq;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceModel.Dtos;
using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using Magazine.ServiceModel.Dtos;

namespace Magazine.Extensions
{
    public static class BlogTemplateExtensions
    {
        public static BlogTemplateDto ToDto(this BlogTemplate entity)
        {
            var culture = ServiceStackExtensions.GetCulture();
            var template = new BlogTemplateDto
                               {
                                   Id = entity.Id,
                                   Name = entity.Name,
                                   Metadata = entity.Metadata
                               };
            if (entity.Body != null && entity.Body.Any())
                template.Body = entity.Body.Any(x => x.Key == culture)
                                    ? entity.Body.First(x => x.Key == culture).Value
                                    : entity.Body.First().Value;
            if (entity.BodyViewArticle != null && entity.BodyViewArticle.Any())
                template.BodyViewArticle = entity.BodyViewArticle.Any(x => x.Key == culture)
                                               ? entity.BodyViewArticle.First(x => x.Key == culture).Value
                                               : entity.BodyViewArticle.First().Value;
            if (entity.BodyViewIndex != null && entity.BodyViewIndex.Any())
                template.BodyViewIndex = entity.BodyViewIndex.Any(x => x.Key == culture)
                                             ? entity.BodyViewIndex.First(x => x.Key == culture).Value
                                             : entity.BodyViewIndex.First().Value;
            return template;
        }
        public static BlogTemplateInfo ToInfo(this BlogTemplate entity)
        {
            return new BlogTemplateInfo
                       {
                           Id = entity.Id,
                           Name = entity.Name
                       };
        }
    }
}
