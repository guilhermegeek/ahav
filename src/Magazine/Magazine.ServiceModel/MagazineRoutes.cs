﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magazine.ServiceModel
{
    public static class MagazineRoutes
    {
        public const string GetArticleView = "/blog/{Id}";
        public const string GetArticlesView = "/blog";
        public const string GetBlog = "/api/blog/{Id}";
        public const string PostBlog = "/api/blog";
        public const string PutBlog = "/api/blog/{Id}";
        public const string DeleteArticle = "/api/blog/article/{ArticleId}";
        public const string PostBlogArticle = "/api/blog/article";
        public const string GetBlogArticle = "/api/blog/article/{ArticleId}";
        public const string GetBlogArticles = "/api/blog/{BlogId}/article";
        public const string PostBlogArticleFile = "/api/blog/article/{ArticleId}/file";
        public const string PutBlogArticle = "/api/blog/article/{Id}";
        public const string DeleteBlogArticleFile = "/api/blog/article/{ArticleId}/file/{FileId}";
        public const string GetArticlesViewTemplate = "/api/blog/{BlogId}/template/articles";
        public const string PostArticlesViewTemplate = "/api/blog/{BlogId}/template/articles";
        public const string PostArticleViewTemplate = "/api/blog/{BlogId}/template/article";
        public const string PostBlogDefault = "/api/blog/{BlogId}/default";
        public const string GetBlogTemplate = "/api/blog/template/{TemplateId}";
    }
}
