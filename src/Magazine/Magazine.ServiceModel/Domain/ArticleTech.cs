﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magazine.ServiceModel.Domain
{
    /// <summary>
    /// A technical article - Example: How-to (task) topics, step-by-step, procedural troubleshooting, specifications, etc.
    /// </summary>
    public class ArticleTech
    {
        /// <summary>
        /// Prerequisites needed to fulfill steps in article.
        /// </summary>
        string Dependencies { get; set; }
        /// <summary>
        /// Proficiency needed for this content; expected values: 'Beginner', 'Expert'.
        /// </summary>
        private string ProfiencyLevel { get; set; }
    }
}
