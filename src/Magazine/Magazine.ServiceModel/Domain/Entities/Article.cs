﻿using System.Collections.Generic;
using Ahav.Interfaces;
using Magazine.ServiceModel.Dtos;
using MongoDB.Bson;

namespace Magazine.ServiceModel.Domain.Entities
{
    public class Article : IHasCommentsBucket
    {
        public Article()
        {
            this.Categories = new ObjectId[0];
            this.Files = new ObjectId[0];
            this.Tags = new string[0];
        }
        public ObjectId Id { get; set; }
        public ObjectId BlogId { get; set; }
        public ObjectId AppId { get; set; }
        /// <summary>
        /// Template Id
        /// </summary>
        public ObjectId TemplateId { get; set; }
        /// <summary>
        /// Tite for article
        /// </summary>
        public Dictionary<string, string> Title { get; set; }
        /// <summary>
        /// Body
        /// </summary>
        public Dictionary<string, string> Body { get; set; }
        /// <summary>
        /// Headline
        /// </summary>
        public Dictionary<string, string> Headline { get; set; }
        public string[] Tags { get; set; }
        public Dictionary<string, string> Excerpt { get; set; }
        /// <summary>
        /// Categories
        /// </summary>
        public ObjectId[] Categories { get; set; }
        /// <summary>
        /// Files attached to article
        /// </summary>
        public ObjectId[] Files { get; set; }
        public ICreator Creator { get; set; }
        /// <summary>
        /// Owner of article
        /// </summary>
        public ObjectId CreatedBy { get; set; }
        public BlogArticleState State { get; set; }

        public string Slug { get; set; }
        /// <summary>
        /// The object id of the post reference in social networks like Facebook 
        /// </summary>
        //public Dictionary<SocialProvider, object> SocialObjectId { get; set; }
        /// <summary>
        /// Likes counter
        /// </summary>
        public int SocialLikesCount { get; set; }
        
        
        /// <summary>
        /// Number of comments that buckets contains
        /// </summary>
        public int NumPages { get; set; }

        public long CommentsCount { get; set; }
        public int CommentsPageCount { get; set; }
    }
}
