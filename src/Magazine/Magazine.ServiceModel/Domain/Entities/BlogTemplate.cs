﻿using System.Collections.Generic;
using Ahav.Interfaces;
using MongoDB.Bson;

namespace Magazine.ServiceModel.Domain.Entities
{
    public class BlogTemplate : IHasAttachments
    {
        public BlogTemplate()
        {
            this.Body=new Dictionary<string, string>();
            this.BodyViewArticle = new Dictionary<string, string>();
            this.BodyViewIndex = new Dictionary<string, string>();
            this.Metadata=new Dictionary<string, string>();
        }
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Body { get; set; }
        public Dictionary<string, string> BodyViewArticle { get; set; }
        public Dictionary<string, string> BodyViewIndex { get; set; }
        public Dictionary<string, string> Metadata { get; set; }
        public ObjectId[] Files { get; set; }
    }
}
