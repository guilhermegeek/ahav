﻿using System.Collections.Generic;
using Magazine.ServiceModel.Dtos;
using MongoDB.Bson;

namespace Magazine.ServiceModel.Domain.Entities
{
    public class BlogArticleCategory
    {
        public BlogArticleCategory()
        {
            this.Posts = new List<BlogArticleRef>();
        }
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public long PostsCount { get; set; }
        public IList<BlogArticleRef> Posts { get; set; }
    }
}
