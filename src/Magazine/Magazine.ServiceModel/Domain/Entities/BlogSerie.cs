﻿using System.Collections.Generic;
using Magazine.ServiceModel.Dtos;
using MongoDB.Bson;

namespace Magazine.ServiceModel.Domain.Entities
{
    /// <summary>
    /// Blog serie
    /// </summary>
    public class BlogSerie
    {
        public BlogSerie()
        {
            Articles=new List<BlogArticleInfo>();
        }
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public IList<BlogArticleInfo> Articles { get; set; } 
        public Dictionary<string, string> Description { get; set; }
        public BlogSerieMemberAllowed MemembersAllowed { get; set; }
    }

    public enum BlogSerieMemberAllowed
    {
        // Visitors are allowed
        VisitorsAllowed, 
        // Only registered are allowed 
        RegisteredOnly, 
        // Only subscribers can access the serie
        BlogSerieSubscriberOnly
    }
}
