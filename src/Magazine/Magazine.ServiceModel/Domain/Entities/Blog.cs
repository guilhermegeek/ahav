﻿using System.Collections.Generic;
using Magazine.ServiceModel.DataDtos;
using MongoDB.Bson;

namespace Magazine.ServiceModel.Domain.Entities
{
    public class Blog
    {
        public Blog()
        {
            this.CategoriesIds = new ObjectId[0];
          //  this.Posts = new List<BlogArticleInfo>();
            this.ArticlesIds = new ObjectId[0];
        }

        public ObjectId Id { get; set; }
        public ObjectId[] ArticlesIds { get; set; }
        public string SubDomain { get; set; }
        //public List<BlogArticleInfo> Posts { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Description { get; set; }
        public ObjectId Owner { get; set; }
        public ObjectId[] CategoriesIds { get; set; }
        public BlogCategoryRef[] Categories { get; set; }

    }
}
