﻿using System.Collections.Generic;
using Magazine.ServiceModel.Dtos;
using MongoDB.Bson;

namespace Magazine.ServiceModel.Domain.Entities
{
    public class BlogCategory
    {
        public BlogCategory()
        {
            this.Blogs = new List<BlogInfo>();
        }
        public ObjectId Id { get; set; }
        public ObjectId Owner { get; set; }
        public Dictionary<string, string> Name { get; set; }
        public Dictionary<string, string> Description { get; set; }
        public IList<BlogInfo> Blogs { get; set; }
        public ObjectId[] BlogsId { get; set; }
    }
}
