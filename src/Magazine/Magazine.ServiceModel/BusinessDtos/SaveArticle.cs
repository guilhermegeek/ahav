﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.BusinessDtos
{
    public class SaveArticle : IReturn<SaveArticleResponse>
    {
        public ObjectId Id { get; set; }
        public ObjectId TemplateId { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Body { get; set; }
        public Dictionary<string, string> Headline { get; set; }
        
    }
    public class SaveArticleResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
