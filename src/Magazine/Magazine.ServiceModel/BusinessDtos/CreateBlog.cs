﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magazine.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.BusinessDtos
{
    public class CreateBlog
    {
        public ObjectId UserId { get; set; }
        public string Name { get; set; }
        public string Domain { get; set; }
        public ObjectId TemplateId { get; set; }
    }

    public class CreateBlogResponse : IHasResponseStatus
    {
        public BlogDto Blog { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
