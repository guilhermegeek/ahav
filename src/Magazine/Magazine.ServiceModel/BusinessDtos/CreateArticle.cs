﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magazine.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.BusinessDtos
{
    public class CreateArticle : IReturn<CreateArticleResponse>
    {
        public ObjectId CreatorId { get; set; }
        public string Name { get; set; }
        public ObjectId TemplateId { get; set; }
        public ObjectId AppId { get; set; }
        public string Slug { get; set; }
        public ObjectId[] Categories { get; set; }
        public ObjectId BlogId { get; set; }
    }
    public class CreateArticleResponse : IHasResponseStatus
    {
        public BlogArticleDto Article { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
