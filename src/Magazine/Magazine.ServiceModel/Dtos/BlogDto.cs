﻿using MongoDB.Bson;

namespace Magazine.ServiceModel.Dtos
{
    public class BlogDto
    {
        public ObjectId Id { get; set; }
        public string SubDomain { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public ObjectId Owner { get; set; }
        public ObjectId[] Categories { get; set; }
        public ObjectId? TemplateId { get; set; }
    }
}
