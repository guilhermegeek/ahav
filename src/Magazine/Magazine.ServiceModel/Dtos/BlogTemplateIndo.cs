﻿using MongoDB.Bson;

namespace Magazine.ServiceModel.Dtos
{
    public class BlogTemplateInfo
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
    }
}
