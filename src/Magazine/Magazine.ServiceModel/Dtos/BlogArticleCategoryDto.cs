﻿using MongoDB.Bson;

namespace Magazine.ServiceModel.Dtos
{
    public class BlogArticleCategoryDto
    {
        public ObjectId Id { get; set; }
        public string Title { get; set; }
    }
}
