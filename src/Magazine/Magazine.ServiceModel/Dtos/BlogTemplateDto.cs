﻿using System.Collections.Generic;
using MongoDB.Bson;

namespace Magazine.ServiceModel.Dtos
{
    public class BlogTemplateDto
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public string Body { get; set; }
        public string BodyViewArticle { get; set; }
        public string BodyViewIndex { get; set; }
        public Dictionary<string, string> Metadata { get; set; }
    }
}
