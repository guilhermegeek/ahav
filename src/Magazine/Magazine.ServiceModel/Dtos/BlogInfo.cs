﻿using MongoDB.Bson;

namespace Magazine.ServiceModel.Dtos
{
    public class BlogInfo
    {
        public ObjectId Id { get; set; }
        public string SubDomain { get; set; }
        public string Name { get; set; }
    }
}
