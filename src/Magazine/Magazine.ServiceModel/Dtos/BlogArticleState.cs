﻿namespace Magazine.ServiceModel.Dtos
{
    public enum BlogArticleState
    {
        Created = 0, Pending = 1, Published = 2, Suspended = 3
    }
}
