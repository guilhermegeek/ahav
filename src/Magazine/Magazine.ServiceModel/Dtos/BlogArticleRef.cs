﻿using MongoDB.Bson;

namespace Magazine.ServiceModel.Dtos
{
    public class BlogArticleRef
    {
        public ObjectId Id { get; set; }
        public string Title { get; set; }
        public BlogArticleState State { get; set; }
    }
}
