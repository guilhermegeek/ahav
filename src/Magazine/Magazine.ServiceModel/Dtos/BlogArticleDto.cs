﻿using System.Collections.Generic;
using Ahav.Interfaces;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson;

namespace Magazine.ServiceModel.Dtos
{
    /// <summary>
    /// This DTO is mainly used to display articles and get them for put
    /// </summary>
    public class BlogArticleDto
    {
        public BlogArticleDto()
        {
            this.Categories = new List<ObjectId>();
        }
        public ObjectId Id { get; set; }
        public string Slug { get; set; }
        public BlogArticleState State { get; set; }
        public ObjectId TemplateId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public ICreator Creator { get; set; }
        public string[] Tags { get; set; }
        public string Excerpt { get; set; }
        public List<MediaRef> Media { get; set; } // int tipo de media, slideshow, o que for
        public Slideshow Slideshow { get; set; }
        public IList<ObjectId> Categories { get; set; }
        public ObjectId CreatedBy { get; set; }
    }
}
