﻿using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;

namespace Magazine.ServiceModel.Dtos
{
    public class BlogArticleInfo
    {
        public ObjectId Id { get; set; }
        public string Slug { get; set; }
        public AuthorInfo CreatedBy { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public ObjectId MediaIcon { get; set; }
    }
}
