﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magazine.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.Controllers
{
    public class ArticleViewController : IReturn<ArticleViewControllerResponse>
    {
        public ObjectId Id { get; set; }
        public ObjectId TemplateCached { get; set; }
    }
    public class ArticleViewControllerResponse : IHasResponseStatus
    {
        public bool ReuseTemplate { get; set; }
        public string RenderBody { get; set; }
        public BlogArticleInfo[] Articles { get; set; } 
        public BlogArticleDto Article { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
