﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.Controllers
{
    public class GetBlogTemplateEdit : IReturn<GetBlogTemplateEditResponse>
    {
        public ObjectId Id { get; set; }
    }
    class GetBlogTemplateEditResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
