﻿using System.Collections.Generic;
using Magazine.ServiceModel.Dtos;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.ApiDtos
{
    public class GetBlogIndexView : IReturn<GetBlogIndexViewResponse>
    {
        
    }
    public class GetBlogIndexViewResponse : IHasResponseStatus
    {
        public GetBlogIndexViewResponse()
        {
            this.Articles = new List<BlogArticleInfo>();
        }
        public IList<BlogArticleInfo> Articles { get; set; }
        public string RenderBody { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
