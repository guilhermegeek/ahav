﻿using System.Collections.Generic;
using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.ApiDtos
{
    public class CreateBlogTemplate : IReturn<CreateBlogTemplateResponse>
    {

        public Dictionary<string, string> Body { get; set; }
        public Dictionary<string, string> BodyViewArticle { get; set; }
        public Dictionary<string, string> BodyViewIndex { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// Set the template as a public template
        /// </summary>
        public bool Public { get; set; }
        /// <summary>
        /// ProjectId is saved when the template isn't public
        /// </summary>
        public ObjectId? ProjectId { get; set; }
        public Dictionary<string, string> Metadata { get; set; } 
    }
    public class CreateBlogTemplateResponse : IHasResponseStatus
    {
        public BlogTemplate BlogTemplate { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
