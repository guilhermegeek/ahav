﻿using System.Collections.Generic;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.ApiDtos
{
    public class UpdateBlog : IReturn<UpdateBlogResponse>
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public string SubDomain { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Description { get; set; }
        public IList<ObjectId> Categories { get; set; }
    }
    public class UpdateBlogResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class UpdateArticle : IReturn<UpdateArticleResponse>
    {
        public ObjectId ArticleId { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Headline { get; set; }
        public Dictionary<string, string> Body { get; set; }
        //public string[] Tags { get; set; }
        //public Slideshow Slideshow { get; set; }
        public string Slug { get; set; }
        //public IList<ObjectId> Categories { get; set; }
        public ObjectId? TemplateId { get; set; }
    }
    public class UpdateArticleResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
