﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Interfaces;
using Ahav.ServiceModel.Interfaces;
using Magazine.ServiceModel.Dtos;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.ApiDtos
{
    public class GetBlogSeries : IReturn<GetBlogSeriesResponse>, IPagedResult
    {
        public int Skip { get; set; }
        public int Take { get; set; }
    }
    public class GetBlogSeriesResponse : IHasResponseStatus
    {
        public IList<BlogSerieDto> Results { get; set; } 
        public ResponseStatus ResponseStatus { get; set; }
    }
}
