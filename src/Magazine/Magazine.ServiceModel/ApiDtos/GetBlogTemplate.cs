﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Dtos;
using Magazine.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.ApiDtos
{
    public class GetBlogTemplate : IReturn<GetBlogTemplateResponse>
    {
        public ObjectId TemplateId { get; set; }
    }
    public class GetBlogTemplateResponse : IHasResponseStatus
    {
        public BlogTemplateDto Template { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
