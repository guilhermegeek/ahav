﻿using System.Collections.Generic;
using MongoDB.Bson;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.ApiDtos
{
    public class UpdateBlogTemplate
    {
        public ObjectId Id { get; set; }
        public Dictionary<string, string> Body { get; set; }
        public Dictionary<string, string> BodyViewArticle { get; set; }
        public Dictionary<string, string> BodyViewIndex { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// Set the template as a public template
        /// </summary>
        public bool Public { get; set; }
        /// <summary>
        /// ProjectId is saved when the template isn't public
        /// </summary>
        public Dictionary<string, string> Metadata { get; set; } 
    }
    public class UpdateBlogTemplateResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
