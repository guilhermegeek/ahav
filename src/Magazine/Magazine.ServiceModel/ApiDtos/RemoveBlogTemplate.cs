﻿using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.ApiDtos
{
    public class RemoveBlotTemplate : IReturn<RemoveBlogTemplateResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class RemoveBlogTemplateResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
