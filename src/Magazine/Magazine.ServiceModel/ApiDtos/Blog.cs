﻿using System.Collections.Generic;
using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Interfaces;
using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using Magazine.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.ApiDtos
{
    /// <summary>
    /// Returns the Template Id for Blog home page
    /// </summary>
    public class GetBlogTemplateDefault : IReturn<GetBlogTemplateDefaultResponse>
    {
        public ObjectId BlogId { get; set; }
        public bool ReturnTemplates { get; set; }
    }
    public class GetBlogTemplateDefaultResponse : IHasResponseStatus
    {
        public GetBlogTemplateDefaultResponse()
        {
            this.Templates = new List<ContentTemplateInfo>();
        }
        public ObjectId TemplateId { get; set; }
        public BlogTemplateDto Template { get; set; }
        public IList<ContentTemplateInfo> Templates { get; set; } 
        public ResponseStatus ResponseStatus { get; set; }
    }
    //public class PutBlogTemplateDefault : IReturn<PutBlogTemplateDefaultResponse>
    //{
    //    public ObjectId BlogId { get; set; }
    //    public ObjectId TemplateId { get; set; }
    //}
    //public class PutBlogTemplateDefaultResponse : IHasResponseStatus, IHasResponseModal
    //{
    //    public ResponseStatus ResponseStatus { get; set; }

    //    public ResponseClient Modal { get; set; }
    //}
    public class PostArticleViewTemplate : IReturn<PostArticleViewTemplateResponse>
    {
        public ObjectId BlogId { get; set; }
        public ObjectId TemplateId { get; set; }
    }
    public class PostArticleViewTemplateResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
    
    
    public class PostBlogDefault : IReturn<PostBlogDefaultResponse>
    {
        public ObjectId BlogId { get; set; }
    }
    public class PostBlogDefaultResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
    /// <summary>
    /// Creates a new Blog
    /// </summary>
    public class PostBlog : IReturn<PostBlogResponse>
    {
        public PostBlog()
        {
            this.Categories=new  ObjectId[0];
        }
        public ObjectId OwnerId { get; set; }
        public string Name { get; set; }
        public string SubDomain { get; set; }
        public ObjectId[] Categories { get; set; }
        public ObjectId TemplateId { get; set; }
    }

    public class PostBlogResponse : IHasResponseStatus, IHasResponseModal
    {
        public ObjectId Id { get; set; }
        public BlogDto Result { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
    public class PutBlog : IReturn<PutBlogResponse>
    {
        public ObjectId BlogId { get; set; }
        public string Name { get; set; }
        public string SubDomain { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Description { get; set; }
        public IList<ObjectId> Categories { get; set; }
    }
    public class PutBlogResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
    public class PutArticleState : IReturn<PutArticleStateResponse>
    {
        public ObjectId ArticleId { get; set; }
        public BlogArticleState ArticleState { get; set; }
    }
    public class PutArticleStateResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
    
    public class PutBlogArticle : IReturn<PutBlogArticleResponse>
    {
        public PutBlogArticle()
        {
            //this.Categories=new List<ObjectId>();
        }
        public ObjectId Id { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Headline { get; set; }
        public Dictionary<string, string> Body { get; set; }
        //public string[] Tags { get; set; }
        //public Slideshow Slideshow { get; set; }
        public string Slug { get; set; }
        //public IList<ObjectId> Categories { get; set; }
        public ObjectId TemplateId { get; set; }
    }
    public class PutBlogArticleResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class DeleteBlogArticle : IReturn<DeleteBlogArticleResponse>
    {
        public ObjectId ArticleId { get; set; }
    }
    public class DeleteBlogArticleResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
    public class DeleteBlogArticleComment : IReturn<DeleteBlogArticleCommentResponse>
    {
        public ObjectId PostId { get; set; }
        public long CommentId { get; set; }
    }
    public class DeleteBlogArticleCommentResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class PostBlogCategory : IReturn<PostBlogCategoryResponse>
    {
        public Dictionary<string, string> Name { get; set; }
        public Dictionary<string, string> Description { get; set; }
    }
   
    public class PostBlogCategoryResponse : IHasResponseStatus
    {
        public BlogCategory Category { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class PostBlogArticleCategory : IReturn<PostBlogArticleCategoryResponse>
    {
        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Description { get; set; }
    }
    public class PostBlogArticleCategoryResponse : IHasResponseStatus
    {
        public BlogArticleCategoryDto Category { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class GetBlogArticleCategory : IReturn<GetBlogArticleCategoryResponse>
    {
        public ObjectId? BlogId { get; set; }
    }
    public class GetBlogArticleCategoryResponse
    {
        public ResponseStatus ResponseStatus { get; set; }
        public IEnumerable<BlogArticleCategoryDto> Categories { get; set; } 
    }
    public class DeleteBlogArticleCategory : IReturn<DeleteBlogArticleCategoryResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class DeleteBlogArticleCategoryResponse {}

    public class GetBlogArticles : IReturn<GetBlogArticlesResponse>
    {
        public ObjectId BlogId { get; set; }
        public ObjectId[] Category { get; set; }
        public int? Skip { get; set; }
        public int? Take { get; set; }
    }
    public class GetBlogArticlesResponse : IHasResponseStatus
    {
        public GetBlogArticlesResponse()
        {
            this.Articles=new List<BlogArticleInfo>();
        }
        public IList<BlogArticleInfo> Articles { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class GetBlogArticle : IReturn<GetBlogArticleResponse>
    {
        public ObjectId Id { get; set; }
        public string Slug { get; set; }
    }
    public class GetBlogArticleResponse : IHasResponseStatus
    {
        public BlogArticleDto Article { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class GetBlogs : IReturn<GetBlogsResponse>
    {
        public ObjectId[] OwnersIds { get; set; }
        public ObjectId[] CategoriesIds { get; set; }
    }
    public class GetBlogsResponse : IHasResponseStatus
    {
        public GetBlogsResponse()
        {
            this.Blogs = new List<BlogInfo>();
        }
        public ResponseStatus ResponseStatus { get; set; }
        public IList<BlogInfo> Blogs { get; set; } 
    }
    public class GetBlog : IReturn<GetBlogResponse>
    {
        public ObjectId Id { get; set; }
        public string Slug { get; set; }
    }

    public class GetBlogResponse : IHasResponseStatus
    {
        public GetBlogResponse()
        {
            this.LastComments=new List<NComments>();
        }
        public BlogDto Blog { get; set; }
        public IList<NComments> LastComments { get; set; }
        public ResponseStatus ResponseStatus { get; set; }

    }



    // Blog Attachements
    public class PostBlogArticleFile : IReturn<PostBlogArticleFileResponse>
    {
        public ObjectId ArticleId { get; set; }
    }
    public class PostBlogArticleFileResponse : IHasResponseStatus, IHasResponseModal
    {
        public PostBlogArticleFileResponse ()
        {
            this.Medias = new List<MediaDto>();
        }
        public IList<MediaDto> Medias { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
    public class DeleteBlogArticleFile : IReturn<DeleteBlogArticleFileResponse>
    {
        public ObjectId BlogId { get; set; }
        public ObjectId FileId { get; set; }
    }
    public class DeleteBlogArticleFileResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
