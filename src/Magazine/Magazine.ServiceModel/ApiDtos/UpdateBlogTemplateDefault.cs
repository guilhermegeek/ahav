﻿using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.ApiDtos
{
    public class UpdateBlogTemplateDefault : IReturn<UpdateBlogTemplateDefaultResponse>
    {
        public ObjectId BlogId { get; set; }
        public ObjectId BlogTemplateId { get; set; }
    }
    public class UpdateBlogTemplateDefaultResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
