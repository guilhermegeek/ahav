﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Dtos;
using Magazine.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.ApiDtos
{
    public class PostBlogArticle : IReturn<PostBlogArticleResponse>
    {
        /// <summary>
        /// If true, article will be published and related in recent articles
        /// </summary>
        //public bool Publish { get; set; }
        public bool HasSerie { get; set; }
        /// <summary>
        /// Article serie id. If null, it's just ignored 
        /// </summary>
        public ObjectId SerieId { get; set; }
        public string SubDomain { get; set; }
        public ObjectId[] Categories { get; set; }
        public ObjectId BlogId { get; set; }
        public ObjectId TemplateId { get; set; }
    }


    public class PostBlogArticleResponse : IHasResponseStatus
    {
        public BlogArticleDto Article { get; set; }
        public BlogArticleInfo ArticleInfo { get; set; }
        public ResponseClient Modal { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
