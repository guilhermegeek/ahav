﻿using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.ApiDtos
{
    public class ReadBlogTemplateDefault : IReturn<ReadBlogTemplateDefaultResponse>
    {
        public ObjectId BlogId { get; set; }
    }
    public class ReadBlogTemplateDefaultResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ObjectId TemplateId { get; set; }
    }
}
