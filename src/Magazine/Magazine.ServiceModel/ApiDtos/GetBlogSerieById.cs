﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Dtos;
using Magazine.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.ApiDtos
{
    /// <summary>
    /// Get Serie by id
    /// </summary>
    public class GetBlogSerieById : IReturn<GetBlogSerieById>
    {
        public ObjectId Id { get; set; }
    }
    public class GetBlogSerieByIdResponse : IHasResponseStatus
    {
        public ObjectId SerieId { get; set; }
        public string Description { get; set; }
        public IList<BlogArticleInfo> Articles { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
