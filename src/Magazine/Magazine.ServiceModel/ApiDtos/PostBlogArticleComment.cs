﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.ApiDtos
{
    public class PostBlogArticleComment : IReturn<PostBlogArticleCommentResponse>
    {
        public ObjectId ArticleId { get; set; }
        public string Message { get; set; }
    }
    public class PostBlogArticleCommentResponse : AddCommentResponse, IHasResponseStatus
    {
        public NComment Comment { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
