﻿using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.ApiDtos
{
    public class ReadBlogTemplate : IReturn<ReadBlogTemplateResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class ReadBlogTemplateResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public BlogTemplate Template { get; set; }
    }
}
