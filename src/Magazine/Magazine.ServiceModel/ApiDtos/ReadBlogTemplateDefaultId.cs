﻿using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.ApiDtos
{
    public class ReadBlogTemplateDefaultId : IReturn<ReadBlogTemplateDefaultIdResponse>
    {
        public ObjectId BlogId { get; set; }
    }
    public class ReadBlogTemplateDefaultIdResponse : IHasResponseStatus
    {
        public ObjectId TemplateId { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
