﻿using System;
using System.Collections.Generic;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Magazine.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.ApiDtos
{
    public class GetBlogArticleView : IReturn<GetBlogArticleViewResponse>
    {
        public ObjectId Id { get; set; }
        public string Slug { get; set; }
    }
    public class GetBlogArticleViewResponse : IHasResponseStatus
    {
        public string RenderBody { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public IList<BlogArticleInfo> ArticlesRelated { get; set; }
        /// <summary>
        /// Its not parsed when Request is comming from AJAX
        /// </summary>
        public IList<BlogArticleInfo> ArticlesMostReaded { get; set; }

        public IList<NComment> Comments { get; set; }
        public IList<NComment> LastComments { get; set; }
        public bool CommentsEnalbed { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
