﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Interfaces;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.ApiDtos
{
    public class GetBlogArticleComments : IReturn<GetBlogArticleCommentsResponse>, IPagedResult
    {
        public ObjectId ArticleId { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
    }
    public class GetBlogArticleCommentsResponse : IHasResponseStatus
    {
        public IList<NComment> Results { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
