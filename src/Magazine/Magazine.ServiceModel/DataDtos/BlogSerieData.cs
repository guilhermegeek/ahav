﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Dtos;
using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using Magazine.ServiceModel.Dtos;
using MongoDB.Bson;

namespace Magazine.ServiceModel.DataDtos
{
    public class BlogSerieData
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public long ArticlesCount { get; set; }
        public IList<BlogArticleInfo> Articles { get; set; }
        public Dictionary<string, string> Description { get; set; }
        public BlogSerieMemberAllowed MemembersAllowed { get; set; }
    }
}
