﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.DataDtos
{
    public class ReadBlogSerie : IReturn<ReadBlogSerieResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class ReadBlogSerieResponse : IHasResponseStatus
    {
        public BlogSerie Serie { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
