﻿using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using MongoDB.Bson;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.DataDtos
{
    public class ReadBlogTemplate
    {
        public ObjectId BlogId { get; set; }
    }
    public class ReadBlogTemplateResponse : IHasResponseStatus
    {
        public BlogTemplate Template { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
