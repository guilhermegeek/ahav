﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.DataDtos
{
    /// <summary>
    /// Create new blog serie
    /// </summary>
    public class CreateBlogSerie : IReturn<CreateBlogSerieResponse>
    {
        public ObjectId BlogId { get; set; }
        public ObjectId[] ArticlesId { get; set; }
        public BlogSerieMemberAllowed MemberAllowed { get; set; }
        public ObjectId UserId { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Description { get; set; }
    }

    public class CreateBlogSerieResponse : IHasResponseStatus
    {
        public BlogSerie Serie { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
