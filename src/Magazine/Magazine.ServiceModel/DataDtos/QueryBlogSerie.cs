﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Interfaces;
using Ahav.ServiceModel.Interfaces;
using Magazine.ServiceModel.Domain;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.DataDtos
{
    public class QueryBlogSerie : IReturn<QueryBlogSerieResponse>, IPagedResult
    {
        public int Skip { get; set; }
        public int Take { get; set; }
    }
    public class QueryBlogSerieResponse : IHasResponseStatus
    {
        public IList<BlogSerieData> Results { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
