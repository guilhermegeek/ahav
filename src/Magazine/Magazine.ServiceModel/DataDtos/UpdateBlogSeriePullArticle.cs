﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.DataDtos
{
    /// <summary>
    /// Pulls a article from a serie
    /// </summary>
    public class UpdateBlogSeriePullArticle : IReturn<UpdateBlogSeriePullArticleResponse>
    {
        public ObjectId SerieId { get; set; }
        public ObjectId ArticleId { get; set; }
    }
    public class UpdateBlogSeriePullArticleResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
