﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Magazine.ServiceModel.DataDtos
{
    /// <summary>
    /// Push a article to a serie
    /// </summary>
    public class UpdateBlogSeriePushArticle : IReturn<UpdateBlogSeriePushArticleResponse>
    {
        public ObjectId SerieId { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Description { get; set; }
    }
    public class UpdateBlogSeriePushArticleResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
