﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magazine.ServiceModel.DataDtos
{
    /// <summary>
    /// DTO used for Blog domain (saved in db)
    /// Acts as an reference, to avoid query(n) for categories 
    /// </summary>
    public class BlogCategoryRef
    {
        public Object Id { get; set; }
        public Dictionary<string, string> Name { get; set; }
    }
}
