﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Infrastructure;
using Ahav.ServiceInterface.Business;
using Ahav.ServiceModel.Dtos;
using Magazine.ServiceInterface.Business;
using Magazine.ServiceModel.Controllers;
using ServiceStack.ServiceInterface;
using ServiceStack.WebHost.Endpoints;

namespace Magazine.ServiceInterface.Controllers
{
    public class ArticleController : Service
    {
        public object Get(ArticleViewController request)
        {
            var response = new ArticleViewControllerResponse();
            
            // Article
            var article = ArticleBus.Get(request.Id);
            response.Article = article;

            // Template
            ContentTemplateDto template;
            if (Request.IsAjax() && request.TemplateCached != article.TemplateId) // No need to update templae
            {
                response.RenderBody = article.Content;
            }
            else if (request.TemplateCached == article.TemplateId) // No need to update the template
            {
                response.ReuseTemplate = true;
                response.RenderBody = article.Content;
            }
            else // Update template
            {
                var tpl = TemplateBus.Get(article.TemplateId);
                response.RenderBody = tpl.Body;
            }
            return response;

        }
        public ArticleBusiness ArticleBus { get; set; }
        public TemplateBusiness TemplateBus { get; set; }
    }
}
