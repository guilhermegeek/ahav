﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ahav.Infrastructure;
using Ahav.ServiceModel;
using Ahav.ServiceModel;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Operations;
using Magazine.ServiceInterface.Business;
using Magazine.ServiceInterface.Controllers;
using Magazine.ServiceInterface.Data;
using Magazine.ServiceInterface.Mappings;
using Magazine.ServiceInterface.Services;
using Magazine.ServiceInterface.Validators;
using Magazine.ServiceModel.ApiDtos;
using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Dtos;
using ServiceStack.ServiceInterface.Validation;
using ServiceStack.WebHost.Endpoints;
using ApplyTo = ServiceStack.ServiceInterface.ApplyTo;
using AutoMapper;

namespace Magazine.ServiceInterface
{
    public class MagazinePlugin : IPlugin
    {
        public void Register(IAppHost appHost)
        {
            var container = appHost.GetContainer();

            appHost.RegisterService<BlogService>();
            container.RegisterAutoWired<BlogRepository>();
            container.RegisterAutoWired<BlogBusiness>();
            appHost.RegisterService<BlogController>();

            container.RegisterAutoWired<BlogCategoryRepository>();

            appHost.RegisterService<ArticleService>();
            appHost.RegisterService<ArticleController>();
            container.RegisterAutoWired<ArticleBusiness>();
            container.RegisterAutoWired<ArticleRepository>();
            
            appHost.RegisterService<BlogTemplateService>();
            container.RegisterAutoWired<BlogTemplateRepository>();
            
            appHost.RegisterService<BlogViewsService>();
            
            appHost.RegisterService<BlogArticleCategoryService>();
            container.RegisterAutoWired<BlogPostCategoryRepository>();

            container.RegisterValidators(typeof(PostBlogValidator).Assembly);
            // AutoMapper
            ArticleDtoMap.Register();
            ArticleInfoMap.Register();
            BlogDtoMap.Register();

            // Mapps MongoDB
            BlogMappings.RegisterMappings();
            BlogCategoryMappings.RegisterMappings();
            BlogArticleMappings.RegisterMappings();
            BlogArticleCategoryMappings.RegisterMappings();
            BlogTemplateMappings.RegisterMappings();

            // Routings
            appHost.Routes
                .Add<GetBlogTemplate>(ServiceModel.MagazineRoutes.GetBlogTemplate, ApplyTo.Get.ToString())
                .Add<GetBlogArticleView>(ServiceModel.MagazineRoutes.GetArticleView, ApplyTo.Get.ToString())
                .Add<GetBlogIndexView>(ServiceModel.MagazineRoutes.GetArticlesView, ApplyTo.Get.ToString())
                .Add<GetBlog>(ServiceModel.MagazineRoutes.GetBlog, ApplyTo.Get.ToString())
                .Add<PostBlog>(ServiceModel.MagazineRoutes.PostBlog, ApplyTo.Post.ToString())
                .Add<PutBlog>(ServiceModel.MagazineRoutes.PutBlog, ApplyTo.Put.ToString())
                .Add<DeleteBlogArticle>(ServiceModel.MagazineRoutes.DeleteArticle, ApplyTo.Delete.ToString())
                .Add<PostBlogArticle>(ServiceModel.MagazineRoutes.PostBlogArticle, ApplyTo.Post.ToString())
                .Add<GetBlogArticle>(ServiceModel.MagazineRoutes.GetBlogArticle, ApplyTo.Get.ToString())
                .Add<GetBlogArticles>(ServiceModel.MagazineRoutes.GetBlogArticles, ApplyTo.Get.ToString())
                .Add<PostBlogArticleFile>(ServiceModel.MagazineRoutes.PostBlogArticleFile, ApplyTo.Post.ToString())
                .Add<PutBlogArticle>(ServiceModel.MagazineRoutes.PutBlogArticle, ApplyTo.Put.ToString())
                .Add<DeleteBlogArticleFile>(ServiceModel.MagazineRoutes.DeleteBlogArticleFile, ApplyTo.Delete.ToString())
                .Add<GetBlogTemplateDefault>(ServiceModel.MagazineRoutes.GetArticlesViewTemplate, ApplyTo.Get.ToString())
                .Add<PostArticleViewTemplate>(ServiceModel.MagazineRoutes.PostArticleViewTemplate, ApplyTo.Post.ToString())
                .Add<PutBlogTemplateDefault>(ServiceModel.MagazineRoutes.PostArticlesViewTemplate, ApplyTo.Post.ToString())
                .Add<PostBlogDefault>(ServiceModel.MagazineRoutes.PostBlogDefault, ApplyTo.Post.ToString());
        }
    }
}
