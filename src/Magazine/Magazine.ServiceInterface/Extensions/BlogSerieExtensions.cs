﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using Magazine.ServiceModel.Dtos;

namespace Magazine.ServiceInterface.Extensions
{
    public static class BlogSerieExtensions
    {
        public static BlogSerieDto ToDto(this BlogSerie entity)
        {
            var culture = ServiceStackExtensions.GetCulture();
            return new BlogSerieDto
                   {
                       Id = entity.Id,
                       Name = entity.Name,
                       Description = entity.Description.Any(x => x.Key == culture)
                       ? entity.Description.First(x => x.Key == culture).Value
                       : entity.Description.FirstOrDefault().Value
                   };
        }
    }
}
