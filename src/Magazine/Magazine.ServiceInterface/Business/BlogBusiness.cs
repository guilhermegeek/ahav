﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Common;
using AutoMapper;
using Magazine.ServiceInterface.Business.Validators;
using Magazine.ServiceInterface.Data;
using Magazine.ServiceModel.BusinessDtos;
using Magazine.ServiceModel.Domain.Entities;
using Magazine.ServiceModel.Dtos;
using Magazine.ServiceModel.Resources;
using MongoDB.Bson;
using ServiceStack.Common.Extensions;

namespace Magazine.ServiceInterface.Business
{
    public class BlogBusiness
    {
        public CreateBlogResponse Create(CreateBlog request)
        {
            
            var entity = BlogRepo.CreateEntity(new Blog
                                               {
                                                   Name = request.Name,
                                                   Owner = request.UserId,
                                                   SubDomain = request.Domain,
                                                   Title = new Dictionary<string, string> {{Culture.pt_PT, BlogResources.CreateBlogDefault_Title}},
                                                   Description = new Dictionary<string, string> {{Culture.pt_PT, BlogResources.CreateBlogDefault_Description}},
                                                   ArticlesIds = new ObjectId[0],
                                                   CategoriesIds = new ObjectId[0]
                                               });
            return new CreateBlogResponse
                   {
                       Blog = Mapper.Map<Blog, BlogDto>(entity)
                   };
        }
        //public IValidator<CreateBlogValidator> CreateBlogValid { get; set; }
        public BlogRepository BlogRepo { get; set; }
        public BlogCategoryRepository BlogCategoryRepo { get; set; }
    }
}
