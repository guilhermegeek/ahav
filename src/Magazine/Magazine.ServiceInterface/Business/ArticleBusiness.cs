﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Filters;
using Ahav.ServiceModel.Common;
using AutoMapper;
using Magazine.ServiceInterface.Data;
using Magazine.ServiceModel.BusinessDtos;
using Magazine.ServiceModel.Domain.Entities;
using Magazine.ServiceModel.Dtos;
using MongoDB.Bson;
using MongoDB.Driver.Builders;

namespace Magazine.ServiceInterface.Business
{
    public class ArticleBusiness 
    {
        [LoggedIn]
        [RequestHasAppId]
        public CreateArticleResponse Create(CreateArticle request)
        {
            var response = new CreateArticleResponse();
            var creator = UserRepo.GetCreator(request.CreatorId);
            var entity = ArticleRepo.CreateEntity(new Article
                                                  {
                                                      Title = new Dictionary<string, string> {{Culture.pt_PT, "Criado Automáticamente"}},
                                                      Body = new Dictionary<string, string> {{Culture.pt_PT, "Criado Automáticamente"}},
                                                      TemplateId = request.TemplateId,
                                                      Creator = creator,
                                                      Slug = request.Slug,
                                                      State = BlogArticleState.Created,
                                                      Categories = request.Categories,
                                                      BlogId = request.BlogId,
                                                      AppId = request.AppId
                                                  });
            BlogRepo.SaveBlogArticle(request.BlogId, entity.Id);
            response.Article = Mapper.Map<Article, BlogArticleDto>(entity);
            return response;
        }

        public SaveArticleResponse Save(SaveArticle request)
        {
            var response = new SaveArticleResponse();
            var update = Update<Article>
                .Set(x => x.Title, request.Title)
                .Set(x => x.Body, request.Body)
                .Set(x => x.TemplateId, request.TemplateId);
            var updateResult = ArticleRepo.Save(request.Id, update);
            return response;
        }

        public BlogArticleDto Get(ObjectId Id)
        {
            var article = ArticleRepo.Get(Id);
            return Mapper.Map<Article, BlogArticleDto>(article);
        }
        public ArticleRepository ArticleRepo { get; set; }
        public BlogRepository BlogRepo { get; set; }
        public UserRepository UserRepo { get; set; }
    }
}
