﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magazine.ServiceModel.BusinessDtos;
using ServiceStack.FluentValidation;

namespace Magazine.ServiceInterface.Business.Validators
{
    public class CreateBlogValidator: AbstractValidator<CreateBlog>
    {
        public CreateBlogValidator()
        {
            RuleFor(x => x.Domain).NotEmpty();
            RuleFor(x => x.Name).NotEmpty();
        }
    }
}
