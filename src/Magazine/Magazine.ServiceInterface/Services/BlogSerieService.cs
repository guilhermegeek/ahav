﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magazine.ServiceInterface.Data;
using ServiceStack.ServiceInterface;

namespace Magazine.ServiceInterface.Services
{
    /// <summary>
    /// Blog Serie are used as journey or campaign in blogging. For example, several parts, etc
    /// Assignment of articles to a serie is done when creating or updating a article
    /// </summary>
    public class BlogSerieService : Service
    {
        public BlogSerieRepository SerieRepo { get; set; }
    }
}
