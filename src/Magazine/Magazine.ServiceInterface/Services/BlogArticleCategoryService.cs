﻿using Ahav.Infrastructure;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Filters;
using Magazine.Extensions;
using Magazine.ServiceInterface.Data;
using Magazine.ServiceModel.ApiDtos;
using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using ServiceStack.Common.Web;
using ServiceStack.ServiceInterface;

namespace Magazine.ServiceInterface.Services
{
    public class BlogArticleCategoryService : Service
    {
        public BlogRepository BlogRepo { get; set; }
        public BlogCategoryRepository CategoryRepo { get; set; }
        public BlogPostCategoryRepository PostCategoryRepo { get; set; }
        public ArticleRepository ArticleRepo { get; set; }
        public ApplicationAhavRepository ProjectRepo { get; set; }
        public SlugRepository SlugRepo { get; set; }
        public TemplateRepository TemplateRepo { get; set; }


        /// <summary>
        /// Returns the category information with articles
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetBlogArticleCategoryResponse Get(GetBlogArticleCategory request)
        {
            var response = new GetBlogArticleCategoryResponse();
            response.Categories = PostCategoryRepo.GetCategories();
            return response;
        }

        /// <summary>
        /// Add a category to post
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public PostBlogArticleCategoryResponse Post(PostBlogArticleCategory request)
        {
            var response = new PostBlogArticleCategoryResponse();
            var category = new BlogArticleCategory { Title = request.Title };
            var postResponse = PostCategoryRepo.Create(category);
            if (!postResponse.Ok)
                throw HttpError.Conflict("Internal");
            response.Category = category.ToDto();
            return response;
        }

        /// <summary>
        /// Delete blog post
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public DeleteBlogArticleCategoryResponse Delete(DeleteBlogArticleCategory request)
        {
            var articleIds = PostCategoryRepo.GetArticleIds(request.Id);
            ArticleRepo.RemoveCategory(articleIds, request.Id);
            PostCategoryRepo.RemoveCategory(request.Id);
            return new DeleteBlogArticleCategoryResponse();
        }

        /// <summary>
        /// Creates a new category in blog 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [LoggedIn]
        public PostBlogCategoryResponse Post(PostBlogCategory request)
        {
            var response = new PostBlogCategoryResponse();
            var category = new BlogCategory
            {
                Owner = Request.GetUserId(),
                Description = request.Description,
                Name = request.Name
            };
            var result = CategoryRepo.Create(category);
            if (!result.Ok)
                throw HttpError.Conflict(result.ErrorMessage);

            response.Category = category;
            return response;
        }
    }
}
