﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Filters;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceInterface.Infrasctructure.Exceptions;
using Ahav.ServiceInterface.Services;
using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.DataDtos;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Operations;
using Ahav.ServiceModel.Resources;
using Magazine.Extensions;
using Magazine.ServiceInterface.Business;
using Magazine.ServiceInterface.Data;
using Magazine.ServiceInterface.Validators;
using Magazine.ServiceModel.ApiDtos;
using Magazine.ServiceModel.BusinessDtos;
using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using Magazine.ServiceModel.Dtos;
using ServiceStack.Common.Web;
using ServiceStack.ServiceInterface;

namespace Magazine.ServiceInterface.Services
{
    public class ArticleService : Service
    {
        /// <summary>
        /// Returns a post by id
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetBlogArticleResponse Get(GetBlogArticle request)
        {
            Article post;
            if (!request.Id.IsEmpty())
                post = BlogPostRepo.Get(request.Id);
            else if (string.IsNullOrEmpty(request.Slug))
            {
                var id = SlugRepo.GetId(Request.GetProjectId(), SlugType.Article, request.Slug);
                post = BlogPostRepo.Get(id);
            }
            else throw HttpError.NotFound("Post dont found");

            var response = new GetBlogArticleResponse
                               {
                                   Article = post.ToDto()
                               };
            return response;
        }

        /// <summary>
        /// Find posts
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetBlogArticlesResponse Get(GetBlogArticles request)
        {
            var response = new GetBlogArticlesResponse();

            response.Articles = BlogPostRepo.GetArticles(request.BlogId, request.Category, 0, 200).ToList();
            return response;
        }

        /// <summary>
        /// Creates a new post
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [LoggedIn(Roles = new[] {BlogRole.Administrator, BlogRole.Writter})]
        [RequestHasAppId,]
        public PostBlogArticleResponse Post(PostBlogArticle request)
        {
            var response = new PostBlogArticleResponse();

            var post = ArticleBus.Create(new CreateArticle
                                         {
                                             BlogId = request.BlogId,
                                             Slug = request.SubDomain,
                                             Categories = request.Categories,
                                             CreatorId = Request.GetUserId(),
                                             TemplateId = request.TemplateId,
                                             AppId = Request.GetProjectId()
                                         });
            // Validate the request blog
            if (request.BlogId.IsEmpty())
            {
                response.ResponseStatus = AhavExtensions.InvalidOperation("Blog not set");
                Response.StatusCode = (int) HttpStatusCode.BadRequest;
                return response;
            }

            
            BlogRepo.SaveBlogArticle(request.BlogId, post.Article.Id);
            
            if (post.Article.Categories != null && post.Article.Categories.Any())
            {
                foreach (var category in post.Article.Categories)
                {
                    PostCategoryRepo.AddPost(category, post.Article.ToRef());
                }
            }
            response.Article = post.Article;
            response.Modal = new ResponseClient
                                 {
                                     Title = BlogResources.ModalPostArticleSuccessTitle,
                                     Message = BlogResources.ModalPostArticleSuccessContent
                                 };
            // Publish
            //if (request.Publish)
            //{
            //    var canPublish = BlogPostRepo.UserCanPublish(newPost.Id, Request.GetUserId());
            //    if (!canPublish)
            //    {
            //        response.Modal = new ResponseClient
            //                             {
            //                                 Title = BlogResources.ModalPostArticleSuccessNotPublishedTitle,
            //                                 Message = BlogResources.ModalPostArticleSuccessNotPublishedContent
            //                             };
            //    }
            //    BlogPostRepo.ChangeState(newPost.Id, BlogArticleState.Published);
            //    response.Modal = new ResponseClient
            //                         {
            //                             Title = BlogResources.ModalPostArticleAndPublishedSuccessTitle,
            //                             Message = BlogResources.ModalPostArticleAndPublishedSuccessContent
            //                         };
            //}


            return response;
        }


        /// <summary>
        /// Updates article
        /// The array categories are intersected
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [LoggedIn]
        [RequestHasAppId]
        public PutBlogArticleResponse Put(PutBlogArticle request)
        {
            var response = new PutBlogArticleResponse();
            var article = BlogPostRepo.Get(request.Id);
            if (article.Id.IsEmpty())
                throw new Exception("Article not found");

            var req = new SaveArticle
                          {
                              Id = request.Id,
                              Title = request.Title,
                              Headline = request.Headline,
                              Body = request.Body,
                              TemplateId = request.TemplateId
                              //Tags = request.Tags,
                          };
            ArticleBus.Save(req);
            //if (request.Slug != article.Slug)
            //{
            //    SlugRepo.AddSlug(Request.GetProjectId(), SlugType.Article, request.Id, request.Slug);
            //    SlugRepo.RemoveSlug(Request.GetProjectId(), SlugType.Article, article.Slug);
            //}
            return response;
        }

        [LoggedIn]
        [RequestHasAppId]
        public PutArticleStateResponse Put(PutArticleState request)
        {
            var response = new PutArticleStateResponse();
            BlogPostRepo.UpdateState(request.ArticleId, request.ArticleState);
            return response;
        }

        [LoggedIn]
        [RequestHasAppId]
        public DeleteBlogArticleResponse Delete(DeleteBlogArticle request)
        {
            var response = new DeleteBlogArticleResponse();
            string slug = BlogPostRepo.Get(request.ArticleId).Slug;
            BlogPostRepo.Delete(request.ArticleId);
            SlugRepo.RemoveSlug(Request.GetProjectId(), SlugType.Article, slug);
            return response;
        }

        /// <summary>
        /// Deletes a comment
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public DeleteBlogArticleCommentResponse Any(DeleteBlogArticleComment request)
        {
            var response = new DeleteBlogArticleCommentResponse();
            BlogPostRepo.DeleteComment(request.PostId, request.CommentId);
            return response;
        }



        [LoggedIn]
        [RequestHasAppId]
        public PostBlogArticleFileResponse Post(PostBlogArticleFile request)
        {
            var response = new PostBlogArticleFileResponse();
            if (!RequestContext.Files.Any())
            {
                response.ResponseStatus = AhavExtensions.InvalidOperation();
                Response.StatusCode = (int) HttpStatusCode.BadRequest;
                return response;
            }
            using (var mediaService = base.ResolveService<MediaService>())
            {
                var res = mediaService.Post(new PostMedia {Files = RequestContext.Files, Thumbnaill = true});
                res.Medias.ToList().ForEach(x => response.Medias.Add(x));
            }
            foreach (var media in response.Medias)
            {
                BlogPostRepo.AddFile(request.ArticleId, media.Id);
            }
            return response;
        }

        [LoggedIn]
        [RequestHasAppId]
        public DeleteBlogArticleFileResponse Delete(DeleteBlogArticleFile request)
        {
            var response = new DeleteBlogArticleFileResponse();
            BlogPostRepo.RemoveFile(request.BlogId, request.FileId);
            GridFs.RemoveFile(request.FileId);
            return response;
        }

        public PostBlogArticleCommentResponse Post(PostBlogArticleComment request)
        {
            if (!Request.IsAuthenticated() && BlogPostRepo.VisitorsCanComment(request.ArticleId))
            {
                throw new ApiErrorException(BlogCodeErrors.VisitorsCantComment.ToString(),
                    BlogResources.VisitorsCantComment);
            }
            var commentRes = CommentsRepo.CreateComment<Article>(new CreateComment
                                                    {
                                                        DocId = request.ArticleId,
                                                        Message = request.Message,
                                                        User = new AuthorInfo {Id = Request.GetUserId()}
                                                    });
            return new PostBlogArticleCommentResponse
                   {
                       Comment = commentRes.Comment
                   };
        }

        public GetBlogArticleCommentsResponse Get(GetBlogArticleComments request)
        {
            var response = new GetBlogArticleCommentsResponse();
            var commentsRes = CommentsRepo.ReadComments<Article>(new ReadComments
                                                                     {
                                                                         Skip = request.Skip,
                                                                         Take = request.Take
                                                                     });
            response.Results = commentsRes.Results;
            return response;
        }
        // Dependency injections
        public ArticleBusiness ArticleBus { get; set; }
        public MongoGridFs GridFs { get; set; }
        public BlogRepository BlogRepo { get; set; }
        public BlogCategoryRepository CategoryRepo { get; set; }
        public BlogPostCategoryRepository PostCategoryRepo { get; set; }
        public ArticleRepository BlogPostRepo { get; set; }
        public ApplicationAhavRepository ProjectRepo { get; set; }
        public SlugRepository SlugRepo { get; set; }
        public TemplateRepository TemplateRepo { get; set; }
        public CommentRepository CommentsRepo { get; set; }
    }
}