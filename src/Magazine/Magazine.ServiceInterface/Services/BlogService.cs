﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Filters;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceInterface.Services;
using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Resources;
using Magazine.Extensions;
using Magazine.ServiceInterface.Business;
using Magazine.ServiceInterface.Data;
using Magazine.ServiceInterface.Validators;
using Magazine.ServiceModel.ApiDtos;
using Magazine.ServiceModel.BusinessDtos;
using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using Magazine.ServiceModel.Dtos;
using ServiceStack.Common.Extensions;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.ServiceModel;
using AutoMapper;

namespace Magazine.ServiceInterface.Services
{
    public class BlogService : Service
    {
        public MediaService MediaService { get; set; }
        /// <summary>
        /// Get blog by id or slug
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetBlogResponse Get(GetBlog request)
        {
            Blog blog = null;
            if (request.Id.IsEmpty() && !string.IsNullOrEmpty(request.Slug))
            {
                var articleId = SlugRepo.GetId(Request.GetProjectId(), SlugType.Blog, request.Slug);
                blog = BlogRepo.Get(articleId);
            }
            else
            {
                blog = BlogRepo.Get(request.Id);
            }

            var response = new GetBlogResponse
                               {
                                   Blog = blog.ToDto()
                               };
            return response;
        }

        /// <summary>
        /// Get blogs
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetBlogsResponse Get(GetBlogs request)
        {
            var response = new GetBlogsResponse();
            response.Blogs = BlogRepo.GetAll(request.CategoriesIds, request.OwnersIds, 0, 100).ToList();
            return response;
        }
        /// <summary>
        /// The default blog is available from /blog
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public PostBlogDefaultResponse Post(PostBlogDefault request)
        {
            var response = new PostBlogDefaultResponse();
            BlogRepo.SetBlogDefault(Request.GetProjectId(), request.BlogId);
            return response;
        }
        /// <summary>
        /// Creates a new blog for the authenticated user
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [LoggedIn]
        [RequestHasAppId]
        public PostBlogResponse Post(PostBlog request)
        {
            var response = new PostBlogResponse();
            if (!BlogRepo.BlogIsEnabled())
            {
                response.ResponseStatus = new ResponseStatus
                                          {
                                              ErrorCode = BlogCodeErrors.BlogsNotAvailable.ToString(),
                                              Message = "Right now, you can't create blogs!"
                                          };


                return response;
            }
            if (!SlugRepo.SlugIsAvailable(Request.GetProjectId(), SlugType.Blog, request.SubDomain))
            {
                response.ResponseStatus = AhavExtensions.InvalidOperation(string.Format("O Url {0} já está a ser usado!", request.SubDomain));
            }
            var blogs = ProjectRepo.GetBlogs(Request.GetProjectId());
            var blogRes = BlogBus.Create(new CreateBlog
                                      {
                                          Domain = request.SubDomain,
                                          Name = request.Name,
                                          TemplateId = request.TemplateId,
                                          UserId = Request.GetUserId()
                                      });
            var blog = blogRes.Blog;
            // It's the first blog for this project, set as default
            if(blogs == null || !blogs.Any())
            {
                BlogRepo.SetBlogDefault(Request.GetProjectId(), blog.Id);
            }

            SlugRepo.AddSlug(Request.GetProjectId(), SlugType.Blog, blog.Id, request.SubDomain);
            // Save this blog in blog category
            blog.Categories.ForEach(x => CategoryRepo.AddBlog(x, new BlogInfo { Id = blog.Id, Name = blog.Name, SubDomain = blog.SubDomain}));
            // Update project Collection
            ProjectRepo.SaveBlog(Request.GetProjectId(), blog.Id);
            // Save Blog as default
            response.Id = blog.Id;
            response.Modal = AhavExtensions.Success(BlogResources.ModalPostBlogOk);
            response.Result = blog;
            return response;
        }
        /// <summary>
        /// Update a Blog
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [LoggedIn]
        [RequestHasAppId]
        public PutBlogResponse Put(PutBlog request)
        {
            var response = new PutBlogResponse();
            var req = new UpdateBlog
                          {
                              Id = request.BlogId,
                              Name = request.Name,
                              Title = request.Title,
                              Description = request.Description,
                              Categories = request.Categories,
                              SubDomain = request.SubDomain
                          };

            var res = BlogRepo.Update(req);
            if (res.ResponseStatus != null)
            {
                response.ResponseStatus = AhavExtensions.InternalServerError();
                return response;
            }
            return response;
        }
        /// <summary>
        /// This dependencies are injected
        /// </summary>
        public BlogRepository BlogRepo { get; set; }
        public BlogBusiness BlogBus { get; set; }
        public BlogCategoryRepository CategoryRepo { get; set; }
        public BlogPostCategoryRepository PostCategoryRepo { get; set; }
        public ArticleRepository BlogPostRepo { get; set; }
        public ApplicationAhavRepository ProjectRepo { get; set; }
        public SlugRepository SlugRepo { get; set; }
        public MongoGridFs GridFs { get; set; }
        public TemplateRepository TemplateRepo { get; set; }

        //public PostArticleViewTemplateResponse Post(PostArticleViewTemplate request)
        //{
        //    var response = new PostArticleViewTemplateResponse();
        //    BlogRepo.SetArticleViewTemplate(request.BlogId, request.TemplateId);
        //    return response;
        //}
        //public PostArticlesViewTemplateResponse Post(PostArticlesViewTemplate request)
        //{
        //    var response = new PostArticlesViewTemplateResponse();
        //    BlogRepo.SetArticlesViewTemplate(request.BlogId, request.TemplateId);
        //    return response;
        //}
        //public GetArticlesViewTemplateResponse Get(GetArticlesViewTemplate request)
        //{
        //    var response = new GetArticlesViewTemplateResponse();
        //    var templateId = BlogRepo.GetArticlesviewTemplate(request.BlogId);
        //    if(templateId.IsEmpty())
        //    {
        //        response.ResponseStatus = AhavExtensions.InvalidOperation("No template for the blog", "NoTemplate");
        //        Response.StatusCode = (int)HttpStatusCode.BadRequest;
        //        return response;
        //    }
        //    if(request.ReturnTemplates)
        //    {
        //        using(var templateService = base.ResolveService<TemplateService>())
        //        {
        //            var tpls = templateService.Get(new GetTemplatesAvailable());
        //            if(tpls.ResponseStatus!=null)
        //            {
        //                response.Templates = tpls.Templates;
        //            }
        //        }
        //    }
        //    response.TemplateId = templateId;
        //    return response;
        //}

    }
}