﻿using System;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceModel.ApiDtos;
using Magazine.Extensions;
using Magazine.ServiceInterface.Data;
using Magazine.ServiceModel.ApiDtos;
using MongoDB.Bson;
using ServiceStack.Common.Web;
using ServiceStack.ServiceInterface;

namespace Magazine.ServiceInterface.Services
{
    public class BlogViewsService : Service
    {
        public BlogRepository BlogRepo { get; set; }
        public TemplateRepository TemplateRepo { get; set; }
        public BlogCategoryRepository CategoryRepo { get; set; }
        public BlogPostCategoryRepository PostCategoryRepo { get; set; }
        public ArticleRepository BlogPostRepo { get; set; }
        public BlogTemplateRepository BlogTemplateRepo { get; set; }
        public ApplicationAhavRepository ProjectRepo { get; set; }
        public SlugRepository SlugRepo { get; set; }

        /// <summary>
        /// Controller for view used to display index of Blog
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception"></exception>
        public object Get(GetBlogIndexView request)
        {
            var response = new GetBlogIndexViewResponse();
            ObjectId blogId = BlogRepo.GetBlogDefault(Request.GetProjectId());

            var res = base.ResolveService<ArticleService>().Get(new GetBlogArticles {BlogId = blogId});
            if (res.ResponseStatus != null)
                throw new Exception(res.ResponseStatus.Message);
            response.Articles = res.Articles;
            if (!Request.IsAjax())
            {
                var tplId = BlogTemplateRepo.GetDefaultTemplateId(new ReadBlogTemplateDefaultId
                                                                      {
                                                                          BlogId = blogId
                                                                      });
                var tpl =
                    base.ResolveService<BlogTemplateService>().Get(new GetBlogTemplate {TemplateId = tplId.TemplateId});
                response.RenderBody = tpl.Template.BodyViewIndex;
            }
            return new HttpResult(response)
                       {
                           View = "BlogIndex",
                           Template = Request.IsAjax() ? "" : "_LayoutForTemplate"
                       };
        }

        /// <summary>
        /// Controller for Article View
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public object Get(GetBlogArticleView request)
        {
            var response = new GetBlogArticleViewResponse();
            

            var article = BlogPostRepo.Get(request.Id);
            var dto = article.ToDto();
            response.Title = dto.Title;
            response.Body = dto.Content;
            
            // Increment view stat
            BlogPostRepo.IncrementViewed(request.Id);
            
            if (!Request.IsAjax())
            {
                var tplRes = BlogTemplateRepo.ReadTemplate(new ReadBlogTemplate
                                                                {
                                                                    Id = article.TemplateId
                                                                });
                var tplDto = tplRes.Template.ToDto();
                response.RenderBody = tplDto.BodyViewArticle;
            }
            else
            {
                response.RenderBody = article.ToDto().Content;
            }

            return new HttpResult(response)
            {
                View = "BlogArticle",
                Template = Request.IsAjax()
                               ? ""
                               : "_LayoutForTemplate"
            };
        }
    }
}
