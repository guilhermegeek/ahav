﻿using System.Collections.Generic;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceModel;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Resources;
using Magazine.Extensions;
using Magazine.ServiceInterface.Data;
using Magazine.ServiceModel.ApiDtos;
using ServiceStack.ServiceInterface;

namespace Magazine.ServiceInterface.Services
{
    public class BlogTemplateService : Service
    {
        public BlogRepository BlogRepo { get; set; }
        public ContentRepository ContentRepo { get; set; }
        public BlogCategoryRepository CategoryRepo { get; set; }
        public BlogPostCategoryRepository PostCategoryRepo { get; set; }
        public ArticleRepository BlogPostRepo { get; set; }
        public BlogTemplateRepository BlogTemplateRepo { get; set; }
        public ApplicationAhavRepository ProjectRepo { get; set; }
        public SlugRepository SlugRepo { get; set; }

        /// <summary>
        /// Get blog template
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetBlogTemplateResponse Get(GetBlogTemplate request)
        {
            var tpl = BlogTemplateRepo.ReadTemplate(new ReadBlogTemplate {Id = request.TemplateId});
            return new GetBlogTemplateResponse
                       {
                           Template = tpl.Template.ToDto()
                       };
        }

        /// <summary>
        /// Set the default templated to be used for the given blog at /blog
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public PutBlogTemplateDefaultResponse Put(PutBlogTemplateDefault request)
        {
            BlogTemplateRepo.SetDefaultTemplate(new UpdateBlogTemplateDefault
                                                    {
                                                        BlogId = request.BlogId,
                                                        BlogTemplateId = request.TemplateId
                                                    });
            return new PutBlogTemplateDefaultResponse();
        }
        /// <summary>
        /// Returns the default template for blog
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetBlogTemplateDefaultResponse Get(GetBlogTemplateDefault request)
        {
            var tplId = BlogTemplateRepo.GetDefaultTemplateId(new ReadBlogTemplateDefaultId { BlogId = request.BlogId });
            var template = BlogTemplateRepo.ReadTemplate(new ReadBlogTemplate {Id = tplId.TemplateId});
            
            return new GetBlogTemplateDefaultResponse
                       {
                           Template = template.Template.ToDto(),
                           TemplateId = template.Template.Id
                       };
        }
        
        /// <summary>
        /// Clones a existing Blog Template
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public PostBlogTemplateCloneResponse Post(PostBlogTemplateClone request)
        {
            var dataRes = BlogTemplateRepo.ReadTemplate(new ReadBlogTemplate { Id = request.BlogTemplateId});
            var dataReq = new CreateBlogTemplate
                              {
                                  Body = dataRes.Template.Body,
                                  BodyViewArticle = dataRes.Template.BodyViewArticle,
                                  BodyViewIndex = dataRes.Template.BodyViewIndex,
                              };
            if(request.Public)
            {
                dataReq.Public = true;
                dataReq.ProjectId = Request.GetProjectId();
            }
            var addRes = BlogTemplateRepo.AddTemplate(dataReq);
            if(!request.Public)
            {
                ProjectRepo.SaveBlogTemplate(Request.GetProjectId(), request.BlogTemplateId);
            }
            return new PostBlogTemplateCloneResponse {BlogTemplateId = addRes.BlogTemplate.Id};
        }
        /// <summary>
        /// Creates a new empty Template
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public PostBlogTemplateBlankResponse Post(PostBlogTemplateBlank request)
        {
            var dataReq = new CreateBlogTemplate
                              {
                                  Body =
                                      new Dictionary<string, string>
                                          {{Culture.pt_PT, BlogDefaultTemplateResources.Body}},
                                  Name = request.Name,
                                  BodyViewArticle = new Dictionary<string, string> { { Culture.pt_PT, BlogDefaultTemplateResources.BodyViewArticle} },
                                  BodyViewIndex = new Dictionary<string, string> { { Culture.pt_PT, BlogDefaultTemplateResources.BodyViewIndex} }
                              };
            if(request.Public)
            {
                dataReq.Public = true;
                dataReq.ProjectId = Request.GetProjectId();
            }
            var dataRes = BlogTemplateRepo.AddTemplate(dataReq);
            ProjectRepo.SaveBlogTemplate(Request.GetProjectId(), dataRes.BlogTemplate.Id);
            if(request.Public)
            {
                ProjectRepo.SaveBlogTemplate(Request.GetProjectId(), dataRes.BlogTemplate.Id);
            }
            return new PostBlogTemplateBlankResponse();
        }
        /// <summary>
        /// Updates a template
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public PutBlogTemplateResponse Put(PutBlogTemplate request)
        {
            var response = new PutBlogTemplateResponse();
            var dataReq = new UpdateBlogTemplate
                              {
                                  Id = request.Id,
                                  Body = request.Body,
                                  BodyViewArticle = request.BodyViewArticle,
                                  BodyViewIndex = request.BodyViewIndex,
                                  Metadata = request.Metadata,
                                  Name = request.Name,
                                  Public = request.Public
                              };
            BlogTemplateRepo.UpdateTemplate(dataReq);
            return response;
        }
        
    }
}
