﻿using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;

namespace Magazine.ServiceInterface.Mappings
{
    public static class BlogArticleMappings
    {
        public static void RegisterMappings()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(Article)))
            {
                BsonClassMap.RegisterClassMap<Article>(cm =>
                {
                    cm.MapIdField(x => x.Id).SetIdGenerator(ObjectIdGenerator.Instance);
                    cm.MapProperty(x => x.BlogId).SetIsRequired(true);
                    cm.MapProperty(x => x.AppId).SetIsRequired(true);
                    cm.MapProperty(x => x.Files);
                    cm.MapProperty(x => x.Title).SetIsRequired(true);
                    cm.MapProperty(x => x.Headline);
                    cm.MapProperty(x => x.Categories);
                    cm.MapProperty(x => x.Body).SetIsRequired(true);
                    cm.MapProperty(x => x.Tags);
                    cm.MapProperty(x => x.CreatedBy).SetIsRequired(true);
                    cm.MapProperty(x => x.CommentsCount);
                    cm.MapProperty(x => x.CommentsPageCount);
                    cm.MapProperty(x => x.Slug);
                    cm.MapProperty(x => x.TemplateId);
                    cm.MapProperty(x => x.State);
                });
            }
        }
    }
    }