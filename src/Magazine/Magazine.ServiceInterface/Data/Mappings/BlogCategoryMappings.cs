﻿using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;

namespace Magazine.ServiceInterface.Mappings
{
    public static class BlogCategoryMappings
    {
        public static void RegisterMappings()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(BlogCategory)))
            {
                BsonClassMap.RegisterClassMap<BlogCategory>(cm =>
                {
                    cm.MapIdField(x => x.Id)
                        .SetIdGenerator(ObjectIdGenerator.Instance);
                    cm.MapProperty(x => x.Owner);
                    cm.MapProperty(x => x.Blogs);
                    cm.MapProperty(x => x.Name);
                });
            }
        }
    }
}
