﻿using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;

namespace Magazine.ServiceInterface.Mappings
{
    public static class BlogMappings
    {
        public static void RegisterMappings()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(Blog)))
            {
                BsonClassMap.RegisterClassMap<Blog>(cm =>
                {
                    cm.MapIdField(x => x.Id)
                        .SetIdGenerator(ObjectIdGenerator.Instance);
                    cm.MapProperty(x => x.Name);
                    cm.MapProperty(x => x.Title);
                    cm.MapProperty(x => x.Description);
                    cm.MapProperty(x => x.Name);
                    cm.MapProperty(x => x.Owner);
                    cm.MapProperty(x => x.ArticlesIds);
                    cm.MapProperty(x => x.SubDomain);
                    cm.MapProperty(x => x.CategoriesIds);
                });
            }
        }
    }
}
