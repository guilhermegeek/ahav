﻿using Magazine.ServiceModel;
using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;

namespace Magazine.ServiceInterface.Mappings
{
   public static class BlogArticleCategoryMappings
    {
       public static void RegisterMappings()
       {
           if (!BsonClassMap.IsClassMapRegistered(typeof(BlogArticleCategory)))
           {
               BsonClassMap.RegisterClassMap<BlogArticleCategory>(cm =>
               {
                   cm.MapIdField(x => x.Id).SetIdGenerator(ObjectIdGenerator.Instance);
                   cm.SetDiscriminator(ArticleBase.BlogArticle);
                   cm.MapProperty(x => x.Title);
                   cm.MapProperty(x => x.PostsCount);
                   cm.MapProperty(x => x.Posts);
               });
           }
       }
    }
}
