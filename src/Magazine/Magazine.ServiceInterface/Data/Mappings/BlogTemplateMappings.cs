﻿using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;

namespace Magazine.ServiceInterface.Mappings
{
    public static class BlogTemplateMappings
    {
        public static void RegisterMappings()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(BlogTemplate)))
            {
                BsonClassMap.RegisterClassMap<BlogTemplate>(cm =>
                {
                    cm.MapIdProperty(x => x.Id).SetIdGenerator(
                        ObjectIdGenerator.Instance);
                    cm.MapProperty(x => x.Name);
                    cm.MapProperty(x => x.Body);
                    cm.MapProperty(x => x.BodyViewIndex);
                    cm.MapProperty(x => x.BodyViewArticle);
                    cm.MapProperty(x => x.Metadata);
                });
            }
        }
    }
}
