﻿using Ahav.Infrastructure;
using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using Magazine.ServiceModel.Dtos;
using MongoDB.Bson;
using MongoDB.Driver.Builders;

namespace Magazine.ServiceInterface.Data
{
    public class BlogCategoryRepository : Repository<BlogCategory>
    {
        public void AddBlog(ObjectId categoryId, ObjectId blogId)
        {
            collection.Update(Query<BlogCategory>.EQ(x => x.Id, categoryId),
                              Update<BlogCategory>.Push(x => x.BlogsId, blogId));
        }
        public void AddBlog(ObjectId categoryId, BlogInfo blog)
        {
            collection.Update(Query<BlogCategory>.EQ(x => x.Id, categoryId),
                              Update<BlogCategory>.Push(x => x.Blogs, blog));
        }
    }
}
