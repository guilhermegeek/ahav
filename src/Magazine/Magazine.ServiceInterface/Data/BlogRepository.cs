﻿using System.Collections.Generic;
using System.Linq;
using Ahav.Infrastructure;
using Magazine.Extensions;
using Magazine.ServiceModel.ApiDtos;
using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using Magazine.ServiceModel.Dtos;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace Magazine.ServiceInterface.Data
{
    public class BlogRepository : Repository<Blog>
    {
        public const string BlogRedisDefault = "p::{0}::b:d";
        /// <summary>
        /// Indicates wether new blogs can be created or not (ie: for space reasons, hacking in course, etc)
        /// </summary>
        public const string BlogRedisEnabled = "b::e::{0}";
        
        public const string RedisArticle = "a::{0}::{1}";
        public const string RedisBlogTemplateDefault = "b::a::t::{0}";
        //public const string RedisArticleViewTemplate = "b::av::t::{0}";
        public IMongoFields BlogInfoFields = Fields<Blog>.Include(x => x.Id,x => x.SubDomain, x =>x.Name);

        public void SetBlogDefault(ObjectId projectId, ObjectId blogId)
        {
            using(var redisClient = RedisManager.GetClient())
            {
                redisClient.Set(string.Format(BlogRedisDefault, projectId), blogId);
            }
        }
        public ObjectId GetBlogDefault(ObjectId projectId)
        {
            using(var redisClient = RedisManager.GetClient())
            {
                return redisClient.Get<ObjectId>(string.Format(BlogRedisDefault, projectId));
            }
        }
        ///// <summary>
        ///// Save the template used to render the article view
        ///// </summary>
        ///// <param name="blogId"></param>
        ///// <param name="templateId"></param>
        //public void SetArticleViewTemplate(ObjectId blogId, ObjectId templateId)
        //{
        //    using(var redisClient = RedisManager.GetClient())
        //    {
        //        redisClient.Set(string.Format(RedisArticleViewTemplate, blogId), templateId);
        //    }
        //}
        ///// <summary>
        ///// Get the template id for blog article view
        ///// </summary>
        ///// <param name="blogId"></param>
        ///// <returns></returns>
        //public ObjectId GetArticleViewTemplate(ObjectId blogId)
        //{
        //    using(var redisClient = RedisManager.GetClient())
        //    {
        //        return redisClient.Get<ObjectId>(string.Format(RedisArticleViewTemplate, blogId));
        //    }
        //}
        /// <summary>
        /// Save the template used to render home of Blog
        /// </summary>
        /// <param name="blogId"></param>
        /// <param name="templateId"></param>
        //public void UpdateBlogTemplateDefault(ObjectId blogId, ObjectId templateId)
        //{
        //    using(var redisClient = RedisManager.GetClient())
        //    {
        //        redisClient.Set(string.Format(RedisBlogTemplateDefault, blogId), templateId);
        //    }
        //}
        //public ObjectId GetBlogTemplateDefault(ObjectId blogId)
        //{
        //    using(var redisClient = RedisManager.GetClient())
        //    {
        //        return redisClient.Get<ObjectId>(string.Format(RedisBlogTemplateDefault, blogId));
        //    }
        //}

        public bool BlogIsEnabled()
        {
            return true;
            using (var client = RedisManager.GetClient())
                return client.Get<bool>(BlogRedisEnabled);
        }
        public UpdateBlogResponse Update(UpdateBlog request)
        {
            var response = new UpdateBlogResponse();
            var res = collection.Update(Query<Blog>.EQ(x => x.Id, request.Id),
                              Update<Blog>.Set(x => x.Title, request.Title)
                                  .Set(x => x.Name, request.Name)
                                  .Set(x => x.Description, request.Description)
                                  .Set(x => x.CategoriesIds, request.Categories)
                                  .Set(x => x.SubDomain, request.SubDomain));
            if(!res.Ok)
            {
                response.ResponseStatus = AhavExtensions.DataInternalError(res.ErrorMessage);
            }
            return response;
        }

        public IEnumerable<BlogInfo> GetAll(ObjectId[] categories, ObjectId[] ownersIds, int skip, int take)
        {
            var queryBuilder = new List<IMongoQuery>();
            var blogs = new List<Blog>();
            var blogsInfo = new List<BlogInfo>();

            if(categories!=null && categories.Any())
                queryBuilder.Add(Query<Blog>.In(x => x.CategoriesIds, categories));
            if(ownersIds!=null && ownersIds.Any())
                queryBuilder.Add(Query<Blog>.In(x => x.Owner, ownersIds));
            
            if (queryBuilder.Any())
                blogs = collection
                    .FindAs<Blog>(Query.And(queryBuilder))
                    .SetFields(BlogInfoFields)
                    .SetLimit(take)
                    .SetSkip(skip).ToList();
            else
                blogs =  collection
                    .FindAllAs<Blog>()
                    .SetFields(BlogInfoFields)
                    .SetLimit(take)
                    .SetSkip(skip).ToList();
            blogs.ForEach(x => blogsInfo.Add(x.ToInfo()));
            return blogsInfo;
        }

        public void SaveBlogArticle(ObjectId blogId, ObjectId articleId)
        {
            collection.Update(Query<Blog>.EQ(x => x.Id, blogId),
                Update<Blog>.Push(x => x.ArticlesIds, articleId));
        }

        public void RemoveBlogArticle(ObjectId blogId, ObjectId articleId)
        {
            collection.Update(Query<Blog>.EQ(x => x.Id, blogId),
                Update<Blog>.Pull(x => x.ArticlesIds, articleId));
        }
    }
}
