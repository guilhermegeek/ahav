﻿using System.Linq;
using Ahav.Interfaces;
using Magazine.ServiceModel.DataDtos;
using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ServiceStack.Redis;

namespace Magazine.ServiceInterface.Data
{
    /// <summary>
    /// Blog Serie data layer
    /// </summary>
    public class BlogSerieRepository : IRepository
    {
        public IRedisClientsManager RedisManager { get; set; }
        private MongoCollection<BlogSerie> serieCollection { get; set; }

        private IMongoFields SerieDataFields = Fields<BlogSerie>.Include(x => x.Id, x => x.Name, x => x.Description,
            x => x.MemembersAllowed, x => x.Articles);

        public QueryBlogSerieResponse Find(QueryBlogSerie request)
        {
            var response = new QueryBlogSerieResponse();
            var pointer = serieCollection.FindAllAs<BlogSerieData>()
                .SetSkip(request.Skip)
                .SetLimit(request.Take)
                .SetFields(SerieDataFields);
            response.Results = pointer.ToList();
            return response;
        }

        public CreateBlogSerieResponse CreateSerie(CreateBlogSerie request)
        {
            var entity = new BlogSerie
                         {
                             Name = request.Name,
                             Description = request.Description,
                             MemembersAllowed = request.MemberAllowed
                         };
            serieCollection.Insert(entity);
            return new CreateBlogSerieResponse {Serie = entity};
        }
    }
}
