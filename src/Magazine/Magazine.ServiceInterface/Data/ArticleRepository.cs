﻿using System.Collections.Generic;
using System.Linq;
using Ahav.Infrastructure;
using Ahav.Interfaces;
using Magazine.Extensions;
using Magazine.ServiceModel.ApiDtos;
using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using Magazine.ServiceModel.Dtos;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace Magazine.ServiceInterface.Data
{
    public class ArticleRepository : Repository<Article>, IAnalysticRepository
    {
        public static IMongoFields BlogArticleInfoFields = Fields<Article>.Include(x => x.Id, x => x.Title, x => x.Body,
                                                                                   x => x.CreatedBy, x => x.BlogId, x => x.Slug, x => x.TemplateId, x=> x.AppId);

        public const string RedisArticlesViewTemplate = "blog::avt::{0}"; // Blog Id
        public const string RedisAnalysticStatsKey = "stats::article::{0}"; 

        public void SaveFiles(ObjectId articleId, ObjectId[] files)
        {
            collection.Update(Query<Article>.EQ(x => x.Id, articleId),
                              Update<Article>.PushAll(x => x.Files, files));
        }

        //public ObjectId GetArticlesViewTemplate(ObjectId blogId)
        //{
        //    using (var redisClient = RedisManager.GetClient())
        //    {
        //        return redisClient.Get<ObjectId>(string.Format(RedisArticlesViewTemplate, blogId));
        //    }
        //}
        public UpdateArticleResponse Save(UpdateArticle request)
        {
            var result = collection.Update(Query<Article>.EQ(x => x.Id, request.ArticleId),
                                           Update<Article>.Set(x => x.Title, request.Title)
                                               .Set(x => x.Body, request.Body)
                                               //.Set(x => x.Tags, request.Tags)
                                               //.PushAll(x => x.Categories, request.Categories)
                                               .Set(x => x.Headline, request.Headline)
                                               .Set(x => x.Slug, request.Slug));
            if (!result.Ok)
                return new UpdateArticleResponse
                           {ResponseStatus = AhavExtensions.DataInternalError(result.ErrorMessage)};
            return new UpdateArticleResponse();
        }

        /// <summary>
        /// Indicathers if a visitor can comment
        /// </summary>
        public const string RedisVisitorsCanComment = "blog::{0}::vc";


        public IEnumerable<BlogArticleInfo> GetArticles(int skip, int take)
        {
            return collection.FindAllAs<BlogArticleInfo>()
                .SetFields(BlogArticleInfoFields)
                .AsEnumerable();
        }

        public IEnumerable<BlogArticleInfo> GetArticles(ObjectId? blogId, ObjectId[] categoriesIds, int skip, int take)
        {
            var queryBuilder = new List<IMongoQuery>();
            var articles = new List<Article>();
            var articlesInfo = new List<BlogArticleInfo>();

            if (blogId.HasValue)
                queryBuilder.Add(Query<Article>.EQ(x => x.BlogId, blogId));
            if (categoriesIds != null && categoriesIds.Any())
                queryBuilder.Add(Query<Article>.In(x => x.Categories, categoriesIds));
            if (queryBuilder.Any())
                articles = collection.FindAs<Article>(Query.And(queryBuilder))
                    .SetFields(BlogArticleInfoFields)
                    .ToList();
            else
                articles = collection.FindAllAs<Article>()
                    .SetFields(BlogArticleInfoFields)
                    .ToList();
            articles.ForEach(x => articlesInfo.Add(x.ToInfo()));
            return articlesInfo;
        }

        /// <summary>
        /// User can publish the post
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool UserCanPublish(ObjectId postId, ObjectId userId)
        {
            return collection.FindOneAs<Article>
                       (Query.And(Query<Article>.EQ(x => x.Id, postId),
                                  Query<Article>.EQ(x => x.CreatedBy, userId))) != null;
        }
        
        public bool VisitorsCanComment(ObjectId postId)
        {
            return RedisManager.GetClient().Get<bool>(string.Format(RedisVisitorsCanComment, postId));
        }

        public WriteConcernResult ChangeState(ObjectId postId, BlogArticleState state)
        {
            return collection.Update(Query<Article>.EQ(x => x.Id, postId),
                                     Update<Article>.Set(x => x.State, state));
        }

        public void AddFile(ObjectId articleId, ObjectId fileId)
        {
            collection.Update(Query<Article>.EQ(x => x.Id, articleId),
                              Update<Article>.Push(x => x.Files, fileId));
        }

        public void RemoveFile(ObjectId articleId, ObjectId fileId)
        {
            collection.Update(Query<Article>.EQ(x => x.Id, articleId),
                              Update<Article>.Pull(x => x.Files, fileId));
        }

        public void RemoveArticle(ObjectId articleId)
        {
            collection.Remove(Query<Article>.EQ(x => x.Id, articleId));
        }

        public void UpdateState(ObjectId articleId, BlogArticleState state)
        {
            collection.Update(Query<Article>.EQ(x => x.Id, articleId),
                              Update<Article>.Set(x => x.State, state));
        }

        public void IncrementViewed(ObjectId id)
        {
            using(var redisClient = RedisManager.GetClient())
            {
                redisClient.Increment(string.Format(RedisAnalysticStatsKey, id), 1);
            }
        }

        public long GetViews(ObjectId id)
        {
            using(var redisClient = RedisManager.GetClient())
            {
                return redisClient.Get<long>(string.Format(RedisAnalysticStatsKey, id));
            }
        }

        public void RemoveCategory(ObjectId[] articleIds, ObjectId categorieId)
        {
            collection.Update(Query<Article>.In(x => x.Id, articleIds),
                Update<Article>.Pull(x => x.Categories, categorieId));
        }
    }
}
