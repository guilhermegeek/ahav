﻿using Ahav.Infrastructure;
using Ahav.Interfaces;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceModel.Common;
using Magazine.ServiceModel.ApiDtos;
using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ServiceStack;
using ServiceStack.Redis;

namespace Magazine.ServiceInterface.Data
{
    public class BlogTemplateRepository : IRepository
    {
        public IRedisClientsManager RedisManager { get; set; }
        public MongoCollection<BlogTemplate> BlogTemplateColl;
        public ApplicationAhavRepository ProjectRepo { get; set; }

        public const string BlogTemplateDefault = "blog::{0}::template_default";

        public BlogTemplateRepository()
        {
            this.BlogTemplateColl =
                MongoDbHelper.Instance.Database.GetCollection<BlogTemplate>(typeof (BlogTemplate).Name);
            
        }
        public ReadBlogTemplateResponse ReadTemplate(ReadBlogTemplate request)
        {
            var tpl = BlogTemplateColl.FindOneByIdAs<BlogTemplate>(request.Id);
            return new ReadBlogTemplateResponse
                       {
                           Template = tpl
                       };
        }
        public ReadBlogTemplateDefaultIdResponse GetDefaultTemplateId(ReadBlogTemplateDefaultId  request)
        {
            using(var redisClient = RedisManager.GetClient())
            {
                var templateId = redisClient.Get<ObjectId>(string.Format(BlogTemplateDefault, request.BlogId));
                if(templateId.IsEmpty())
                {
                    return new ReadBlogTemplateDefaultIdResponse
                               {
                                   ResponseStatus =
                                       AhavExtensions.DataInternalError(
                                           string.Format("Default template for Blog: {0} is empty"))
                               };
                }
                return new ReadBlogTemplateDefaultIdResponse {TemplateId = templateId};
            }
        }
        public UpdateBlogTemplateDefaultResponse SetDefaultTemplate(UpdateBlogTemplateDefault request)
        {
            using(var redisClient = RedisManager.GetClient())
            {
                redisClient.Set(string.Format(BlogTemplateDefault, request.BlogId), request.BlogTemplateId);
            }
            return new UpdateBlogTemplateDefaultResponse();
        }
        
        //public ReadBlogTemplateResponse GetTemplate(ReadBlogTemplate request)
        //{
        //    var blog = BlogTemplateColl.FindOneByIdAs<BlogTemplate>(request.Id);
        //    var 
        //    return new ReadBlogTemplateResponse
        //               {
        //                   Template = blog
        //               };
        //}
        public CreateBlogTemplateResponse AddTemplate(CreateBlogTemplate request)
        {
            var response = new CreateBlogTemplateResponse();
            var template = new BlogTemplate
                               {
                                   Body = request.Body,
                                   BodyViewArticle = request.BodyViewIndex,
                                   BodyViewIndex = request.BodyViewArticle,
                                   Name = request.Name
                               };
            BlogTemplateColl.Insert(template);
            // Add the template Id to project
            if (request.ProjectId.HasValue)
            {
                ProjectRepo.SaveBlogTemplate(request.ProjectId.Value, template.Id);
            }
            response.BlogTemplate = template;
            return response;
        }
        public UpdateBlogTemplateResponse UpdateTemplate(UpdateBlogTemplate request)
        {
            BlogTemplateColl.Update(Query<BlogTemplate>.EQ(x => x.Id, request.Id),
                                    Update<BlogTemplate>
                                        .Set(x => x.Name, request.Name)
                                        .Set(x => x.Body, request.Body)
                                        .Set(x => x.BodyViewArticle, request.BodyViewArticle)
                                        .Set(x => x.BodyViewIndex, request.BodyViewIndex)
                                        .Set(x => x.Metadata, request.Metadata));
            return new UpdateBlogTemplateResponse();
        }

    }
}
