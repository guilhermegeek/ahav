﻿using System.Collections.Generic;
using System.Linq;
using Ahav.Infrastructure;
using Magazine.Extensions;
using Magazine.ServiceModel.Domain;
using Magazine.ServiceModel.Domain.Entities;
using Magazine.ServiceModel.Dtos;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ServiceStack.Redis;

namespace Magazine.ServiceInterface.Data
{
    public class BlogPostCategoryRepository : Repository<BlogArticleCategory>
    {
        public IMongoFields PostCategoryFields = Fields<BlogArticleCategory>.Include(x => x.Id, x => x.Title);

        private MongoCollection collection =
            MongoDbHelper.Instance.Database.GetCollection<BlogArticleCategory>(typeof (BlogArticleCategory).Name);

        private MongoCollection collectionArticle =
            MongoDbHelper.Instance.Database.GetCollection<Article>(typeof (Article).Name);


        public void AddPost(ObjectId id, BlogArticleRef post)
        {
            var result = collection.Update(Query<BlogArticleCategory>.EQ(x => x.Id, id),

                              Update<BlogArticleCategory>.Push(x => x.Posts, post)
                              .Inc(x => x.PostsCount, 1));
        }
        public IEnumerable<BlogArticleCategoryDto> GetCategories()
        {
            var categories = new List<BlogArticleCategory>();
            var categoriesDto = new List<BlogArticleCategoryDto>();
            categories = collection.FindAllAs<BlogArticleCategory>()
                .SetFields(PostCategoryFields).ToList();
            categories.ForEach(x => categoriesDto.Add(x.ToDto()));
            return categoriesDto;
        }

        public void RemoveCategory(ObjectId catecoryId)
        {
            collection.Remove(Query<BlogArticleCategory>.EQ(x => x.Id, catecoryId));
        }

        public ObjectId[] GetArticleIds(ObjectId categoryId)
        {
            return collection.FindOneByIdAs<BlogArticleCategory>(categoryId).Posts.Select(x => x.Id).ToArray();
        }

        public IRedisClientsManager RedisManager { get; set; }
    }
}
