﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Infrastructure;
using AutoMapper;
using Magazine.ServiceModel.Domain.Entities;
using Magazine.ServiceModel.Dtos;

namespace Magazine.ServiceInterface.Mappings
{
    public static class BlogDtoMap
    {
        public static void Register()
        {
            Mapper.CreateMap<Blog, BlogDto>().ConvertUsing(m =>
                                                           {
                                                               var culture = ServiceStackExtensions.GetCulture();
                                                               return new BlogDto
                                                                      {
                                                                          Id = m.Id,
                                                                          Title = m.Title.Any(x => x.Key == culture)
                                                                              ? m.Title.First(x => x.Key == culture).Value
                                                                              : m.Title.First().Value,
                                                                          Description = m.Description.Any(x => x.Key == culture)
                                                                              ? m.Description.First(x => x.Key == culture).Value
                                                                              : m.Description.First().Value,
                                                                          Name = m.Name,
                                                                          Owner = m.Owner,
                                                                          SubDomain = m.SubDomain,
                                                                          Categories = m.CategoriesIds,
                                                                      };
                                                           });
        }
    }
}
