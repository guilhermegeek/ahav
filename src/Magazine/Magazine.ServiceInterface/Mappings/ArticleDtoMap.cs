﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Infrastructure;
using AutoMapper;
using Magazine.ServiceModel.Domain.Entities;
using Magazine.ServiceModel.Dtos;

namespace Magazine.ServiceInterface.Mappings
{
    public static class ArticleDtoMap
    {
        public static void Register()
        {
            Mapper.CreateMap<Article, BlogArticleDto>().ConvertUsing(m =>
                                                                         {
                                                                             var culture = ServiceStackExtensions.GetCulture();
                                                                             return new BlogArticleDto
                                                                                    {
                                                                                        Id = m.Id,
                                                                                        TemplateId = m.TemplateId,
                                                                                        Creator = m.Creator,
                                                                                        Content = m.Body.Any(x => x.Key == culture)
                                                                                            ? m.Body.First(x => x.Key == culture).Value
                                                                                            : m.Body.FirstOrDefault().Value,
                                                                                        Title = m.Title.Any(x => x.Key == culture)
                                                                                            ? m.Title.First(x => x.Key == culture).Value
                                                                                            : m.Title.FirstOrDefault().Value,
                                                                                        Excerpt = m.Excerpt.Any(x => x.Key == culture)
                                                                                            ? m.Excerpt.First(x => x.Key == culture).Value
                                                                                            : m.Excerpt.First().Value,
                                                                                        Categories = m.Categories,
                                                                                        CreatedBy = m.CreatedBy,
                                                                                        Slug = m.Slug
                                                                                    };
                                                                         });
        }
    }
}
