﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Infrastructure;
using Ahav.ServiceModel.Dtos;
using AutoMapper;
using Magazine.ServiceModel.Domain.Entities;
using Magazine.ServiceModel.Dtos;

namespace Magazine.ServiceInterface.Mappings
{
    public static class ArticleInfoMap
    {
        public static void Register()
        {
            Mapper.CreateMap<Article, BlogArticleInfo>().ConvertUsing(m =>
                                                                          {
                                                                              var culture = ServiceStackExtensions.GetCulture();
                                                                              return new BlogArticleInfo
                                                                                     {
                                                                                         Id = m.Id,
                                                                                         Content = m.Body.Any(x => x.Key == culture)
                                                                                             ? m.Body.FirstOrDefault(x => x.Key == culture).Value
                                                                                             : m.Body.FirstOrDefault().Value,
                                                                                         Title = m.Title.Any(x => x.Key == culture)
                                                                                             ? m.Title.First(x => x.Key == culture).Value
                                                                                             : m.Title.FirstOrDefault().Value,
                                                                                         CreatedBy = new AuthorInfo
                                                                                                     {
                                                                                                         Id = m.Creator.Id
                                                                                                     },
                                                                                         Slug = m.Slug
                                                                                     };
                                                                          });
        }
    }
}
