﻿using Magazine.ServiceModel.ApiDtos;
using ServiceStack.FluentValidation;
using ServiceStack.ServiceInterface;

namespace Magazine.ServiceInterface.Validators
{
    public enum BlogCodeErrors
    {
        BlogsNotAvailable,
        VisitorsCantComment,
        CantEditArticle,
        CantPublishArticle,
    }
    
    public class AddBlogPostCommentValidator : AbstractValidator<PostBlogArticleComment>
    {
        public AddBlogPostCommentValidator()
        {
            RuleSet(ApplyTo.Post, () =>
                                      {
                                          RuleFor(x => x.Message).NotEmpty();
                                      });
        }
    }
    
    public class PutBlogArticleValidator : AbstractValidator<PutBlogArticle>
    {
        public PutBlogArticleValidator()
        {
            RuleSet(ApplyTo.Put, () =>
                                     {
                                         RuleFor(x => x.Title).NotEmpty();
                                         RuleFor(x => x.Body).NotEmpty();
                                     });
        }
    }
    public class AddBlogCategoryValidator : AbstractValidator<PostBlogCategory>
    {
        public AddBlogCategoryValidator()
        {
            RuleSet(ApplyTo.Post, () =>
            {
                RuleFor(x => x.Name).NotEmpty();
                RuleFor(x => x.Description).NotEmpty();
            });
        }
    }
}
