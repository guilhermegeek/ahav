﻿using Ahav.ServiceModel.ApiDtos;
using ServiceStack.FluentValidation;

namespace Magazine.ServiceInterface.Validators
{
    public class PostBlogTemplateValidator : AbstractValidator<PutBlogTemplate>
    {
        public PostBlogTemplateValidator()
        {
            RuleFor(x => x.Body).NotEmpty();
            RuleFor(x => x.BodyViewIndex).NotEmpty();
            RuleFor(x => x.BodyViewArticle).NotEmpty();
        }
    }
}
