﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Magazine.ServiceModel.ApiDtos;
using ServiceStack.FluentValidation;
using ServiceStack.ServiceInterface;

namespace Magazine.ServiceInterface.Validators
{
    public class PostBlogValidator : AbstractValidator<PostBlog>
    {
        public PostBlogValidator()
        {
            RuleSet(ApplyTo.Post, () =>
            {
                RuleFor(x => x.Name).NotEmpty();
            });
        }
    }
}
