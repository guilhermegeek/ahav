﻿using Magazine.ServiceModel.ApiDtos;
using ServiceStack.FluentValidation;

namespace Magazine.ServiceInterface.Validators
{
    public class CreateBlogTemplateValidator : AbstractValidator<CreateBlogTemplate>
    {
        public CreateBlogTemplateValidator()
        {
            RuleFor(x => x.Body).NotEmpty();
            RuleFor(x => x.BodyViewArticle).NotEmpty();
            RuleFor(x => x.BodyViewIndex).NotEmpty();
        }
    }
}
