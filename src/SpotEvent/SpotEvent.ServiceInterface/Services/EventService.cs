﻿using Ahav.Infrastructure;
using ServiceStack.ServiceInterface;
using SpotEvent.ServiceInterface.Data;
using SpotEvent.ServiceModel.SpotEvent.ApiDtos;
using SpotEvent.ServiceModel.SpotEvent.DataDtos;
using SpotEvent.ServiceModel.SpotEvent.Resources;

namespace SpotEvent.ServiceInterface.Services
{
    public class EventService : Service
    {
        public EventRepository EventRepo { get; set; }
        public PostEventResponse Post(PostEvent request)
        {
            var response = new PostEventResponse();
            var dataR = new CreateEvent
            {
                Title = request.Title,
                Description = request.Descriptions,
                EventCategoryIds = request.EventCategoryIds,
                OwnerId = Request.GetUserId(),
                PlaceId = request.PlaceId
            };
            var eventRes = EventRepo.Create(dataR);
            if (eventRes.ResponseStatus != null)
            {
                response.ResponseStatus = AhavExtensions.InvalidOperation(EventResources.PostEventFail);
                return response;
            }
            response.Modal = AhavExtensions.Success(EventResources.PostEventOk);
            response.Event = eventRes.Event;
            return response;
        }

        public GetEventResponse Get(GetEvent request)
        {
            var response = new GetEventResponse();
            var eventRes = EventRepo.Read(new ReadEvent { Id = request.Id });
            if (eventRes.ResponseStatus != null)
            {
                response.ResponseStatus = AhavExtensions.DataInternalError();
                return response;
            }
            response.Event = eventRes.Event;
            return response;
        }
    }
}
