﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using SpotEvent.ServiceModel.SpotEvent.Domain;

namespace SpotEvent.ServiceInterface.Data.Mappings
{
    public static class EventCategoryMap
    {
        public static void RegisterMapping()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(EventCategory)))
            {
                BsonClassMap.RegisterClassMap<EventCategory>(cm =>
                {
                    cm.MapIdProperty(x => x.Id)
                        .SetIdGenerator(ObjectIdGenerator.Instance);
                    cm.MapProperty(x => x.Name);
                    cm.MapProperty(x => x.Description);
                });
            }
        }
    }
}
