﻿using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using SpotEvent.ServiceModel.SpotEvent.Domain;

namespace SpotEvent.ServiceInterface.Data.Mappings
{
    public static class EventMap
    {
        public static void RegisterMappings()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(Event)))
            {
                BsonClassMap.RegisterClassMap<Event>(cm =>
                {
                    cm.MapIdProperty(x => x.Id)
                        .SetIdGenerator(ObjectIdGenerator.Instance);
                    cm.MapProperty(x => x.Title);
                    cm.MapProperty(x => x.Description);
                    cm.MapProperty(x => x.Location);
                    cm.MapProperty(x => x.When);
                    cm.MapProperty(x => x.Categories);
                });
            }
        }
    }
}
