﻿using Ahav.Infrastructure;
using MongoDB.Driver;
using SpotEvent.ServiceInterface.Extensions;
using SpotEvent.ServiceModel.SpotEvent.DataDtos;
using SpotEvent.ServiceModel.SpotEvent.Domain;

namespace SpotEvent.ServiceInterface.Data
{
    public class EventRepository
    {
        private MongoCollection eventColl = MongoDbHelper.Instance.Database.GetCollection<Event>(typeof (Event).Name);

        private MongoCollection eventCatColl =
            MongoDbHelper.Instance.Database.GetCollection<EventCategory>(typeof (EventCategory).Name);

        public CreateEventResponse Create(CreateEvent request)
        {
            var response = new CreateEventResponse();
            var entity = new Event
                         {
                             Title = request.Title,
                             Description = request.Description,
                             Categories = request.EventCategoryIds
                         };
            var res = eventColl.Insert(entity);
            if (!res.Ok)
            {
                response.ResponseStatus = AhavExtensions.InvalidOperation(res.ErrorMessage);
                return response;
            }
            response.Event = entity.ToDto();
            return response;
        }

        public ReadEventResponse Read(ReadEvent request)
        {
            var response = new ReadEventResponse();
            var eventD = eventColl.FindOneByIdAs<Event>(request.Id);
            if (eventD == null)
            {
                response.ResponseStatus = AhavExtensions.DataNotFound(request.Id.ToString());
                return response;
            }
            response.Event = eventD.ToDto();
            return response;
        }
    }
}
