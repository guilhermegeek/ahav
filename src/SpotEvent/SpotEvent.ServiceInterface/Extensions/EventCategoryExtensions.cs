﻿using System.Linq;
using Ahav.Infrastructure;
using SpotEvent.ServiceModel.SpotEvent;
using SpotEvent.ServiceModel.SpotEvent.Domain;

namespace SpotEvent.ServiceInterface.Extensions
{
    public static class EventCategoryExtensions
    {
        public static EventCategoryDto ToDto(this EventCategory entity)
        {
            var culture = ServiceStackExtensions.GetCulture();
            return new EventCategoryDto
                   {
                       Id = entity.Id,
                       Name = entity.Name.Any(x => x.Key == culture)
                           ? entity.Name.First(x => x.Key == culture).Value
                           : entity.Name.FirstOrDefault().Value
                   };
        }

        public static EventCategoryInfo ToInfo(this EventCategory entity)
        {
            var culture = ServiceStackExtensions.GetCulture();
            return new EventCategoryInfo
                   {
                       Id = entity.Id,
                       Name = entity.Name.Any(x => x.Key == culture)
                           ? entity.Name.First(x => x.Key == culture).Value
                           : entity.Name.FirstOrDefault().Value
                   };
        }
    }
}
