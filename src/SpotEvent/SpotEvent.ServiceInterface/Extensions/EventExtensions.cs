﻿using System.Linq;
using Ahav.Infrastructure;
using SpotEvent.ServiceModel.SpotEvent;
using SpotEvent.ServiceModel.SpotEvent.Domain;

namespace SpotEvent.ServiceInterface.Extensions
{
    public static class EventExtensions
    {
        public static EventDto ToDto(this Event entity)
        {
            var culture = ServiceStackExtensions.GetCulture();
            return new EventDto
                   {
                       Id = entity.Id,
                       Title = entity.Title.Any(x => x.Key == culture)
                           ? entity.Title.First(x => x.Key == culture).Value
                           : entity.Title.FirstOrDefault().Value
                   };
        }

        public static EventInfo ToInfo(this Event entity)
        {
            var culture = ServiceStackExtensions.GetCulture();
            return new EventInfo
                   {
                       Id = entity.Id,
                       Title = entity.Title.Any(x => x.Key == culture)
                           ? entity.Title.First(x => x.Key == culture).Value
                           : entity.Title.FirstOrDefault().Value
                   };
        }
    }
}
