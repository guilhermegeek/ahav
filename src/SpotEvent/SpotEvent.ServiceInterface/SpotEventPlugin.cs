﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.WebHost.Endpoints;
using SpotEvent.ServiceInterface.Data;
using SpotEvent.ServiceInterface.Data.Mappings;
using SpotEvent.ServiceInterface.Services;

namespace SpotEvent.ServiceInterface
{
    public class SpotEventPlugin : IPlugin
    {
        public void Register(IAppHost appHost)
        {
            var container = appHost.GetContainer();

            appHost.RegisterService<EventService>();
            container.RegisterAutoWired<EventRepository>();
            EventMap.RegisterMappings();
            EventCategoryMap.RegisterMapping();
        }
    }
}
