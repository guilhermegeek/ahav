﻿using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;

namespace SpotEvent.ServiceModel.SpotEvent
{
    public class EventDto
    {
        public ObjectId Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public PlaceInfo Place { get; set; }
    }
}
