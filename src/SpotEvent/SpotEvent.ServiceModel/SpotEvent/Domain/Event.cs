﻿using System;
using System.Collections.Generic;
using Ahav.Interfaces;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;

namespace SpotEvent.ServiceModel.SpotEvent.Domain
{
    public class Event : IThink
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public IPlace Location { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Description { get; set; }
        public DateTime When { get; set; }
        public ObjectId[] Categories { get; set; }
    }
}
