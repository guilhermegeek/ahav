﻿using System.Collections.Generic;
using MongoDB.Bson;

namespace SpotEvent.ServiceModel.SpotEvent.Domain
{
    public class EventCategory
    {
        public ObjectId Id { get; set; }
        public Dictionary<string, string> Name { get; set; }
        public Dictionary<string, string> Description { get; set; }
    }
}
