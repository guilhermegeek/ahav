﻿using System.Collections.Generic;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Driver.Builders;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;
using SpotEvent.ServiceModel.SpotEvent.Domain;

namespace SpotEvent.ServiceModel.SpotEvent.DataDtos
{
    public class QueryEvent : IReturn<QueryEventResponse>
    {
        QueryBuilder<Event> Query { get; set; }
    }
    public class QueryEventResponse : IHasResponseStatus, IHasResponseModal
    {
        public IList<EventInfo> Events { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
