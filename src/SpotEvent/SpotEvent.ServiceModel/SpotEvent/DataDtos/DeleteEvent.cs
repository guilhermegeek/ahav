﻿using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace SpotEvent.ServiceModel.SpotEvent.DataDtos
{
    public class DeleteEvent : IReturn<DeleteEventResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class DeleteEventResponse :IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
