﻿using System.Collections.Generic;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace SpotEvent.ServiceModel.SpotEvent.DataDtos
{
    public class CreateEvent : IReturn<CreateEventResponse>
    {
        public ObjectId UserId { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public ObjectId PlaceId { get; set; }
        public ObjectId OwnerId { get; set; }
        public Dictionary<string, string> Description { get; set; }
        public ObjectId[] EventCategoryIds { get; set; }
    }
    public class CreateEventResponse : IHasResponseStatus, IHasResponseModal
    {
        public EventDto Event { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
