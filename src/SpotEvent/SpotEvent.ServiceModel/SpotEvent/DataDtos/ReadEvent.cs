﻿using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace SpotEvent.ServiceModel.SpotEvent.DataDtos
{
    public class ReadEvent : IReturn<ReadEventResponse>
    {
        public ObjectId Id { get; set; }
    }

    public class ReadEventResponse : IHasResponseStatus
    {
        public EventDto Event { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
