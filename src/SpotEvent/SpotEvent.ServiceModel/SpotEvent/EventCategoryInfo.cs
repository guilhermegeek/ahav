﻿using System;

namespace SpotEvent.ServiceModel.SpotEvent
{
    public class EventCategoryInfo
    {
        public Object Id { get; set; }
        public string Name { get; set; }
    }
}
