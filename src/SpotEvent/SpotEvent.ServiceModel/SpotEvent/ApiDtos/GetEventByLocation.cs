﻿using System.ComponentModel;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace SpotEvent.ServiceModel.SpotEvent.ApiDtos
{
    /// <summary>
    /// Returns a list of events near the client location
    /// </summary>
    [Description("Return a list of events near the client location")]
    public class GetEventByLocation : IReturn<GetEventByLocationResponse>
    {
        /// <summary>
        /// Client latitude position
        /// </summary>
        public decimal Latitude { get; set; }
        /// <summary>
        /// Client longitude position
        /// </summary>
        public decimal Longitude { get; set; }
        /// <summary>
        /// Filter the events by Categories
        /// </summary>
        public ObjectId[] CategoriesId { get; set; }
    }
    public class GetEventByLocationResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public EventInfo[] Events { get; set; }
    }
}
