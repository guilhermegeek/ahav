﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace SpotEvent.ServiceModel.SpotEvent.ApiDtos
{
    public class FindEvent : IReturn<FindEventResponse>
    {
        public ObjectId[] Ids { get; set; }
        public string QueryName { get; set; }
        public DateTime From { get; set; }
        public DateTime Until { get; set; }
        // 0 if free
        public decimal MaxPrice { get; set; }
    }

    public class FindEventResponse : IHasResponseStatus
    {
        public IList<EventInfo> Events { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
