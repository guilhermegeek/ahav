﻿using System.Collections.Generic;
using System.ComponentModel;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;
using SpotEvent.ServiceModel.SpotEvent.DataDtos;

namespace SpotEvent.ServiceModel.SpotEvent.ApiDtos
{
    /// <summary>
    /// Create a event
    /// </summary>
    [Description("Create a event")]
    public class PostEvent : IReturn<CreateEventResponse>
    {
        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Descriptions { get; set; }
        public ObjectId PlaceId { get; set; }
        public ObjectId[] EventCategoryIds { get; set; }
        public ObjectId OwnerId { get; set; }
    }
    public class PostEventResponse : IHasResponseStatus, IHasResponseModal
    {
        public EventDto Event { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
