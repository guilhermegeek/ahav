﻿using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace SpotEvent.ServiceModel.SpotEvent.ApiDtos
{
    public class GetEvent : IReturn<GetEventResponse>
    {
        public ObjectId Id { get; set; }
    }

    public class GetEventResponse : IHasResponseStatus
    {
        public EventDto Event { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
