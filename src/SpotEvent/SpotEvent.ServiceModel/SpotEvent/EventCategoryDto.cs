﻿using MongoDB.Bson;

namespace SpotEvent.ServiceModel.SpotEvent
{
    public class EventCategoryDto
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
