﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace Ahav.Interfaces
{
    public interface IHasAttachments
    {
        ObjectId[] Files { get; set; }
    }
}
