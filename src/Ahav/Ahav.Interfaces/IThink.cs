﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Ahav.Interfaces
{
    public interface IThink
    {
        string Name { get; set; }
        Dictionary<string, string> Title { get; set; }
        Dictionary<string, string> Description { get; set; }
    }
}
