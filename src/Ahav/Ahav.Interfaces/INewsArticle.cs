﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ahav.Interfaces
{
    /// <summary>
    /// A news article, exists on a print edition
    /// </summary>
    public interface INewsArticle
    {
        /// <summary>
        /// The location where the NewsArticle was produced.
        /// </summary>
        string Dateline { get; set; }
        /// <summary>
        /// The number of the column in which the NewsArticle appears in the print edition.
        /// </summary>
        string PrintColumn { get; set; }
        /// <summary>
        /// The edition of the print product in which the NewsArticle appears.
        /// </summary>
        string PrintEdition { get; set; }
        /// <summary>
        /// If this NewsArticle appears in print, this field indicates the name of the page on which the article is found. Please note that this field is intended for the exact page name (e.g. A5, B18).
        /// </summary>
        string PrintPage { get; set; }
        /// <summary>
        /// If this NewsArticle appears in print, this field indicates the print section in which the article appeared.
        /// </summary>
        string PrintSection { get; set; }
    }
}
