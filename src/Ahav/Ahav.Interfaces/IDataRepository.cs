﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace Ahav.Interfaces
{
    public interface IDataRepository<T>
    {
        T Insert(T entity);
        void Save(ObjectId userId, Dictionary<object, object> fieldValue);
    }
}
