﻿namespace Ahav.Interfaces
{
    public interface IPagedResult
    {
        int Skip { get; set; }
        int Take { get; set; }
        //object SortBy { get; set; }
    }
}
