﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace Ahav.Interfaces
{
    public interface ICreator
    {
        ObjectId Id { get; set; }
        string Name { get; set; }
        string ThumbnailUri { get; set; }
        string Uri { get; set; }
    }
}
