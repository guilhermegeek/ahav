﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ahav.Interfaces
{
    public interface IHasCommentsBucket
    {
        long CommentsCount { get; set; }
        int CommentsPageCount { get; set; }
    }
}
