﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ahav.Interfaces
{
    public interface ICreativeWork : IThink
    {
        /// <summary>
        /// The intended audience of the item, i.e. the group for whom the item was created.
        /// </summary>
        string[] AudienceType { get; set; }
        
    }
    
}
