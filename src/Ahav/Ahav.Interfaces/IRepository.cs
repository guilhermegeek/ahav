﻿using ServiceStack.Redis;

namespace Ahav.Interfaces
{
    public interface IRepository
    {
        IRedisClientsManager RedisManager { get; set; }
    }
}
