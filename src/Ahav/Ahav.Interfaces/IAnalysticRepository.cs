﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace Ahav.Interfaces
{
    /// <summary>
    /// Repository implement methods for analystics stats 
    /// </summary>
    public interface IAnalysticRepository
    {
        /// <summary>
        /// Document key
        /// </summary>
        /// <param name="key"></param>
        void IncrementViewed(ObjectId id);

        long GetViews(ObjectId id);

    }
}
