﻿using System.Linq;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Operations;
using ServiceStack.ServiceClient.Web;
using ServiceStack.WebHost.Endpoints;

namespace Ahav.ServiceInterface.Extensions
{
    public static class JsonServiceClientExtensions
    {

        public static AhavAuth Login(this JsonServiceClient client, string email = "email@guilhermecardoso.pt", string password = "123")
        {
            client.AllowAutoRedirect = false;
            var responseLogin = client.Post(new PostLogin { Email = email, Password = password });
            var auth = responseLogin.Result;

            client.SetAuth(responseLogin.Auth.Id, responseLogin.Auth.Key);

            var container = EndpointHost.Config.ServiceManager.Container;
            var projectRepo = container.Resolve<ApplicationAhavRepository>();
            var ids = projectRepo.GetProjectsIdByUser(responseLogin.Auth.Id);
            if (ids != null && ids.Any())
                client.SetProjectCredentials(ids[0]);
            return auth;
        }
    }
}
