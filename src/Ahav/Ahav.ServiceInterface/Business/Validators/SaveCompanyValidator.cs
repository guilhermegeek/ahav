﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.BusinessDtos;
using ServiceStack.FluentValidation;

namespace Ahav.ServiceInterface.Business.Validators
{
    public class SaveCompanyValidator : AbstractValidator<SaveCompany>
    {
        public SaveCompanyValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
        }
    }
}
