﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.BusinessDtos;
using ServiceStack.FluentValidation;

namespace Ahav.ServiceInterface.Business.Validators
{
    public class CreateOrderValidator : AbstractValidator<CreateOrder>
    {
        public CreateOrderValidator()
        {
            RuleFor(x => x.PaymentDue).NotEmpty();
            RuleFor(x => x.OrderStatus).NotEmpty();
        }
    }
}
