﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.BusinessDtos;
using ServiceStack.FluentValidation;

namespace Ahav.ServiceInterface.Validators
{
    public class CreateCompanyValidator: AbstractValidator<CreateCompany>
    {
        public CreateCompanyValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
        }
    }
}
