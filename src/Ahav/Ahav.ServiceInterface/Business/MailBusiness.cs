﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Services;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Operations;
using MongoDB.Bson;
using ServiceStack.Configuration;

namespace Ahav.ServiceInterface.Business
{
    public class MailBusiness
    {
        public MailRepository MailRepo { get;set; }

        private void SendEmail()
        {
            
        }

        public void SendMailConfirmation(ObjectId userId, string recipient, string token)
        {
            var appSettings = new AppSettings();
            var mailId = MailRepo.GetInitMailId(MailCommon.ConfirmAccount);
            var email = MailRepo.Get(mailId);
            var formatedEmail = email.ToDto();
            // Init email for ConfirmAccount has two replaces
            string content = string.Format(formatedEmail.Content, userId, token);
            var response = new SendEmailIHttpResponse();
            
            SmtpClient client = new SmtpClient();
            client.Port = appSettings.Get("SmtpPort", 25);
            client.Host = appSettings.Get("SmtpHost", "localhost");
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            if (appSettings.GetString("SmtpCredentialUser") != null &&
                appSettings.GetString("SmtpCredentialPassword") != null)
                client.Credentials = new System.Net.NetworkCredential(appSettings.GetString("SmtpCredentialUser"), appSettings.GetString("SmtpCredentialPassword"));
            
            var emailsNotSented = new List<string>();
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("email@guilhermecardoso.pt");
                mail.To.Add(new MailAddress(recipient));
                mail.Subject = formatedEmail.Subject;
                mail.Body = content;
                mail.IsBodyHtml = true;
                client.Send(mail);
            }
            catch (Exception ex)
            {
                emailsNotSented.Add(recipient);
            }
        }
    }
}
