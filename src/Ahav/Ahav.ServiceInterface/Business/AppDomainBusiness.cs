﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceInterface.Data;

namespace Ahav.ServiceInterface.Business
{
    /// <summary>
    /// Public domains are available to new Ahav Applications and stored in memory
    /// </summary>
    public class AppDomainBusiness
    {
        public void AddPublicDomain(string domain)
        {
            AppDomainRepo.AddPublicDomain(domain);
        }

        public void RemovePublicDomain(string domain)
        {
            AppDomainRepo.RemovePublicDomain(domain);
        }

        public string[] GetPublicDomains()
        {
            var domains = AppDomainRepo.GetPublicDomains();
            return domains;
        }
        public AppDomainRepository AppDomainRepo { get; set; }
    }
}
