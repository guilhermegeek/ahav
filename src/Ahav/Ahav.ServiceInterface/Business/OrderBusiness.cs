﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Infrastructure;
using Ahav.ServiceInterface.Business.Validators;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceModel.BusinessDtos;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using MongoDB.Driver.Linq;
using AutoMapper;
using ServiceStack.Common;
using ServiceStack.Common.Web;
using ServiceStack.FluentValidation;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;
using ServiceStack.ServiceInterface.Validation;

namespace Ahav.ServiceInterface.Business
{
    public class OrderBusiness
    {
        public ReadOrderResponse Read(ReadOrder request)
        {
            var response = new ReadOrderResponse();
            var order = OrderRepo.GetById(request.Id);
            Mapper.Map(order, response.Order);
            return response;
        }

        public CreateOrderResponse Create(CreateOrder request)
        {
            var response = new CreateOrderResponse();
            var result = CreateOrderValidator.Validate(request);
            if (!result.IsValid)
            {
                response.ResponseStatus = result.ResultToResponseStatus();
                return response;
            }
            // Get user DTO to be normalized
            var creator = UserRepo.GetCreator(request.UserId);
            var order = OrderRepo.Insert(new Order
                                         {
                                             Customer = creator,
                                             Offers = request.Offers,
                                             PaymentDue = request.PaymentDue,
                                             Status = request.OrderStatus,
                                         });
            response.Order = Mapper.Map<Order, OrderDto>(order);
            return response;
        }

        // Data layers
        public OrderRepository OrderRepo { get; set; }
        public UserRepository UserRepo { get; set; }

        // Validators
        public IValidator<CreateOrder> CreateOrderValidator{ get; set; }
        
    }
}
