﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceModel.BusinessDtos;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using AutoMapper;
using MongoDB.Driver;
using ServiceStack.Common.Utils;
using ServiceStack.Common.Web;

namespace Ahav.ServiceInterface.Business
{
    public class ContentBusiness
    {
        /// <summary>
        /// Returns a Content
        /// </summary>
        /// <param name="appId"></param>
        /// <param name="slug"></param>
        /// <param name="templateCached"></param>
        /// <returns>Render Body</returns>
        public ContentView GetView(ObjectId appId, string slug, bool isAjax, ObjectId templateCached)
        {
            Content content;
            var response = new ContentView();
            if (string.IsNullOrEmpty(slug) || slug == AhavExtensions.InternalDefaultPageRedirect)
            {
                var id = ContentRepo.GetContentDefault(appId);
                content = ContentRepo.Get(id);
            }
            else
            {
                content = ContentRepo.GetBySlug(slug);
            }
            if (content == null)
            {
                throw new HttpError(HttpStatusCode.NotFound, "NotFound", string.Format("A página com o endereço <b><u>{0}<u><b> não existe neste site", slug));
            }
            
            var dto = Mapper.Map<Content, ContentDto>(content);

            if (!isAjax && (content.Template != templateCached || templateCached == ObjectId.Empty))
            {
                var tpl = TemplateRepo.GetTemplate(content.Template);
                var tplDto = tpl.ToDto();
                response.RenderBody = tplDto.Body.Replace(TemplateExtensions.HtmlDivRenderId, TemplateExtensions.TemplateUiView);
            }
            else
            {
                response.RenderBody = dto.Body;
            }
            // Meta Tags related to template (not the project in global, as google)
            
            response.MetaTags = new Dictionary<string, string>
                                    {
                                        {"og:title", dto.Title},
                                        {"og:image", dto.Thumbnail}, // Square, no smaller than 300x300 px
                                        {"og:description", dto.Description},
                                        {"og:type", "website"},
                                        {"og:url", dto.Url},
                                        {"twitter:card", "summary"},
                                        {"twitter:url", dto.Url},
                                        {"twitter:title", dto.Title},
                                        {"twitter:description", dto.Description},
                                        {"twitter:image", dto.Thumbnail}
                                        // Square, no smaller than 60x60 px
                                    };
            return response;
        }
        public ContentDto Get(ObjectId id)
        {
            var content = ContentRepo.Get(id);
            return Mapper.Map<Content, ContentDto>(content);
        }
        public ContentDto GetBySlug(string slug)
        {
            var content = ContentRepo.GetBySlug(slug);
            return Mapper.Map<Content, ContentDto>(content);
        }
        /// <summary>
        /// Create a new content, adds his Id to Template
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public CreateContentResponse Create(CreateContent request)
        {
            var response = new CreateContentResponse();
            var content = ContentRepo.Create(new Content
            {
                Title =
                    new Dictionary<string, string> { { Culture.pt_PT, "Página Exemplo" } },
                Slug = request.Slug,
                Body =
                    new Dictionary<string, string> { { Culture.pt_PT, "<div>ola</div>" } },
                ProjectId = request.AppId,
                Owner = request.OwnerId,
                Template = request.TemplateId
            });
            
            // Save in Template
            TemplateRepo.AddContentIdToTemplate(request.TemplateId, content.Id);
            
            // Save in Application
            AppRepo.AddContent(request.AppId, content.Id);
            response.Content = Mapper.Map<Content, ContentDto>(content);
            return response;
        }
        public TemplateRepository TemplateRepo { get; set; }
        public ContentRepository ContentRepo { get; set; }
        public ApplicationAhavRepository AppRepo { get; set; }
    }
}
