﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using AutoMapper;
using MongoDB.Bson;

namespace Ahav.ServiceInterface.Business
{
    public class TemplateBusiness
    {
        public TemplateRepository TemplateRepo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">Application id</param>
        /// <returns></returns>
        public ContentTemplateInfo[] GetTemplatesForApp(ObjectId id)
        {
            return TemplateRepo.GetByProject(id).ToArray();
        }

        public ContentTemplateDto Get(ObjectId id)
        {
            var template = TemplateRepo.GetTemplate(id);
            return Mapper.Map<Template, ContentTemplateDto>(template);
        }
        public Template GetTemplate(ObjectId templateId)
        {
            var template = TemplateRepo.GetTemplate(templateId);
            return template;
        }
    }
}
