﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Infrastructure;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Validators;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.BusinessDtos;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.FluentValidation;
using AutoMapper;

namespace Ahav.ServiceInterface.Business
{
    public class CompanyBusiness
    {
        public CreateCompanyResponse Create(CreateCompany request)
        {
            var response = new CreateCompanyResponse();
            // Validate
            var validation = CreateCompanyVal.Validate(request);
            if (!validation.IsValid)
            {
                response.ResponseStatus = validation.ResultToResponseStatus();
                return response;
            }
            return response;
        }

        public CompanyDto Get(ObjectId companyId)
        {
            var company = CompanyRepo.Get(companyId);
            return Mapper.Map<Company, CompanyDto>(company);
        }

        public SaveCompanyResponse Save(SaveCompany request)
        {
            var response = new SaveCompanyResponse();
            var validation = SaveCompanyVal.Validate(request);
            if (!validation.IsValid)
            {
                response.ResponseStatus = validation.ResultToResponseStatus();
                return response;
            }
            return response;
        }
        // Data layers
        public CompanyRepository CompanyRepo { get; set; }

        // Validators
        public IValidator<CreateCompany> CreateCompanyVal { get; set; }
        public IValidator<SaveCompany> SaveCompanyVal { get; set; }

    }
}
