﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using AutoMapper;
using MongoDB.Bson;

namespace Ahav.ServiceInterface.Business
{
    public class ApplicationBusiness
    {
        public void Delete(ObjectId appId)
        {
            ApplicationRepo.Delete(appId);
        }
        public AhavProjectInfo[] Query(ObjectId ownerId)
        {
            var ids = ApplicationRepo.GetProjectsIdByUser(ownerId);
            var apps = ApplicationRepo.Find(ids);
            return apps.ToArray();
        }

        public AhavProjectDto Get(ObjectId id)
        {
            var app = ApplicationRepo.Get(id);
            var appDto = Mapper.Map<ApplicationAhav, AhavProjectDto>(app);
            return appDto;
        }

        public ApplicationAhavRepository ApplicationRepo { get; set; }
    }
}
