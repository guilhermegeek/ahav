﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using AutoMapper;

namespace Ahav.ServiceInterface.Mappings
{
    public static class CompanyDtoMapping
    {
        public static void Register()
        {
            Mapper.CreateMap<Company, CompanyDto>().ConvertUsing(x =>
                                                                 {
                                                                     return new CompanyDto
                                                                            {
                                                                                Id = x.Id,
                                                                                Name = x.Name
                                                                            };                                                                 });
        }
    }
}
