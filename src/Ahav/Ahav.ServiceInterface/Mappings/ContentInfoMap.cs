﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Infrastructure;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using AutoMapper;

namespace Ahav.ServiceInterface.Mappings
{
    public static class ContentInfoMap
    {
        public static void Register()
        {
            Mapper.CreateMap<Content, ContentInfo>().ConvertUsing(m =>
                                                                  {
                                                                      var culture = ServiceStackExtensions.GetCulture();
                                                                      return new ContentInfo
                                                                             {
                                                                                 Id = m.Id,
                                                                                 Title = m.Title.Any(x => x.Key == culture)
                                                                                     ? m.Title.First(x => x.Key == culture).Value
                                                                                     : m.Title.FirstOrDefault().Value,
                                                                                 Files = m.Files,
                                                                                 Slug = m.Slug
                                                                             };
                                                                  });
        }
    }
}
