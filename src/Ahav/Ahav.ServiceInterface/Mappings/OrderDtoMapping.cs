﻿using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using AutoMapper;

namespace Ahav.ServiceInterface.Mappings
{
    public static class OrderDtoMap
    {
        public static void Register()
        {
            Mapper.CreateMap<Order, OrderDto>().ConvertUsing(m =>
                                                             {
                                                                 return new OrderDto
                                                                        {
                                                                            Id = m.Id,
                                                                            Offers = m.Offers,
                                                                            Creator = m.Customer,
                                                                            PaymentDue = m.PaymentDue,
                                                                            PaymentMethod = m.PaymentMethod,
                                                                            Status = m.Status
                                                                        };
                                                             });
        }
    }
}
