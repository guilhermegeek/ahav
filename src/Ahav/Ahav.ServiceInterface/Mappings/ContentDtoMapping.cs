﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Infrastructure;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using AutoMapper;

namespace Ahav.ServiceInterface.Mappings
{
    public class ContentDtoMapping
    {
        public static void Register()
        {
            Mapper.CreateMap<Content, ContentDto>().ConvertUsing(m =>
                                                                 {
                                                                     var culture = ServiceStackExtensions.GetCulture();
                                                                     var content = new ContentDto
                                                                                   {
                                                                                       Id = m.Id,
                                                                                       Title = m.Title.Any(x => x.Key == culture)
                                                                                           ? m.Title.First(x => x.Key == culture).Value
                                                                                           : m.Title.FirstOrDefault().Value,
                                                                                       //Description = m.Description.Any(x => x.Key == culture)
                                                                                       //    ? m.Description.First(x => x.Key == culture).Value
                                                                                       //    : m.Description.FirstOrDefault().Value,
                                                                                       Body = m.Body.Any(x => x.Key == culture)
                                                                                           ? m.Body.First(x => x.Key == culture).Value
                                                                                           : m.Body.FirstOrDefault().Value,
                                                                                       Css = m.Css,
                                                                                       Javascript = m.Javascript,
                                                                                       ProjectId = m.ProjectId,
                                                                                       Slug = m.Slug
                                                                                   };
                                                                  
                                                                         content.TemplateId = m.Template;
                                                                     
                                                                     return content;
                                                                 });
        }
    }
}
