﻿using System.Collections.Generic;
using System.Linq;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.Interfaces;
using Ahav.ServiceModel.DataDtos;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ServiceStack.Redis;

namespace Ahav.ServiceInterface.Data
{
    public class ContentRepository : Repository<Content>
    {
        public IRedisClientsManager RedisManager { get; set; }
        public MongoDatabase MongoDatabase { get; set; }

        public const string RedisContent = "p::{0}::{1}";
        public const string RedisContentDefault = "p::{0}::c::d";
        public IMongoFields ContentInfoFields = Fields<Content>.Include(x => x.Id, x => x.Title, x => x.Slug, x => x.ProjectId);
        public IMongoFields ContentDtoFields = Fields<Content>.Include(x => x.Id, x => x.Title, x => x.Body, x => x.Slug, x => x.ProjectId);

        private MongoCollection<Content> collectionContent;
        private MongoCollection<User> userColl;
        private MongoCollection<ApplicationAhav> projectColl;

        public ApplicationAhavRepository ProjectRepo { get; set; }

        public ContentRepository()
        {
            this.collectionContent = MongoDbHelper.Instance.Database.GetCollection<Content>(typeof(Content).Name);
            this.userColl = MongoDbHelper.Instance.Database.GetCollection<User>(typeof (User).Name);
            this.projectColl = MongoDbHelper.Instance.Database.GetCollection<ApplicationAhav>(typeof (ApplicationAhav).Name);
        }

        public Content Add(Content entity, ObjectId userId)
        {
            entity.Owner = userId;
            var insertRes = collectionContent.Insert(entity);
            
            if (!insertRes.Ok || entity.Id.IsEmpty()) return null;
            return entity;
        }
        public bool IsNotRegistered(ObjectId accountId)
        {
            return userColl.FindOneByIdAs<User>(accountId) == null;
        }

        public Content GetBySlug(string slug)
        {
            return collectionContent.FindOneAs<Content>(Query<Content>.EQ(x => x.Slug, slug));
        }
        public Content Get(ObjectId contentId)
        {
            return collectionContent.FindOneByIdAs<Content>(contentId);
        }
        public bool IsOwner(ObjectId contentId, ObjectId userId)
        {
            return collectionContent.FindOneByIdAs<Content>(contentId).Owner == userId;
        }
        public ReadContentsResponse Get(ReadContents request)
        {
            var response = new ReadContentsResponse();
            var contents = collectionContent.FindAs<Content>(Query<Content>.In(x => x.Id, request.Ids))
                .SetFields(ContentInfoFields)
                .SetSkip(request.Skip)
                .SetLimit(request.Take)
                .SetSortOrder(SortBy<Content>.Descending(x => x.Id));
            contents.ToList().ForEach(x => response.Contents.Add(x.ToInfo()));
            return response;
        }
        public IList<ContentInfo> GetAll(int skip, int take)
        {

            var contentList = collectionContent.FindAllAs<Content>()
                .SetFields(ContentInfoFields)
                .SetSkip(skip)
                .SetLimit(take)
                .SetSortOrder(SortBy<Content>.Descending(x => x.Id))
                .ToList();
            var contentInfo = new List<ContentInfo>();
            contentList.ForEach(x => contentInfo.Add(x.ToInfo()));
            return contentInfo;
        }
        public ContentInfo[] FindByApplicationId(ObjectId applicationId)
        {
            var cursor = collectionContent.FindAs<Content>(Query<Content>.EQ(x => x.ProjectId, applicationId))
                .Take(200);
            return null;
        }
        public QueryContentResponse Find(QueryContent request)
        {
            
            var response = new QueryContentResponse();
            var cursor = request.Queries.Any()
                             ? collectionContent.FindAs<Content>(Query.And(request.Queries))
                             : collectionContent.FindAllAs<Content>();
            
                    var list = cursor.SetSkip(request.Skip)
                    .SetLimit(request.Take)
                    .SetSortOrder(SortBy<Content>.Descending(x => x.Id))
                    .ToList();
           response.Content=new List<ContentInfo>();
            list.ForEach(x => response.Content.Add(x.ToInfo()));

            return response;
        }

        public IList<ContentInfo> GetByProject(ObjectId projectId)
        {
            var ids = ProjectRepo.GetContents(projectId);
            var contents = collectionContent.FindAs<Content>(Query<Content>.In(x => x.Id, ids))
                .SetFields(ContentInfoFields)
                .ToList();
            var cinfo = new List<ContentInfo>();
            contents.ForEach(x => cinfo.Add(x.ToInfo()));
            return cinfo;
        }
       
        public WriteConcernResult Update(ObjectId id, Dictionary<string, string> title, Dictionary<string, string> body, string javascript, string slug, ObjectId? template)
        {
            var a=  collectionContent.Update(Query<Content>.EQ(x => x.Id, id),
                              Update<Content>.Set(x => x.Title, title)
                                  .Set(x => x.Body, body)
                                  .Set(x => x.Javascript, javascript)
                                  .Set(x => x.Slug, slug)
                                  .Set(x => x.Template, template));
            return a;
        }
        public Content Create(Content content)
        {
            collectionContent.Insert(content);
            return content;
        }
        public WriteConcernResult Delete(ObjectId contentId)
        {
            return collectionContent.Remove(Query<Content>.EQ(x => x.Id, contentId));
        }
        public WriteConcernResult DeleteFile(ObjectId contentId, ObjectId fileId)
        {
            return collectionContent.Update(
                Query<Content>.EQ(x => x.Id, contentId),
                Update<Content>.Pull(x => x.Files, fileId));
        }
        public WriteConcernResult Addfile(ObjectId contentId, ObjectId fileId)
        {
            return collectionContent.Update(
                Query<Content>.EQ(x => x.Id, contentId),
                Update<Content>.Push(x => x.Files, fileId));
        }
        public WriteConcernResult Addfile(ObjectId contentId, ObjectId[] fileId)
        {
            return collectionContent.Update(
                Query<Content>.EQ(x => x.Id, contentId),
                Update<Content>.PushAll(x => x.Files, fileId));
        }
        public IList<ObjectId> GetFilesIds(ObjectId contentId, int skip = 0, int take = 100)
        {
            return collectionContent.FindOneByIdAs<Content>(contentId).Files;
        }

        public ObjectId GetContentDefault(ObjectId projectId)
        {
            using(var redisClient = RedisManager.GetClient())
            {
                return redisClient.Get<ObjectId>(string.Format(RedisContentDefault, projectId));
            }
        }
        public void SetContentDefault(ObjectId projectId, ObjectId contentId)
        {
            using(var redisClient = RedisManager.GetClient())
            {
                redisClient.Set(string.Format(RedisContentDefault, projectId), contentId);
            }
        }
    }
}
