﻿using MongoDB.Bson;
using ServiceStack.Redis;

namespace Ahav.ServiceInterface.Data
{
    public enum Token
    {
        Auth = 1, ConfirmEmail = 2, RecoverPassword = 3
    }
    /// <summary>
    /// Tokens are saved in Redis for fast instance
    /// </summary>
    public class TokenRepository
    {
        public IRedisClientsManager RedisManager { get; set; }
        /// <summary>
        /// Get the User token
        /// {0} - UserId
        /// </summary>
        public const string RedisToken = "t::{0}::{1}";
        public string GetToken(ObjectId userId, Token type)
        {
            using (var client = RedisManager.GetClient())
            {
                return client.Get<string>(string.Format(RedisToken, type, userId));
            }
        }
        public void SaveToken(ObjectId userId, Token type, string token)
        {
            using (var client = RedisManager.GetClient())
            {
                client.Set(string.Format(RedisToken, type, userId), token);
            }
        }
        public void DeleteToken(ObjectId userId, Token type)
        {
            using (var client = RedisManager.GetClient())
            {
                client.As<string>().Delete(string.Format(RedisToken, type, userId));
            }
        }
    }
}
