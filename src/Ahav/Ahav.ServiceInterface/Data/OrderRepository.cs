﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Infrastructure;
using Ahav.Interfaces;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson;
using MongoDB.Driver;
using ServiceStack.Redis;

namespace Ahav.ServiceInterface.Data
{
    public class OrderRepository : IRepository
    {
        public IRedisClientsManager RedisManager { get; set; }
        private MongoCollection orderCollection;
        public OrderRepository()
        {
            orderCollection = MongoDbHelper.Instance.Database.GetCollection<Order>(typeof (Order).Name);
        }

        public Order Insert(Order entity)
        {
            if(entity.Offers == null || entity.Offers.Length == 0)
                entity.Offers= new OrderOffer[0];
            orderCollection.Insert(entity);
            return entity;
        }

        public Order GetById(ObjectId entityId)
        {
            return orderCollection.FindOneByIdAs<Order>(entityId);
        }

    }
}
