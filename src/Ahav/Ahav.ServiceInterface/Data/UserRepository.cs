﻿using System.Collections.Generic;
using Ahav.Infrastructure;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using MongoDB.Driver;
using ServiceStack.Redis;

namespace Ahav.ServiceInterface.Data
{
    public class UserRepository
    {
        
        public User SaveUser(User user)
        {
            var result = UserColl.Insert(user);
            using (var redisClient = RedisManager.GetClient())
            {
                redisClient.As<UserInfo>().Store(user.ToInfo());
            }
            return user;
        }
        public UserInfo GetUserInfo(ObjectId userId)
        {
            using (var redisClient = RedisManager.GetClient())
            {
                return redisClient.As<UserInfo>().GetById(userId);
            }
        }
        public IList<UserInfo> GetUserInfo(ObjectId[] userIds)
        {
            using (var redisClient = RedisManager.GetClient())
            {
                return redisClient.As<UserInfo>().GetByIds(userIds);
            }
        }

        public CreatorPersonInfo GetCreator(ObjectId userId)
        {
            var user = UserColl.FindOneByIdAs<User>(userId);
            return new CreatorPersonInfo
                   {
                       Name = user.DisplayName,
                       ThumbnailUri = "",
                       Uri = string.Format("/user/{0}", userId)
                   };
        }

        public IRedisClientsManager RedisManager { get; set; }
        public MongoCollection UserColl = MongoDbHelper.Instance.Database.GetCollection<User>(typeof(User).Name);

    }
}
