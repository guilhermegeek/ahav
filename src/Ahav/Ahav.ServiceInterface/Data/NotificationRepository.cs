﻿using System.Linq;
using Ahav.Interfaces;
using Ahav.ServiceModel.DataDtos;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using ServiceStack.Redis;

namespace Ahav.ServiceInterface.Data
{
    public class NotificationRepository : IRepository
    {
        public IRedisClientsManager RedisManager { get; set; }
        public const string RedisNotificationUser = "notif::user::{0}";

        public ReadNotificationsResponse ReadNotifications(ReadNotifications request)
        {
            using(var redis = RedisManager.GetClient())
            {
                var notifs = redis.As<Notification>()
                    .SortedSets[string.Format(RedisNotificationUser, request.UserId)]
                    .GetRange(request.Skip, request.Skip + request.Take)
                    .ToList();
                return new ReadNotificationsResponse
                           {
                               Results = notifs
                           };

            }
        }
        public CreateNotificationResponse CreateNotification(CreateNotification request)
        {
            using(var redisClient  =RedisManager.GetClient())
            {
                redisClient.As<Notification>()
                    .SortedSets[string.Format(RedisNotificationUser, request.UserId)]
                    .Add(new Notification
                             {
                                 UserId = request.UserId
                             });
            }
            return new CreateNotificationResponse();
        }
    }
}
