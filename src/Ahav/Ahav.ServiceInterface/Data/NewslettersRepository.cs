﻿using System.Linq;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System.Collections.Generic;

namespace Ahav.ServiceInterface.Data
{
    public class NewslettersRepository : Repository<Newsletter>
    {
        public IMongoFields NewsletterInfoFields = Fields<NewsletterInfo>.Include(x => x.Id, x => x.State,
                                                                                  x => x.Subject);
        public IList<NewsletterInfo> GetAll(int skip, int take)
        {
            var results = collection.FindAllAs<Newsletter>()
                .SetFields(NewsletterInfoFields)
                .SetSkip(skip)
                .SetLimit(take > 0 ? take : 20)
                .SetSortOrder(SortBy<Newsletter>.Descending(x => x.Id))
                .ToList();
            var infoList = new List<NewsletterInfo>();
            results.ForEach(x => infoList.Add(x.ToInfo()));
            return infoList;
        }
        public WriteConcernResult Update(ObjectId id, Dictionary<string, string> subject, Dictionary<string, string> content, bool isHtml , string name)
        {
            return Save(id, new[]
                                                 {
                                                     Update<Newsletter>.Set(x => x.Subject, subject),
                                                     Update<Newsletter>.Set(x => x.Content, content),
                                                     Update<Newsletter>.Set(x => x.IsHtml, isHtml),
                                                     Update<Newsletter>.Set(x => x.Name, name)

                                                 });
        }
        public void AddFile(ObjectId newsletterId, ObjectId fileId)
        {
            collection.Update(Query<Newsletter>.EQ(x => x.Id, newsletterId),
                Update<Newsletter>.Push(x => x.Files, fileId));
        }
        public  void RemoveFile(ObjectId newsletterId, ObjectId fileId)
        {
            collection.Update(Query<Newsletter>.EQ(x => x.Id, newsletterId),
                              Update<Newsletter>.Pull(x => x.Files, fileId));
        }
    }
}
