﻿using System.Collections.Generic;
using System.Linq;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.Interfaces;
using Ahav.ServiceModel.DataDtos;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ServiceStack.Redis;

namespace Ahav.ServiceInterface.Data
{
    public class PackageRepository : IRepository
    {
        public IRedisClientsManager RedisManager { get; set; }
        private MongoCollection collectionPackage;
        private IMongoFields FieldsPackageInfo = Fields<AhavPackage>.Include(x => x.Id, x => x.Ammount, x => x.IsActive);

        public PackageRepository()
        {
            this.collectionPackage = MongoDbHelper.Instance.Database.GetCollection<AhavPackage>(typeof(AhavPackage).Name);
        }
        
        //public AhavPackage CreatePackage(AhavPackage entity)
        //{
        //    var result = collectionPackage.Insert(entity);
        //    if (!result.Ok)
        //        return null;
        //    return entity;
        //}
        public IList<AhavPackage> GetPackages(bool active, int skip = 0, int take = 40)
        {
            return collectionPackage.FindAs<AhavPackage>(Query<AhavPackage>.EQ(x => x.IsActive, active))
                .SetSkip(skip)
                .SetLimit(take)
                .ToList();
        }
        public AhavPackage GetPackage(ObjectId packageId)
        {
            return collectionPackage.FindAs<AhavPackage>(Query<AhavPackage>.EQ(x => x.Id, packageId))
                .Single();
        }
        
        public CreatePackageResponse Add(CreatePackage request)
        {
            var response = new CreatePackageResponse();
            var package = new AhavPackage
                              {
                                  Ammount = request.Ammount,
                                  Title = request.Title,
                                  Description = request.Description,
                                  Start = request.Start,
                                  End = request.End,
                                  IsActive = request.IsActive,
                                  DaysPrice = request.DaysPrice,
                                  Features = request.Features
                              };
            var result = collectionPackage.Insert(package);
            if(!result.Ok)
            {
                response.ResponseStatus = AhavExtensions.DataInternalError(result.ErrorMessage);
                return response;
            }
            response.Package = package;
            return response;
        }
        public ReadPackagesResponse ReadPackages(ReadPackages request)
        {
            var queries = new List<IMongoQuery>();
            if(request.FreeOnly)
            {
                queries.Add(Query<AhavPackage>.EQ(x => x.Ammount, 0));
            }
            queries.Add(Query<AhavPackage>.EQ(x => x.IsActive, request.Unactive));
            var packages = collectionPackage.FindAs<AhavPackage>(Query.And(queries))
                .SetFields(FieldsPackageInfo)
                .ToList();
            var response = new ReadPackagesResponse();
            packages.ForEach(x => response.Packages.Add(x.ToInfo()));
            return response;
        }
    }
}
