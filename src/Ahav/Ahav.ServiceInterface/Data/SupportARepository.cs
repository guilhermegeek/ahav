﻿using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.Interfaces;
using Ahav.ServiceModel.DataDtos;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Operations;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ServiceStack.Redis;

namespace Ahav.ServiceInterface.Data
{
    public class SupportARepository : IRepository
    {
        public IRedisClientsManager RedisManager { get; set; }
        private MongoCollection messageColl;

        public SupportARepository()
        {
            messageColl = MongoDbHelper.Instance.Database.GetCollection<SupportContact>(typeof (SupportContact).Name);
        }
        public AddSupportContactResponse AddContact(CreateSupportContact request)
        {
            var response = new AddSupportContactResponse();
            var entity = new SupportContact
                             {
                                 Name = request.Name,
                                 Email = request.Email,
                                 Contact = request.Contact,
                                 Subject = request.Subject,
                                 Message = request.Message,
                                 State = SupportContactState.NotReaded
                             };
            var insertResult = messageColl.Insert(entity);
            if(!insertResult.Ok)
            {
                response.ResponseStatus = AhavExtensions.DataInternalError();
                return response;
            }
            response.Contact = entity.ToDto();
            return response;
        }
        public ReadSupportContactResponse ReadContact(ReadSupportContact request)
        {
            var response = new ReadSupportContactResponse();
            var entity = messageColl.FindOneByIdAs<SupportContact>(request.Id);
            if(entity == null)
            {
                response.ResponseStatus = AhavExtensions.DataInternalError();
                return response;
            }
            response.Contact = entity.ToDto();
            return response;
        }
    }
    public class SupportTicketRepository : Repository<SupportTicket>
    {
        public WriteConcernResult EditState(ObjectId id, SupportTicketState state)
        {
            return collection.Update(Query<SupportTicket>.EQ(x => x.Id, id),
                                     Update<SupportTicket>.Set(x => x.State, state));
        }
        public WriteConcernResult Edit(ObjectId id, string title, string content, SupportTicketPriority priority)
        {
            return collection.Update(Query<SupportTicket>.EQ(x => x.Id, id),
                                     Update<SupportTicket>
                                         .Set(x => x.Title, title)
                                         .Set(x => x.Content, content)
                                         .Set(x => x.Priority, priority));
        }
    }
}
