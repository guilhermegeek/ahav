﻿using System.Collections.Generic;
using System.Linq;
using Ahav.Infrastructure;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace Ahav.ServiceInterface.Data
{
    public class MailRepository : Repository<Mail>
    {
        public IMongoFields EmailDtoFields = Fields<MailDto>.Include(x => x.Id, x => x.Subject, x => x.Content,
                                                                     x => x.IsHtml);

        public const string RedisInitEmails = "init::emails::{0}";
        public const string RedisEmailQueue = "mail::{0}::queue";
        public const string RedisEmailError = "mail::{0}::error";
        public const string RedisEmailSuspended = "mail::{0}::suspended";

        public const string EmailOrderCanceled = "mailservice:ocanceled";
        public const string EmailOrderCompleted = "mailservice::ocompleted";

        public Mail Add(Mail email)
        {
            collection.Insert(email);
            return email;
        }

        public IList<MailQueue> GetQueue(ObjectId mailId)
        {
            using (var client = RedisManager.GetClient())
            {
                return client.As<MailQueue>()
                    .Sets[string.Format(RedisEmailQueue, mailId)]
                    .ToList();
            }
        }

        public void RemoveQueue(ObjectId mailId, MailQueue[] queues)
        {
            using (var client = RedisManager.GetClient())
            {
                foreach (var mailQueue in queues)
                {
                    client.As<MailQueue>()
                        .Sets[string.Format(RedisEmailQueue, mailId)]
                        .Remove(mailQueue);
                }
            }
        }
        /// <summary>
        /// Saves the initial emails templates required by application
        /// </summary>
        public void AddInitialEmails(Dictionary<MailCommon, ObjectId> emails)
        {
            using (var redisClient = RedisManager.GetClient())
            {
                emails.ToList().ForEach(x => redisClient.Set(string.Format(RedisInitEmails, x.Key), x.Value));
            }
        }

        public ObjectId GetInitMailId(MailCommon mailCommon)
        {
            using (var redisClient = RedisManager.GetClient())
            {
                return redisClient.Get<ObjectId>(string.Format(RedisInitEmails, mailCommon));
            }
        }

        public void AddToQueue(ObjectId mailId, MailQueue queue)
        {
            using (var redisClient = RedisManager.GetClient())
            {
                redisClient.As<MailQueue>()
                    .Sets[string.Format(RedisEmailQueue, mailId)]
                    .Add(new MailQueue {Email = queue.Email, Parameters = queue.Parameters});
            }

        }

        /// <summary>
        /// Indicates if there'res any sending in queue list for the email
        /// </summary>
        /// <param name="mailId"></param>
        /// <returns></returns>
        public bool EmailHasQueue(ObjectId mailId)
        {
            using (var client = RedisManager.GetClient())
            {
                return !client
                            .As<MailQueue>()
                            .Sets[string.Format(RedisEmailQueue, mailId)]
                            .Any();
            }
        }

        public Mail GetEmailOrderCompleted()
        {
            using (var redisClient = RedisManager.GetClient())
            {
                var id = redisClient.Get<ObjectId>(EmailOrderCompleted);
                return FindOne(Query.EQ("_id", id));
            }
        }

        public void SaveEmailOrderCompleted(ObjectId emailId)
        {
            using (var client = RedisManager.GetClient())
                client.Set(EmailOrderCompleted, emailId);
        }

        public void SaveEmailOrderCanceled(ObjectId emailId)
        {
            using (var client = RedisManager.GetClient())
                client.Set(EmailOrderCanceled, emailId);
        }
    }
}
