﻿using ServiceStack.Redis;

namespace Ahav.ServiceInterface.Data
{
    public class SetupRepository
    {
        public const string RedisRegistrationsClosed = "setup::on";

        public IRedisClientsManager RedisManager { get; set; }
 
        public bool RegistrationsAreOpen()
        {
            return RedisManager.GetClient().Get<bool>(RedisRegistrationsClosed);
        }
    }
}
