﻿using System.Linq;
using MongoDB.Bson;
using ServiceStack.Redis;

namespace Ahav.ServiceInterface.Data
{
    public class AhavHubRepository
    {
        public IRedisClientsManager RedisManager { get; set; }

        // User connections
        public const string RedisUserConnectionsId = "ruser::{0}::connections";
        // Users Online
        public const string RedisOnlineUsers = "rusers::online";
        public const string RedisUserFriends = "ruser::{0}::friends";
        public const string RedisUserFriendsOnline = "ruser::{0}::friends_on";

        public void Connect(string connectionId, ObjectId userId)
        {
            using (var client = RedisManager.GetClient())
            {
                var friendsSet = client
                    .As<ObjectId>()
                    .Sets[string.Format(RedisUserFriends, userId)];

                var usersOnline = client
                    .As<ObjectId>()
                    .Sets[string.Format(RedisOnlineUsers)];

                var friendsOnline = usersOnline.Intersect(friendsSet);
                
                if (friendsOnline.Any())
                {
                    foreach (var friend in friendsOnline)
                    {
                        // Add current user to friends sets
                        client.As<ObjectId>()
                            .Sets[string.Format(RedisUserFriendsOnline, friend)]
                            .Add(userId);

                        // Set user friends online
                        client.As<ObjectId>()
                            .Sets[string.Format(RedisUserFriendsOnline, userId)]
                            .Add(friend);
                    }
                }
            }
        }
    }
}