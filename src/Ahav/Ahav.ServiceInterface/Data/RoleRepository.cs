﻿using System.Linq;
using Ahav.ServiceModel.Common;
using MongoDB.Bson;
using ServiceStack.Redis;

namespace Ahav.ServiceInterface.Data
{
    public class RoleRepository
    {
        public IRedisClientsManager RedisManager { get; set; }
        public string[] GetRoles(ObjectId userId)
        {
            using (var client = RedisManager.GetClient())
            {
                return client.As<string>()
                    .Sets[RedisKeys.Roles(userId)]
                    .ToArray();
            }
        }
        public void SaveRole(ObjectId userId, string role)
        {
            using (var client = RedisManager.GetClient())
            {
                client.As<string>()
                    .Sets[RedisKeys.Roles(userId)]
                    .Add(role);
            }
        }
        public void DeleteRole(ObjectId userId, string role)
        {
            using (var client = RedisManager.GetClient())
            {
                client.As<string>()
                    .Sets[RedisKeys.Roles(userId)]
                    .Remove(role);
            }
        }

    }
}
