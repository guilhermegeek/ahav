﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.Interfaces;
using Ahav.ServiceModel.DataDtos;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Operations;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ServiceStack.Redis;

namespace Ahav.ServiceInterface.Data
{
    public class TemplateRepository : IRepository
    {
        public MongoDatabase MongoDatabase { get; set; }
        public IRedisClientsManager RedisManager { get; set; }
        public const string RedisTemplatesAvailable = "templates::available";
        public const string RedisTemplate = "template::{0}";
        private MongoCollection<ContentTemplateIds> collectionContentTemplateIds;
        private MongoCollection<User> userCool;
        private MongoCollection<ApplicationAhav> projectColl;
        private MongoCollection<Template> templateColl;

        public TemplateRepository()
        {
            this.collectionContentTemplateIds = MongoDbHelper.Instance.Database.GetCollection<ContentTemplateIds>(typeof(ContentTemplateIds).Name);
            this.userCool = MongoDbHelper.Instance.Database.GetCollection<User>(typeof(User).Name);
            this.projectColl = MongoDbHelper.Instance.Database.GetCollection<ApplicationAhav>(typeof (ApplicationAhav).Name);
            this.templateColl = MongoDbHelper.Instance.Database.GetCollection<Template>(typeof (Template).Name);
        }

        public bool IsOwner(ObjectId templateId, ObjectId userId)
        {
            throw new NotImplementedException();
            ;
            return false;
        }

        public Template GetTemplate(ObjectId templateId)
        {
            return templateColl.FindOneByIdAs<Template>(templateId);
            //using (var redisClient = RedisManager.GetClient())
            //{
            //    return redisClient.As<ContentTemplate>().GetById(templateId);
            //}
        }
        public FindTemplateResponse FindAll(FindTemplate request)
        {
            using(var redisClient = RedisManager.GetClient())
            {
                var tpls =  redisClient.As<Template>().GetAll().Take(100).ToList();
                var response = new FindTemplateResponse();
                tpls.ForEach(x => response.Template.Add(x.ToInfo()));
                return response;
            }
        }
        public ObjectId[] GetTemplatesAvailable(ObjectId projectId)
        {
            return projectColl.FindOneByIdAs<ApplicationAhav>(projectId).Templates;
        }
        public List<Template> GetTemplates(ObjectId[] templateIds, int skip = 0, int take = 100)
        {
            return templateColl.FindAs<Template>(Query<Template>.In(x => x.Id, templateIds))
                .ToList();
            //using (var redisClient = RedisManager.GetClient())
            //{
            //    return redisClient.As<ContentTemplate>().GetByIds(templateIds).ToList();
            //}
        }
        public IList<ContentTemplateInfo> GetByProject(ObjectId projectId)
        {
            var ids = projectColl.FindOneByIdAs<ApplicationAhav>(projectId).Templates;
            var result = new List<ContentTemplateInfo>();
            var tpls = templateColl.FindAs<Template>(Query<Template>.In(x => x.Id, ids))
                .ToList();
            foreach (var tpl in tpls)
            {
                result.Add(tpl.ToInfo());
            }
            return result;
        }
        public Template AddTemplate(ObjectId projectId, Template template)
        {
            using (var redisClient = RedisManager.GetClient())
            {
                //template.Id = redisClient.As<ContentTemplate>().GetNextSequence();
                //redisClient
                //    .Store(template);
                templateColl.Insert(template);
                collectionContentTemplateIds.Save(new ContentTemplateIds { TemplateId = template.Id });
                projectColl.Update(Query<ApplicationAhav>.EQ(x => x.Id, projectId),
                                Update<ApplicationAhav>.Push(x => x.Templates, template.Id));
            }
            return template;
        }
        public void AddContentIdToTemplate(ObjectId templateId, ObjectId contentId)
        {
            collectionContentTemplateIds.Update(Query<ContentTemplateIds>.EQ(x => x.TemplateId, templateId),
                                                Update<ContentTemplateIds>.Push(x => x.ContentId, contentId));
        }
        public void RemoveContentIdFromTemplate(ObjectId templateId, ObjectId contentId)
        {
            collectionContentTemplateIds.Update(Query<ContentTemplateIds>.EQ(x => x.TemplateId, templateId),
                                                Update<ContentTemplateIds>.Pull(x => x.ContentId, contentId));
        }
        public ContentTemplateDto SaveTemplate(UpdateContentTemplate contentTemplate)
        {
            using (var redisClient = RedisManager.GetClient())
            {
                templateColl.Update(Query<Template>.EQ(x => x.Id, contentTemplate.Id),
                                    Update<Template>
                                        .Set(x => x.Name, contentTemplate.Name)
                                        .Set(x => x.Title, contentTemplate.Title)
                                        .Set(x => x.Body, contentTemplate.Body)
                                        .Set(x => x.Less, contentTemplate.Less));
                //collectionContentTemplateIds.Save(new ContentTemplateIds { TemplateId = template.Id });
            }
            return templateColl.FindOneByIdAs<Template>(contentTemplate.Id).ToDto();
        }
        public void MigrateContentTemplate(ObjectId templateId, ObjectId templateMigrationId, ObjectId[] contentsId)
        {
            foreach (var id in contentsId)
            {
                collectionContentTemplateIds.Update(Query<ContentTemplateIds>.EQ(x => x.TemplateId, templateId),
                                         Update<ContentTemplateIds>.Set(x => x.TemplateId, templateMigrationId));
            }
        }
        public void RemoveTemplate(ObjectId templateId)
        {
            templateColl.Remove(Query<Template>.EQ(x => x.Id, templateId));
            //using (var redisClient = RedisManager.GetClient())
            //{
            //    redisClient.DeleteById<ContentTemplate>(templateId);
            //}
        }
        public ObjectId[] GetContentIdsFromTemplate(ObjectId templateId)
        {
            var template = collectionContentTemplateIds.FindOneByIdAs<ContentTemplateIds>(templateId);
            if (template == null)
                return null;
            return template.ContentId;
        }
        public ObjectId[] GetAvailableTemplatesIds(int skip, int take)
        {
            using (var redisClient = RedisManager.GetClient())
            {
                return redisClient
                    .As<ObjectId>()
                    .Lists[string.Format(RedisTemplatesAvailable)]
                    .GetRange(skip, skip + take).ToArray();
            }
        }
        /// <summary>
        /// Set the template available to replicate
        /// </summary>
        public void SaveTemplateAvailable(ObjectId templateId)
        {
            using (var redisClient = RedisManager.GetClient())
            {
                redisClient
                    .As<ObjectId>()
                    .Lists[string.Format(RedisTemplatesAvailable)]
                    .Add(templateId);
            }
        }
        public WriteConcernResult DeleteFile(ObjectId contentId, ObjectId fileId)
        {
            return templateColl.Update(
                Query<Template>.EQ(x => x.Id, contentId),
                Update<Template>.Pull(x => x.Files, fileId));
        }
        public WriteConcernResult AddFile(ObjectId templateId, ObjectId fileId)
        {
            return templateColl.Update(
                Query<Template>.EQ(x => x.Id, templateId),
                Update<Template>.Push(x => x.Files, fileId));
        }
        public WriteConcernResult AddFile(ObjectId templateId, ObjectId[] fileId)
        {
            return templateColl.Update(
                Query<Template>.EQ(x => x.Id, templateId),
                Update<Template>.PushAll(x => x.Files, fileId));
        }
        public IList<ObjectId> GetFilesIds(ObjectId templateId, int skip = 0, int take = 100)
        {
            return templateColl.FindOneByIdAs<Template>(templateId).Files;
        }
        
        public void SaveViewPage(ObjectId templateId, string body, string culture, ViewsTypes view)
        {
            templateColl.Update(Query<Template>.EQ(x => x.Id, templateId),
                Update.Push("Views.))
        }
    }
}
