﻿using System;
using System.Linq;
using Ahav.Infrastructure;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace Ahav.ServiceInterface.Data
{

    public class AccountRepository : Repository<User>
    {
        public MongoDatabase MongoDb { get; set; }
        public IMongoFields UserDtoFields = Fields<UserDto>.Include(x => x.Id, x => x.AccountState,x => x.Email);
        //private MongoCollection<AccountAhav> collectionAhavAccount;


        public AccountRepository()
        {
            //this.collectionAhavAccount =
            //    MongoDbHelper.Instance.Database.GetCollection<AccountAhav>(typeof (AccountAhav).Name);
        }
        
        private string IndexAuth(ObjectId userId)
        {
            return string.Format("urn:Auth:{0}", userId);
        }
        private string UserRecoverPassword(string email)
        {
            return string.Format("urn:recoverpw:{0}", email);
        }
        public bool CheckEmailAvailability(string email)
        {
            return collection.FindOneAs<User>(Query<User>.EQ(x => x.PrimaryEmail, email)) == null;
        }
        //public bool CheckMailAhavAccountAvaileForRegister(string email)
        //{
        //    return collectionAhavAccount.FindOneAs<AccountAhav>(Query<AccountAhav>.EQ(x => x.PrimaryEmail, email)) == null;
        //}
        //public WriteConcernResult AddProject(ObjectId userId, ObjectId projectId)
        //{
        //    return collectionAhavAccount.Update(Query<AccountAhav>.EQ(x => x.Id, userId),
        //                                 Update<AccountAhav>.Push(x => x.Projects, projectId));
        //}
        /// <summary>
        /// Save permissions on account feature
        /// </summary>
        /// <param name="feature"></param>
        /// <param name="userId"></param>
        public void SaveProjectPermissions(AhavProjectPermissions entity)
        {
            using(var redisClient = RedisManager.GetClient())
            {
                redisClient.As<AhavProjectPermissions>()
                    .Store(entity);
            }
        }
        public AhavProjectPermissions GetPermissions(ObjectId projectId)
        {
            using(var redisClient = RedisManager.GetClient())
            {
                return redisClient.As<AhavProjectPermissions>()
                    .GetById(projectId);
            }
        } 
        //public AccountAhav CreateAccount(AccountAhav account)
        //{
        //    var result = collectionAhavAccount.Insert(account);
        //    return account;
        //}
        //public AccountAhav GetAccount(ObjectId accountId)
        //{
        //    return collectionAhavAccount.FindOneByIdAs<AccountAhav>(accountId);
        //}

        public UserDto GetByEmail(string email)
        {
            return collection.FindAs<UserDto>(Query<User>.EQ(x => x.PrimaryEmail, email))
                .SetFields(UserDtoFields).SingleOrDefault();
        }

        public void UpdatePassword(ObjectId userId, string newPassword)
        {
            var result = collection.Update(
                Query<User>.EQ(x => x.Id, userId),
                Update<User>.Set(x => x.Password, newPassword)
                );
            if(!result.Ok)
                throw new MongoInternalException("Update password failed");
        }
        public void SaveRecoverPasswordToken(string email, string token)
        {
            using(var redisClient = RedisManager.GetClient())
            {
                redisClient.Set(UserRecoverPassword(email), token);
            }
        }
        public bool ConfirmRecoverPasswordToken(string email, string token)
        {
            using(var redisClient = RedisManager.GetClient())
            {
                return redisClient.Get<string>(UserRecoverPassword(email)) == token;
            }
        }
        public string GetRecoverPasswordToken(string email)
        {
            using(var redisClient = RedisManager.GetClient())
            {
                return redisClient.Get<string>(UserRecoverPassword(email));
            }
        }
        
        
        public void Update(ObjectId userId, IMongoUpdate[] updateValues)
        {
            IMongoUpdate update = Update<User>.Combine(updateValues);
            var query = Query<User>.EQ(x => x.Id, userId);
            collection.Update(query, update); 
        }
        public void UpdateUserState(ObjectId userId, AccountState state)
        {
            collection.Update(Query<User>.EQ(x => x.Id, userId), Update<User>.Set(x => x.AccountState, state));
        }
        public bool IsConfirmed(ObjectId userId)
        {
            var user = collection
                .FindOneAs<User>(Query<User>.EQ(x => x.Id, userId));
            if(user == null)
                throw new ArgumentNullException(string.Format("User {0} doesn't exists", userId));
            return user.AccountState == AccountState.Confirmed;

        }
        public User Get(ObjectId userId)
        {
            return collection.FindOneByIdAs<User>(userId);
        }
        
       
        public bool ValidatePassord(ObjectId userId, string password)
        {
            var user = collection.FindOneByIdAs<User>(userId);
            if (user == null)
                throw new Exception("User not found");
            return user.Password == password;
        }
        public bool ValidatePassord(string email, string password)
        {
            return FindOne(Query.And(
                Query<User>.EQ(x => x.PrimaryEmail, email),
                Query<User>.EQ(x => x.Password, password))
                       ) != null;
        }
        public User FindByEmailAndPassword(string email, string password)
        {
            return FindOne(Query.And(
                Query<User>.EQ(x => x.PrimaryEmail, email),
                Query<User>.EQ(x => x.Password, password))
                );
        }
    }
}
