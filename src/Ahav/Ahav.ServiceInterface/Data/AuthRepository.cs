﻿using Ahav.Interfaces;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson;
using ServiceStack.Redis;

namespace Ahav.ServiceInterface.Data
{
    public class AuthRepository : IRepository
    {
        public IRedisClientsManager RedisManager { get; set; }

        public AhavAuth GetAuth(ObjectId userId)
        {
            using (var client = RedisManager.GetClient())
            {
                return client.As<AhavAuth>().GetValue(RedisKeys.Auth(userId));
            }
        }
        public void CreateAuth(AhavAuth auth)
        {
            using (var client = RedisManager.GetClient())
            {
                client.As<AhavAuth>().SetEntry(RedisKeys.Auth(auth.UserId), auth);
            }
        }
        public void DeleteAuth(ObjectId userId)
        {
            using (var client = RedisManager.GetClient())
            {
                client.Remove(RedisKeys.Auth(userId));
                client.Remove(RedisKeys.TokenAccess(userId));
            }
        }
    }
}
