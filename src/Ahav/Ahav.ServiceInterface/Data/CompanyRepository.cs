﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Infrastructure;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;

namespace Ahav.ServiceInterface.Data
{
    public class CompanyRepository : Repository<Company>
    {
        public CreatorOrganization GetCreator(ObjectId companyId)
        {
            var company = this.Get(companyId);
            return new CreatorOrganization
                   {
                       Name = company.Name,
                       Uri = string.Format("/{0}", company.Name)
                   };

        }
    }
}
