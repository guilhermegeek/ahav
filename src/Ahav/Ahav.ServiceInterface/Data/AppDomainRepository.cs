﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Interfaces;
using ServiceStack.Redis;

namespace Ahav.ServiceInterface.Data
{
    public class AppDomainRepository : IRepository
    {
        public void AddPublicDomain(string domain)
        {
            using (var redisClient = RedisManager.GetClient())
            {
                redisClient.Lists[RedisSetPublicDomains].Add(domain);
            }
        }

        public void RemovePublicDomain(string domain)
        {
            using (var redisClient = RedisManager.GetClient())
            {
                redisClient.Lists[RedisSetPublicDomains].Remove(domain);
            }
        }

        public string[] GetPublicDomains()
        {
            using (var redisClient = RedisManager.GetClient())
            {
                return redisClient.Lists[RedisSetPublicDomains].ToArray();
            }
        }

        public const string RedisSetPublicDomains = "public::domains";
        public IRedisClientsManager RedisManager { get; set; }
    }
}
