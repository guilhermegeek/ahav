﻿using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;

namespace Ahav.ServiceInterface.Data.Mappings
{
    public static class MapNewsletterTemplateCollection
    {
        public static void Map()
        {
            if(!BsonClassMap.IsClassMapRegistered(typeof(NewsletterTemplate)))
            {
                BsonClassMap.RegisterClassMap<NewsletterTemplate>(cm =>
                                                                      {
                                                                          cm.MapIdProperty(x => x.Id).SetIdGenerator(
                                                                              ObjectIdGenerator.Instance);
                                                                          cm.MapProperty(x => x.Body);
                                                                          cm.MapProperty(x => x.Subject);
                                                                      });
            }
        }
    }
}
