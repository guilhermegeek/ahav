﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson.Serialization;

namespace Ahav.ServiceInterface.Data.Mappings
{
    public static class MapPlaceCollection 
    {
        public static void Register()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof (Place)))
            {
                BsonClassMap.RegisterClassMap<Place>(m =>
                                                     {
                                                         m.MapIdProperty(x => x.Id);
                                                         m.MapProperty(x => x.Name);
                                                         m.MapProperty(x => x.Address);
                                                         m.MapProperty(x => x.Location);
                                                         m.MapProperty(x => x.OpeningHours);
                                                         m.MapProperty(x => x.Title);
                                                         m.MapProperty(x => x.Description);
                                                     });
            }
        }
    }
}
