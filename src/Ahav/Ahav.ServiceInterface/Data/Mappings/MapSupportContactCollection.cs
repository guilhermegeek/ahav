﻿using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;

namespace Ahav.ServiceInterface.Data.Mappings
{
    public static class MapSupportContactCollection
    {
        public static void RegisterMappings()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(SupportContact)))
            {
                BsonClassMap.RegisterClassMap<SupportContact>(cm =>
                {
                    cm.MapIdField(x => x.Id).SetIdGenerator(
                        ObjectIdGenerator.Instance);
                    cm.MapProperty(x => x.Subject);
                    cm.MapProperty(x => x.Message);
                    cm.MapProperty(x => x.Name);
                    cm.MapProperty(x => x.Email);
                    cm.MapProperty(x => x.Contact);
                    cm.MapProperty(x => x.State);
                });
            }
        }
    }
}
