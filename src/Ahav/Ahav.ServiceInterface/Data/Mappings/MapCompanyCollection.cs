﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;

namespace Ahav.ServiceInterface.Data.Mappings
{
    public static class MapCompanyCollection
    {
        public static void Register()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof (Company)))
            {
                BsonClassMap.RegisterClassMap<Company>(m =>
                                                       {
                                                           m.MapIdProperty(x => x.Id).SetIdGenerator(ObjectIdGenerator.Instance);
                                                           m.MapProperty(x => x.EmployeIds);
                                                           m.MapProperty(x => x.Name);
                                                       });
            }
        }
    }
}
