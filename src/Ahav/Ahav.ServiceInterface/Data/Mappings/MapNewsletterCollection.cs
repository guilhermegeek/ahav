﻿using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;

namespace Ahav.ServiceInterface.Data.Mappings
{
    public static class MapNewsletterCollection
    {
        public static void RegisterMappings()
        {
            // Newsletters
            if (!BsonClassMap.IsClassMapRegistered(typeof(Newsletter)))
            {
                BsonClassMap.RegisterClassMap<Newsletter>(cm =>
                {
                    cm.MapIdField(x => x.Id)
                        .SetIdGenerator(ObjectIdGenerator.Instance);
                    cm.MapProperty(x => x.Subject);
                    cm.MapProperty(x => x.Content);
                    cm.MapProperty(x => x.IsHtml);
                    cm.MapProperty(x => x.State);
                    cm.MapProperty(x => x.Files);
                    cm.MapProperty(x => x.Name);
                });
            }
        }
    }
}
