﻿using Ahav.Infrastructure;
using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver.Builders;

namespace Ahav.ServiceInterface.Data.Mappings
{
    public static class MapContentCollection
    {
        public static void RegisterMappings()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(Content)))
            {
                BsonClassMap.RegisterClassMap<Content>(cm =>
                {
                    cm.MapIdField(x => x.Id).SetIdGenerator(
                        ObjectIdGenerator.Instance);
                    cm.MapProperty(x => x.ProjectId).SetIsRequired(true);
                    cm.MapProperty(x => x.Slug);
                    cm.MapProperty(x => x.Title);
                    cm.MapProperty(x => x.Body);
                    cm.MapProperty(x => x.Javascript);
                    cm.MapProperty(x => x.Owner);
                    cm.MapProperty(x => x.Template);
                    cm.MapProperty(x => x.Files);
                    cm.MapProperty(x => x.Comments);
                    cm.MapProperty(x => x.CommentsCount);
                    cm.MapProperty(x => x.Css);
                    cm.MapProperty(x => x.Description);
                });
                MongoDbHelper.Instance.Database.GetCollection<Content>(typeof(Content).Name).EnsureIndex(new IndexKeysBuilder<Content>().Ascending(x => x.Slug), IndexOptions.SetUnique(true));
            }
        }
    }
}
