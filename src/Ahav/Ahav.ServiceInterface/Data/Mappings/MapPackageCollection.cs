﻿using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;

namespace Ahav.ServiceInterface.Data.Mappings
{
    public class MapPackageCollection
    {
        public static void RegisterMappings()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(AhavPackage)))
            {
                BsonClassMap.RegisterClassMap<AhavPackage>(cm =>
                {
                    cm.MapIdProperty(x => x.Id)
                        .SetIdGenerator(ObjectIdGenerator.Instance);
                    cm.MapProperty(x => x.Title);
                    cm.MapProperty(x => x.Description);
                    cm.MapProperty(x => x.IsActive);
                    cm.MapProperty(x => x.Start);
                    cm.MapProperty(x => x.End);
                    cm.MapProperty(x => x.Ammount);
                    cm.MapProperty(x => x.Features);
                });
            }
        }
    }
}
