﻿using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;

namespace Ahav.ServiceInterface.Data.Mappings
{
    public static class MapTemplateCollection
    {
        public static void RegisterMappings()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(Template)))
            {
                BsonClassMap.RegisterClassMap<Template>(cm =>
                {
                    cm.MapIdProperty(x => x.Id).SetIdGenerator(ObjectIdGenerator.Instance);
                    cm.MapProperty(x => x.Name);
                    cm.MapProperty(x => x.Title);
                    cm.MapProperty(x => x.Body);
                    cm.MapProperty(x => x.BodyBackStretchImagesUrl);
                    cm.MapProperty(x => x.Less);
                    cm.MapProperty(x => x.Menu);
                    cm.MapProperty(x => x.Files);
                    cm.MapProperty(x => x.FavIcon);
                    cm.MapProperty(x => x.TemplateType).SetIsRequired(true);
                });
            }
        }
    }
}
