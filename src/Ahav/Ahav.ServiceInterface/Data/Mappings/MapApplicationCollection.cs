﻿using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using AutoMapper;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;

namespace Ahav.ServiceInterface.Data.Mappings
{
    public static class MapApplicationCollection
    {
        public static void RegisterMappings()
        {
            /// MongoDb Mappings
            if (!BsonClassMap.IsClassMapRegistered(typeof(ApplicationAhav)))
            {
                BsonClassMap.RegisterClassMap<ApplicationAhav>(cm =>
                {
                    cm.MapIdProperty(x => x.Id)
                        .SetIdGenerator(ObjectIdGenerator.Instance);
                    cm.MapProperty(x => x.Name);
                    cm.MapProperty(x => x.Description);
                    cm.MapProperty(x => x.OwnerId);
                    cm.MapProperty(x => x.Packages);
                    cm.MapProperty(x => x.Domain);
                    cm.MapProperty(x => x.Coupons);
                    cm.MapProperty(x => x.Contents);
                    cm.MapProperty(x => x.Templates);
                    cm.MapProperty(x => x.Features);
                    cm.MapProperty(x => x.Blogs);
                    cm.MapProperty(x => x.Articles);
                    cm.MapProperty(x => x.Attachments);
                    cm.MapProperty(x => x.ProjectState);
                    cm.MapProperty(x => x.BlogTemplates);
                    cm.MapProperty(x => x.SocialProiderAppsIds);
                });
            }

            // AutoMapper Mappings
            Mapper.CreateMap<ApplicationAhav, AhavProjectDto>().ConvertUsing(app =>
                                                                             {
                                                                                 return new AhavProjectDto
                                                                                        {
                                                                                            Id = app.Id,
                                                                                            Name = app.Name,
                                                                                            Description = app.Description,
                                                                                            Domain = app.Domain,
                                                                                            Attachments = app.Attachments,
                                                                                            Features = app.Features,
                                                                                            Packages = app.Packages,
                                                                                            ProjectState = app.ProjectState
                                                                                        };
                                                                             });
        }
    }
}
