﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;

namespace Ahav.ServiceInterface.Data.Mappings
{
    public static class MapLogCollection
    {
        public static void Register()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof (LogEntity)))
            {
                BsonClassMap.RegisterClassMap<LogEntity>(m =>
                                                         {
                                                             m.MapIdProperty(x => x.Id).SetIdGenerator(ObjectIdGenerator.Instance);
                                                             m.MapProperty(x => x.Message);
                                                         });
            }
        }
    }
}
