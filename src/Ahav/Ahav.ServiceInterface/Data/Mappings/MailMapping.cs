﻿using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;

namespace Ahav.ServiceInterface.Data.Mappings
{
    public static class MailMapping
    {
        public static void RegisterMappings()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(Mail)))
            {
                BsonClassMap.RegisterClassMap<Mail>(cm =>
                {
                    cm.MapIdField(x => x.Id).SetIdGenerator(
                        ObjectIdGenerator.Instance);
                    cm.MapProperty(x => x.Subject);
                    cm.MapProperty(x => x.Content);
                    cm.MapProperty(x => x.IsHtml);
                    cm.MapProperty(x => x.MailRecipients);
                });
            }
        }
    }
}
