﻿using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson.Serialization;

namespace Ahav.ServiceInterface.Data.Mappings
{
    public static class ContentTemplateIdsMappings
    {
        public static void RegisterMappings()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(ContentTemplateIds)))
            {
                BsonClassMap.RegisterClassMap<ContentTemplateIds>(cm =>
                {
                    cm.MapIdField(x => x.TemplateId);
                    cm.MapProperty(x => x.ContentId);
                });
            }
        }
    }
}
