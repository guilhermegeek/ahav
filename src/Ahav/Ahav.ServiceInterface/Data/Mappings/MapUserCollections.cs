﻿using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;

namespace Ahav.ServiceInterface.Data.Mappings
{
    public static class MapUserCollections
    {
        public static void RegisterMappings()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(User)))
                BsonClassMap.RegisterClassMap<User>(cm =>
                {
                    cm.MapIdField(x => x.Id).SetIdGenerator(
                        ObjectIdGenerator.Instance);
                    cm.MapProperty(x => x.PrimaryEmail).SetOrder(2).SetIsRequired(true);
                    cm.MapProperty(x => x.FirstName).SetOrder(3).SetIsRequired(
                        true);
                    cm.MapProperty(x => x.LastName).SetOrder(4).SetIsRequired(
                        true);
                    cm.MapProperty(x => x.Password).SetOrder(5);
                    cm.MapProperty(x => x.Birthday).SetOrder(6).SetIsRequired(
                        true);
                    cm.MapProperty(x => x.AccountState);
                    cm.MapProperty(x => x.Websites);
                    cm.MapProperty(x => x.Mobiles);
                    cm.MapProperty(x => x.PublicEmail);
                    cm.MapProperty(x => x.Projects);
                    // Ecommerce
                    cm.MapProperty(x => x.OrdersCompleted);
                });
        }
    }
}
