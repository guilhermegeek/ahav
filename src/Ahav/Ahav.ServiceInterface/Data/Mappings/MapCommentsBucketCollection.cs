﻿using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson.Serialization;

namespace Ahav.ServiceInterface.Data.Mappings
{
    public static class MapCommentsBucketCollection 
    {
        public static void Map()
        {
            if(!BsonClassMap.IsClassMapRegistered(typeof(CommentsBucket)))
            {
                BsonClassMap.RegisterClassMap<CommentsBucket>(cm =>
                                                                  {
                                                                      cm.MapIdProperty(x => x.RelativeDocumentId);
                                                                      cm.MapProperty(x => x.Comments);
                                                                      cm.MapProperty(x => x.Count);
                                                                      cm.MapProperty(x => x.Index);
                                                                  });
            }
        }
    }
}
