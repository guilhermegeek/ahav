﻿using Ahav.ServiceModel.Operations;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;

namespace Ahav.ServiceInterface.Data.Mappings
{
    public static class MapSupportTicketCollection
    {
        public static void RegisterMappings()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof (SupportTicket)))
            {
                BsonClassMap.RegisterClassMap<SupportTicket>(cm =>
                                                                 {
                                                                     cm.MapIdField(x => x.Id).SetIdGenerator(
                                                                         ObjectIdGenerator.Instance);
                                                                     cm.MapProperty(x => x.Title);
                                                                     cm.MapProperty(x => x.Content);
                                                                     cm.MapProperty(x => x.Priority);
                                                                     cm.MapProperty(x => x.State);
                                                                     // NComments
                                                                     cm.MapProperty(x => x.Comments);
                                                                     cm.MapProperty(x => x.CommentsCount);
                                                                 });
            }
        }
    }
}