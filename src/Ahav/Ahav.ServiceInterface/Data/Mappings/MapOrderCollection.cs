﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using ServiceStack.Common;

namespace Ahav.ServiceInterface.Data.Mappings
{
    public static class MapOrderCollection
    {
        public static void Register()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof (Order)))
            {
                BsonClassMap.RegisterClassMap<Order>(m =>
                                                     {
                                                         m.MapIdProperty(x => x.Id).SetIdGenerator(ObjectIdGenerator.Instance);
                                                         m.MapProperty(x => x.Offers);
                                                         m.MapProperty(x => x.Customer);
                                                         m.MapProperty(x => x.Merchant);
                                                         m.MapProperty(x => x.PaymentDue);
                                                         m.MapProperty(x => x.PaymentMethod);
                                                         m.MapProperty(x => x.PaymentMethodId);
                                                         m.MapProperty(x => x.DeliveryMethod);
                                                         m.MapProperty(x => x.Status).SetIsRequired(true);

                                                     });
            }
        }
    }
}
