﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Interfaces;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson;
using MongoDB.Driver;
using ServiceStack.Redis;

namespace Ahav.ServiceInterface.Data
{
    public class PlaceRepository : IRepository
    {
        public IRedisClientsManager RedisManager { get; set; }
        private MongoCollection<Place> PlaceCollection { get; set; }

        public Place Create(Place entity)
        {
            PlaceCollection.Insert(entity);
            return entity;
        }

        public Place GetById(ObjectId id)
        {
            return PlaceCollection.FindOneById(id);
        }
    }
}
