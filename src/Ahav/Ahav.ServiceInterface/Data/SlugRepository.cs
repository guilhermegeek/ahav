﻿using Ahav.Infrastructure;
using Ahav.Interfaces;
using MongoDB.Bson;
using ServiceStack.Redis;

namespace Ahav.ServiceInterface.Data
{
    public enum SlugType { Content, Article, Blog }
    public class SlugRepository : IRepository
    {
        public IRedisClientsManager RedisManager { get; set; }
        /// <summary>
        /// Redis Slug - Slug Type, Project Id, Slug
        /// </summary>
        public const string RedisSlug = "urn::{0}::{1}::{2}";

        public void RemoveSlug(ObjectId projectId, SlugType type, string slug)
        {
            using (var redisClient = RedisManager.GetClient())
            {
                redisClient.Remove(string.Format(RedisSlug, type, projectId, slug));
            }
        }
        public void AddSlug(ObjectId projectId, SlugType type, ObjectId contentId, string slug)
        {
            using (var redisClient = RedisManager.GetClient())
            {
                redisClient
                    .Set(string.Format(RedisSlug, type, projectId, slug), contentId);
            }
        }
        /// <summary>
        /// Indicates if a slug doesnt exist (true)
        /// </summary>
        /// <param name="slug"></param>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public bool SlugIsAvailable(ObjectId projectId, SlugType type, string slug)
        {
            using (var redisClient = RedisManager.GetClient())
            {
                return redisClient.Get<ObjectId>(string.Format(RedisSlug, type, projectId, slug)).IsEmpty();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="slug"></param>
        /// <param name="contentId">The actual content Id to be ignored</param>
        /// <returns></returns>
        public bool SlugIsAvailable(string slug, SlugType type, ObjectId projectId, ObjectId contentId)
        {
            using (var redisClient = RedisManager.GetClient())
            {
                var rSlug = redisClient.Get<ObjectId>(string.Format(RedisSlug, type, projectId, slug));
                if (rSlug.IsEmpty() || rSlug == contentId)
                    return true;
                return false;
            }
        }
        public ObjectId GetId(ObjectId projectId, SlugType type, string slug)
        {
            using (var redisClient = RedisManager.GetClient())
            {
                return redisClient.Get<ObjectId>(string.Format(RedisSlug, type, projectId, slug));
            }
        }
    }
}
