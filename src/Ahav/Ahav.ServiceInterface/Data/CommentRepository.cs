﻿using System;
using System.Collections.Generic;
using Ahav.Infrastructure;
using Ahav.Interfaces;
using Ahav.ServiceModel.DataDtos;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using MongoDB.Driver.Builders;
using ServiceStack.Redis;

namespace Ahav.ServiceInterface.Data
{
    /// <summary>
    /// Comments repository. 
    /// </summary>
    /// Comments are stored in MongoDB
    /// http://docs.mongodb.org/ecosystem/use-cases/storing-comments/
    public class CommentRepository : IRepository
    {
        public IRedisClientsManager RedisManager { get; set; }

        public ReadCommentResponse ReadComments<T>(ReadComments request)
        {
            var CommentsCollection = MongoDbHelper.Instance.Database
                .GetCollection<CommentsBucket>(string.Format("{0}{1}", typeof(T).Name, typeof(CommentsBucket).Name));
            int skip = request.Skip;
            int limit = request.Take;
            var comments = new List<NComment>();

            var buckets =
                CommentsCollection.Find(Query<CommentsBucket>.EQ(x => x.RelativeDocumentId, request.DocumentId))
                    .SetFields(Fields<CommentsBucket>
                        .Include(x => x.Comments)
                        .Slice(x => x.Comments, skip, limit))
                    .SetSortOrder(SortBy<CommentsBucket>.Ascending(x => x.Index));
            
            foreach (var bucket in buckets)
            {
                comments.AddRange(bucket.Comments);
                skip = skip - bucket.Count;
                limit = bucket.Count;
            }
            return new ReadCommentResponse
                   {
                       Results = comments
                   };
        }

        public CreateCommentResponse CreateComment<T>(CreateComment request)
        {
            var CommentsCollection = MongoDbHelper.Instance.Database
                .GetCollection<CommentsBucket>(string.Format("{0}{1}", typeof (T).Name, typeof (CommentsBucket).Name));
            var documentCollection =
                MongoDbHelper.Instance.Database.GetCollection<T>(typeof (T).Name);
            
            var comment = new NComment
                              {
                                  Comment = request.Message,
                                  CreatedAt = DateTime.Now,
                                  User = new AuthorInfo { }
                              };
            // Get the current bucket page
            var bucketIndex = documentCollection.FindOneAs<CommentsBucketData>(Query<CommentsBucketData>.EQ(x => x.Id, request.DocId))
                .CommentsPageCount;
            
            // Insert comment into bucket
            var queries = Query.And(
                Query<CommentsBucket>.EQ(x => x.RelativeDocumentId, request.DocId), 
                Query<CommentsBucket>.EQ(x => x.Index, bucketIndex));
            var bucketResponse = CommentsCollection.FindAndModify(queries, SortBy<CommentsBucket>.Ascending(x => x.Index),
                                                Update<CommentsBucket>
                                                .Push(x => x.Comments, comment)
                                                .Inc(x => x.Count, 1)
                                                , true, true);
            
            // If bucket already passed 100 comments, update document page size
            if (bucketResponse.GetModifiedDocumentAs<CommentsBucket>().Count > 100)
            {
                    documentCollection.Update(Query<CommentsBucketData>.EQ(x => x.Id, request.DocId),
                        Update < CommentsBucketData>.Inc(x => x.CommentsPageCount, 1));

            }
            
            return new CreateCommentResponse {Comment = comment};
        }
    }
}
