﻿using System;
using System.Linq;
using Ahav.Infrastructure;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Social.Domain;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ServiceStack.Redis;

namespace Ahav.ServiceInterface.Data
{
    public class SocialRepository
    {
        private MongoCollection userColl = MongoDbHelper.Instance.Database.GetCollection<User>(typeof (User).Name);

        public IRedisClientsManager RedisManager { get; set; }

        public const string RedisUserSocial = "user::{0}::stokens";
        public const string RedisFacebookTab = "fbtab::{0}";

        public void Save(ObjectId userId, SocialProvider provider, object providerId, string token, DateTime expiresAt)
        {
            var result = userColl.Update(Query<User>.EQ(x => x.Id, userId),
                Update<User>.Push(x => x.Social, new UserSocial
                                                 {
                                                     Provider = provider,
                                                     ProviderId =  providerId,
                                                     Token = token,
                                                     ExpiresAt = expiresAt
                                                 }));
            if (!result.Ok)
            {
                throw new Exception();
            }
        }
        public UserSocial GetAccessToken(ObjectId userId, SocialProvider provider)
        {
            return userColl.FindOneByIdAs<User>(userId)
                .Social.FirstOrDefault(x => x.Provider == provider);
        }
        public void SaveFacebookTab(string subDomain, ObjectId contentId)
        {
            using(var redisCient = RedisManager.GetClient())
            {
                redisCient.Set(string.Format(RedisFacebookTab, subDomain), contentId);
            }
        }
        public ObjectId GetFacebookTab(string subDomain)
        {
            using(var redisClient = RedisManager.GetClient())
            {
                return redisClient.Get<ObjectId>(string.Format(RedisFacebookTab, subDomain));
            }
        }
    }
}
