﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ServiceStack.Redis;

namespace Ahav.ServiceInterface.Data
{
    public class ApplicationAhavRepository : Repository<ApplicationAhav>
    {
        public IRedisClientsManager RedisManager { get; set; }
        
        private MongoCollection collectionProject;
        private MongoCollection collectionPackage;
        private MongoCollection userColl;

        public static IMongoFields ProjectDtoFields = Fields<ApplicationAhav>.Include(x => x.Id, x => x.Name, x => x.Description, x => x.Domain, x => x.Features, x => x.Packages);
        public static IMongoFields ProjectInfoFields = Fields<ApplicationAhav>.Include(x => x.Id, x => x.Name, x => x.Domain);

        public const string RedisSubdomain = "subd::{0}";
        public const string RedisVideoInit = "proj::{0}::init-video";


        public ApplicationAhavRepository()
        {
            this.collectionProject =
                MongoDbHelper.Instance.Database.GetCollection<ApplicationAhav>(typeof (ApplicationAhav).Name);
            this.collectionPackage =
                MongoDbHelper.Instance.Database.GetCollection<AhavPackage>(typeof (AhavPackage).Name);
            this.userColl =
                MongoDbHelper.Instance.Database.GetCollection<User>(typeof (User).Name);
        }
        public bool IsOwner(ObjectId projectId, ObjectId userId)
        {
            return collectionProject.FindOneByIdAs<ApplicationAhav>(projectId).OwnerId == userId;
        }
        public void SaveSubdomain(ObjectId projectId, string subDomain)
        {
            collectionProject.Update(Query<ApplicationAhav>.EQ(x => x.Id, projectId),
                                     Update<ApplicationAhav>.Set(x => x.Domain, subDomain));
            using(var redisClient = RedisManager.GetClient())
            {
                redisClient.Set(string.Format(RedisSubdomain, subDomain), projectId);
            }

        }
        public string GetSubdomain(ObjectId projectId)
        {
            using(var redisClient = RedisManager.GetClient())
            {
                return collectionProject.FindOneByIdAs<ApplicationAhav>(projectId).Domain;
            }
        }
        public ObjectId[] GetProjectsIdByUser(ObjectId userId)
        {
            var account = userColl.FindOneByIdAs<User>(userId);
            return account == null ? null : account.Projects;
        }
        public void SaveOAuthProvider(ObjectId appId, OAuthAppsProviders provider, string clientId, string secret)
        {
            using(var redisClient = RedisManager.GetClient())
            {
                
            }
        }
        public ObjectId GetProjectIdBySubdomain(string subDomain)
        {
            using(var redisClient = RedisManager.GetClient())
            {
                return redisClient.Get<ObjectId>(string.Format(RedisSubdomain, subDomain));
            }
        }
        public ApplicationAhav AddCity(string name, string domain)
        {
            var project = new ApplicationAhav()
                              {
                                  Name = name,
                                  Domain = domain
                              };
            collectionProject.Insert(project);
            return project;
        }
        /// <summary>
        /// Creates a new document in Project collection
        /// Saves subdomain in redis
        /// Push projectId to User collection
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="subDomain"></param>
        /// <param name="features"></param>
        /// <returns></returns>
        public ApplicationAhav Add(ObjectId userId, string name, string description, string subDomain, AhavFeature[] features, AhavProjectState projectState)
        {
            var project = new ApplicationAhav
                          {
                              OwnerId = userId,
                              Features = features,
                              Name = name,
                              Description = description,
                              Domain = subDomain,
                              ProjectState = projectState
                          };
            collectionProject.Insert(project);
            if (project.Id.IsEmpty()) return null;

            var userRes = userColl.Update(Query<User>.EQ(x => x.Id, userId), Update<User>.Push(x => x.Projects, project.Id));
            if (!userRes.Ok)
            {
                collectionProject.Remove(Query<ApplicationAhav>.EQ(x => x.Id, project.Id));
                throw new Exception();
            }

            using (var redisClient = RedisManager.GetClient())
            {
                redisClient.Set(string.Format(RedisSubdomain, subDomain), project.Id);
            }
            return project;
        }
        public WriteConcernResult UpdateProject(ObjectId projectId, string name, string description, string subDomain)
        {
            return collectionProject.Update(Query<ApplicationAhav>.EQ(x => x.Id, projectId),
                                            Update<ApplicationAhav>
                                                .Set(x => x.Name, name)
                                                .Set(x => x.Description, description)
                                                .Set(x => x.Domain, subDomain));
        }
        
        public void AddPackageToProject(ObjectId projectId, ObjectId packageId)
        {
            collectionProject.Update(Query<ApplicationAhav>.EQ(x => x.Id, projectId),
                                     Update<ApplicationAhav>.Push(x => x.Packages, packageId));
        }
        public ApplicationAhav Get(ObjectId id)
        {
            return collectionProject.FindAs<ApplicationAhav>(Query<ApplicationAhav>.EQ(x => x.Id, id))
                .SingleOrDefault();
        }

        public ObjectId[] GetContents(ObjectId projectId)
        {
            return collectionProject.FindOneByIdAs<ApplicationAhav>(projectId).Contents;
        }

        public void AddContent(ObjectId projectId, ObjectId contentId)
        {
            var res = collectionProject.Update(Query<ApplicationAhav>.EQ(x => x.Id, projectId),
                Update<ApplicationAhav>.Push(x => x.Contents, contentId));
        }
        public IList<AhavProjectInfo> Find(ObjectId[] projectId = null, ObjectId[] ownerId = null, int skip = 0, int take = 20)
        {
            var queryBuilder = new List<IMongoQuery>();
            if(projectId!=null && projectId.Any())
                queryBuilder.Add(Query<ApplicationAhav>.In(x => x.Id, projectId));
            if(ownerId!=null && ownerId.Any())
                queryBuilder.Add(Query<ApplicationAhav>.In(x => x.OwnerId, ownerId));

            if (queryBuilder.Any())
                return collectionProject.FindAs<AhavProjectInfo>(Query.And(queryBuilder))
                    .SetSkip(skip)
                    .SetLimit(take)
                    .SetFields(ProjectInfoFields)
                    .ToList();
            return collectionProject.FindAllAs<AhavProjectInfo>()
                .SetSkip(skip)
                .SetLimit(take)
                .SetFields(ProjectInfoFields)
                .ToList();
        }
        /// <summary>
        /// Confirm if the Project has the features.
        /// Features are in redis for fast access
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="features"></param>
        /// <returns></returns>
        public bool ValidateFeatures(ObjectId projectId, AhavFeature[] features)
        {
            using (var redisClient = RedisManager.GetClient())
            {
                var projectFeatures = GetFeatures(projectId);
                return projectFeatures.Except(features).Any();
            }
        }
        /// <summary>
        /// Save features of a project in Redis
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="features"></param>
        public void SaveFeaturesRef(ObjectId projectId, AhavFeature[] features)
        {
            using (var redisClient = RedisManager.GetClient())
            {
                features.ToList().ForEach(x => redisClient.As<AhavFeature>()
                    .Sets[string.Format(ProjectExtensions.RedisFeatures, projectId)]
                    .Add(x));
            }
        }
        /// <summary>
        /// Get features available in Project
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public AhavFeature[] GetFeatures(ObjectId projectId)
        {
            using (var redisClient = RedisManager.GetClient())
            {
                return redisClient
                    .As<AhavFeature>()
                    .Sets[string.Format(ProjectExtensions.RedisFeatures, projectId)]
                    .GetAll().ToArray();
            }
        }
        public ObjectId[] GetBlogs(ObjectId projectId)
        {
            return collectionProject.FindOneByIdAs<ApplicationAhav>(projectId).Blogs;
        }
        public void SaveBlog(ObjectId projectId, ObjectId blogId)
        {
            collectionProject.Update(Query<ApplicationAhav>.EQ(x => x.Id, projectId),
                Update<ApplicationAhav>.Push(x => x.Blogs, blogId));
        }
        public void SaveBlogTemplate(ObjectId projectId, ObjectId blogTemplateId)
        {
            collectionProject.Update(Query<ApplicationAhav>.EQ(x => x.Id, projectId),
                                     Update<ApplicationAhav>.Push(x => x.BlogTemplates, blogTemplateId));
        }
        public void RemoveBlogTemplate(ObjectId projectId, ObjectId blogId)
        {
            collectionProject.Update(Query<ApplicationAhav>.EQ(x => x.Id, projectId),
                                     Update<ApplicationAhav>.Pull(x => x.BlogTemplates, blogId));
        }
        public ObjectId[] GetBlogTemplates(ObjectId projectId)
        {
            return collectionProject.FindOneByIdAs<ApplicationAhav>(projectId).BlogTemplates;
        }
        public void RemoveBlog(ObjectId projectId, ObjectId blogId)
        {
            collectionProject.Update(Query<ApplicationAhav>.EQ(x => x.Id, projectId),
                Update<ApplicationAhav>.Pull(x => x.Blogs, blogId));
        }

        public void SaveVideoInit(ObjectId projectId, ObjectId fileId)
        {
            using (var redisClient = RedisManager.GetClient())
            {
                redisClient.Set(string.Format(RedisVideoInit, projectId), fileId);
            }
        }
        public ObjectId? GetVideoInit(ObjectId projectId)
        {
            using(var redisClient = RedisManager.GetClient())
            {
                var id = redisClient.Get<ObjectId>(string.Format(RedisVideoInit, projectId));
                if (id.IsEmpty()) return null;
                return id;
            }
        }
        public void DeleteVideoInit(ObjectId projectId)
        {
            using(var redisClient = RedisManager.GetClient())
            {
                redisClient.Remove(string.Format(RedisVideoInit, projectId));
            }
        }
        public ObjectId[] GetAttachments(ObjectId projectId)
        {
            return
                collectionProject.FindAs<ApplicationAhav>(Query<ApplicationAhav>.EQ(x => x.Id, projectId)).SetFields(
                    Fields<ApplicationAhav>.Include(x => x.Attachments)).First().Attachments;
        }
        public void SaveAttachment(ObjectId projectId, ObjectId fileId)
        {
            collectionProject.Update(Query<ApplicationAhav>.EQ(x => x.Id, projectId),
                                     Update<ApplicationAhav>.Push(x => x.Attachments, fileId));
        }
        public void RemoveAttachment(ObjectId projectId, ObjectId fileId)
        {
            collectionProject.Update(Query<ApplicationAhav>.EQ(x => x.Id, projectId),
                                     Update<ApplicationAhav>.Pull(x => x.Attachments, fileId));
        }

        public void SaveRouteModel(ObjectId appId, AppRouteModel model)
        {
            using (var redisClient = RedisManager.GetClient())
            {
                redisClient.Set(string.Format(RedisAppRouteModel, appId), model);
            }
        }

        public AppRouteModel GetRouteModel(ObjectId appId)
        {
            using (var redisClient = RedisManager.GetClient())
            {
                var model = redisClient.Get<AppRouteModel>(string.Format(RedisAppRouteModel, appId));
                return model;
            }
        }

        public const string RedisAppRouteModel = "app::route::{0}";
    }

    public class AppRouteModel
    {
        public RouteA Default { get; set; }
        public ObjectId DefaultId { get; set; }
    }

    public enum RouteA
    {
        Content = 0, Product = 1, StoreIndex = 2, BlogIndex = 3
    }
}
