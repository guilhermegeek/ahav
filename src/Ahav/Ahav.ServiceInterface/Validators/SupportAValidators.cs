﻿using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Operations;
using ServiceStack.FluentValidation;
using ServiceStack.ServiceInterface;

namespace Ahav.ServiceInterface.Validators
{
    public class PostSupportContactValidator : AbstractValidator<PostSupportContact>
    {
        public PostSupportContactValidator()
        {
            RuleSet(ApplyTo.Post, () =>
                                      {
                                          RuleFor(x => x.Name).NotEmpty();
                                          RuleFor(x => x.Contact).NotEmpty();
                                          RuleFor(x => x.Email).NotEmpty();
                                          RuleFor(x => x.Message).NotEmpty();
                                      });
        }
    }
    public class PostSupportTicketValidator : AbstractValidator<PostSupportTicket>
    {
        public PostSupportTicketValidator()
        {
            RuleSet(ApplyTo.Post, () =>
            {
                RuleFor(x => x.Content).NotEmpty();
                RuleFor(x => x.Title).NotEmpty();
            });
        }
    }
    public class GetSupportTicketValidator : AbstractValidator<GetSupportTicket>
    {
        public GetSupportTicketValidator()
        {
            RuleSet(ApplyTo.Get, () =>
            {
                //RuleFor(x => x.Id)         
            });
        }
    }
    public class PostSupportTicketCommentValidator : AbstractValidator<PostSupportTicketComment>
    {
        public PostSupportTicketCommentValidator()
        {
            RuleSet(ApplyTo.Post, () =>
                                      {
                                          RuleFor(x => x.Comment).NotEmpty();
                                      });
        }
    }
    public class EditSupportTicketValidator :AbstractValidator<EditSupportTicket>
    {
        public EditSupportTicketValidator()
        {
            RuleSet(ApplyTo.Post, () =>
                                      {
                                          RuleFor(x => x.Title).NotEmpty();
                                          RuleFor(x => x.Content).NotEmpty();
                                          //RuleFor(x => x.Priority).NotEmpty();
                                      });
        }
    }
    public class EditSupportTicketStateValidator : AbstractValidator<EditSupportTicketState>
    {
        public EditSupportTicketStateValidator()
        {
            RuleSet(ApplyTo.Post, () =>
                                      {
                                          RuleFor(x => x.State).NotEmpty();
                                      });
        }
    }
}
