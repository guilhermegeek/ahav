﻿using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Operations;
using MongoDB.Bson;
using ServiceStack.FluentValidation;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace Ahav.ServiceInterface.Validators
{
   
    public enum AccountValidationError
    {
        EmailInUse, InvalidToken, InvalidEmailOrPassword
    }
    public class LoginValidator : AbstractValidator<PostLogin>
    {
        public LoginValidator()
        {
            RuleSet(ApplyTo.Post, () =>
                                      {
                                          RuleFor(x => x.Email).NotEmpty().WithErrorCode("NotEmpty");
                                          RuleFor(x => x.Password).NotEmpty().WithErrorCode("NotEmpty");
                                          //RuleFor(x => x).Must(c => AccountRepository.ValidatePassord(c.Email, c.Password));
                                      });
        }
    }
   

    public class ConfirmationValidator : AbstractValidator<PostRegistrationConfirm>
    {
        public ConfirmationValidator()
        {
            RuleSet(ApplyTo.Post, () =>
                                      {
                                          RuleFor(x => x.UserId).NotEmpty().Must(x => x != ObjectId.Empty);
                                          RuleFor(x => x.Token).NotEmpty();
                                      });
        }
    }
    public class BasicInfoValidator : AbstractValidator<BasicInfo>
    {
        public BasicInfoValidator()
        {

            RuleSet(ApplyTo.Post, () =>
                                      {
                                          RuleFor(x => x.FirstName).NotEmpty();
                                          RuleFor(x => x.LastName).NotEmpty();
                                      });
        }
    }
    public class RecoverPasswordValidator : AbstractValidator<PostRecoverPassword>
    {
        public RecoverPasswordValidator()
        {

            RuleSet(ApplyTo.Post, () =>
                                      {
                                          RuleFor(x => x.Email).NotEmpty();
                                      });
        }
    }
    public class ConfirmRecoverPasswordValidator : AbstractValidator<GetConfirmRecoverPassword>
    {
        public ConfirmRecoverPasswordValidator()
        {
            RuleSet(ApplyTo.Post, () =>
                                      {
                                          RuleFor(x => x.Email).NotEmpty();
                                          RuleFor(x => x.Token).NotEmpty();
                                      });
        }
    }
    public class ChangecultureValidator : AbstractValidator<ChangeCulture>
    {
        public ChangecultureValidator()
        {
            RuleSet(ApplyTo.All, () =>
                                     {
                                         RuleFor(x => x.Culture).NotEmpty();
                                     });
        }
    }
    public class ChangePasswordValidator : AbstractValidator<PostAccountPassword>, IRequiresHttpRequest
    {
        public IHttpRequest HttpRequest { get; set; }
        public ChangePasswordValidator()
        {

            RuleSet(ApplyTo.Post, () =>
                                      {
                                          RuleFor(x => x.Password)
                                              .Equal(x => x.PasswordConfirm).WithErrorCode("PwNotMatching")
                                              .NotEqual(x => x.ActualPassword).WithErrorCode("PwIsSame")
                                              .NotEmpty().WithErrorCode("Empty");
                                          RuleFor(x => x.PasswordConfirm)
                                              .NotEmpty().WithErrorCode("Empty");
                                          RuleFor(x => x.ActualPassword)
                                              .NotEmpty().WithErrorCode("empty");
                                      });
        }
    }
    public class ContactValidator : AbstractValidator<Contact>
    {
        public ContactValidator()
        {
            RuleSet(ApplyTo.Post,() =>
                                     {
                                         // id is empty == false
                                     });
        }
    }
}