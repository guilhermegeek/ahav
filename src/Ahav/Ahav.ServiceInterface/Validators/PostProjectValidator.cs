﻿using Ahav.ServiceModel.ApiDtos;
using ServiceStack.FluentValidation;
using ServiceStack.ServiceInterface;

namespace Ahav.ServiceInterface.Validators
{
    public class PostProjectValidator : AbstractValidator<PostApplication>
    {
        public PostProjectValidator()
        {
            RuleSet(ApplyTo.Post, () =>
                                      {
                                          RuleFor(x => x.Name).NotEmpty();
                                          RuleFor(x => x.Description).NotEmpty();
                                          RuleFor(x => x.PackageId).NotEmpty();
                                          RuleFor(x => x.Domain).NotEmpty();
                                      });
        }
    }
}
