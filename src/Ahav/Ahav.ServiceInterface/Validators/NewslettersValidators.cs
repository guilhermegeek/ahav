﻿using Ahav.ServiceModel.Operations;
using ServiceStack.FluentValidation;
using ServiceStack.ServiceInterface;

namespace Ahav.ServiceInterface.Validators
{
    public class PostNewsletterValidator : AbstractValidator<PostNewsletter>
    {
        public PostNewsletterValidator()
        {
            RuleSet(ApplyTo.Post,() =>
                                     {
                                         RuleFor(x => x.Name).NotEmpty();
                                     } );
        }
    }
    public class PutNewsletterValidator : AbstractValidator<PutNewsletter>
    {
        public PutNewsletterValidator()
        {
            RuleSet(ApplyTo.Put, () =>
                                     {
                                         RuleFor(x => x.Body).NotEmpty();
                                         RuleFor(x => x.Subject).NotEmpty();
                                         RuleFor(x => x.Name).NotEmpty();
                                     });
        }
    }
}
