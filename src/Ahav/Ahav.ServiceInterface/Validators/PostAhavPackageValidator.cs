﻿using Ahav.ServiceModel.ApiDtos;
using ServiceStack.FluentValidation;

namespace Ahav.ServiceInterface.Validators
{
    public class PostAhavPackageValidator : AbstractValidator<PostAhavPackage>
    {
        public PostAhavPackageValidator()
        {
            RuleFor(x => x.Duration).NotEmpty();
            RuleFor(x => x.Start).NotEmpty();
            RuleFor(x => x.Features).NotEmpty();
            RuleFor(x => x.Title).NotEmpty();
            RuleFor(x => x.Description).NotEmpty();
        }
    }
}
