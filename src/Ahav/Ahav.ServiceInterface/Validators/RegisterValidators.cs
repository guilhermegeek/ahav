﻿using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Operations;
using ServiceStack.FluentValidation;
using ServiceStack.ServiceInterface;

namespace Ahav.ServiceInterface.Validators
{
    public class PostBasicRegisterValidator : AbstractValidator<PostRegisterBasic>
    {
        public PostBasicRegisterValidator()
        {
            RuleSet(ApplyTo.Post, () =>
            {
                RuleFor(x => x.Password).NotEmpty();
                RuleFor(x => x.FirstName).NotEmpty();
                RuleFor(x => x.LastName).NotEmpty();
                RuleFor(x => x.Email)
                    .NotEmpty();
                RuleFor(x => x.Email)
                    .EmailAddress();
            });
        }
    }
}
