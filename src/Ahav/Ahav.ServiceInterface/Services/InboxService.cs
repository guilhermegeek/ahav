﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Interfaces;
using ServiceStack.Redis;
using ServiceStack.ServiceInterface;

namespace Ahav.ServiceInterface.Services
{
    /// <summary>
    /// Inbox system implemented with search capability for typeahead in mind
    /// </summary>
    /// Inbox requires search functionality. To implement it, HBase solution shared by Facebook is what i'm taking
    /// Inbox in one table on HBase, with user id as row key, word as column and message id is version
    /// Find more information here
    /// http://www.slideshare.net/brizzzdotcom/facebook-messages-hbase
    /// http://sites.computer.org/debull/A12june/facebook.pdf
    public class InboxService
    {
        /// <summary>
        /// Send a new message
        /// </summary>
        /// <param name="fromUserId"></param>
        /// <param name="toUserId"></param>
        public void SendMessage(string fromUserId, string toUserId, string message)
        {
            
        }
    }
    public class InboxRepository : IRepository
    {
        public void SaveMessage(string fromUserId, string toUserId, string message)
        {
            
        }

        public IRedisClientsManager RedisManager { get; set; }
    }
}
