﻿using System;
using System.Collections.Generic;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.DataDtos;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Operations;
using Ahav.ServiceModel.Resources;
using ServiceStack.Redis;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace Ahav.ServiceInterface.Services
{
    public class BootService : Service
    {
        public object Any(BootInit request)
        {
            MongoDbHelper.Instance.Database.Drop();
            RedisManager.GetClient().FlushDb();
            var user = AccountRepo.CreateEntity(new User
                               {
                                   AccountState = AccountState.Confirmed,
                                   FirstName = "Guilherme",
                                   LastName = "Cardoso",
                                   DisplayName = "Gui",
                                   Birthday = DateTime.Now.AddYears(-22),
                                   Password = "123",
                                   PrimaryEmail = "email@guilhermecardoso.pt"
                               });
            //AuthRepo.CreateAuth(new AhavAuth { Token = RsaTokenProvider.Instance.RandomString(10), UserId = user.Id });
            
            var res = base.ResolveService<LoginService>().Post(new PostLogin
                                                                   {
                                                                       Email = "email@guilhermecardoso.pt",
                                                                       Password = "123"
                                                                   });
            if(res.ResponseStatus != null)
                throw new Exception(res.ResponseStatus.Message);


            RoleRepo.SaveRole(user.Id, AccountRole.Administrator);
            RoleRepo.SaveRole(user.Id, AccountRole.Gui);
            Request.SetAuth(new AhavAuth { Token = res.Auth.Key, UserId = res.Auth.Id});
            
            var package = PackageRepo.Add(new CreatePackage {
                                              IsActive = true,
                                              Ammount = 0,
                                              Start = DateTime.Now,
                                              DaysPrice = new Dictionary<long, decimal>() {{31*3, 0}}, // 3 months, free
                                              Title =
                                                  new Dictionary<string, string>
                                                      {
                                                          {Culture.pt_PT, "Gratuíto"},
                                                          {Culture.en_US, "Free!"}
                                                      },
                                              Description = new Dictionary<string, string>()
                                                                {
                                                                    {Culture.pt_PT, "Na fase de lançamento os nossos serviços serão gratuítos durante um ano."},
                                                                    {Culture.en_US, "During the release, our services will be free during a year."}
                                                                },
                                              Features =
                                                  new[]
                                                      {
                                                          AhavFeature.Support, AhavFeature.Newsletters,
                                                          AhavFeature.Ecommerce, AhavFeature.Cms, AhavFeature.Blog,
                                                      }
                                          });
            var projectRes = base.ResolveService<ApplicationAhavService>()
                .Post(new PostApplication
                          {
                              Name = "Main",
                              PackageId = package.Package.Id,
                              Description = "Test",
                              Domain = "localhost"
                          });
            // Initial templates
            Request.SetProjectId(projectRes.Project.Id);
            var tplRes = base.ResolveService<TemplateService>()
                .Post(new PostContentTemplate {Name = BootData.Tpl1_Name});
            base.ResolveService<TemplateService>().Put(new PutTemplate
                                                       {
                                                           TemplateId = tplRes.Template.Id,
                                                           Name = BootData.Tpl1_Name,
                                                           Title = new Dictionary<string, string> {{ Culture.pt_PT, "Criado de Raíz"}},
                                                           Body = new Dictionary<string, string> {{Culture.pt_PT, BootData.Tpl1_Body}},
                                                           Css = string.Format("{0}{1}", BootstrapDefaultCss.Min, BootData.Tpl1_Css)
                                                       });
            //base.ResolveService<TemplateService>().SaveStaticCss(tplRes.Template.Id.ToString(), string.Format("{0}{1}", BootstrapDefaultCss.Min, BootData.Tpl1_Css));
                var contentRes = base.ResolveService<ContentService>()
                    .Post(new PostContent
                              {
                                  Slug = "about",
                                  Template = tplRes.Template.Id
                              });
            base.ResolveService<ContentService>().Put(new PutContent
                                                      {
                                                          ContentId = contentRes.Content.Id,
                                                           Slug = contentRes.Content.Slug,
                                                           Template = tplRes.Template.Id,
                                                          Title = new Dictionary<string, string> { { Culture.pt_PT, "Criado de Raíz" } },
                                                           Body = new Dictionary<string, string> {{Culture.pt_PT, BootData.ContentAbout}},
                                                      });
            contentRes = base.ResolveService<ContentService>()
                    .Post(new PostContent
                    {
                        Slug = "faq",
                        Template = tplRes.Template.Id
                    });
            base.ResolveService<ContentService>().Put(new PutContent
            {
                ContentId = contentRes.Content.Id,
                Slug = contentRes.Content.Slug,
                Template = tplRes.Template.Id,
                Title = new Dictionary<string, string> { { Culture.pt_PT, "Criado de Raíz" } },
                Body = new Dictionary<string, string> { { Culture.pt_PT, BootData.ContentFaq } },
            });
            base.ResolveService<ContentService>().Put(new PutContentDefault {ContentId = contentRes.Content.Id});
            using (var mailService = base.ResolveService<MailService>())
            {
                mailService.Post(new InitMailService());
            }
            Response.Redirect("/admin");
            return true;
        }
        public AccountRepository AccountRepo { get; set; }
        public ApplicationAhavRepository ProjectRepo { get; set; }
        public RoleRepository RoleRepo { get; set; }
        public PackageRepository PackageRepo { get; set; }
        public AuthRepository AuthRepo { get; set; }
        public IRedisClientsManager RedisManager { get; set; }

    }

    public class BootInit : IReturn<BootInitResponse>
    {
        
    }
    public class BootInitResponse { }
}
