﻿using System;
using System.Globalization;
using System.Net;
using System.Threading;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Operations;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.Text;

namespace Ahav.ServiceInterface.Services
{
    public static class CommonExtensions
    {
        /// <summary>
        /// Cleans www. from Host
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        public static string GetHostname(Uri uri)
        {
            return uri.Host.StartsWith("www.")
                            ? uri.Host.Remove(0, 4)
                            : uri.Host;
        }
        public static NCommentSnapshot ToSnapshot(this NComment comment)
        {
            return new NCommentSnapshot
                       {
                           Message = comment.Comment,
                           ModifiedAt = DateTime.Now
                       };
        }

        /// <summary>
        /// Set the current defined by request, if any
        /// </summary>
        public static Action<IHttpRequest, IHttpResponse> RequestSetCultureAction = (req, res) =>
                                                                                        {
                                                                                            try
                                                                                            {
                                                                                                string parameterLanguage = req.QueryString.Get("language");
                                                                                                if (!String.IsNullOrEmpty(parameterLanguage))
                                                                                                {
                                                                                                    Thread.CurrentThread.CurrentCulture = new CultureInfo(parameterLanguage);

                                                                                                    res.Cookies.DeleteCookie("language");
                                                                                                    res.Cookies.AddCookie(new Cookie
                                                                                                                              {
                                                                                                                                  Value = parameterLanguage,
                                                                                                                                  Name = "language",
                                                                                                                                  Expires = DateTime.Now.AddMonths(1)
                                                                                                                              });
                                                                                                }
                                                                                                else if (req.Cookies != null && req.Cookies.ContainsKey("language") && req.Cookies["language"] != null)
                                                                                                {
                                                                                                    Thread.CurrentThread.CurrentCulture = new CultureInfo(req.Cookies["language"].Value);
                                                                                                }
                                                                                            }
                                                                                            catch (CultureNotFoundException ex)
                                                                                            {
                                                                                                Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-PT");
                                                                                            }
                                                                                        };



        
        /// <summary>
        /// Receptor for Auth, Project and Blog
        /// Check for credentials in headers, cookies and url query and store in HttpRequest.Items collection
        /// </summary>
        public static Action<IHttpRequest, IHttpResponse> ApiCredentialsAction = (req, res) =>
                                                                                     {
                                                                                         //var accountService = req.TryResolve<AccountService>();
                                                                                         //var authService = req.TryResolve<AuthService>();
                                                                                         //var roleRepo = req.TryResolve<RoleRepository>();


                                                                                         //// Get auth credentials from cookies, headers and query parameters
                                                                                         //var requestAuth = req.GetAuthorization();
                                                                                         //if (requestAuth == null) return;
                                                                                         
                                                                                         //if (requestAuth != null && !requestAuth.Id.IsEmpty() && !string.IsNullOrEmpty(requestAuth.Key))
                                                                                         //{
                                                                                         //    var authRes = authService.Get(new AuthToken
                                                                                         //                                      {
                                                                                         //                                          Token = requestAuth.Key,
                                                                                         //                                          UserId = requestAuth.Id
                                                                                         //                                      });
                                                                                             
                                                                                         //    // User sent credentials but isn't authenticated
                                                                                         //    if (!authRes.IsAuthenticated){
                                                                                         //      //  res.ReturnAuthRequired();
                                                                                         //        return;
                                                                                         //    }
                                                                                             
                                                                                         //    //Token is expired
                                                                                         //    if (authRes.Result.TokenExpires <= DateTime.Now)
                                                                                         //    {
                                                                                         //        // @TODO
                                                                                         //    }
                                                                                         //    var rolesResponse = roleRepo.GetRoles(authRes.Result.UserId);

                                                                                         //    // Request is sucefully authorized, save credentials in Request Items
                                                                                         //    req.SetAuth(authRes.Result, rolesResponse.ToArray());
                                                                                         //}
                                                                                     };
   
    };

}
