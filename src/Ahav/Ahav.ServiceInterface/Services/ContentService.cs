﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Business;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Filters;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.BusinessDtos;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.DataDtos;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Enums;
using Ahav.ServiceModel.Operations;
using Ahav.ServiceModel.Resources;
using AutoMapper;
using MongoDB.Bson;
using ServiceStack.Common.Web;
using ServiceStack.Configuration;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.ServiceModel;
using ServiceStack.Text;

namespace Ahav.ServiceInterface.Services
{
    /// <summary>
    /// Content is the default base page. Can use templates, comments, social assets, etc
    /// </summary>
    public class ContentService : Service
    {
        public ContentRepository ContentRepo { get; set; }
        public TemplateRepository TemplateRepo { get; set; }
        public ApplicationAhavRepository ProjectRepo { get; set; }
        public SlugRepository SlugRepo { get; set; }
        public MongoGridFs GridFs { get; set; }


        /// <summary>
        /// By default, slugs are stored without "/"
        /// This function ensures all documents slugs are trimmed if needed
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private string RemovePathSlash(string path)
        {
            if (path.StartsWith("/"))
                return path.Remove(0, 1);
            return path;
        }

        private ObjectId GetTemplateCached()
        {
            ObjectId outA;
            return !string.IsNullOrEmpty(Request.QueryStringValue("template")) &&
                   ObjectId.TryParse(Request.QueryStringValue("template"), out outA)
                       ? outA
                       : ObjectId.Empty;
        }
        [RequestHasAppId]
        public object Any(GetContentView request)
        {
            var response = new GetContentViewResponse();

            // Non authorized .extension
            if (request.Path.Contains("."))
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return new ResponseStatus {Message = "I don't like this"};
            }
            ObjectId templateCached = GetTemplateCached();

            var content = ContentBus.GetView(Request.GetProjectId(), request.Path, Request.IsAjax(), templateCached);
            response.RenderBody = content.RenderBody;
            response.MetaTags = content.MetaTags;
            response.ReuseTemplate = content.ReuseTemplate;
            bool isHtml = true;
            if (isHtml)
                Response.ContentType = "text/html";

            // Content has Template

                // Save the path for CSS stylesheet
                // @TODO APPLY ONLY ON HTML
                //if (isHtml)
                //    Request.Items[TemplateExtensions.HttpItemTemplateSrc] = string.Format("{0}{1}.css",
                //                                                                          new AppSettings().Get(
                //                                                                              "StaticCssUri", ""),
                //                                                                          content.Template.Value);
                // Template Cached is the same as Content Template. No need to send it back
            
            Request.Items[TemplateExtensions.MetaTags] = JsonSerializer.SerializeToString(response.MetaTags);
            
            return new HttpResult(response)
            {
                View = "ContentView",
                Template = Request.IsAjax()
                               ? ""
                               : "_LayoutForTemplate"
            };
        }
       

        [LoggedIn]
        public GetContentResponse Get(GetContent request)
        {
            var response = new GetContentResponse();
            var content = ContentRepo.Get(request.ContentId);
            response.Content = content.ToDto();
            return response;
        }

        [LoggedIn]
        [RequestHasAppId]
        public GetContentsResponse Get(GetContents request)
        {
            var response = new GetContentsResponse();
            // Get Contents
            if (request.ProjectId.IsEmpty())
                request.ProjectId = Request.GetProjectId();
            var contents = ProjectRepo.GetContents(request.ProjectId);
            var res = ContentRepo.Get(new ReadContents {Ids = contents, Take = request.Take});
            if (res.ResponseStatus != null)
            {
                response.ResponseStatus = AhavExtensions.InternalServerError();
                Response.StatusCode = (int) HttpStatusCode.BadRequest;
                return response;
            }
            response.Contents = res.Contents;
            return response;
        }

        [LoggedIn]
        [RequestHasAppId]
        public PostContentResponse Post(PostContent request)
        {
            var response = new PostContentResponse();
            request.Slug = RemovePathSlash(request.Slug);

            // Confirms if the slug is available to use
            // Slugs are unique per project
            //if (!SlugRepo.SlugIsAvailable(Request.GetProjectId(), SlugType.Content, request.Slug))
            //{
            //    response.ResponseStatus = AhavExtensions.InvalidOperation(ContentResources.SlugNotAvailable,
            //                                                              ContentError.SlugInUse);
            //    Response.StatusCode = (int) HttpStatusCode.BadRequest;
            //    return response;
            //}
            var res = ContentBus.Create(new CreateContent
                              {
                                  AppId = Request.GetProjectId(),
                                  OwnerId = Request.GetUserId(),
                                  Slug = request.Slug,
                                  TemplateId = request.Template
                              });

            // Push the new Content to AhavProject document
            response.Content = res.Content;
            // Save the slug in redis
            //SlugRepo.AddSlug(Request.GetProjectId(), SlugType.Content, content.Id, content.Slug);
            response.Modal = AhavExtensions.Success(ContentResources.PostSucefully);
            response.Modal.Buttons = new ModalButton[] {new ModalButton {Action = "alert('ola');", Name = "Ver Página"}};

            return response;
        }

        [LoggedIn]
        [RequestHasAppId]
        public PutContentResponse Put(PutContent request)
        {
            var response = new PutContentResponse();
            request.Slug = RemovePathSlash(request.Slug);
            // Validation
            var isOwner = ContentRepo.IsOwner(request.ContentId, Request.GetUserId());
            if (!isOwner && !Request.IsAdmin())
            {
                response.ResponseStatus = AhavExtensions.Unauthorized();
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return response;
            }
            var c = ContentRepo.Get(request.ContentId);
            // Update 
            var updateResult = ContentRepo.Update(request.ContentId, request.Title, request.Body, request.Javascript,
                                                  request.Slug, request.Template);
            if (!updateResult.Ok)
            {
                throw HttpError.Conflict(JsonSerializer.SerializeToString(updateResult));
            }
            // Content was using a Template and the request don't. Remove it
            //if (request.Template.IsEmpty() && c.Template.HasValue && !c.Template.Value.IsEmpty())
            //{
            //    TemplateRepo.RemoveContentIdFromTemplate(request.Template, request.ContentId);
            //}
                // Content was using a template and the request uses a new one. Update
            if (c.Template != request.Template)
            {
                TemplateRepo.RemoveContentIdFromTemplate(c.Template, request.ContentId);
                TemplateRepo.AddContentIdToTemplate(request.Template, request.ContentId);
            }
            //    // Content dont had a Template and request has. Add
            //else if (!c.Template.HasValue && !request.Template.IsEmpty())
            //{
            //    TemplateRepo.AddContentIdToTemplate(request.Template, request.ContentId);
            //}
            // Update slug
            //if (c.Slug != request.Slug)
            //{
            //    // If contentId is null, slug is available
            //    var contentId = SlugRepo.GetId(Request.GetProjectId(), SlugType.Content, request.Slug);
            //    // The slug is available
            //    if (contentId.IsEmpty())
            //    {
            //        // Save new slug
            //        SlugRepo.AddSlug(Request.GetProjectId(), SlugType.Content, request.ContentId, request.Slug);
            //        // Delete slug in use
            //        SlugRepo.RemoveSlug(Request.GetProjectId(), SlugType.Content, c.Slug);
            //    }
            //        // If the slug is already in use, and not by the current content
            //    else if (contentId != request.ContentId)
            //    {
            //        response.ResponseStatus = AhavExtensions.InvalidOperation(ContentResources.SlugNotAvailable,
            //                                                                  ContentError.SlugInUse);
            //        Response.StatusCode = (int) HttpStatusCode.BadRequest;
            //        return response;
            //    }
            //}
            response.Modal = AhavExtensions.Success(ContentResources.PutSuccefully);
            return response;
        }

        [LoggedIn]
        [RequestHasAppId]
        public DeleteContentResponse Delete(DeleteContent request)
        {
            var response = new DeleteContentResponse();
            var isOwner = ContentRepo.IsOwner(request.ContentId, Request.GetUserId());
            if (!isOwner)
            {
                response.ResponseStatus = AhavExtensions.Unauthorized();
                return response;
            }
            var c = ContentRepo.Get(request.ContentId);
            var deleteResult = ContentRepo.Delete(request.ContentId);
            if (!deleteResult.Ok)
            {
                throw HttpError.Conflict(JsonSerializer.SerializeToString(deleteResult));
            }
            // Delete slug
            //SlugRepo.RemoveSlug(Request.GetProjectId(), SlugType.Content, c.Slug);
            // Delete template association if content had one
            
                TemplateRepo.RemoveContentIdFromTemplate(c.Template, request.ContentId);
            
            response.Modal = AhavExtensions.Success(ContentResources.ContentRemovedOk);
            return response;
        }

        [LoggedIn]
        [RequestHasAppId]
        public PostValidateContentSlugResponse Post(PostValidateContentSlug request)
        {
            var response = new PostValidateContentSlugResponse();
            request.Slug = RemovePathSlash(request.Slug);
            var isAvailable = request.ContentId.HasValue
                                  ? SlugRepo.SlugIsAvailable(request.Slug, SlugType.Content, Request.GetProjectId(),
                                                             request.ContentId.Value)
                                  : SlugRepo.SlugIsAvailable(Request.GetProjectId(), SlugType.Content, request.Slug);
            if (!isAvailable)
            {
                Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                response.ResponseStatus = new ResponseStatus
                                              {
                                                  Errors = new List<ResponseError>
                                                               {
                                                                   new ResponseError
                                                                       {
                                                                           ErrorCode = ContentError.SlugInUse,
                                                                           FieldName = "slug",
                                                                           Message = ContentResources.SlugNotAvailable
                                                                       }
                                                               }
                                              };
            }
            return response;
        }

        // Files
        public GetContentFilesResponse Get(GetContentFiles request)
        {
            var response = new GetContentFilesResponse();
            var files = ContentRepo.GetFilesIds(request.ContentId, request.Skip, request.Take);
            if (files.Any())
            {
                using (var mediaService = base.ResolveService<MediaService>())
                {
                    var mediaResponse = mediaService.Get(new GetMedias {Ids = files.ToArray()});
                    string path = new AppSettings().Get("FilesUploadPath", @"c:\tmp\");
                    mediaResponse.Medias.ToList().ForEach(
                        x => response.Files.Add(string.Format("{0}{1}{2}", path, x.Id, x.Extension)));
                    response.Medias = mediaResponse.Medias;
                }
            }
            return response;
        }

        [LoggedIn]
        [RequestHasAppId]
        public PostContentFileResponse Post(PostContentFile request)
        {
            var response = new PostContentFileResponse();

            // unit tests and others services in future may requir epost request and not dto based
            var isOwner = ContentRepo.IsOwner(request.ContentId, Request.GetUserId());
            if (!isOwner && !Request.IsAdmin())
            {
                response.ResponseStatus = AhavExtensions.Unauthorized();
                Response.StatusCode = (int) HttpStatusCode.BadRequest;
                return response;
            }
            if (!RequestContext.Files.Any())
            {
                response.ResponseStatus = AhavExtensions.InternalServerError();
                Response.StatusCode = (int) HttpStatusCode.BadRequest;
                return response;
            }
            using (var mediaService = base.ResolveService<MediaService>())
            {
                var mediaRes = mediaService.Post(new PostMedia
                                                     {
                                                         Files = RequestContext.Files,
                                                         MediaType = MediaObjType.Content
                                                     });
                mediaRes.Medias.ToList().ForEach(x => response.Medias.Add(x));
            }
            ContentRepo.Addfile(request.ContentId, response.Medias.Select(x => x.Id).ToArray());
            response.Modal = AhavExtensions.Success(ContentResources.FilePostSucefull);
            return response;
        }

        [LoggedIn]
        [RequestHasAppId]
        public DeleteContentFileResponse Delete(DeleteContentFile request)
        {
            var response = new DeleteContentFileResponse();
            var isOwner = ContentRepo.IsOwner(request.ContentId, Request.GetUserId());
            if (!isOwner)
            {
                response.ResponseStatus = AhavExtensions.Unauthorized();
                Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                return response;
            }
            var result = ContentRepo.DeleteFile(request.ContentId, request.FileId);
            if (!result.Ok)
            {
                response.ResponseStatus = AhavExtensions.InternalServerError();
                Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                throw HttpError.Conflict(JsonSerializer.SerializeToString(result));
            }
            //using(var mediaService = base.ResolveService<MediaService>())
            //{
            //    var deleteResponse = mediaService.Delete(new DeleteMedia {Id = request.FileId});
            //    if(deleteResponse.ResponseStatus!=null)
            //    {
            //        throw HttpError.Conflict(JsonSerializer.SerializeToString(deleteResponse.ResponseStatus));
            //    }
            //}
            GridFs.RemoveFile(request.FileId);
            response.Modal = AhavExtensions.Success(ContentResources.FileDeleteSucefull);
            return response;
        }
        /// <summary>
        /// The default content is used at / (root slug)
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [LoggedIn]
        [RequestHasAppId]
        public PutContentDefaultResponse Put(PutContentDefault request)
        {
            var response = new PutContentDefaultResponse();
            ContentRepo.SetContentDefault(Request.GetProjectId(), request.ContentId);
            response.Modal = AhavExtensions.Success(ContentResources.SetContentDefaultOk);
            return response;
        }

        public GetContentDefaultResponse Get(GetContentDefault request)
        {
            var response = new GetContentDefaultResponse();
            var contentId = ContentRepo.GetContentDefault(Request.GetProjectId());
            if (contentId.IsEmpty())
                throw HttpError.Conflict(string.Format("Project {0} don't have default content", Request.GetProjectId()));
            var content = ContentRepo.Get(contentId);
            if (content == null)
                throw HttpError.Conflict(string.Format("Content with id {0} doesnt exist", contentId));
            response.Content = content.ToDto();
            return response;
        }
        public ContentBusiness ContentBus { get; set; }
    }
}
