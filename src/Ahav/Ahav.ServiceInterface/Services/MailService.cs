﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Operations;
using MongoDB.Bson;
using ServiceStack.Configuration;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceInterface.Services
{
    public class MailService : Service
    {
        public MailRepository MailRepo { get; set; }

        /// <summary>
        /// Calld just once
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public InitMailServiceResponse Post(InitMailService request)
        {
            var appSettings = new AppSettings();
            var response = new InitMailServiceResponse();
            var initmails = new Dictionary<MailCommon, ObjectId>(100);
            var mailResponse = Post(new AddEmail
                     {
                         Subject = new Dictionary<string, string> {{Culture.pt_PT, "Recover Password"}},
                         Content = new Dictionary<string, string> { { Culture.en_US, "Recover Password" } },
                         IsHtml = true
                     });
            initmails.Add(MailCommon.RecoverPassword, mailResponse.Result.Id);
            string url = "<a href=\"http://" + appSettings.GetString("BaseDomain")  + AhavRoute.PostConfirmAccount + "?userId={0}&token={1}\">http://" + appSettings.GetString("BaseDomain") + AhavRoute.PostConfirmAccount + "?userId={0}&token={1}</a>";
            mailResponse = Post(new AddEmail
            {
                Subject = new Dictionary<string, string> { { Culture.pt_PT, "Confirma a tua conta" } },
                Content = new Dictionary<string, string> { { Culture.pt_PT, string.Format("<p>Só falta confirmares a tua conta! Entra aqui:{0} Obrigado!", url) } },
                IsHtml = true
            });
            initmails.Add(MailCommon.ConfirmAccount, mailResponse.Result.Id);
            MailRepo.AddInitialEmails(initmails);
            return response;
        }
        /// <summary>
        /// Adds a new email. An email object can be a Newsletter, a special email, etc
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public AddEmailResponse Post(AddEmail request)
        {
            var response = new AddEmailResponse();
            var email = new Mail()
                            {
                                Subject = request.Subject,
                                Content = request.Content,
                                IsHtml = request.IsHtml
                            };
            MailRepo.Create(email);
            response.Result = email.ToDto();
            return response;
        }

        public SendQueueEmailsResponse Post(SendQueueEmails request)
        {
            var mail = MailRepo.Get(request.EmailId);
            var queue = MailRepo.GetQueue(request.EmailId);
            var mailDto = mail.ToDto();
            foreach (var mailQueue in queue)
            {
                if (mailQueue.Parameters != null && mailQueue.Parameters.Any())
                {
                    mailDto.Content = string.Format(mailDto.Content, mailQueue.Parameters);
                    mailDto.Subject = string.Format(mailDto.Content, mailQueue.Parameters);
                }
                this.Any(new SendEmailI
                {
                    Content = mailDto.Content,
                    Subject = mailDto.Subject,
                    IsHtml = mailDto.IsHtml,
                    Recipients = new[] { mailQueue.Email }
                });
            }
            
            MailRepo.RemoveQueue(mail.Id, queue.ToArray());
            return new SendQueueEmailsResponse
                   {

                   };
        } 
        /// <summary>
        /// Add emails to the queue list
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public AddEmailQueueResponse Post(AddEmailQueue request)
        {
            foreach (var queue in request.Queues)
            {
                MailRepo.AddToQueue(request.EmailId, queue);
            }
            Post(new SendQueueEmails {EmailId = request.EmailId});
            return new AddEmailQueueResponse();
        }
        /// <summary>
        /// Sends an email throught SMTP
        /// This must be requested internaly only
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SendEmailIHttpResponse Any(SendEmailI request)
        {
            var appSettings = new AppSettings();
            var response = new SendEmailIHttpResponse();
            SmtpClient client = new SmtpClient();
            client.Port = appSettings.Get("SmtpPort", 25);
            client.Host = appSettings.Get("SmtpHost", "localhost");
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            if (appSettings.GetString("SmtpCredentialUser") != null &&
                appSettings.GetString("SmtpCredentialPassword") != null)
                client.Credentials = new System.Net.NetworkCredential(appSettings.GetString("SmtpCredentialUser"), appSettings.GetString("SmtpCredentialPassword"));
            foreach (var recipient in request.Recipients)
            {
                
                try
                {
                    MailMessage mail = new MailMessage();
                    mail.From = new MailAddress("email@guilhermecardoso.pt");
                    mail.To.Add(new MailAddress(recipient));
                    mail.Subject = request.Subject;
                    mail.Body = request.Content;
                    mail.IsBodyHtml = request.IsHtml;
                    client.Send(mail);
                }
                catch(Exception ex)
                {
                    response.EmailsNotSented.Add(recipient);
                }
            }
            return response;
        }
        public DeleteEmailResponse Post(DeleteEmail request)
        {
            var response = new DeleteEmailResponse();
            if(!MailRepo.EmailHasQueue(request.Id))
            {
                response.ResponseStatus = new ResponseStatus
                                              {
                                                  ErrorCode = MailError.DeleteHasPendingQueue.ToString(),
                                                  Message = "This email has pending users. Do you want to remove it anyway?"
                                              };
                return response;
            }
            MailRepo.Delete(request.Id);
            return response;
        }
    }
    public enum  MailError
    {
        DeleteHasPendingQueue,

    }
    public static class MailExtensions
    {
        public static MailDto ToDto(this Mail email)
        {
            var culture = ServiceStackExtensions.GetCulture();
            return new MailDto
                       {
                           Id = email.Id,
                           Content = email.Content.Any(x => x.Key == culture)
                                         ? email.Content.First(x => x.Key == culture).Value
                                         : email.Content.FirstOrDefault().Value,
                           Subject = email.Subject.Any(x => x.Key == culture)
                                         ? email.Subject.First(x => x.Key == culture).Value
                                         : email.Subject.FirstOrDefault().Value,
                           IsHtml = email.IsHtml
                       };
        }
    }
    
    
}
