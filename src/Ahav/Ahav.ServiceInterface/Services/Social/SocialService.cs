﻿using System;
using System.Net;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Filters;
using Ahav.ServiceInterface.Services.Social.Providers;
using Ahav.ServiceModel.Social;
using Ahav.ServiceModel.Social.Domain;
using Ahav.ServiceModel.Social.Resources;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace Ahav.ServiceInterface.Services.Social
{
    public class SocialService : Service
    {
        public SocialRepository SocialRepo { get; set; }
        public ApplicationAhavRepository ProjectRepo { get; set; }
        public ContentRepository ContentRepo { get; set; }

        public GetFacebookTabResponse Get(GetFacebookTab request)
        {
            var response = new GetFacebookTabResponse();
            var contentId = SocialRepo.GetFacebookTab(request.SubDomain);
            if(contentId.IsEmpty())
            {
                response.ResponseStatus = AhavExtensions.InvalidOperation();
                Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                return response;
            }
            var content = ContentRepo.Get(contentId);
            response.RenderBody = content.ToDto().Body;
            return response;
        }

        [RequestHasAppId]
        public PostFacebookTabResponse Post(PostFacebookTab request)
        {
            var response = new PostFacebookTabResponse();
            SocialRepo.SaveFacebookTab(request.SubDomain, request.ContentId);
            response.Modal = AhavExtensions.Success(SocialResources.PostFacebookTabOk);
            return response;
        }

        /// <summary>
        /// Publish a reputation in each Social Provider available
        /// Like in Facebook, +1 in Google Plus, et
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("/api/social/{Provider}/{ObjectId}/like")]
        public PostSocialLikeResponse Post(PostSocialLike request)
        {
            ISocialProviderBase provider;
            switch (request.Provider)
            {
                case SocialProvider.Facebook:
                    provider = new FacebookProvider();
                    break;
                default:
                    throw new NotImplementedException();
            }
            var response = new PostSocialLikeResponse();
            provider.Like(request.ObjectId);
            return response;
        }
    }
}
