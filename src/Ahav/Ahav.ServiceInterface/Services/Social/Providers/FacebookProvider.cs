﻿using System;
using System.Dynamic;
using Ahav.ServiceModel.Social.Domain;
using Facebook;

namespace Ahav.ServiceInterface.Services.Social.Providers
{
    public class FacebookProvider : ISocialProviderBase
    {
        public int CommentsCount(string url)
        {
            var fb = new FacebookClient();
            dynamic parameters = new ExpandoObject();
            parameters.q = "";
            dynamic results = fb.Get("/fql", parameters);
            int count = 1;
            return count;
        }
        /// <summary>
        /// Renews a Facebook token
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns>Token expiration date</returns>
        private DateTime RenewFacebookToken(string accessToken)
        {
            var fb = new FacebookClient();
            dynamic result = fb.Get("oauth/access_token",
                                    new
                                    {
                                        grant_type = "fb_exchange_token",
                                        fb_exchange_token = accessToken
                                    });

            return result.access_token.expires_at;
        }
        public string Token { get; set; }
        public FacebookProvider(string token)
        {
            Token = token;
        }
        public FacebookProvider()
        {

        }
        public void Share(string message)
        {
            var fb = new FacebookClient(Token);
            fb.Post(string.Format("/{0}/"));
        }
        public void Like(object id)
        {
            var fb = new FacebookClient(Token);
            dynamic result = fb.Post(string.Format("/{0}/like", id));
        }

        public UserSocial GetToken(object id, string token)
        {
            var facebookClient = new FacebookClient(token);
            dynamic accessToken = facebookClient.Get("oauth/access_token");
            dynamic user = facebookClient.Get("me");
            return user != null;
        }
        public SocialAccount Get(object id)
        {
            var fbClient = new FacebookClient(Token);
            dynamic user = fbClient.Get("/me");
            return new SocialAccount
            {
                Id = user.id,
                Email = user.email,
                FirstName = user.first_name,
                LastName = user.last_name,
                Birthday = user.birthday
            };
        }
    }
}
