﻿namespace Ahav.ServiceInterface.Services.Social
{
    public static class SocialExtensions
    {
        /// <summary>
        /// facebook::tab::subDomain - Return Content Object Id
        /// </summary>
        public const string RedisFacebookTab = "facebook::tab::{0}";
    }
    public static class SocialError
    {
        public const string InvalidToken = "invalidtoken";
    }
}
