﻿using System;
using System.Net;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Services.Social.Providers;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Operations;
using Ahav.ServiceModel.Social;
using Ahav.ServiceModel.Social.Domain;
using ServiceStack.ServiceInterface;

namespace Ahav.ServiceInterface.Services.Social
{
    public class RegisterSocialService : Service
    {
        public SocialRepository SocialRepo { get; set; }
        public AccountRepository UserRepo { get; set; }

        /// <summary>
        /// Creates a new account using a Social Network credentials
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RegisterSocialResponse Post(RegisterSocial request)
        {
            var response = new RegisterSocialResponse();
            ISocialProviderBase socialProvider;
            switch (request.Provider)
            {
                case SocialProvider.Facebook:
                    socialProvider = new FacebookProvider();
                    break;
                case SocialProvider.Google:
                    throw new NotImplementedException("Not implemented!");
                default:
                    throw new NotImplementedException();
            }
            var token = socialProvider.GetToken(request.Id, request.Token);
            if (token == null)
            {
                response.ResponseStatus = AhavExtensions.InvalidOperation("Invalid Token", SocialError.InvalidToken);
                return response;
            }
            // Set Token in Provider (injected?!?!)
            socialProvider.Token = request.Token;

            // Get user from Facebook, Google, etc
            var user = socialProvider.Get(token.ProviderId);

            if (UserRepo.CheckEmailAvailability(user.Email))
            {
                var postRequest = new PostRegisterBasic
                {
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName
                };
                using (var registerService = base.ResolveService<RegisterService>())
                {
                    var registrationResponse = (PostRegisterBasicResponse)registerService.Post(postRequest);
                    if (registrationResponse.UserId.IsEmpty())
                    {
                        response.ResponseStatus = AhavExtensions.InternalServerError();
                        Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        return response;
                    }
                    response.UserId = registrationResponse.UserId;
                }
            }
            // If user is already registered, just update the data
            SocialRepo.Save(Request.GetUserId(), request.Provider, request.Id, request.Token, DateTime.Now.AddDays(2));
            return response;
        }
    }
}
