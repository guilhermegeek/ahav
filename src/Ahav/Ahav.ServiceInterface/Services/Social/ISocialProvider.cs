﻿using Ahav.ServiceModel.Social.Domain;

namespace Ahav.ServiceInterface.Services.Social
{
    public interface ISocialProviderBase
    {
        string Token { get; set; }
        void Share(string message);
        void Like(object id);
        SocialAccount Get(object id);
        UserSocial GetToken(object id, string token);
        int CommentsCount(string url);
    }
}
