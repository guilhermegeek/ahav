﻿using System;
using System.Net;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.DataDtos;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Operations;
using ServiceStack.Common.Web;
using ServiceStack.ServiceInterface;

namespace Ahav.ServiceInterface.Services
{
    public class SupportAService : Service
    {
        public SupportTicketRepository TicketRepo { get; set; }
        public SupportARepository SupportRepo { get; set; }
        /// <summary>
        /// Creates a new Contact Message
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public PostSupportContactResponse Post(PostSupportContact request)
        {
            var response = new PostSupportContactResponse();
            var contact = SupportRepo.AddContact(new CreateSupportContact
                                       {
                                           Name = request.Name,
                                           Contact = request.Contact,
                                           Email = request.Email,
                                           Message = request.Message,
                                           Subject = request.Subject
                                       });
            if(contact.ResponseStatus!=null)
            {
                response.ResponseStatus = AhavExtensions.InvalidOperation();
                Response.StatusCode = (int) HttpStatusCode.BadRequest;
                return response;
            }
            response.Contact = contact.Contact;
            return response;
        }
        public GetSupportContactResponse Get(GetSupportContact request)
        {
            var response = new GetSupportContactResponse();
            var contact = SupportRepo.ReadContact(new ReadSupportContact { Id = request.Id});
            if(contact.ResponseStatus!=null)
            {
                response.ResponseStatus = AhavExtensions.InvalidOperation();
                Response.StatusCode = (int) HttpStatusCode.BadRequest;
            }
            response.Contact = contact.Contact;
            return response;
        }

        /// <summary>
        /// Creates a new ticket
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public object Post(PostSupportTicket request)
        {
            var response = new PostSupportTicketResponse();
            var ticket = new SupportTicket
            {
                Title = request.Title,
                Content = request.Content,
                Priority = request.Priority,
                State = SupportTicketState.Created
            };
            TicketRepo.Create(ticket);
            response.Result = ticket.ToDto();
            return new HttpResult(response)
            {
                StatusCode = HttpStatusCode.Created
            };
        }
        public GetSupportTicketResponse Get(GetSupportTicket request)
        {
            var response = new GetSupportTicketResponse();
            var ticket = TicketRepo.Get(request.Id);
            if (ticket == null)
                throw HttpError.Conflict("Interno");
            response.Result = ticket.ToDto();
            if (request.GetComments)
                response.Comments = new NComments {Comments = ticket.Comments, CommentsCount = ticket.CommentsCount};
            return response;
        }
        public AddSupportTicketCommentResponse Post(PostSupportTicketComment request)
        {
            var response = new AddSupportTicketCommentResponse();
            var comment = new NComment {Comment = request.Comment, CreatedAt = DateTime.Now};
            var result = TicketRepo.SaveComment(request.Id, comment);
            response.Result = comment;
            return response;
        }
        public GetSupportTicketCommentsResponse Get(GetSupportTicketComments request)
        {
            var response = new GetSupportTicketCommentsResponse();
            response.Result = TicketRepo.GetNComments(request.Id, request.Skip,
                                                      (request.Take == 0) ? 20 : request.Take);
            return response;
        }
        public DeleteSupportTicketCommentsResponse Any(DeleteSupportTicketComments request)
        {
            var response = new DeleteSupportTicketCommentsResponse();
            var result = TicketRepo.DeleteComment(request.TicketId, request.CommentId);
            return response;
        }
        public object Any(EditSupportTicket request)
        {
            var response = new EditSupportTicketResponse();
            TicketRepo.Edit(request.Id, request.Title, request.Content, request.Priority);
            return new HttpResult(response);
        }
        public object Post(EditSupportTicketState request)
        {
            var response = new EditSupportTicketStateResponse();
            TicketRepo.EditState(request.Id, request.State);
            return new HttpResult(response);
        }
    }
}
