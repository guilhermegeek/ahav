﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Filters;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.DataDtos;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Enums;
using Ahav.ServiceModel.Operations;
using Ahav.ServiceModel.Resources;
using ServiceStack.Configuration;
using ServiceStack.ServiceInterface;

namespace Ahav.ServiceInterface.Services
{
    public class TemplateService : Service
    {
        public TemplateRepository TemplateRepo { get; set; }
        public MongoGridFs GridFs { get; set; }

        public void SaveStaticCss(string fileName, string content)
        {
            var appSettings = new AppSettings();
            var rootPath = string.Format("{0}{1}.css", appSettings.Get("StaticCssPath", ""), fileName);
            using (StreamWriter writter = File.CreateText(rootPath))
            {
                writter.Write(content);
            }
        }

        [LoggedIn]
        [RequestHasAppId]
        public PostTemplateResponse Post(PostContentTemplate request)
        {
            var tpl = new Template
                          {
                              Body = new Dictionary<string, string>() { { Culture.pt_PT, ContentTemplateDefaultResources.Body } },
                              Name = request.Name,
                              Title = new Dictionary<string, string>() { { Culture.pt_PT, "Test" } }
                          };

            var response = new PostTemplateResponse();
                var template = TemplateRepo.AddTemplate(Request.GetProjectId(), tpl);
            response.Template = template.ToInfo();
            // Create the .css file 
            SaveStaticCss(template.Id.ToString(), BootstrapDefaultCss.Min);
            response.Modal = AhavExtensions.Success(ContentResources.PostTemplateSuccess);
            return response;
        }
        /// <summary>
        /// Get the template
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [LoggedIn]
        [RequestHasAppId]
        public GetTemplateResponse Get(GetTemplate request)
        {
            var response = new GetTemplateResponse();
            var template = TemplateRepo.GetTemplate(request.TemplateId);
            if (template == null)
            {
                throw new Exception();
            }
            response.Template = template.ToDto();
            return response;
        }

        [LoggedIn]
        [RequestHasAppId]
        public GetTemplatesResponse Get(GetTemplates request)
        {
            var response = new GetTemplatesResponse();
            var templates = TemplateRepo.GetTemplates(request.Ids, request.Skip, request.Take);
            templates.ToList().ForEach(x => response.Templates.Add(x.ToInfo()));
            return response;
        }

        /// <summary>
        /// Save Template
        /// The template is saved in Redis
        /// LESS code is saved in MongoDB for CRUD only
        /// The CSS code is received from client side (compiled with less.js) and saved disk
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [LoggedIn]
        [RequestHasAppId]
        public PutTemplateResponse Put(PutTemplate request)
        {
            var response = new PutTemplateResponse();
            var template = TemplateRepo.SaveTemplate(new UpdateContentTemplate
                                                     {
                                                         Id = request.TemplateId,
                                                         Body = request.Body,
                                                         Name = request.Name,
                                                         Title = request.Title,
                                                         Less = request.Less
                                                     });
            response.Template = template;
            SaveStaticCss(template.Id.ToString(), request.Css);
            response.Modal = AhavExtensions.Success(ContentResources.PutTemplateSuccess);
            return response;
        }

        [LoggedIn]
        [RequestHasAppId]
        public DeleteTemplateResponse Delete(DeleteTemplate request)
        {
            var response = new DeleteTemplateResponse();
            var contents = TemplateRepo.GetContentIdsFromTemplate(request.TemplateId);
            TemplateRepo.RemoveTemplate(request.TemplateId);
            if (contents != null && contents.Any())
            {
                TemplateRepo.MigrateContentTemplate(request.TemplateId, request.IdMigration, contents);
                response.Modal =
                    AhavExtensions.Success(string.Format(ContentResources.DeleteTemplateSuccessMigrations,
                        contents.Count()));
            }
            else
            {
                response.Modal =
                    response.Modal = AhavExtensions.Success(ContentResources.DeleteTemplateSuccessNoMigrations);
            }
            return response;
        }

        [LoggedIn]
        [RequestHasAppId]
        public PutTemplatePublicResponse Put(PutTemplatePublic request)
        {
            var response = new PutTemplatePublicResponse();
            TemplateRepo.SaveTemplateAvailable(request.TemplateId);
            response.Modal = AhavExtensions.Success(ContentResources.PutTemplateSuccess);
            return response;
        }

        [LoggedIn]
        [RequestHasAppId]
        public GetTemplatesAvailableResponse Get(GetTemplatesAvailable request)
        {
            if (request.Take == 0) request.Take = 20;
            var clist = TemplateRepo.GetByProject(Request.GetProjectId());
            return new GetTemplatesAvailableResponse
                   {
                       Templates = clist
                   };
        }
        
        // Files
        public GetTemplateFilesResponse Get(GetTemplateFiles request)
        {
            var response = new GetTemplateFilesResponse();
            var files = TemplateRepo.GetFilesIds(request.TemplateId, request.Skip, request.Take > 0 ? request.Take : 100);
            if (files.Any())
            {
                using (var mediaService = base.ResolveService<MediaService>())
                {
                    var mediaResponse = mediaService.Get(new GetMedias { Ids = files.ToArray() });
                    string upath = new AppSettings().Get("FilesUploadPath", @"c:\tmp\");
                    string path = string.Format("{0}/template", upath);
                    mediaResponse.Medias.ToList().ForEach(x => response.Files.Add(string.Format("{0}{1}{2}", path, x.Id, x.Extension)));
                    response.Medias = mediaResponse.Medias;
                }
            }
            return response;
        }
        [LoggedIn]
        [RequestHasAppId]
        public PostTemplateFileResponse Post(PostTemplateFile request)
        {

            var response = new PostTemplateFileResponse();

            // unit tests and others services in future may requir epost request and not dto based
            //var isOwner = TemplateRepo.IsOwner(request.TemplateId, Request.GetUserId());
            //if (!isOwner && !Request.IsAdmin())
            //{
            //    response.ResponseStatus = AhavExtensions.Unauthorized();
            //    Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            //    return response;
            //}
            if (!RequestContext.Files.Any())
            {
                response.ResponseStatus = AhavExtensions.InternalServerError();
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return response;
            }
            using(var mediaService = base.ResolveService<MediaService>())
            {
                var res = mediaService.Post(new PostMedia
                                                {
                                                    Files = RequestContext.Files,
                                                    MediaType = MediaObjType.Template,
                                                    Thumbnaill = true,
                                                    ThumbnaillResizeWidth = new []{50, 250, 500}
                                                });
                res.Medias.ToList().ForEach(x => response.Medias.Add(x));
            }
            
            TemplateRepo.AddFile(request.TemplateId, response.Medias.Select(x => x.Id).ToArray());
            response.Modal = AhavExtensions.Success(TemplateResources.UploadFileOk);
            return response;
        }
        [LoggedIn]
        [RequestHasAppId]
        public DeleteTemplateFileResponse Delete(DeleteTemplateFile request)
        {
            var response = new DeleteTemplateFileResponse();
            //var isOwner = TemplateRepo.IsOwner(request.TemplateId, Request.GetUserId());
            //if (!isOwner)
            //{
            //    response.ResponseStatus = AhavExtensions.Unauthorized();
            //    Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            //    return response;
            //}
            var result = TemplateRepo.DeleteFile(request.TemplateId, request.FileId);
            if (!result.Ok)
            {
                response.ResponseStatus = AhavExtensions.InternalServerError();
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return response;
            }
            using (var mediaService = base.ResolveService<MediaService>())
            {
                var deleteResponse = mediaService.Delete(new DeleteMedia { Id = request.FileId });
                if (deleteResponse.ResponseStatus != null)
                {
                    response.ResponseStatus = AhavExtensions.InvalidOperation();
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return response;
                }
            }

            response.Modal = AhavExtensions.Success(TemplateResources.DeleteFileOk);
            return response;
        }
        /// <summary>
        /// Updates the top menu from a template
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public PutContentTemplateNavbarResponse Put(PutTemplateNavbar request)
        {
            throw new NotImplementedException();
        }
    }
}