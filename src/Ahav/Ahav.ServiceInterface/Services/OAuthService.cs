﻿using Ahav.Infrastructure;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Filters;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Operations;
using ServiceStack.ServiceInterface;

namespace Ahav.ServiceInterface.Services
{
    public class OAuthService : Service
    {
        public AccountRepository AccountRepo { get; set; }
        public AuthRepository AuthRepo { get; set; }
        public AAuthenticateResponse Get(AAuthenticate request)
        {
            var auth = AuthRepo.GetAuth(request.UserId);
            if (auth == null)
                return new AAuthenticateResponse { IsAuthenticated = false };

            if (auth.Token != request.Token)
                return new AAuthenticateResponse { IsAuthenticated = false };

            return new AAuthenticateResponse { Result = auth, IsAuthenticated = true };
        }
        public AuthTokenResponse Get(AuthToken request)
        {
            var response = new AuthTokenResponse();
            var auths = AuthRepo.GetAuth(request.UserId);
            if (auths == null)
                response.IsAuthenticated = false;
            else if (auths.Token == request.Token)
            {
                response.IsAuthenticated = true;
                response.Result = AuthRepo.GetAuth(request.UserId);
            }
            else if (response.Result == null)
                response.IsAuthenticated = false;
            if (response.Result != null)
                response.Result.UserId = request.UserId; // Id isn't serialized on repository, so we've to populate it again
            return response;
        }
        [LoggedIn]
        public AAuthenticateResponse Delete(AAuthenticate request)
        {
            var response = new AAuthenticateResponse();
            AuthRepo.DeleteAuth(Request.GetAuth().UserId);
            return response;
        }
        public CreateAuthResponse Post(CreateAuth request)
        {
            var response = new CreateAuthResponse();
            var auth = new AhavAuth
            {
                Roles = request.Roles,
                Token = RsaTokenProvider.Instance.RandomString(15),
                UserId = request.UserId
            };
            AuthRepo.CreateAuth(auth);
            response.Result = auth;
            return response;
        }
    }
}
