﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.ApiDtos;
using ServiceStack.FluentValidation;

namespace Ahav.ServiceInterface.Services.Validators
{
    public class PostCompanyValidator : AbstractValidator<PostCompany>
    {
        public PostCompanyValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
        }   
    }
}
