﻿using System.Net;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Filters;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Operations;
using MongoDB.Driver.Builders;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceInterface.Services
{
    public class UserService : Service
    {
        public AccountRepository AccountRepo { get; set; }
        public MailService mailService { get; set; }
        public MailRepository MailRepository { get; set; }
        public UserRepository UserRepo { get; set; }

        private void CreateNewAccount()
        {

        }
        
        
        /// <summary>
        /// Admin only
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [LoggedIn(Roles = new[] { AccountRole.Administrator })]
        public PostUserResponse Post(PostUser request)
        {
            var response = new PostUserResponse();
            var newPassword = RsaTokenProvider.Instance.GenerateNew();
            var user = new User
            {
                AccountState = request.AutoConfirmEmail
                                   ? AccountState.Confirmed
                                   : AccountState.NotConfirmed,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Birthday = request.Birthday,
                PrimaryEmail = request.Email,
                Password = newPassword
            };
            if (!request.AutoConfirmEmail)
            {
                response.ConfirmationToken = RsaTokenProvider.Instance.RandomString(14);
            }
            UserRepo.SaveUser(user);
            if (user.Id.IsEmpty())
            {
                response.ResponseStatus = new ResponseStatus
                {
                    ErrorCode = HttpStatusCode.InternalServerError.ToString(),
                    Message = "Não foi possível criar a conta"
                };
            }
            response.UserId = user.Id;
            response.Password = newPassword;
            return response;
        }
        // basic info
        [LoggedIn]
        public BasicInfoResponse Post(BasicInfo request)
        {
            var response = new BasicInfoResponse();
            var update = new[]
                             {
                                 Update<User>.Set(x => x.FirstName, request.FirstName),
                                 Update<User>.Set(x => x.LastName, request.LastName)
                             };
            AccountRepo.Update(Request.GetAuth().UserId, update);
            return response;
        }
        [LoggedIn]
        public BasicInfoResponse Get(BasicInfo request)
        {
            var response = new BasicInfoResponse();
            var user = AccountRepo.Get(Request.GetUserId());
            response.Result = new BasicInfo
            {
                FirstName = user.FirstName,
                LastName = user.LastName
            };
            return response;
        }

        // contact details
        [LoggedIn]
        public ContactResponse Get(Contact request)
        {
            var response = new ContactResponse();
            var user = AccountRepo.Get(Request.GetUserId());
            response.Mobiles = user.Mobiles;
            response.PublicEmail = user.PublicEmail;
            response.Websites = user.Websites;
            return response;
        }
        [LoggedIn]
        public ContactResponse Post(Contact request)
        {
            var response = new ContactResponse();
            var update = new[]
                             {
                                 Update<User>.Set(x => x.Mobiles, request.Mobiles),
                                 Update<User>.Set(x => x.Websites, request.Websites),
                                 Update<User>.Set(x => x.PublicEmail, request.PublicEmail)
                             };
            AccountRepo.Update(Request.GetAuth().UserId, update);
            return response;
        }
        
    }
}
