﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceInterface.Business;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.BusinessDtos;
using AutoMapper;
using ServiceStack.ServiceInterface;

namespace Ahav.ServiceInterface.Services
{
    public class CompanyService : Service
    {
        public GetCompanyResponse Get(GetCompany request)
        {
            var company = CompanyBus.Get(request.Id);
            return new GetCompanyResponse
                   {
                       Company = company
                   };
        }

        public PostCompanyResponse Post(PostCompany request)
        {
            var response = new PostCompanyResponse();
            var res = CompanyBus.Create(new CreateCompany
                            {
                                Name = request.Name
                            });
            if (res.ResponseStatus != null)
            {
                response.ResponseStatus = res.ResponseStatus;
                return response;
            }
            response.Company = res.Company;
            return response;
        }

        public PutCompanyResponse Put(PutCompany request)
        {
            var response = new PutCompanyResponse();
            var res = CompanyBus.Save(new SaveCompany
                                      {
                                          Id = request.Id,
                                          Name = request.Name
                                      });
            if (res.ResponseStatus != null)
            {
                response.ResponseStatus = res.ResponseStatus;
                return response;
            }
            return response;
        }
        public CompanyBusiness CompanyBus { get; set; }
    }
}
