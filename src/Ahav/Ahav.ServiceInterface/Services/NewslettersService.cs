﻿using System.Collections.Generic;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Filters;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Operations;
using Ahav.ServiceModel.Resources;
using ServiceStack.Common.Web;
using ServiceStack.ServiceInterface;

namespace Ahav.ServiceInterface.Services
{
    public class NewslettersService : Service
    {
        public NewslettersRepository NewslettersRepo { get; set; }
        public MongoGridFs GridFs { get; set; }

        [LoggedIn]
        [RequestHasAppId]
        public GetNewsletterResponse Get(GetNewsletter request)
        {
            var response = new GetNewsletterResponse();
            var newsletter = NewslettersRepo.Get(request.Id);
            if (newsletter == null)
                throw HttpError.NotFound("Newsletter not found");

            response.Newsletter = newsletter.ToDto();
            return response;
        }
        
        [LoggedIn]
        [RequestHasAppId]
        public GetNewslettersResponse Get(GetNewsletters request)
        {
            var response = new GetNewslettersResponse();
            var newsletters = NewslettersRepo.GetAll(request.Skip, request.Take);
            response.Newsletters = newsletters;
            return response;
        }
        /// <summary>
        /// Creates a new Newsletter
        /// Initial state is draft
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [LoggedIn]
        [RequestHasAppId]
        public PostNewsletterResponse Post(PostNewsletter request)
        {
            var response = new PostNewsletterResponse();
            var newsletter = new Newsletter()
            {
                Subject = new Dictionary<string, string> {{ Culture.pt_PT, "Exemplo"}},
                Content = new Dictionary<string, string> {{ Culture.pt_PT, "Exemplo"}},
                IsHtml = true,
                State = NewsletterState.Draft,
                Name = request.Name
            };
            NewslettersRepo.Create(newsletter);
            response.Result = newsletter;
            response.Modal = AhavExtensions.Success(NewslettersResources.PostNewsletterOk);
            return response;
        }
        [LoggedIn]
        [RequestHasAppId]
        public PutNewsletterResponse Put(PutNewsletter request)
        {
            var response = new PutNewsletterResponse();
            NewslettersRepo.Update(request.Id, request.Subject, request.Body, request.IsHtml, request.Name);
            response.Modal = AhavExtensions.Success(NewslettersResources.PutNewsletterOk);
            return response;
        }

        [LoggedIn]
        [RequestHasAppId]
        public PostNewsletterFilesResponse Post(PostNewsletterFiles request)
        {
            var response = new PostNewsletterFilesResponse();
            foreach(var file in RequestContext.Files)
            {
                var gridFile = GridFs.AddFile(file.InputStream, file.FileName, Request.GetProjectId());
                response.Files.Add(gridFile.ToMediaDto());
            }
            return response;
        }

        [LoggedIn]
        [RequestHasAppId]
        public DeleteNewsletterFileResponse Delete(DeleteNewsletterFile request)
        {
            var response = new DeleteNewsletterFileResponse();
            GridFs.RemoveFile(request.FileId);
            NewslettersRepo.RemoveFile(request.NewsletterId, request.FileId);
            return response;
        }
    }
}
