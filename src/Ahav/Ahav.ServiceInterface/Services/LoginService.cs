﻿using System.Net;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Operations;
using Ahav.ServiceModel.Resources;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace Ahav.ServiceInterface.Services
{
    public class LoginService : Service
    {
        public AccountRepository AccountRepo { get; set; }
        public MailService mailService { get; set; }
        public MailRepository MailRepository { get; set; }
        public RoleRepository RoleRepo { get; set; }
        public AuthRepository AuthRepo { get; set; }
        
        /// <summary>
        /// Login
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public LoginResponse Post(PostLogin request)
        {
            var response = new LoginResponse();

            var result = AccountRepo.FindByEmailAndPassword(request.Email, request.Password);

            if (result == null || result.Id.IsEmpty())
            {
                response.ResponseStatus = AhavExtensions.InvalidOperation(AccountResources.InvalidEmailOrPassword, "InvalidEmailOrPassword");
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return response;
            }

            using (var authService = base.ResolveService<OAuthService>())
            {
                var authResponse = authService.Post(new CreateAuth { UserId = result.Id });
                var auth = authResponse.Result;
                Response.UpdateAuth(auth);
                Request.SetAuth(auth);
                response.Result = auth;
                response.Auth = new AuthApi { Id = auth.UserId, Key = auth.Token };
            }
            // If the authentication is done by XHR, there's no need to redirect and append Location header in response (may give troubles for clients libraries)
            if (Request.IsAjax())
            {
                Response.StatusCode = (int)HttpStatusCode.OK;
                return response;
            }

            string redirectParameter = Request.GetParam("redirect");
            string redirect = !string.IsNullOrEmpty(redirectParameter)
                                  ? redirectParameter
                                  : "/admin";

            Response.AddHeader("Location", redirect);
            Response.StatusCode = (int)HttpStatusCode.Redirect;
            return response;
        }
    }
}
