﻿using ServiceStack.ServiceInterface;

namespace Ahav.ServiceInterface.Services
{
    /// <summary>
    /// Invoice API is also consumed by third plugins like RoomRent
    /// </summary>
    /// Invoices are created internaly by others requests, like confirm an order
    public class InvoiceService : Service
    {
        
    }
}
