﻿using System;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Business;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceInterface.Infrasctructure.Exceptions;
using Ahav.ServiceInterface.Validators;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Errors;
using Ahav.ServiceModel.Operations;
using Ahav.ServiceModel.Resources;
using MongoDB.Bson;
using ServiceStack.Common.Web;
using ServiceStack.ServiceInterface;

namespace Ahav.ServiceInterface.Services
{
    /// <summary>
    /// This service register new users in Ahav
    /// </summary>
    public class RegisterService : Service
    {
        public AuthRepository AuthRepo { get; set; }
        public AccountRepository AccountRepo { get; set; }
        public UserRepository UserRepo { get; set; }
        public TokenRepository TokenRepo { get; set; }
        public MailRepository MailRepo { get; set; }

        public MailBusiness MailB { get; set; }


        /// <summary>
        /// Generate random token for confirmation
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private string CreateConfirmationToken(ObjectId userId)
        {
            string token = RsaTokenProvider.Instance.RandomString(10);
            TokenRepo.SaveToken(userId, Token.ConfirmEmail, token);
            return token;
        }

        /// <summary>
        /// Creates a new account with the default state as WaitingForConfirmation
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public object Post(PostRegisterBasic request)
        {
            var response = new PostRegisterBasicResponse();
            
            // Validate Email
            if(!AccountRepo.CheckEmailAvailability(request.Email))
            {
                //response.ResponseStatus = AhavExtensions.InvalidOperation(RegisterResources.EmailInUse,
                //                                                          AccountValidationError.EmailInUse.ToString());
                //Response.StatusCode = (int) HttpStatusCode.BadRequest;
                throw new ApiErrorException(AccountValidationError.EmailInUse.ToString(), RegisterResources.EmailInUse);
                return response;
            }
            
            // Create User collection
            var user = new User
            {
                PrimaryEmail = request.Email,
                Password = request.Password,
                FirstName = request.FirstName,
                LastName = request.LastName,
                AccountState = AccountState.NotConfirmed,
                DisplayName = string.Format("{0} {1}", request.FirstName, request.LastName)
            };
            user = UserRepo.SaveUser(user);
            if (user == null || user.Id.IsEmpty())
            {
                throw new Exception();
            }
            response.UserId = user.Id;

            // Generate token confirmation
            string confirmationToken = this.CreateConfirmationToken(response.UserId);
            
            // Create new auth credentials
            //using (var authService = base.ResolveService<OAuthService>())
            //{
            //    var authResponse = authService.Post(new CreateAuth { UserId = response.UserId });
            //    response.ConfirmationToken = confirmationToken;
            //}
            response.ConfirmationToken = confirmationToken;
            // Email Notification
            MailB.SendMailConfirmation(user.Id, user.PrimaryEmail, confirmationToken);

            response.Modal = AhavExtensions.Success(RegisterResources.WaitingForEmailConfirmation);
            return response;
        }
        /// <summary>
        /// Confirms a account with an email and token
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public object Any(PostRegistrationConfirm request)
        {
            var response = new PostRegistrationConfirmResponse();
            var token = TokenRepo.GetToken(request.UserId, Token.ConfirmEmail);
            // Validate token
            if (token != request.Token)
            {
                throw new ApiErrorException(RegisterErrors.InvalidConfirmationToken, RegisterResources.InvalidConfirmationToken);
            }
            TokenRepo.DeleteToken(request.UserId, Token.ConfirmEmail);
            AccountRepo.UpdateUserState(request.UserId, AccountState.Confirmed);
            response.IsConfirmed = true;

            return new HttpResult(response)
                   {
                       View = "Registration",
                       Template = "_LayoutWelcome"
                   };
        }
       
    }
}
