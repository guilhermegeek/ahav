﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Filters;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Operations;
using Ahav.ServiceModel.Resources;
using MongoDB.Driver.Builders;
using ServiceStack.Common.Web;
using ServiceStack.ServiceClient.Web;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceInterface.Services
{
    public class AccountService : Service
    {
        // permission roles
        [LoggedIn]
        public PermissionRolesResponse Get(PermissionRoles request)
        {
            var response = new PermissionRolesResponse();
            var roles = RoleRepo.GetRoles(request.UserId);
            response.Roles = roles;
            return response;
        }
        [LoggedIn(Roles = new[] { CommonRoles.Administrator })]
        public PermissionRolesResponse Post(PermissionRoles request)
        {
            var response = new PermissionRolesResponse();
            RoleRepo.SaveRole(request.UserId, request.Role);
            return response;
        }
        [LoggedIn(Roles = new[]{CommonRoles.Administrator})]
        public PermissionRolesResponse Delete(PermissionRoles request)
        {
            var response = new PermissionRolesResponse();
            RoleRepo.DeleteRole(request.UserId, request.Role);
            return response;
        }
       
        

        // logout
        [LoggedIn]
        public HttpResult Get(AnyLogout request)
        {
            // passará a apagar apenas a session id da auth
            // a auth deverá existir sempre
            //AccountRepo.DeleteAuth(Request.GetAuth().UserId);
            //Response.Cookies.DeleteCookie(AccountExtensions.AngularAuth);
            //Response.AddHeader("Location", "/welcome");
            AuthRepo.DeleteAuth(Request.GetUserId());
            var result= new HttpResult()
                   {
                       StatusCode = HttpStatusCode.Redirect
                   };
            result.Headers.Remove(AccountExtensions.ClientAuthId);
            result.Headers.Remove(AccountExtensions.ClientAuthKey);
            result.Headers.Add("Location", "/welcome");
            result.DeleteCookie(AccountExtensions.AngularAuth);
            return result;
        }
        
      
        

        /// <summary>
        /// Updates the logged user password
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [LoggedIn]
        public PostAccountPasswordResponse Post(PostAccountPassword request)
        {
            var response = new PostAccountPasswordResponse();
            if(!AccountRepo.ValidatePassord(Request.GetAuth().UserId, request.ActualPassword))
            {
                response.ResponseStatus = new ResponseStatus()
                                              {
                                                  Errors = new List<ResponseError>
                                                               {
                                                                   new ResponseError {ErrorCode = "PwWrong"}
                                                               },
                                                              ErrorCode = HttpStatusCode.InternalServerError.ToString()
                                              };
                return response;
            }
            AccountRepo.Update(Request.GetAuth().UserId, new[] {Update<User>.Set(x => x.Password, request.Password)});
            return response;
        }
        
        /// <summary>
        /// Recover a password
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RecoverPasswordResponse Post(PostRecoverPassword request)
        {
            var response = new RecoverPasswordResponse();
            var asduser = AccountRepo.GetByEmail(request.Email);
            if (AccountRepo.CheckEmailAvailability(request.Email))
            {
                response.ResponseStatus = new ResponseStatus
                                              {
                                                  ErrorCode = "EmailNotRegistered",
                                                  Message = string.Format("<i>{0} is not registered. Have you misspelled it?")
                                              };
                return response;
            }

            var token = GenerateConfirmationToken();
            AccountRepo.SaveRecoverPasswordToken(request.Email, token);
            try
            {
                var emailId = MailRepository.GetInitMailId(MailCommon.RecoverPassword);
                mailService.Post(new AddEmailQueue {EmailId = emailId, Queues = new List<MailQueue>{
                                                                                                       {
                                                                                                           new MailQueue
                                                                                                           {
                                                                                                               Email = request.Email,
                                                                                                               Parameters = new[]{ asduser.Id.ToString(), token}
                                                                                                           }
                                                                                                       }}});
            }
            catch(WebServiceException ex)
            {
                var mailRepo = base.TryResolve<MailRepository>();
                //mailRepo.sa
            }
            return response;
        }

        /// <summary>
        /// This request is usually received from emails, not ajax requests
        /// If confirmed, a new password is generated and return in response 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetConfirmRecoverPasswordResponse Get(GetConfirmRecoverPassword request)
        {
            var response = new GetConfirmRecoverPasswordResponse();
            if (!AccountRepo.ConfirmRecoverPasswordToken(request.Email, request.Token))
            {
                response.ResponseStatus = new ResponseStatus
                                              {
                                                  ErrorCode = "InvalidToken",
                                                  Errors = new List<ResponseError>
                                                               {
                                                                   new ResponseError
                                                                       {
                                                                           ErrorCode = "InvalidToken",
                                                                           FieldName = "Token",
                                                                           Message = AccountResources.InvalidToken
                                                                       }
                                                               },
                                                  Message = AccountResources.InvalidToken
                                              };
                return response;
            }
            var user = AccountRepo.GetByEmail(request.Email);
            AuthRepo.GetAuth(user.Id);
            string newPassword = RsaTokenProvider.Instance.GenerateNew();
            AccountRepo.UpdatePassword(user.Id, newPassword);
            using(var loginService = base.ResolveService<LoginService>())
            {
                loginService.Post(new PostLogin { Email = request.Email, Password = newPassword });
                //@TODO VALIDATE LOGIN RESULT
                response.NewPassword = newPassword;
            }
            return response;
        }
        public ChangecultureResponse Any(ChangeCulture request)
        {
            var response = new ChangecultureResponse();
            Thread.CurrentThread.CurrentCulture = new CultureInfo(request.Culture);
            Response.Cookies.AddCookie(new Cookie { Value = request.Culture, Expires = DateTime.Now.AddMonths(1), Name = "language" });
            Response.AddHeader("Location", "/home");
            Response.StatusCode = (int) HttpStatusCode.Redirect;
            return response;
        }

        public AccountRepository AccountRepo { get; set; }
        public MailService mailService { get; set; }
        public MailRepository MailRepository { get; set; }
        public RoleRepository RoleRepo { get; set; }
        public AuthRepository AuthRepo { get; set; }

        private string GenerateConfirmationToken()
        {
            return RsaTokenProvider.Instance.RandomString(10);
        }


    }    
}
