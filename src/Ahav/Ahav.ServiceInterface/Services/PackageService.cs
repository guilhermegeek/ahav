﻿using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Filters;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.DataDtos;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Resources;
using ServiceStack.Common.Web;
using ServiceStack.ServiceInterface;

namespace Ahav.ServiceInterface.Services
{
    public class PackageService : Service
    {
        public PackageRepository PackageRepo { get; set; }

        [LoggedIn(Roles = new[] { AccountRole.Administrator })]
        public PostAhavPackageResponse Post(PostAhavPackage request)
        {
            var packageRes = PackageRepo.Add(new CreatePackage 
            {
            
                Ammount = request.Ammount,
                Title = request.Title,
                Description = request.Description,
                Features = request.Features,
                IsActive = request.Activate,
                Start = request.Start,
                End = request.End
            });
            if (packageRes == null || packageRes.Package.Id.IsEmpty())
                throw HttpError.Conflict("Package or Id are empty");
            return new PostAhavPackageResponse
            {
                Package = packageRes.Package,
                Modal = AhavExtensions.Success(ProjectResources.PackageCreatedOk)
            };
        }
    }
}
