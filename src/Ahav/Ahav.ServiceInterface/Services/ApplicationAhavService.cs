﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Business;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Filters;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Enums;
using Ahav.ServiceModel.Operations;
using Ahav.ServiceModel.Resources;
using MongoDB.Bson;
using ServiceStack.Common.Web;
using ServiceStack.ServiceInterface;
using ServiceStack.Text;

namespace Ahav.ServiceInterface.Services
{
    public class ApplicationAhavService : Service
    {
        
        /// <summary>
        /// To create a Project, a package must be choosed
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [LoggedIn]
        public PostApplicationResponse Post(PostApplication request)
        {
            var response = new PostApplicationResponse();
            // Validate Domain
            if(ProjectRepo.GetProjectIdBySubdomain(request.Domain) != ObjectId.Empty)
            {
                response.ResponseStatus = AhavExtensions.InvalidOperation(ProjectResources.AppDomainAlreadyTaken, ProjectError.AppDomainAlreadyTaken);
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return response;
            }
            request.Domain = request.Domain.ToLower();
            var package = PackageRepo.GetPackage(request.PackageId);
            // Check if the package is active 
            if (!package.IsActive || package.Start > DateTime.Now || package.End < DateTime.Now)
            {
                response.ResponseStatus = AhavExtensions.InvalidOperation(ProjectResources.PackageNotAvailable, ProjectError.NotAvailable);
                Response.StatusCode = (int) HttpStatusCode.BadRequest;
                return response;
            }
            // It's a free package or a Trial package
            // Project can be created 
            AhavProjectState projectState = package.Ammount == 0 ? AhavProjectState.Enabled : AhavProjectState.Waiting;
            var project = ProjectRepo.Add(Request.GetUserId(), request.Name, request.Description, request.Domain,
                                          request.Features, projectState);
            // Create a new invoice
            if(projectState == AhavProjectState.Waiting)
            {
                
            }
                if (project == null || project.Id.IsEmpty())
                    throw HttpError.Conflict("Project or Project Id are empty");

                response.Project = project.ToDto();
            response.Modal = AhavExtensions.Success(ProjectResources.PostProjectOk);
            return response;
        }
        [LoggedIn]
        public PutProjectResponse  Put(PutApplicationBasic request)
        {
            var project = ProjectRepo.Get(request.Id);
            if(!ProjectRepo.IsOwner(request.Id, Request.GetUserId()) && !Request.IsAdmin())
            {
                return new PutProjectResponse
                           {
                               ResponseStatus =
                                   AhavExtensions.InvalidOperation(BaseResources.DontHavePermissions,
                                                                   ErrorBase.DontHavePermission)
                           };
            }
            var updateResult = ProjectRepo.UpdateProject(request.Id, request.Name, request.Description, request.Domain);
            if (!updateResult.Ok)
                throw HttpError.Conflict(updateResult.ErrorMessage);
            
            // Update domain only if it changed
            if(request.Domain != project.Domain)
            {
                ProjectRepo.SaveSubdomain(request.Id, request.Domain);    
            }
            
            return new PutProjectResponse
                       {
                           Modal = AhavExtensions.Success(ProjectResources.PutProjectOk)
                       };
        }
        [LoggedIn]
        public GetProjectResponse Get(GetProject request)
        {
            var response = new GetProjectResponse();
            if (!ProjectRepo.IsOwner(request.Id, Request.GetUserId()))
            {
                response.ResponseStatus = AhavExtensions.Unauthorized();
                return response;
            }
            var project = ProjectRepo.Get(request.Id);
            response.Project = project.ToDto();
            return response;
        }
        [LoggedIn]
        public GetProjectsResponse Get(GetProjects request)
        {
            var response = new GetProjectsResponse();
            // Only admin can request projects without owners id
            if (!Request.IsAdmin())
                if (request.OwnersIds == null || !request.OwnersIds.Any())
                {
                    request.OwnersIds = new[] {Request.GetUserId()};
                }
            var projects = ProjectRepo.Find(request.ProjectsIds, request.OwnersIds, request.Skip,
                                            request.Take > 0 ? request.Take : 20);
            response.Projects = projects;
            return response;
        }
        
        public GetAhavPackagesAvailableResponse Get(GetAhavPackagesAvailable request)
        {
            var packages = PackageRepo.GetPackages(true, 0, 1000);
            var list = new List<AhavPackageInfo>();
            if (packages != null && packages.Any())
                packages.ToList().ForEach(x => list.Add(x.ToInfo()));
            return new GetAhavPackagesAvailableResponse
                       {
                           Packages = list
                       };
        }
        public GetProjectInitModalResponse Get(GetProjectInitModal request)
        {
            var response = new GetProjectInitModalResponse();
            if (request.ProjectId.IsEmpty())
                request.ProjectId = Request.GetProjectId();
            var id = ProjectRepo.GetVideoInit(request.ProjectId);
            if (!id.HasValue){
                response.HasVideo = false;
                return response;
            }
            using (var mediaService = base.TryResolve<MediaService>())
            {
                var res = mediaService.Get(new GetMedia {FileId = id.Value});
                if (res.ResponseStatus != null)
                    throw HttpError.Conflict(JsonSerializer.SerializeToString(res.ResponseStatus));
                response.Video = res.File;
                response.HasVideo = true;
            }
            
            return response;
        }

        /// <summary>
        /// Loads a dialog first session user request, showing a video
        /// A cookie is used to avoid the reload of video
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [LoggedIn]
        [RequestHasAppId]
        public PostProjectInitModalResponse Post(PostProjectInitModal request)
        {
            var response = new PostProjectInitModalResponse();
            ProjectRepo.SaveVideoInit(Request.GetProjectId(), request.FileId);
            return response;
        }
        [LoggedIn]
        [RequestHasAppId]
        public DeleteProjectInitModalResponse Delete(DeleteProjectInitModal request)
        {
            var response = new DeleteProjectInitModalResponse();
            ProjectRepo.DeleteVideoInit(Request.GetProjectId());
            return response;
        }
        [LoggedIn]
        [RequestHasAppId]
        public GetAttachmentsResponse Get(GetAttachments request)
        {
            var response = new GetAttachmentsResponse();
            using(var mediaService = base.ResolveService<MediaService>())
            {
                var files = ProjectRepo.GetAttachments(Request.GetProjectId());
                if (files != null && files.Any())
                {
                    var res = mediaService.Get(new GetMedias {Ids = files});
                    if (res.ResponseStatus != null)
                        throw new Exception(JsonSerializer.SerializeToString(res.ResponseStatus));
                    response.Files = res.Medias;
                }
            }
            return response;
        }
        [LoggedIn]
        [RequestHasAppId]
        public PostAttachmentResponse Post(PostAttachment request)
        {
            var response = new PostAttachmentResponse();
            if (!RequestContext.Files.Any())
                throw HttpError.Conflict("Not files selected to post attachment");
            using(var mediaService = base.ResolveService<MediaService>())
            {
                var res = mediaService.Post(new PostMedia
                                                {
                                                    Files = RequestContext.Files,
                                                    MediaType = MediaObjType.Attachment
                                                });
                res.Medias.ToList().ForEach(x => response.File = x);
            }
            ProjectRepo.SaveAttachment(Request.GetProjectId(), response.File.Id);
            return response;
        }

        [LoggedIn]
        public DeleteApplicationResponse Delete(DeleteApplication request)
        {
            var response = new DeleteApplicationResponse();
            ApplicationBus.Delete(request.Id);
            return response;
        }
        public ApplicationAhavRepository ProjectRepo { get; set; }
        public ApplicationBusiness ApplicationBus { get; set; }
        public PackageRepository PackageRepo { get; set; }
        public AccountRepository AccountRepo { get; set; }
    }
}
