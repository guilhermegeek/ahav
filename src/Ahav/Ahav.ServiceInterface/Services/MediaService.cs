﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Filters;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceModel.DataDtos;
using Ahav.ServiceModel.Enums;
using Ahav.ServiceModel.Operations;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.GridFS;
using ServiceStack.Common.Web;
using ServiceStack.Configuration;
using ServiceStack.ServiceInterface;
using ServiceStack.Text;

namespace Ahav.ServiceInterface.Services
{
    /// <summary>
    /// Manage file uploads
    /// </summary>
    public class MediaService : Service 
    {
        public MongoGridFs GridFs { get; set; }

        /// <summary>
        /// Reads files from Request and RequestContext!
        /// http://stackoverflow.com/questions/13900473/how-to-use-servicestack-postfilewithrequest
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [LoggedIn]
        public PostMediaResponse Post(PostMedia request)
        {
            var response = new PostMediaResponse();
            //if (RequestContext != null && RequestContext.Files != null && RequestContext.Files.Any())
            //{
            //    Request.Files.ToList().ForEach(x => request.Files.Add(x));
            //}
            if (!request.Files.Any())
            {
                response.ResponseStatus = AhavExtensions.InvalidOperation("You havent selected any file", "NoFiles");
                Response.StatusCode = (int) HttpStatusCode.BadRequest;
                return response;
            }
            foreach (var file in request.Files)
            {
                var options = new MongoGridFSCreateOptions();
                options.ContentType = file.ContentType;

                var fileInfo = GridFs.AddFile(file.InputStream, file.FileName, Request.GetProjectId(), options);

                if (fileInfo.GetMediaType() == MediaType.Image && request.Thumbnaill)
                {
                    if(request.ThumbnaillResizeWidth == null || !request.ThumbnaillResizeWidth.Any())
                    {
                        request.ThumbnaillResizeWidth = new int[] { 50, 150, 250};
                    }
                    var appSettings = new AppSettings();
                    string opath = appSettings.Get("FilesUploadPath", @"c:\tmp\");
                    foreach (var size in request.ThumbnaillResizeWidth)
                    {

                        Image originalImage = Image.FromStream(file.InputStream, true, true);
                        if (size < originalImage.Width)
                        {
                            Image resizedImage = originalImage.GetThumbnailImage(size, (size*originalImage.Height)/size,
                                                                                 null, IntPtr.Zero);
                            string resizeName = opath + file.FileName;
                            resizedImage.Save(resizeName);
                            using (var stream = File.OpenRead(resizeName))
                            {
                                for (int i = 0; i < request.ThumbnaillResizeWidth.Length; i++)
                                {
                                    var opt = options;
                                    opt.Metadata["ThumbParent"] = fileInfo.Id;
                                    GridFs.AddFile(stream, file.FileName, Request.GetProjectId(), options);
                                }
                            }
                            File.Delete(resizeName);
                        }
                    }
                }

                response.Medias.Add(fileInfo.ToMediaDto());
            }
            return response;
        }

        /// <summary>
        /// Returns the RAW of the media (Image, Video, etc)
        /// Use AsAttachment flag to promp the client to download the file
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public object Get(GetMediaRaw request)
        {
            MemoryStream s = new MemoryStream();
            GridFs.Grid.Download(s, Query.EQ("_id", request.FileId));
            string contentType = GridFs.GetFileInfo(request.FileId).ContentType;
            return new HttpResult(s, contentType);
        }
        /// <summary>
        /// Returns the Media by Id
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetMediaResponse Get(GetMedia request)
        {
            var response = new GetMediaResponse();
            var fileInfo = GridFs.GetFileInfo(request.FileId);
            response.File = fileInfo.ToMediaDto();
            return response;
        }

        public GetMediasResponse Get(GetMedias request)
        {
            var response = new GetMediasResponse();
            //var files = MediaRepository.GetAll(request.OwnerId, request.Ids.ToArray(), request.MediaType);
            var queries = new List<IMongoQuery>();
            //if (request.ProjectId.HasValue && !request.ProjectId.Value.IsEmpty())
            //    files = GridFs.GetByProject(request.ProjectId.Value);
            //else if (request.Ids != null && request.Ids.Any())
            //    files = GridFs.Find(request.Ids);
            var gridReq = new ReadMedias ();
            if(request.ProjectId.HasValue) 
                gridReq.ProjectId = request.ProjectId;
            if(request.MediaType.HasValue)
                gridReq.MediaType = request.MediaType.Value;
            if(request.Ids != null && request.Ids.Any())
                gridReq.Ids = request.Ids;
            var mediaRes = GridFs.Find(gridReq);
            if (mediaRes.ResponseStatus != null)
                throw new Exception(JsonSerializer.SerializeToString(mediaRes.ResponseStatus));
            
            var files = mediaRes.Files;

            foreach (var file in files)
            {
                var dto = file.ToMediaDto();
                response.Medias.Add(dto);
                response.Files.Add(dto.Url);
            }
            return response;
        }
        [LoggedIn]
        public DeleteMediaResponse Delete(DeleteMedia request)
        {
            var response = new DeleteMediaResponse();
            if(!GridFs.FileExists(request.Id))
            {
                Response.StatusCode = (int) HttpStatusCode.NotFound;
                response.ResponseStatus = AhavExtensions.InvalidOperation("Not Found");
                return response;
            }
            GridFs.RemoveFile(request.Id);
            return response;
        }
    }
}
