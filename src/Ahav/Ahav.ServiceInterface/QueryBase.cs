﻿using System.Collections.Generic;

namespace Ahav.ServiceInterface
{
    public class QueryBase
    {
        public string Query { get; set; }
        public int Offset { get; set; }
        public int Limit { get; set; }
    }
    public class NPage
    {
        public int Pages { get; set; }
        public int CurrentPage { get; set; }
        public int ResultsPerPage { get; set; }
        public IEnumerable<object> Results { get; set; } 
    }
}
