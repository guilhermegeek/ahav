﻿using System;
using System.IO;
using System.Net;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Services;
using Ahav.ServiceModel;
using Ahav.ServiceModel.ApiDtos;
using MongoDB.Bson;
using ServiceStack.Common.Web;
using ServiceStack.ServiceInterface;

namespace Ahav.ServiceInterface
{
    /// <summary>
    /// This service handles the Fallback response, and resolve it to a blog, content view, etc
    /// </summary>
    public class AhavRouteResolver : Service
    {
        public ApplicationAhavRepository ProjectRepo { get; set; }
        public ContentRepository ContentRepo { get; set; }
        public SlugRepository SlugRepo { get; set; }
        /// <summary>
        /// Falback manages the Content and Template rendering
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public object Any(Fallback request)
        {
            var response = new FallbackResponse();
            var requestUri = new Uri(Request.AbsoluteUri);

            //string subDomain = CommonExtensions.GetHostname(requestUri);
            var slug = Path.GetFileNameWithoutExtension(requestUri.LocalPath);

            //// Fallback should be fired only for AhavProjects requests, including a subdomain
            //if (string.IsNullOrEmpty(subDomain))
            //    throw new Exception(string.Format("The subdomain {0} doesnt exist, with the slug {1}", subDomain, slug));

            ////request.Path = request.Path.Replace(".html", "").ToLower();

            //var projectId = ProjectRepo.GetProjectIdBySubdomain(subDomain);
            
           

            using(var contentService = base.ResolveService<ContentService>())
            {
                return contentService.Any(new GetContentView {Path = slug});
            }
           
        }
    }
}
