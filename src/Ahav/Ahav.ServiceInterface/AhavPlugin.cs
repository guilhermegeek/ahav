﻿using System;
using System.Net;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Business;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Infrasctructure;
using Ahav.ServiceInterface.Infrasctructure.Exceptions;
using Ahav.ServiceInterface.Mappings;
using Ahav.ServiceInterface.Services;
using Ahav.ServiceInterface.Services.Social;
using Ahav.ServiceInterface.Validators;
using Ahav.ServiceModel;
using Ahav.ServiceInterface.Data.Mappings;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Operations;
using Ahav.ServiceModel.Social;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;
using ServiceStack.Common.Web;
using ServiceStack.Configuration;
using ServiceStack.Redis;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.ServiceModel;
using ServiceStack.ServiceInterface.Validation;
using ServiceStack.Text;
using ServiceStack.WebHost.Endpoints;
using AutoMapper;

namespace Ahav.ServiceInterface
{

    /// <summary>
    /// Base Plugin
    /// </summary>
    public class AhavPlugin : IPlugin
    {
        /// <summary>
        /// Returns an ObjectId.Empty instance if null
        /// </summary>
        /// <returns></returns>
  
        public void Register(IAppHost appHost)
        {
            var appSettings = new AppSettings();
            var container = appHost.GetContainer();
            string mongoHost = appSettings.GetString("MongoDbHost");
            string mongoDb = appSettings.GetString("MongoDbDatabase");
            string redisHost = appSettings.GetString("RedisHost");

            // IOC Dependencies
            // Services
            appHost.RegisterService<LoginService>();
            appHost.RegisterService<ApplicationAhavService>();
            appHost.RegisterService<PackageService>();
            appHost.RegisterService<OAuthService>();
            appHost.RegisterService<ContentService>();
            appHost.RegisterService<TemplateService>();
            appHost.RegisterService<MediaService>();
            appHost.RegisterService<MailService>();

            // Bussiness, Data, Mappers
            container.Register<MongoDatabase>(x =>new MongoClient(mongoHost).GetServer().GetDatabase(mongoDb));
            container.Register<IRedisClientsManager>(x => new PooledRedisClientManager(20, 60, redisHost));

            container.RegisterAutoWired<MongoGridFs>();
            appHost.RegisterService<AhavRouteResolver>();

#if DEBUG
            // @TODO REMOVE THIS HACK! SOMESONE MAY BE STUPID ENOUGH TO PUBLISH IN DEBUG AND A BACKDOOR IS LEAVED FOR BOTS...
            // Reset database and add my initial account
            appHost.RegisterService<BootService>();
            appHost.Routes
                .Add<BootInit>("/api/restart", ApplyTo.All);
#endif
            // Infrastructure
            container.RegisterAutoWired<LoggerBusiness>();
            // Social
            container.RegisterAutoWired<SocialRepository>();

            // Ahav

            container.RegisterAutoWired<RoleRepository>();
            container.RegisterAutoWired<AuthRepository>();
            container.RegisterAutoWired<TokenRepository>();
            container.RegisterAutoWired<SetupRepository>();
            container.RegisterAutoWired<UserRepository>();

            container.RegisterAutoWired<CommentRepository>();

            container.RegisterAutoWired<PackageRepository>();

            appHost.RegisterService<ApplicationAhavService>();
            container.RegisterAutoWired<ApplicationAhavRepository>();
            container.RegisterAutoWired<AppDomainBusiness>();
            container.RegisterAutoWired<ApplicationBusiness>();
            container.RegisterAutoWired<AppDomainRepository>();

            container.RegisterAutoWired<AccountRepository>();


            container.RegisterAutoWired<PlaceBusiness>();
            container.RegisterAutoWired<PlaceRepository>();

            container.RegisterAutoWired<MailRepository>();
            container.RegisterAutoWired<MailBusiness>();
            container.RegisterAutoWired<ContentBusiness>();
            container.RegisterAutoWired<ContentRepository>();

            appHost.RegisterService<TemplateService>();
            container.RegisterAutoWired<TemplateBusiness>();
            container.RegisterAutoWired<TemplateRepository>();

            container.RegisterAutoWired<SupportTicketRepository>();
            container.RegisterAutoWired<SupportARepository>();

            container.RegisterAutoWired<NewslettersRepository>();
            // Company
            container.RegisterAutoWired<CompanyRepository>();
            container.RegisterAutoWired<CompanyBusiness>();
            appHost.RegisterService<CompanyService>();
            
            container.RegisterValidators(typeof(PostBasicRegisterValidator).Assembly); // ServiceModel
            container.RegisterValidators(typeof(PostAhavPackageValidator).Assembly); // Logic

            container.RegisterAutoWired<SlugRepository>();

            // Order
            container.RegisterAutoWired<OrderBusiness>();
            container.RegisterAutoWired<OrderRepository>();

            appHost.RegisterService<FacebookAppService>();
            // Filters

            // Resolve application domain for request
            appHost.PreRequestFilters.Add((req, res) =>
                                          {
                                            

                                              


                                              //var uri = new Uri(req.AbsoluteUri);
                                              //var projectRepo = req.TryResolve<ApplicationAhavRepository>();


                                              //var id = projectRepo.GetProjectIdBySubdomain(uri.Host);
                                              //req.SetProjectId(id);
                                              //// Get project credentials from cookies, headers and query parameters
                                              //var projectId = GetCredentialProjectFromRequest(req);
                                              //if (!projectId.IsEmpty())
                                              //{
                                              //    req.SetProjectId(projectId);
                                              //}
                                          });

            appHost.PreRequestFilters.Add(CommonExtensions.RequestSetCultureAction);

            //Switch ServiceStack’s mode of operation to buffered
            appHost.PreRequestFilters.Add((req, res) => req.UseBufferedStream = true);

            // Routes
            appHost.Routes
                .Add<FacebookTabRequest>(AhavRoute.FacebookTabRequest)
                // Register
                .Add<PostRegisterBasic>(AhavRoute.PostRegisterBasic)

                // Media
                .Add<GetMediaRaw>(AhavRoute.GetMedia, ApplyTo.Get.ToString())
                .Add<PostMedia>(AhavRoute.PostMedia, ApplyTo.Post.ToString())

                //Common 
                .Add<PostLogin>("/login")
                .Add<AnyLogout>("/logout", ApplyTo.All)
                .Add<PostAhavAccount>("/api/account")
                .Add<PostRegistrationConfirm>(AhavRoute.PostConfirmAccount)
                .Add<PostRecoverPassword>("/recover")
                .Add<GetThumbs>("/thumbs", ApplyTo.Get.ToString())
                .Add<ChangeCulture>("/culture/{Culture}", ApplyTo.All)
                
                // SupportTicket
                .Add<PostSupportTicket>("/tickets")
                .Add<GetSupportTicket>("/ticket/")
                .Add<PostSupportContact>(AhavRoute.PostSupportContact, ApplyTo.Post.ToString())
                .Add<GetSupportContact>(AhavRoute.GetSupportContact, ApplyTo.Get.ToString())

                // Newsletters
                .Add<PostNewsletter>(AhavRoute.PostNewsletter, ApplyTo.Post.ToString())
                .Add<PutNewsletter>(AhavRoute.PutNewsletter, ApplyTo.Put.ToString())
                .Add<GetNewsletter>(AhavRoute.GetNewsletter, ApplyTo.Get.ToString())
                .Add<GetNewsletters>(AhavRoute.GetNewsletters, ApplyTo.Get.ToString())
                .Add<PostNewsletterFiles>(AhavRoute.PostNewsletterFiles, ApplyTo.Post.ToString())
                .Add<DeleteNewsletterFile>(AhavRoute.DeleteNewsletterFile, ApplyTo.Delete.ToString())

                // Template
                .Add<PostContentTemplate>(AhavRoute.PostTemplate, ApplyTo.Post.ToString())
                .Add<GetTemplates>(AhavRoute.GetTemplates, ApplyTo.Get.ToString())
                .Add<GetTemplate>(AhavRoute.GetTemplate, ApplyTo.Get.ToString())
                .Add<PutTemplate>(AhavRoute.PutTemplate, ApplyTo.Put.ToString())
                .Add<DeleteTemplate>(AhavRoute.DeleteTemplate, ApplyTo.Delete.ToString())
                .Add<PutTemplatePublic>(AhavRoute.PutTemplatePublic, ApplyTo.Put.ToString())
                .Add<PostTemplateFile>(AhavRoute.PostTemplateFile, ApplyTo.Post.ToString())
                .Add<GetTemplateFiles>(AhavRoute.GetTemplateFiles, ApplyTo.Get.ToString())
                .Add<DeleteTemplateFile>(AhavRoute.DeleteTemplateFile, ApplyTo.Delete.ToString())

                // Content
                .Add<PostContent>(AhavRoute.PostContent, ApplyTo.Post.ToString())
                .Add<GetContent>(AhavRoute.GetContent, ApplyTo.Get.ToString())
                .Add<PutContent>(AhavRoute.PutContent, ApplyTo.Put.ToString())
                .Add<DeleteContent>(AhavRoute.DeleteContent, ApplyTo.Delete.ToString())
                .Add<PostValidateContentSlug>(AhavRoute.PostValidateContentSlug, ApplyTo.Post.ToString())
                .Add<GetContentFiles>(AhavRoute.GetContentFiles, ApplyTo.Get.ToString())
                .Add<PostContentFile>(AhavRoute.PostContentFile, ApplyTo.Post.ToString())
                .Add<DeleteContentFile>(AhavRoute.DeleteContentFile, ApplyTo.Delete.ToString())
                .Add<PutContentDefault>(AhavRoute.PutContentDefault, ApplyTo.Put.ToString())
                .Add<GetContentDefault>(AhavRoute.GetContentDefault, ApplyTo.Get.ToString())

                // Applications
                .Add<PostApplication>(AhavRoute.PostProject, ApplyTo.Post.ToString())
                .Add<PutApplicationBasic>(AhavRoute.PutProject, ApplyTo.Put.ToString())
                .Add<GetProject>(AhavRoute.GetProject, ApplyTo.Get.ToString())
                .Add<GetProjects>(AhavRoute.GetProjects, ApplyTo.Get.ToString())
                .Add<PostAhavPackage>(AhavRoute.PostAhavPackage, ApplyTo.Post.ToString())
                .Add<GetAhavPackagesAvailable>(AhavRoute.GetAhavPackagesAvailable, ApplyTo.Get.ToString())
                .Add<GetProjectInitModal>(AhavRoute.GetProjectInitModal, ApplyTo.Get.ToString())
                .Add<PostProjectInitModal>(AhavRoute.PostProjectInitModal, ApplyTo.Post.ToString())
                .Add<DeleteProjectInitModal>(AhavRoute.DeleteProjectInitModal, ApplyTo.Delete.ToString())
                .Add<GetAttachments>(AhavRoute.GetAttachments, ApplyTo.Get.ToString())
                .Add<PostAttachment>(AhavRoute.PostAttachment, ApplyTo.Post.ToString())
                .Add<DeleteApplication>(AhavRoute.DeleteApplication, ApplyTo.Delete.ToString())


                // Social
                .Add<RegisterSocial>(SocialRoute.RegisterSocial, ApplyTo.Post.ToString())
                .Add<PostSocialStream>(SocialRoute.PostSocialStream, ApplyTo.Post.ToString())
                .Add<PostSocialLike>(SocialRoute.PostSocialLike, ApplyTo.Post.ToString());

            // AutoMapper
            ContentDtoMapping.Register();
            MapLogCollection.Register();
            MapNewsletterTemplateCollection.Map();
            MapPlaceCollection.Register();
            MapOrderCollection.Register();
            MapCompanyCollection.Register();
            OrderDtoMap.Register();

            // MongoDB Mappings
            MapApplicationCollection.RegisterMappings();
            MapPackageCollection.RegisterMappings();
            MailMapping.RegisterMappings();
            MapUserCollections.RegisterMappings();
            MapContentCollection.RegisterMappings();
            MapTemplateCollection.RegisterMappings();
            ContentTemplateIdsMappings.RegisterMappings();
            MapSupportContactCollection.RegisterMappings();
            MapSupportTicketCollection.RegisterMappings();
            MapNewsletterCollection.RegisterMappings();
            
            // Ncomments is mapped because comments repository use it as a main collection
            if (!BsonClassMap.IsClassMapRegistered(typeof(NComments)))
            {
                BsonClassMap.RegisterClassMap<NComments>(cm =>
                {
                    cm.MapMember(x => x.Comments);
                    cm.MapProperty(x => x.CommentsCount);
                });
            }
            if (!BsonClassMap.IsClassMapRegistered(typeof(Media)))
            {
                BsonClassMap.RegisterClassMap<Media>(cm =>
                {
                    cm.MapIdProperty(x => x.Id).SetIdGenerator(
                        ObjectIdGenerator.Instance);
                    cm.MapProperty(x => x.MediaType);
                    cm.MapProperty(x => x.Extension);
                    cm.MapProperty(x => x.OwnerId);
                    cm.MapProperty(x => x.ThumbnailUrl);
                    cm.MapProperty(x => x.Name);
                });
            }
            var myConventions = new ConventionPack();
            myConventions.Add(new CamelCaseElementNameConvention());
            ConventionRegistry.Register("My Custom Conventions", myConventions, t => t.FullName.StartsWith("Common."));
            appHost.ServiceExceptionHandler = (req, request, exception) =>
                                              {
                                                  //ErrorLog.GetDefault(HttpContext.Current).Log(new Error(exception,
                                                  //                                                       HttpContext.
                                                  //                                                           Current));
                                                  var logger = req.TryResolve<LoggerBusiness>();
                                                
                                               
                                                  var error = exception as HttpError;
                                                  // Not authenticated exception
                                                  if (error != null)
                                                  {
                                                      switch (error.StatusCode)
                                                      {
                                                          case HttpStatusCode.Unauthorized:
                                                              if (req.IsAjax())
                                                                  return new ResponseStatus {Message = "Sorry but you don't have enough permissions to perfor this task."};
                                                              return HttpResult.Redirect(string.Format("/unauthorized"));
                                                          case HttpStatusCode.NotFound:
                                                      //        if (req.IsAjax())
                                                                  return new ResponseStatus {Message = "Not found, sorry!"};
                                                              HttpResult.Redirect(string.Format("/not-found"));
                                                              break;
                                                              // Internal operation error (something that shouln't happen)
                                                          case HttpStatusCode.BadRequest:
                                                              logger.Error(exception.Message);
                                                    //          if (req.IsAjax())
                                                                  return new ResponseStatus {Message = "Sorry but something went wrong.<br>I'm a indevelopment developer without any business support, so bugs can happen.<br>I've logged this error but if you can, help me <b>improve</b>."};
                                                              return HttpResult.Redirect(string.Format("/something-went-wrong"));
                                                      }
                                                  }
                                                  logger.Error(exception.Message);

                                                  //if (req.IsAjax())
                                                      return new ResponseStatus {Message = "Sorry but something went wrong.<br>I'm a indevelopment developer without any business support, so bugs can happen.<br>I've logged this error but if you can, help me <b>improve</b>."};
                                                  return HttpResult.Redirect(string.Format("/something-went-wrong"));
                                              };
        }
    }
    public static class ConfigureAppHost
    {
        
        public static IAppHost ConfigureApp(this IAppHost appHost)
        {
            throw new Exception("not used anymore");
        }
    }
    public class AccountPluginSettings
    {

    }

}
