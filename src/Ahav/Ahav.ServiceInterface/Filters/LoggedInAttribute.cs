﻿using System;
using System.Linq;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Services;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Operations;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using ServiceStack.Text;

namespace Ahav.ServiceInterface.Filters
{
    /// <summary>
    /// This exception is fired when a authenticated user hans't the right permissions for the request
    /// </summary>
    public class RoleMissingException : Exception
    {
        public string[] RolesMissing { get; set; }
        public string AbsoluteUri { get; set; }

        public RoleMissingException(string[] roles, string absoluteUri)
        {
            this.RolesMissing = roles;
            this.AbsoluteUri = absoluteUri;
        }
    }
    
    /// <summary>
    /// Request must be authorized from cookies, query parameter, post value, etc
    /// </summary>
    public class LoggedInAttribute : RequestFilterAttribute 
    {
        public string[] Roles { get; set; }
     
        public override void Execute(IHttpRequest req, IHttpResponse res, object requestDto)
        {
            var authService = req.TryResolve<OAuthService>();
            var roleRepo = req.TryResolve<RoleRepository>();

            // Get auth credentials from cookies, headers and query parameters
            var requestAuth = req.GetAuthorization();
            if (requestAuth == null) return;

            if (requestAuth != null && !requestAuth.Id.IsEmpty() && !string.IsNullOrEmpty(requestAuth.Key))
            {
                var authRes = authService.Get(new AuthToken
                {
                    Token = requestAuth.Key,
                    UserId = requestAuth.Id
                });

                // User sent credentials but isn't authenticated
                if (!authRes.IsAuthenticated)
                {
                    //  res.ReturnAuthRequired();
                    return;
                }

                //Token is expired
                if (authRes.Result.TokenExpires <= DateTime.Now)
                {
                    // @TODO
                }
                var rolesResponse = roleRepo.GetRoles(authRes.Result.UserId);
                // Request is sucefully authorized, save credentials in Request Items
                req.SetAuth(authRes.Result, rolesResponse.ToArray());
            }

            if (!req.IsAuthenticated())
            {
                throw HttpError.Unauthorized(req.AbsoluteUri); // absolute uri is passed as message, to redirect url
            }
            if (Roles != null)
            {
                var userRoles = roleRepo.GetRoles(req.GetUserId());
                // this means that the user has the right permission, and the main 
                if (!Roles.Any(x => userRoles.Contains(x)) && !userRoles.Contains(AccountRole.Administrator))
                {
                    throw new RoleMissingException(Roles, req.AbsoluteUri);
                }
            }
        }
    }
}
