﻿using System;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using MongoDB.Bson;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace Ahav.ServiceInterface.Filters
{
    /// <summary>
    /// Reads first the appId from query url
    /// </summary>
    public class RequestHasAppIdAttribute : RequestFilterAttribute 
    {
        public RequestHasAppIdAttribute()
        {
            this.RequireProject = true;
        }
        public AhavFeature[] Features { get; set; }
        public bool RequireProject { get; set; }
        public override void Execute(IHttpRequest req, IHttpResponse res, object requestDto)
        {
            var id = req.GetProjectId();
            if (id.IsEmpty() && !req.IsAdmin())
            {
                
                throw HttpError.Unauthorized("Don't have project");
            }
        }
    }
}
