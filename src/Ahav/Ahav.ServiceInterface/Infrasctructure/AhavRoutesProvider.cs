﻿using Ahav.ServiceModel;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Operations;
using Ahav.ServiceModel.Social;
using ServiceStack.ServiceInterface;
using ServiceStack.WebHost.Endpoints;

namespace Ahav.ServiceInterface.Infrasctructure
{
    public static class AhavRoutesProvider
    {
        public static void Configure(IAppHost appHost)
        {
            // Routes
            appHost.Routes
                // Register
                .Add<PostRegisterBasic>(AhavRoute.PostRegisterBasic)

                // Media
                .Add<GetMediaRaw>(AhavRoute.GetMedia, ApplyTo.Get.ToString())
                .Add<PostMedia>(AhavRoute.PostMedia, ApplyTo.Post.ToString())

                //Common 
                .Add<PostLogin>("/login")
                .Add<AnyLogout>("/logout")
                .Add<PostAhavAccount>("/api/account")
                .Add<PostRegistrationConfirm>(AhavRoute.PostConfirmAccount)
                .Add<PostRecoverPassword>("/recover")
                .Add<GetThumbs>("/thumbs", ApplyTo.Get.ToString())

                // SupportTicket
                .Add<PostSupportTicket>("/tickets")
                .Add<GetSupportTicket>("/ticket/")
                .Add<PostSupportContact>(AhavRoute.PostSupportContact, ApplyTo.Post.ToString())
                .Add<GetSupportContact>(AhavRoute.GetSupportContact, ApplyTo.Get.ToString())

                // Newsletters
                .Add<PostNewsletter>(AhavRoute.PostNewsletter, ApplyTo.Post.ToString())
                .Add<PutNewsletter>(AhavRoute.PutNewsletter, ApplyTo.Put.ToString())
                .Add<GetNewsletter>(AhavRoute.GetNewsletter, ApplyTo.Get.ToString())
                .Add<GetNewsletters>(AhavRoute.GetNewsletters, ApplyTo.Get.ToString())
                .Add<PostNewsletterFiles>(AhavRoute.PostNewsletterFiles, ApplyTo.Post.ToString())
                .Add<DeleteNewsletterFile>(AhavRoute.DeleteNewsletterFile, ApplyTo.Delete.ToString())

                

                // Template
                .Add<PostContentTemplate>(AhavRoute.PostTemplate, ApplyTo.Post.ToString())
                .Add<GetTemplates>(AhavRoute.GetTemplates, ApplyTo.Get.ToString())
                .Add<GetTemplate>(AhavRoute.GetTemplate, ApplyTo.Get.ToString())
                .Add<PutTemplate>(AhavRoute.PutTemplate, ApplyTo.Put.ToString())
                .Add<DeleteTemplate>(AhavRoute.DeleteTemplate, ApplyTo.Delete.ToString())
                .Add<PutTemplatePublic>(AhavRoute.PutTemplatePublic, ApplyTo.Put.ToString())
                .Add<PostTemplateFile>(AhavRoute.PostTemplateFile, ApplyTo.Post.ToString())
                .Add<GetTemplateFiles>(AhavRoute.GetTemplateFiles, ApplyTo.Get.ToString())
                .Add<DeleteTemplateFile>(AhavRoute.DeleteTemplateFile, ApplyTo.Delete.ToString())

                // Content
                .Add<PostContent>(AhavRoute.PostContent, ApplyTo.Post.ToString())
                .Add<GetContent>(AhavRoute.GetContent, ApplyTo.Get.ToString())
                .Add<PutContent>(AhavRoute.PutContent, ApplyTo.Put.ToString())
                .Add<DeleteContent>(AhavRoute.DeleteContent, ApplyTo.Delete.ToString())
                .Add<PostValidateContentSlug>(AhavRoute.PostValidateContentSlug, ApplyTo.Post.ToString())
                .Add<GetContentFiles>(AhavRoute.GetContentFiles, ApplyTo.Get.ToString())
                .Add<PostContentFile>(AhavRoute.PostContentFile, ApplyTo.Post.ToString())
                .Add<DeleteContentFile>(AhavRoute.DeleteContentFile, ApplyTo.Delete.ToString())
                .Add<PutContentDefault>(AhavRoute.PutContentDefault, ApplyTo.Put.ToString())
                .Add<GetContentDefault>(AhavRoute.GetContentDefault, ApplyTo.Get.ToString())

                // Projects
                .Add<PostApplication>(AhavRoute.PostProject, ApplyTo.Post.ToString())
                .Add<PutApplicationBasic>(AhavRoute.PutProject, ApplyTo.Put.ToString())
                .Add<GetProject>(AhavRoute.GetProject, ApplyTo.Get.ToString())
                .Add<GetProjects>(AhavRoute.GetProjects, ApplyTo.Get.ToString())
                .Add<PostAhavPackage>(AhavRoute.PostAhavPackage, ApplyTo.Post.ToString())
                .Add<GetAhavPackagesAvailable>(AhavRoute.GetAhavPackagesAvailable, ApplyTo.Get.ToString())
                .Add<GetProjectInitModal>(AhavRoute.GetProjectInitModal, ApplyTo.Get.ToString())
                .Add<PostProjectInitModal>(AhavRoute.PostProjectInitModal, ApplyTo.Post.ToString())
                .Add<DeleteProjectInitModal>(AhavRoute.DeleteProjectInitModal, ApplyTo.Delete.ToString())
                .Add<GetAttachments>(AhavRoute.GetAttachments, ApplyTo.Get.ToString())
                .Add<PostAttachment>(AhavRoute.PostAttachment, ApplyTo.Post.ToString())


                // Social
                .Add<RegisterSocial>(SocialRoute.RegisterSocial, ApplyTo.Post.ToString())
                .Add<PostSocialStream>(SocialRoute.PostSocialStream, ApplyTo.Post.ToString())
                .Add<PostSocialLike>(SocialRoute.PostSocialLike, ApplyTo.Post.ToString());
        }
    }
}
