﻿using System;
using System.Collections.Generic;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceInterface.Data;
using Ahav.ServiceInterface.Extensions;
using Ahav.ServiceModel;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.DataDtos;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Operations;
using ServiceStack.Redis;
using ServiceStack.ServiceClient.Web;
using ServiceStack.WebHost.Endpoints;

namespace Ahav.ServiceInterface.Infrasctructure
{
    public static class BootDatabases
    {
        public static void Boot()
        {

            var client = new JsonServiceClient("http://localhost:1337/");
            client.AllowAutoRedirect = false;


            //client.Headers["Accepet"] = "application/json, text/plain, */*";
            var container = EndpointHost.Config.ServiceManager.Container;
            MongoDbHelper.Instance.Database.Drop();

            var redis = container.TryResolve<IRedisClientsManager>();
            redis.GetClient().FlushAll();
            var userRepo = container.TryResolve<AccountRepository>();
            var roleRepo = container.TryResolve<RoleRepository>();
            var authRepo = container.TryResolve<AuthRepository>();
            var tokenRepo = container.TryResolve<TokenRepository>();

            // Mail
            client.Post(new InitMailService());

            //// Account
            var requestRegister = new PostRegisterBasic
            {
                Email = "email@guilhermecardoso.pt",
                FirstName = "Guilherme",
                LastName = "Cardoso",
                Password = "123"
            };
            var responseRegister = client.Post(requestRegister);
            client.Post(new PostRegistrationConfirm { Token = responseRegister.ConfirmationToken, UserId = responseRegister.UserId });
            roleRepo.SaveRole(responseRegister.UserId, AccountRole.Administrator);
            roleRepo.SaveRole(responseRegister.UserId, AccountRole.Gui);
            var auth = client.Post(new PostLogin { Email = requestRegister.Email, Password = requestRegister.Password });
            var loginResponse = client.Login();
            //client.SetAuth(auth.Auth.Id, auth.Auth.Key);

            var packageResponse = container.Resolve<PackageRepository>().Add(new CreatePackage
            {
                IsActive = true,
                //DaysPrice = new 
                Ammount = 0,
                Duration = new TimeSpan(365, 0, 0, 0, 0),
                Start = DateTime.Now.AddDays(-1),
                Title = new Dictionary<string, string>() { { Culture.pt_PT, "Testa!" } },
                Description = new Dictionary<string, string>() { { Culture.pt_PT, "Pacote de teste" } },
                Features = new[] { AhavFeature.Support, AhavFeature.Newsletters, AhavFeature.Ecommerce, AhavFeature.Cms, AhavFeature.Blog }
            });
            if (packageResponse.ResponseStatus != null) throw new Exception("Boot db failed");
            var paidPackageRes = container.Resolve<PackageRepository>().Add(new CreatePackage
                                                                                {
                                                                                    IsActive = true,
                                                                                    Ammount = 20,
                                                                                    Duration =
                                                                                        new TimeSpan(365, 0, 0, 0, 0),
                                                                                    Start = DateTime.Now.AddDays(-1),
                                                                                    Title =
                                                                                        new Dictionary<string, string>()
                                                                                            {{Culture.pt_PT, "Testa!"}},
                                                                                    Description =
                                                                                        new Dictionary<string, string>()
                                                                                            {
                                                                                                {
                                                                                                    Culture.pt_PT,
                                                                                                    ""
                                                                                                }
                                                                                            },
                                                                                    Features =
                                                                                        new[]
                                                                                            {
                                                                                                AhavFeature.Support,
                                                                                                AhavFeature.Newsletters,
                                                                                                AhavFeature.Ecommerce,
                                                                                                AhavFeature.Cms,
                                                                                                AhavFeature.Blog
                                                                                            }
                                                                                });
            var projectResponse = client.Post(new PostApplication
            {
                PackageId = packageResponse.Package.Id,
                Description = "This project contains Bambora, AdegasDoDao and my website all in localhost domain (for tests porpuses). Project Slug is kept at slug first segment. Ie: /bamboraonline.com/bio, /adegasdodao.pt/home",
                Name = "Tests",
                Domain = "localhost",
            });
            if (projectResponse.ResponseStatus != null) throw new Exception("Boot db failed");
            if (projectResponse.Project.Id.IsEmpty()) throw new Exception("Failed");
            client.SetProjectCredentials(projectResponse.Project.Id);

        }
    }
}
