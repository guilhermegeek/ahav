﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Ahav.Infrastructure;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceModel.DataDtos;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Enums;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.GridFS;

namespace Ahav.ServiceInterface.Infrasctructure
{
    public static class MongoGridFsExtensions
    {
        public static MediaType GetMediaType(this MongoGridFSFileInfo fileInfo)
        {
            if(fileInfo.ContentType == "video/mpeg" || fileInfo.ContentType == "video/mpeg2"  || fileInfo.ContentType == "video/mp4"|| fileInfo.ContentType == "video/quicktime" || fileInfo.ContentType =="video/x-msvideo" )
                return MediaType.Video;
            if (fileInfo.ContentType == "image/gif" || fileInfo.ContentType == "image/x-xbitmap" || fileInfo.ContentType == "image/x-xpixmap" || fileInfo.ContentType == "image/x-png" || fileInfo.ContentType == "image/jpeg" || fileInfo.ContentType == "image/png")
                return MediaType.Image;
            return MediaType.Other;
        }
        public static BsonValue[] GetContentType(MediaType mediaType)
        {
            switch (mediaType)
            {
                case MediaType.Video:
                    return new BsonValue[] { "video/mpeg", "video/mpeg2", "video/mp4", "video/quicktime", "video/x-msvideo" };
                case MediaType.Image:
                    return new BsonValue[] { "image/gif", "image/x-xbitmap", "image/x-xpixmap", "image/x-png", "image/jpeg", "image/png" };
                case MediaType.Other:
                    return new BsonValue[]
                               {
                                   "application/pdf", "application/rtf", "application/x-pdf", "application/msword",
                                   "application/macwriteii", "application/x-gtar", "application/x-tar", "application/zip"
                               };
                case MediaType.All:
                    return null;
            }
            return null;
        }
        public static MediaDto ToMediaDto(this MongoGridFSFileInfo fileInfo)
        {
            return new MediaDto
                       {
                           Id = (ObjectId) fileInfo.Id,
                           Name = fileInfo.Name,
                           MediaType = fileInfo.GetMediaType()
                       };
        }
    }
    public class MongoGridFs
    {
        private const string ColumnMetadataProject = "Project";
        

        public MongoGridFS Grid
        {
            get { return MongoDbHelper.Instance.Database.GridFS; }
        }
        
        public MongoGridFSFileInfo[] Find(ObjectId[] ids, MediaType mediaType = MediaType.All)
        {
            
            var values = new List<BsonValue>();
            for (int i = 0; i < ids.Length; i++)
            {
                values.Add(ids[i]);
            }
            var queries = new List<IMongoQuery>();
            if(values.Any())
                queries.Add(Query.In("_id", values));
            if (mediaType != MediaType.All)

                queries.Add(Query.In("contentType", MongoGridFsExtensions. GetContentType(mediaType).AsEnumerable()));
            if(!queries.Any())
                throw new Exception("Cannot query without find parameters");

            var files = Grid.Find(Query.And(queries)).ToList();
            return files.ToArray();
        }
        public ReadMediasResponse Find(ReadMedias request)
        {
            var response = new ReadMediasResponse();
            var values = new List<BsonValue>();
            if(request.Ids != null && request.Ids.Any())
            for (int i = 0; i < request.Ids.Length; i++)
            {
                values.Add(request.Ids[i]);
            }
            var queries = new List<IMongoQuery>();
            if (values.Any())
                queries.Add(Query.In("_id", values));
            if (request.MediaType.HasValue && request.MediaType.Value != MediaType.All)
                queries.Add(Query.In("contentType", MongoGridFsExtensions.GetContentType(request.MediaType.Value).AsEnumerable()));
            if(request.ProjectId.HasValue && !request.ProjectId.Value.IsEmpty())
            {
                queries.Add(Query.EQ(string.Format("metadata.{0}", ColumnMetadataProject), request.ProjectId.Value.ToString()));
            }
            if (!queries.Any())
                throw new Exception("Cannot query without find parameters");

            var files = Grid.Find(Query.And(queries)).ToList();
            response.Files = files;
            return response;
        }
        public MongoGridFSFileInfo[] GetByProject(ObjectId projectId, MediaType mediaType = MediaType.All)
        {
            var files = Grid.Find(Query.EQ(string.Format("metadata.{0}", ColumnMetadataProject), projectId.ToString()));
            return files.ToArray();
        }
        //public MongoGridFSFileInfo AddFile(Stream fileStream, string fileName, ObjectId projectId)
        //{
        //    var fileInfo = Grid.Upload(fileStream, fileName);
        //    return fileInfo;
        //}

        /// <summary>
        /// Add a new file
        /// Project should be passed (altought could be defined in options, it's a way to force devs to always append project in ahav medias)
        /// </summary>
        /// <param name="fileStream"></param>
        /// <param name="fileName"></param>
        /// <param name="projectId"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public MongoGridFSFileInfo AddFile(Stream fileStream, string fileName, ObjectId projectId, MongoGridFSCreateOptions options = null)
        {
            if (options == null) options = new MongoGridFSCreateOptions();
            if (options.Metadata == null)
                options.Metadata = new BsonDocument();

            options.Metadata[ColumnMetadataProject] = projectId.ToString();
            return Grid.Upload(fileStream, fileName, options);
        }
        public void UpdateThumbs(ObjectId fileId, Dictionary<int, ObjectId> uizeId)
        {
            
        }
        public void RemoveFile(ObjectId fileId)
        {
            Grid.DeleteById(fileId);
        }
        public bool FileExists(ObjectId fileId)
        {
            return Grid.ExistsById(fileId);
        }
        public MongoGridFSFileInfo GetFileInfo(ObjectId fileId)
        {
            return Grid.FindOneById(fileId);
        }
        /// <summary>
        /// Returns a stream for a file
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Stream GetFile(ObjectId id)
        {
            var file = Grid.FindOneById(id);
            return file.OpenRead();
        }
    }
}
