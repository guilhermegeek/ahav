﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Ahav.ServiceInterface.Infrasctructure
{
    public class RsaTokenProvider
    {
        public static RsaTokenProvider Instance
        {
            get
            {
                if (_instance == null) _instance = new RsaTokenProvider();
                return _instance;
            }
        }
        private static RsaTokenProvider _instance;
        private static Random random = new Random((int)DateTime.Now.Ticks);//thanks to McAden
        public string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }
        public string GenerateNew()
        {
            string token = Encript(DateTime.Now.ToString());
            return Encript(token);
        }
        
        public string Encript(string value)
        {
            var byteConverter = new UnicodeEncoding();
            byte[] dataToEncrypt = byteConverter.GetBytes(value);
            byte[] encryptedData;

            using (var RSA = new RSACryptoServiceProvider())
            {
                encryptedData = RSA.Encrypt(dataToEncrypt, false);
                return value;
            }
        }
        public string Decript(string value)
        {
            var byteConverter = new UnicodeEncoding();
            byte[] dataToDencrypt = byteConverter.GetBytes(value);
            byte[] encryptedData;

            using (var RSA = new RSACryptoServiceProvider())
            {
                encryptedData = RSA.Decrypt(dataToDencrypt, false);
                return value;
            }
        }
    }
}
