﻿using System;

namespace Ahav.ServiceInterface.Infrasctructure.Exceptions
{
    public class ApiErrorException : Exception
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public ApiErrorException(string errorCode, string errorMessage)
        {
            this.ErrorMessage = errorMessage;
            this.ErrorCode = errorCode;
        }
    }
}
