﻿using Ahav.ServiceModel.Dtos;

namespace Ahav.ServiceModel.Interfaces
{
    /// <summary>
    /// With this model, clients will render the advertise and return a read++
    /// </summary>
    public interface IHasAdvertiseModel
    {
        AdvertiseDto[] Advertisements { get; set; }
    }
}
