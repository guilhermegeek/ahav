﻿using System.Collections.Generic;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;

namespace Ahav.ServiceModel.Interfaces
{
    public interface IHasComments
    {
        /// <summary>
        /// Comments. I hope to refactor this to buckets or something else really soon!
        /// </summary>
        IList<NComment> Comments { get; set; }
        /// <summary>
        /// Comments counter
        /// </summary>
        long CommentsCount { get; set; }
    }
}
