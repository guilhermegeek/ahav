﻿using Ahav.ServiceModel.Common;

namespace Ahav.ServiceModel.Interfaces
{
    public interface IHasResponseModal
    {
        ResponseClient Modal { get; set; }
    }
}
