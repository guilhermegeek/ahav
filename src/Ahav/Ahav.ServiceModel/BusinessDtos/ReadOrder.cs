﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.BusinessDtos
{
    public class ReadOrder : IReturn<ReadOrderResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class ReadOrderResponse : IHasResponseStatus
    {
        public OrderDto Order { get; set; } 
        public ResponseStatus ResponseStatus { get; set; }
    }
}
