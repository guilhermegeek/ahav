﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.BusinessDtos
{
    /// <summary>
    /// Saves a new Application
    /// </summary>
    public class CreateApplication : IReturn<CreateApplicationResponse>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Domain { get; set; }
        public AhavFeature[] Features { get; set; }
        public ObjectId OwnerId { get; set; }
    }
    public class CreateApplicationResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public AhavProjectDto Project { get; set; }
    }
}
