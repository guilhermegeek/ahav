﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.BusinessDtos
{
    public class CreateCompany : IReturn<CreateCompanyResponse>
    {
        public string Name { get; set; }
        public ObjectId UserId { get; set; }
    }
    public class CreateCompanyResponse : IHasResponseStatus
    {
        public CompanyDto Company { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
