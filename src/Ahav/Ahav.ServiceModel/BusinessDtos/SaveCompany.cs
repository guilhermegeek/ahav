﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.BusinessDtos
{
    public class SaveCompany : IReturn<SaveCompanyResponse>
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
    }
    public class SaveCompanyResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
