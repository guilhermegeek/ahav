﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Interfaces;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.BusinessDtos
{
    public class CreateOrder
    {
        public ObjectId UserId { get; set; }
        public ObjectId CompanyId { get; set; }
        public OrderOffer[] Offers { get; set; }
        /// <summary>
        /// Initial status for created order
        /// </summary>
        public OrderStatus OrderStatus { get; set; }
        public DateTime PaymentDue { get; set; }
    }

    public class CreateOrderResponse : IHasResponseStatus
    {
        public OrderDto Order { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
