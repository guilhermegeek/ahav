﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.BusinessDtos
{
    public class CreateContent : IReturn<CreateContentResponse>
    {
        public ObjectId OwnerId { get; set; }
        public ObjectId AppId { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public ObjectId TemplateId { get; set; }
    }

    public class CreateContentResponse : IHasResponseStatus
    {
        public ContentDto Content { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}