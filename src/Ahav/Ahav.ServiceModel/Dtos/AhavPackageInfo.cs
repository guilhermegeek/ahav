﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Common;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Dtos
{
    /// <summary>
    /// DTO for Package details
    /// </summary>
    public class AhavPackageInfo
    {
        public ObjectId Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? End { get; set; }
        public AhavFeature[] Features { get; set; }
        public decimal Ammount { get; set; }
    }
}
