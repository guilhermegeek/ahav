﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ahav.ServiceModel.Dtos
{
    public enum NewsState
    {
        Draft = 0, Pending = 1, Published = 2, Canceled = 3
    }
}
