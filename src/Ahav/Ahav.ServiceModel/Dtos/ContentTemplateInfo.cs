﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Dtos
{
    public class ContentTemplateInfo
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public ObjectId ThumbnailSrc { get; set; }
    }
}
