﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Dtos
{
    public class ContentDto
    {
        public ContentDto()
        {
            this.Files = new List<ObjectId>();
        }

        public ObjectId Id { get; set; }
        public string Css { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string Description { get; set; }
        public string Javascript { get; set; }
        public IList<ObjectId> Files { get; set; }
        public string ThumbnailSrc { get; set; }

        public string Url
        {
            get { return this.Slug; }
        }

        public ObjectId TemplateId { get; set; }
        public string Thumbnail { get; set; }
        public ObjectId ProjectId { get; set; }
    }
}
