﻿using MongoDB.Bson;

namespace Ahav.ServiceModel.Dtos
{
    public class PlaceDto
    {
        public ObjectId Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Name { get; set; }
    }
}
