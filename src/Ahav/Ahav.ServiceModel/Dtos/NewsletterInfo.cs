﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Domain;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Dtos
{
    public class NewsletterInfo
    {
        public ObjectId Id { get; set; }
        public string Subject { get; set; }
        public string Name { get; set; }
        public string StateDisplay { get; set; }
        public NewsletterState State { get; set; }
        public string Url { get; set; }
    }
}
