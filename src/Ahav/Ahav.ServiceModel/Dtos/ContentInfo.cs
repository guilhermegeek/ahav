﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Dtos
{
    public class ContentInfo
    {
        public ObjectId Id { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
        public IList<ObjectId> Files { get; set; }
        public string ThumbnaillSrc { get; set; }
    }
}
