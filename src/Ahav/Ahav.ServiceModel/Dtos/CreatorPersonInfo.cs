﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Interfaces;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Dtos
{
    public class CreatorPersonInfo : ICreator
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public string ThumbnailUri { get; set; }
        public string Uri { get; set; }
    }
}
