﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Interfaces;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Dtos
{
    /// <summary>
    /// Dto used to mock queries on data
    /// </summary>
    public class CommentsBucketData : IHasCommentsBucket
    {
        public ObjectId Id { get; set; }
        public long CommentsCount { get; set; }
        public int CommentsPageCount { get; set; }
    }
}
