﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Common;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Dtos
{
    /// <summary>
    /// DTO for Project
    /// </summary>
    public class AhavProjectDto
    {
        public AhavProjectDto()
        {
            this.Attachments = new ObjectId[0];
        }
        public ObjectId[] Attachments { get; set; }
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public AhavFeature[] Features { get; set; }
        public ObjectId[] Packages { get; set; }
        public string Domain { get; set; }
        public AhavProjectState ProjectState { get; set; }
        public string AbsoluteUrl
        {
            get { return Domain; }
        }
    }
}
