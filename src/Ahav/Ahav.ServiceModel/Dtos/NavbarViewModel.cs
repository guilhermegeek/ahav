﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ahav.ServiceModel.Dtos
{
    /// <summary>
    /// DTO model used in Views
    /// </summary>
    public class NavbarPutViewModel
    {
        public string Culture { get; set; }
        public string Name { get; set; }
        public string Href { get; set; }
        public IList<NavbarPutViewModel> Menus { get; set; } 
    }
}
