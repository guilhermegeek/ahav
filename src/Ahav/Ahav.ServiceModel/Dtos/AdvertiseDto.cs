﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Dtos
{
    /// <summary>
    /// DTO returned by AdvertiseModel
    /// </summary>
    public class AdvertiseDto
    {
        /// <summary>
        /// Id of advertisement
        /// </summary>
        public ObjectId Id { get; set; }
        /// <summary>
        /// Html code, more likely a angular directive (for client side)
        /// </summary>
        public string HtmlRender { get; set; }
    }
}
