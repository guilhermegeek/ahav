﻿using Ahav.Interfaces;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Dtos
{
    public class CreatorOrganization : ICreator
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public string ThumbnailUri { get; set; }
        public string Uri { get; set; }
        public double[] Location { get; set; }
        public string Website { get; set; }
    }
}
