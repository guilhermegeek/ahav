﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Dtos
{
    public class AuthorInfo
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public string ImageSrc { get; set; }
    }
}
