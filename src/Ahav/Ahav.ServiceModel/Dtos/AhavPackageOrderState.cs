﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ahav.ServiceModel.Dtos
{
    public enum AhavPackageOrderState
    {
        /// <summary>
        /// Order was created
        /// </summary>
        Created = 0,
        /// <summary>
        /// User request the features. In case of free offer, the order is Confirmed. If not, awaits for the paymment to change the state to Confirmed
        /// </summary>
        Requested = 1,
        /// <summary>
        /// Order is confirmed and the features were accepeted
        /// </summary>
        Confirmed = 2,
        /// <summary>
        /// Features installed and ready to use
        /// </summary>
        Finalized = 3
    }
}
