﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Domain;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Dtos
{
    public class ContentTemplateDto
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string Less { get; set; }
        public string ThumbnailSrc { get; set; }
        public ObjectId Favicon { get; set; }
        public ObjectId[] Files { get; set; }
    }

}
