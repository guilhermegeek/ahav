﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Enums;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Dtos
{
    public class MediaDto
    {
        public ObjectId Id { get; set; }
        public string Url { get { return string.Format("/api/file/{0}", this.Id); } }
        public string ThumbnailUrl { get { return string.Format("/api/file/{0}", this.Id); } }
        public string ApiUri { get { return string.Format("/api/file/{0}", this.Id); } }
        public MediaType MediaType { get; set; }
        public string Extension { get; set; }
        public string Name { get; set; }
    }
}
