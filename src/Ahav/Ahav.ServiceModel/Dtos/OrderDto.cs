﻿using System;
using Ahav.Interfaces;
using Ahav.ServiceModel.Domain;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Dtos
{
    public class OrderDto
    {
        public ObjectId Id { get; set; }
        public OrderOffer[] Offers { get; set; }
        public ICreator Creator { get; set; }
        /// <summary>
        /// The current status of the order.
        /// </summary>
        public OrderStatus Status { get; set; }
        /// <summary>
        /// The date that payment is due.
        /// </summary>
        public DateTime PaymentDue { get; set; }
        /// <summary>
        /// The name of the credit card or other method of payment for the order.
        /// </summary>
        public PaymentMethod PaymentMethod { get; set; }
    }
}
