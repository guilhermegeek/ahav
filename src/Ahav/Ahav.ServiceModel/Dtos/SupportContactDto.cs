﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Domain;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Dtos
{
    public class SupportContactDto
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public SupportContactState State { get; set; }
    }
}
