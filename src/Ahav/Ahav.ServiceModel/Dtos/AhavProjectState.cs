﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ahav.ServiceModel.Dtos
{
    public enum AhavProjectState
    {
        // Project was created, is waiting for approval, payment, creation, etc. It's not available to owner
        Waiting = 0,
        Enabled = 1,
        Expired = 2,
        Suspended = 3
    }
}
