﻿namespace Ahav.ServiceModel.Common
{
    public static class Culture
    {
        public const string pt_PT = "pt-PT";
        public const string en_US = "en-US";
    }
}
