﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ahav.ServiceModel.Common
{
    public class WebLayouts
    {
        public const string LayoutAdmin = "_LayoutAdmin";
        public const string LayoutCity = "_LayoutCity";
        public const string LayoutRoom = "_LayoutRoom";
    }
}
