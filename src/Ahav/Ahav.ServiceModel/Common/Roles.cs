﻿namespace Ahav.ServiceModel.Common
{
    public static class CommonRoles
    {
        public const string Administrator = "admin";
        public const string User = "user";
    }
}
