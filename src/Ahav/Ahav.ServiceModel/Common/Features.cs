﻿namespace Ahav.ServiceModel.Common
{
    public enum AhavFeature
    {
        Cms = 0,
        Ecommerce = 1,
        Newsletters = 2,
        Blog = 3,
        Support = 4
    }
}
