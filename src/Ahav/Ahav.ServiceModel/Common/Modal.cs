﻿namespace Ahav.ServiceModel.Common
{
    public class ModalButton
    {
        public string Name { get; set; }
        public string Action { get; set; }
    }
    public enum ModalType
    {
        Normal = 1
    }
}
