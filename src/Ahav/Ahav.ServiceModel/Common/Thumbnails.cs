﻿using MongoDB.Bson;
using ServiceStack.ServiceHost;

namespace Ahav.ServiceModel.Common
{
    public class GetThumbs : IReturn<Thumbnails>
    {
        public ObjectId Id { get; set; }
    }
    public class Thumbnails
    {
        public ObjectId? Parent { get; set; }
        public Thumb[] Thumbs { get; set; }
    }
    public interface IThumb
    {

    }
    public enum ThumbType
    {
        Folder = 1,
        Image = 2
    }
    public class Thumb : IThumb
    {
        public ThumbType ThumbType { get; set; }
        public string Icon { get; set; }
        public string Uri { get; set; }
    }
    public class ThumbFolder : IThumb
    {
        public string Icon { get; set; }
    }
}
