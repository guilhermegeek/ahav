﻿using MongoDB.Bson;

namespace Ahav.ServiceModel.Common
{
    public static class RedisKeys
    {  
        public static string Auth(ObjectId id)
        {
            return string.Format("auth:{0}", id.ToString());
        }
        public static string TokenAccess(ObjectId id)
        {
            return string.Format("token:{0}", id);
        }
        public static string TokenConfirmation(ObjectId userId)
        {
            return string.Format("token:c:{0}", userId);
        }
        public static string Access(ObjectId userId)
        {
            return string.Format("user::{0}::access", userId);
        }
        public static string Roles(ObjectId userId)
        {
            return string.Format("user::{0}::roles", userId);
        }
        public static string NewsComment(ObjectId newsId)
        {
            return string.Format("news::{0}::comments", newsId);
        }
    }
}
