﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ahav.ServiceModel.Common
{
    /// <summary>
    /// Units of measurement are used for pricing, shipping, etc
    /// </summary>
    /// Ideally we should implement UN/CEFACT common codes:
    /// http://www.heppnetz.de/projects/goodrelations/primer/#6_ANNEX:_POPULAR_UN.2FCEFACT_COMMON_CODES
    /// I'll implement them as i need
    public enum UnitOfMeasurement
    {
        /// <summary>
        /// KGM
        /// </summary>
        Kilogram,
        /// <summary>
        /// CMT
        /// </summary>
        Centimeter,
        /// <summary>
        /// CMQ
        /// </summary>
        CentimeterCubic,
        /// <summary>
        /// MTR
        /// </summary>
        Meter,
    }
}
