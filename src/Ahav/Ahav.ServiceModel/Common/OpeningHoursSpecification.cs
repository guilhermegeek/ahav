﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ahav.ServiceModel.Common
{
    /// <summary>
    /// A structured value providing information about the opening hours of a place or a certain service inside a place.
    /// </summary>
    /// http://schema.org/OpeningHoursSpecification
    public class OpeningHoursSpecification
    {
        /// <summary>
        /// The closing hour of the place or service on the given day(s) of the week.
        /// </summary>
        public DateTime Opens { get; set; }
        /// <summary>
        /// The closing hour of the place or service on the given day(s) of the week.
        /// </summary>
        public DateTime Closes { get; set; }
        /// <summary>
        /// The day of the week for which these opening hours are valid.
        /// </summary>
        public DayOfWeek DayOfWeek { get; set; }

    }
}
