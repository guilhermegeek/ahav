﻿using System.Runtime.Remoting.Messaging;

namespace Ahav.ServiceModel.Common
{
    public static class AhavRoute
    {
        public const string FacebookTabRequest = "/facebook";
        public const string Reset = "/api/reset";
        public const string AdminConnect = "/api/admin/connect";
        public const string AhavConnect = "/api/ahav/connect";

        // Register
        public const string PostRegisterBasic = "/api/register";
        public const string PostConfirmAccount = "/api/confirm";

        // Media
        public const string GetMedia = "/api/file/{FileId}";
        public const string PostMedia = "/api/file";

        // Template
        public const string PostTemplate = "/api/template";
        public const string GetTemplates = "/api/template";
        public const string GetTemplate = "/api/template/{TemplateId}";
        public const string PutTemplate = "/api/template/{TemplateId}";
        public const string DeleteTemplate = "/api/template/{TemplateId}";
        public const string PutTemplatePublic = "/api/template/{TemplateId}/public";
        public const string PostTemplateFile = "/api/template/{TemplateId}/file/";
        public const string DeleteTemplateFile = "/api/template/{TemplateId}/file/{FileId}";
        public const string GetTemplateFiles = "/api/template/{TemplateId}/file/";

        // Content
        public const string PostContentFile = "/api/content/{ContentId}/file/";
        public const string DeleteContentFile = "/api/content/{ContentId}/file/{FileId}";
        public const string GetContentFiles = "/api/content/{ContentId}/file/";
        public const string PostContent = "/api/content/";
        public const string GetContent = "/api/content/{ContentId}/";
        public const string DeleteContent = "/api/content/{ContentId}";
        public const string PutContent = "/api/content/{ContentId}";
        public const string PostValidateContentSlug = "/api/content/slug";
        public const string PutContentDefault = "/api/content/default";
        public const string GetContentDefault = "/api/content/default";

        // Application
        public const string PostProject = "/api/project/";
        public const string PutProject = "/api/project/{Id}";
        public const string GetProjects = "/api/project/";
        public const string GetProject = "/api/project/{Id}";
        public const string PostAhavPackage = "/api/package/";
        public const string GetAhavPackagesAvailable = "/api/package/available";
        public const string GetProjectInitModal = "/api/project/init-modal";
        public const string PostProjectInitModal = "/api/project/init-modal";
        public const string DeleteProjectInitModal = "/api/project/init-video";
        public const string GetAttachments = "/api/attachment";
        public const string PostAttachment = "/api/attachment";
        public const string DeleteApplication = "/api/app/{Id}";

        // SpotEvent
        public const string GetEvent = "/api/event/";
        public const string PostEvent = "/api/event";
        public const string PutEvent = "/api/event/{Id}";
        public const string DeleteEvent = "/api/event/{Id}";

        // Support
        public const string GetSupportContact = "/api/contact/{Id}";
        public const string PostSupportContact = "/api/contact/";

        // Blog
       

        // Newsletter
        public const string GetNewsletter = "/api/newsletter/{Id}";
        public const string GetNewsletters = "/api/newsletter";
        public const string PostNewsletter = "/api/newsletter";
        public const string PutNewsletter = "/api/newsletter/{Id}";
        public const string PostNewsletterFiles = "/api/newsletter/{Id}/file";
        public const string DeleteNewsletterFile = "/api/newsletter/{NewsletterId}/file/{FileId}";
    }
}
