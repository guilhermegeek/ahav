﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ahav.ServiceModel.Common
{
    public class ResponseClient
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public bool Backdrop { get; set; }
        public ModalType Type { get; set; }
        public ModalButton[] Buttons { get; set; }
    }
}
