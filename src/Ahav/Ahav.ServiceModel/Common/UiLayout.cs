﻿using System;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Common
{
    public class Slideshow
    {
        public TimeSpan Interval { get; set; }
        public ObjectId[] Media { get; set; }
        //public bool Fluid { get; set; }
        //public bool Fixed { get; set; }
        //public Position Position { get; set; }
        public bool AutoPlay { get; set; }
        public bool Replay { get; set; }
        public bool HideControlls { get; set; }
        // @TODO public bool PauseOnHover { get; set; }
    }
    public enum Position
    {
        Top, Right, Bottom, Left, Middle
    }
    public class Gslide
    {
        public string[] Images { get; set; }
    }
}
