﻿using System.Collections.Generic;
using Ahav.Interfaces;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Enums;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using MongoDB.Driver;
using ServiceStack;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;


namespace Ahav.ServiceModel.Operations
{
    public class GetMediaRaw : IReturn<GetMediaRawResponse>
    {
        public ObjectId FileId { get; set; }
        public bool AsAttachment { get; set; }
    }
    public class GetMediaRawResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class GetMedia : IReturn<GetMediaResponse>
    {
        public ObjectId FileId { get; set; }
    }
    public class GetMediaResponse : IHasResponseStatus
    {
        public MediaDto File { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class PostMedia : IReturn<PostMediaResponse>
    {
        public PostMedia()
        {
            this.Files=new List<IFile>();
            this.Thumbnaill = true;
        }
        public MediaObjType MediaType { get; set; }
        public IList<IFile> Files { get; set; }
        public bool Thumbnaill { get; set; }
        /// <summary>
        /// Resize width
        /// </summary>
        public int[] ThumbnaillResizeWidth { get; set; }
        /// <summary>
        /// Folder path inside Files Upload Directory
        /// If empty, it's saved to the root parent 
        /// </summary>
        
    }
    public class PostMediaResponse : IHasResponseStatus
    {
        public PostMediaResponse()
        {
            this.Medias=new List<MediaDto>();
            this.Files = new List<string>();
        }
        public IList<MediaDto> Medias { get; set; }
        public IList<string> Files { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class GetMedias : IReturn<GetMediasResponse>, IPagedResult
    {
        
        public ObjectId[] Ids { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
        public IMongoSortBy SortBy { get; set; }
        public MediaType? MediaType { get; set; }
        public ObjectId? OwnerId { get; set; }
        public ObjectId? ProjectId { get; set; }
    }
    public class GetMediasResponse : IHasResponseStatus
    {
        public GetMediasResponse()
        {
            Medias=new List<MediaDto>();
            this.Files=new List<string>();
        }
        public IList<MediaDto> Medias { set; get; }
        public IList<string> Files { get; set; } 
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class DeleteMedia : IReturn<DeleteMediaResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class DeleteMediaResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
   
}
