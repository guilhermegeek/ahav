﻿


using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.Operations
{
    public class ChangeCulture : IReturn<ChangecultureResponse>
    {
        /// <summary>
        /// If you change the name, update the route
        /// </summary>
        public string Culture { get; set; }
    }
    public class ChangecultureResponse
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
