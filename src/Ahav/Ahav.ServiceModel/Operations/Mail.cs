﻿using System.Collections.Generic;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;


namespace Ahav.ServiceModel.Operations
{
    public class InitMailService : IReturn<InitMailServiceResponse>
    {
        
    }

    public class SendQueueEmails : IReturn<SendQueueEmailsResponse>
    {
        public ObjectId EmailId { get; set; }
    }

    public class SendQueueEmailsResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
    public class InitMailServiceResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class AddEmail : IReturn<AddEmailResponse>
    {
        public Dictionary<string, string> Subject { get; set; }
        public Dictionary<string, string> Content { get; set; }
        public bool IsHtml { get; set; }
    }
    public class AddEmailResponse
    {
        public MailDto Result { get; set; }
    }

    public class AddEmailQueue : IReturn<AddEmailQueueResponse>
    {
        public ObjectId EmailId { get; set; }
        public IList<MailQueue> Queues { get; set; }
    }
    public class AddEmailQueueResponse
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class AddEmailInternalQueue : IReturn<AddEmailInternalQueueResponse>
    {
        
    }
    public class AddEmailInternalQueueResponse
    {
        
    }
    public class SendEmailI : IReturn<SendEmailIHttpResponse>
    {
        public bool IsHtml { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string[] Recipients { get; set; }
    }
    public class SendEmailIHttpResponse
    {
        public SendEmailIHttpResponse()
        {
            this.EmailsNotSented=new    List<string>();
        }
        public List<string> EmailsNotSented { get; set; }
    }
    public class CancelEmail : IReturn<CancelEmailResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class CancelEmailResponse
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class DeleteEmail : IReturn<DeleteEmailResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class DeleteEmailResponse
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class AddEmailRecipient : IReturn<AddEmailRecipientResponse>
    {
        /// <summary>
        /// Email Id
        /// </summary>
        public ObjectId Id { get; set; }
        public string[] Emails { get; set; }
    }
    public class AddEmailRecipientResponse
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class DeleteEmailRecipient : IReturn<DeleteEmailRecipientResponse>
    {
        public string Email { get; set; }
    }
    public class DeleteEmailRecipientResponse
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
