﻿using System;
using System.Collections.Generic;
using System.Text;
using Ahav.Interfaces;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using MongoDB.Driver;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;


namespace Ahav.ServiceModel.Operations
{
    // DATA
    public class FindTemplate : IReturn<FindTemplateResponse>, IPagedResult
    {
        public int Skip { get; set; }
        public int Take { get; set; }
    }
    public class FindTemplateResponse : IHasResponseStatus
    {
        public FindTemplateResponse()
        {
            this.Template=new List<ContentTemplateInfo>();
        }
        public IList<ContentTemplateInfo> Template { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
    // API
    
    
    
    
    
    
    public class GetTemplatesAvailable : IReturn<GetTemplatesAvailableResponse>, IPagedResult
    {
        public int Skip { get; set; }
        public int Take { get; set; }
    }
    public class GetTemplatesAvailableResponse : IHasResponseStatus
    {
        public IList<ContentTemplateInfo> Templates { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class PostTemplateFile : IReturn<PostTemplateFileResponse>
    {
        public ObjectId TemplateId { get; set; }
    }
    public class PostTemplateFileResponse : IHasResponseStatus, IHasResponseModal
    {
        public PostTemplateFileResponse()
        {
            this.Medias = new List<MediaDto>();
        }
        public IList<MediaDto> Medias { get; set; }
        public string File { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }

    public class GetTemplateFiles : IReturn<GetTemplateFilesResponse>, IPagedResult
    {
        public ObjectId TemplateId { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
    }
    public class GetTemplateFilesResponse : IHasResponseStatus
    {
        public GetTemplateFilesResponse()
        {
            this.Files = new List<string>();
            this.Medias = new List<MediaDto>();
        }
        public IList<string> Files { get; set; }
        public IList<MediaDto> Medias { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class DeleteTemplateFile : IReturn<DeleteTemplateFileResponse>
    {
        public ObjectId TemplateId { get; set; }
        public ObjectId FileId { get; set; }
    }
    public class DeleteTemplateFileResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
