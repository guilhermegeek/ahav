﻿using System.Collections.Generic;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;


namespace Ahav.ServiceModel.Operations
{
    // dtos
    public class SupportDashboard
    {

    }
    
    public class GetSupportDashboard : IReturn<GetSupportDashboardResponse>
    {

    }
    public class GetSupportDashboardResponse
    {
        public SupportDashboard Result { get; set; }
    }
    public class SupportTicket
    {
        public SupportTicket()
        {
            Comments = new List<NComment>();
        }
        public ObjectId Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public SupportTicketPriority Priority { get; set; }
        public SupportTicketState State { get; set; }
        // NComments
        public IList<NComment> Comments { get; set; }
        public long CommentsCount { get; set; }
    }

    public class SupportTicketDto
    {
        public ObjectId Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public SupportTicketPriority Priority { get; set; }
        public SupportTicketState State { get; set; }
    }

    public class SupportTicketInfo
    {
        public ObjectId Id { get; set; }
        public string Title { get; set; }
        public NComment LastComment { get; set; }
        public SupportTicketPriority Priority { get; set; }
        public SupportTicketState State { get; set; }
    }

    public enum SupportTicketPriority
    {
        Normal = 0
    }
    public enum SupportTicketState
    {
        Opened = 1, Closed = 2, Created = 3,
    }
    public class PostSupportTicket : IReturn<PostSupportTicketResponse>
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public SupportTicketPriority Priority { get; set; }
    }
    
    public class PostSupportTicketResponse
    {
        public SupportTicketDto Result { get; set; }
    }
    public class GetSupportTicket : IReturn<GetSupportTicketResponse>
    {
        public ObjectId Id { get; set; }
        public bool GetComments { get; set; }
    }
  
    public class GetSupportTicketResponse
    {
        public SupportTicketDto Result { get; set; }
        public NComments Comments { get; set; }
    }
   
    public class EditSupportTicket : IReturn<EditSupportTicketResponse>
    {
        public ObjectId Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public SupportTicketPriority Priority { get; set; }
    }
    public class EditSupportTicketResponse
    {

    }
    public class PostSupportTicketComment : AddComment, IReturn<AddSupportTicketCommentResponse>
    {
      
    }
    public class AddSupportTicketCommentResponse : AddCommentResponse
    {
        
    }
    public class GetSupportTicketComments : GetComments, IReturn<GetSupportTicketCommentsResponse>
    {
     
    }
    public class GetSupportTicketCommentsResponse : GetCommentsResponse
    {
       
    }
    public class DeleteSupportTicketComments : IReturn<DeleteSupportTicketCommentsResponse>
    {
        public ObjectId TicketId { get; set; }
        public long CommentId { get; set; }
    }
    public class DeleteSupportTicketCommentsResponse
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class EditSupportTicketState : IReturn<EditSupportTicketState>
    {
        public ObjectId Id { get; set; }
        public SupportTicketState State { get; set; }
    }
    public class EditSupportTicketStateResponse
    {

    }

}
