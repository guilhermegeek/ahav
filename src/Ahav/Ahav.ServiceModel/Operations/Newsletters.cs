﻿using System.Collections.Generic;
using Ahav.Interfaces;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;


namespace Ahav.ServiceModel.Operations
{
    public class GetNewsletter : IReturn<GetNewsletterResponse>
    {
        public ObjectId Id { get; set; }
    }

    public class GetNewsletterResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public NewsletterDto Newsletter { get; set; }
    }

    public class GetNewsletters : IReturn<GetNewslettersResponse>, IPagedResult
    {
        public ObjectId[] Ids { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
    }

    public class GetNewslettersResponse : IHasResponseStatus
    {
        public GetNewslettersResponse()
        {
            this.Newsletters = new List<NewsletterInfo>();
        }

        public IList<NewsletterInfo> Newsletters { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }

    public class PostNewsletter : IReturn<PostNewsletterResponse>
    {
        public string Name { get; set; }
    }

    public class PostNewsletterResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public Newsletter Result { get; set; }
        public ResponseClient Modal { get; set; }
    }

    public class PutNewsletter : IReturn<PutNewsletterResponse>
    {
        public ObjectId Id { get; set; }
        public Dictionary<string, string> Subject { get; set; }
        public Dictionary<string, string> Body { get; set; }
        public bool IsHtml { get; set; }
        public string Name { get; set; }
    }

    public class PutNewsletterResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }

    /// <summary>
    /// Opt Out - Unsubscribe from a Newsletter. Doesn't need any form or user action
    /// </summary>
    public class AnyOptou : IReturn<AnyOptOutResponse>
    {

    }

    public class AnyOptOutResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }

    public class PostNewsletterFiles : IReturn<PostNewsletterFilesResponse>
    {
        public ObjectId Id { get; set; }
    }

    public class PostNewsletterFilesResponse : IHasResponseStatus, IHasResponseModal
    {
        public PostNewsletterFilesResponse()
        {
            this.Files = new List<MediaDto>();
        }

        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
        public IList<MediaDto> Files { get; set; }
    }
    public class DeleteNewsletterFile : IReturn<DeleteNewsletterFileResponse>
    {
        public ObjectId NewsletterId { get; set; }
        public ObjectId FileId { get; set; }
    }
    public class DeleteNewsletterFileResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
