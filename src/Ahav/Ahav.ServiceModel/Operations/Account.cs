﻿using System;
using System.Collections.Generic;
using Ahav.ServiceModel.ApiDtos;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;


namespace Ahav.ServiceModel.Operations
{
    public class PostAhavAccount : IReturn<PostAhavAccountResponse>
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public bool Singular { get; set; }
        /// <summary>
        /// Indicates wether a new user should be created with the input information
        /// If the user already exist, it return a warning, not an error
        /// </summary>
        public bool RegisterUser { get; set; }
        
    }
    public class PostAhavAccountResponse : IHasResponseStatus, IHasResponseModal
    {
        public ObjectId AccountId { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
    /// <summary>
    /// Registration
    /// </summary>
    public class PostUser : IReturn<PostUserResponse>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public bool AutoConfirmEmail { get; set; }
    }
    public class PostUserResponse : IHasResponseStatus
    {
        public ObjectId UserId { get; set; }
        /// <summary>
        /// @todo REMOVE THIS FROM HERE OTHERWISE REQUESTS FROM REGISTER WILL RECEIVE CONFIRMATION TOKEN
        /// @todo THAT MEANS THEY CAN CONFIRM ANY EMAIL ACCOUNT
        /// </summary>
        public string ConfirmationToken { get; set; }
        public string ReferrerUrl { get; set; }
        public string Password { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
    
    
    public class GetProfilePicture : IReturn<GetProfilePictureResponse>
    {
        
    }
    public class GetProfilePictureResponse : IHasResponseStatus
    {
        //public FolderResult Directory { get; set; }
       //public FileResult File { get; set; }

        /// <summary>
        /// Gets or sets the ResponseStatus. The built-in IoC used with ServiceStack autowires this property.
        /// </summary>		 
        public ResponseStatus ResponseStatus { get; set; }
    }


   
    //public enum TokenType
    //{
    //    ConfirmAccount = 1,
    //    Access = 2,
    //    ChangePrimaryEmail = 3
    //}
    //public class Token : IReturn<TokenResponse>
    //{
    //    public TokenType TokenType { get; set; }
    //    public string Value { get; set; }
    //}

    //public class TokenResponse : IHasResponseStatus
    //{
    //    public string Result { get; set; }
    //    public ResponseStatus ResponseStatus { get; set; }
    //}
    public class PostAccountPassword : IReturn<PostAccountPasswordResponse>
    {
        public string ActualPassword { get; set; }
        public string Password { get; set; }
        public string PasswordConfirm { get; set; }
    }
    public class PostAccountPasswordResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class BasicInfo : IReturn<BasicInfoResponse> 
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        
    }
    public class BasicInfoResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public BasicInfo Result { get; set; }
    }
    public class Contact : IReturn<ContactResponse>
    {
        public IList<string> Mobiles { get; set; }
        public IList<string> Websites { get; set; }
        public IList<string> PublicEmail { get; set; }
    }

    public class ContactResponse
    {
        public IList<string> Mobiles { get; set; }
        public IList<string> Websites { get; set; }
        public IList<string> PublicEmail { get; set; }
    }


    

    public class PostRecoverPassword : IReturn<RecoverPasswordResponse>
    {
        public string Email { get; set; }
    }
    public class RecoverPasswordResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class GetConfirmRecoverPassword : IReturn<GetConfirmRecoverPasswordResponse>
    {
        public string Email { get; set; }
        public string Token { get; set; }
    }
    public class GetConfirmRecoverPasswordResponse
    {
        public AuthApi Auth { get; set; }
        public string NewPassword { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class GetAuth : IReturn<GetAuthResponse>
    {
        public ObjectId UserId { get; set; }
    }
    public class GetAuthResponse
    {
        public AhavAuth Result { get; set; }
    }
    public class CreateAuth : IReturn<CreateAuthResponse>
    {
        public ObjectId UserId { get; set; }
        public string[] Permissions { get; set; }
        public string[] Roles { get; set; }
    }
    public class CreateAuthResponse
    {
        public AhavAuth Result { get; set; }
    }
    public class AAuthenticate : IReturn<AAuthenticateResponse>
    {
        public ObjectId UserId { get; set; }
        public string Token { get; set; }
    }
    public class AAuthenticateResponse
    {
        public AhavAuth Result { get; set; }
        public bool IsAuthenticated { get; set; }
    }
    public class AuthToken : IReturn<AuthTokenResponse>
    {
        public ObjectId UserId { get; set; }
        public string Token { get; set; }
    }
    public class AuthTokenResponse
    {
        public AhavAuth Result { get; set; }
        public bool IsAuthenticated { get; set; }
    }
}
