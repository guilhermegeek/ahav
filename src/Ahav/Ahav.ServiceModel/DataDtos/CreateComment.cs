﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.DataDtos
{
    public class CreateComment : IReturn<CreateCommentResponse>
    {
        public ObjectId DocId { get; set; }
        public string Message { get; set; }
        public AuthorInfo User { get; set; }
    }
    public class CreateCommentResponse : IHasResponseStatus
    {
        public NComment Comment { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        
    }
}
