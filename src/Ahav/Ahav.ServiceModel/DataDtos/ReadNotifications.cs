﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Interfaces;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.DataDtos
{
    public class ReadNotifications : IPagedResult, IReturn<ReadNotificationsResponse>
    {
        public ObjectId UserId { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
    }
    public class ReadNotificationsResponse : IHasResponseStatus
    {
        public IList<Notification> Results { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
