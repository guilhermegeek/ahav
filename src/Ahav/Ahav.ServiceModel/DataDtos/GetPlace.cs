﻿using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.DataDtos
{
    public class GetPlace : IReturn<GetPlaceResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class GetPlaceResponse : IHasResponseStatus
    {
        public PlaceDto Place { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
