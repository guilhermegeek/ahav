﻿using System;
using System.Collections.Generic;
using System.Text;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;


namespace Ahav.ServiceModel.DataDtos
{
    public class CreateSupportContact : IReturn<AddSupportContactResponse>
    {
        public string Name { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
    }

    public class AddSupportContactResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public SupportContactDto Contact { get; set; }
    }
    public class ReadSupportContact : IReturn<ReadSupportContactResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class ReadSupportContactResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public SupportContactDto Contact { get; set; }
    }
}
