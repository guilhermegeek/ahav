﻿using System;
using System.Collections.Generic;
using System.Text;
using Ahav.ServiceModel.Domain;
using MongoDB.Bson;

namespace Ahav.ServiceModel.DataDtos
{
    public class UpdateContentTemplate
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Body { get; set; }
        public string Less { get; set; }
        public string[] BackStretchImages { get; set; }
    }
}
