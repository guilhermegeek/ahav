﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.DataDtos
{
    public class ReadComment : IReturn<ReadCommentResponse>
    {
        public object DocId { get; set; }
    }
    public class ReadCommentResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public IList<NComment> Results { get; set; }
    }
}
