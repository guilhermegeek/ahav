﻿using System;
using System.Collections.Generic;
using System.Text;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Enums;
using MongoDB.Bson;
using MongoDB.Driver.GridFS;
using ServiceStack.ServiceInterface.ServiceModel;


namespace Ahav.ServiceModel.DataDtos
{
    public class ReadMedias
    {
        public ObjectId[] Ids { get; set; }
        public MediaType? MediaType { get; set; }
        public ObjectId? ProjectId { get; set; }
    }
    public class ReadMediasResponse : IHasResponseStatus
    {
        public ReadMediasResponse()
        {
            this.Files = new List<MongoGridFSFileInfo>();
        }
        public IList<MongoGridFSFileInfo> Files { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
