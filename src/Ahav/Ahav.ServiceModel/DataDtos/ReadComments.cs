﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Interfaces;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.DataDtos
{
    public class ReadComments : IPagedResult
    {
        public ObjectId DocumentId { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
    }
    public class ReadCommentsResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
