﻿using System.Collections.Generic;
using Ahav.Interfaces;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Driver;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.DataDtos
{
    public class QueryContent : IReturn<QueryContentResponse>, IPagedResult
    {
        public QueryContent()
        {
            this.Queries = new List<IMongoQuery>();
        }
        public IList<IMongoQuery> Queries { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
    }
    public class QueryContentResponse : IHasResponseStatus
    {
        public IList<ContentInfo> Content { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
