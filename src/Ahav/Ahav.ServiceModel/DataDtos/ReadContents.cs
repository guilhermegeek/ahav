﻿using System;
using System.Collections.Generic;
using System.Text;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;


namespace Ahav.ServiceModel.DataDtos
{
    public class ReadContents : IReturn<ReadContentsResponse>
    {
        public ReadContents()
        {
            this.Take = 20;
        }
        public ObjectId[] Ids { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
    }
    public class ReadContentsResponse : IHasResponseStatus
    {
        public ReadContentsResponse()
        {
            this.Contents=new List<ContentInfo>();
        }
        public IList<ContentInfo> Contents { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
