﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Dtos;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.DataDtos
{
    public class ReadPackages : IReturn<ReadPackagesResponse>
    {
        /// <summary>
        /// If true, get only free packages
        /// </summary>
        public bool FreeOnly { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool Unactive { get; set; }
    }
    public class ReadPackagesResponse : IHasResponseStatus
    {
        public ReadPackagesResponse()
        {
            Packages=new List<AhavPackageInfo>();
        }

        public IList<AhavPackageInfo> Packages { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
