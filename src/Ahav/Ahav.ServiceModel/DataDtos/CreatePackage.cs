﻿using System;
using System.Collections.Generic;
using System.Text;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Interfaces;
using ServiceStack;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;


namespace Ahav.ServiceModel.DataDtos
{
    public class CreatePackage : IReturn<CreatePackageResponse>
    {
        public TimeSpan Duration { get; set; }
        public decimal Ammount { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Description { get; set; }
        public AhavFeature[] Features { get; set; }
        public bool IsActive { get; set; }
        public DateTime Start { get; set; }
        public DateTime? End { get; set; }
        public Dictionary<long, decimal> DaysPrice { get; set; }
    }
    public class CreatePackageResponse : IHasResponseStatus
    {
        public AhavPackage Package { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
