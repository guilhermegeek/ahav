﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18408
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ahav.ServiceModel.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class ContentResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ContentResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Ahav.ServiceModel.Resources.ContentResources", typeof(ContentResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The page was removed.
        /// </summary>
        public static string ContentRemovedOk {
            get {
                return ResourceManager.GetString("ContentRemovedOk", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your template was sucefully removed. 
        ///We migrated {0} contents using this template to the template you requested..
        /// </summary>
        public static string DeleteTemplateSuccessMigrations {
            get {
                return ResourceManager.GetString("DeleteTemplateSuccessMigrations", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your template was sucefully removed..
        /// </summary>
        public static string DeleteTemplateSuccessNoMigrations {
            get {
                return ResourceManager.GetString("DeleteTemplateSuccessNoMigrations", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The file was sucefully delete..
        /// </summary>
        public static string FileDeleteSucefull {
            get {
                return ResourceManager.GetString("FileDeleteSucefull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File uploaded.
        /// </summary>
        public static string FilePostSucefull {
            get {
                return ResourceManager.GetString("FilePostSucefull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your page was sucefully created.
        /// </summary>
        public static string PostSucefully {
            get {
                return ResourceManager.GetString("PostSucefully", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You&apos;ve created a new template! You can now create new content depending on him..
        /// </summary>
        public static string PostTemplateSuccess {
            get {
                return ResourceManager.GetString("PostTemplateSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your page was sucefully updated..
        /// </summary>
        public static string PutSuccefully {
            get {
                return ResourceManager.GetString("PutSuccefully", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Template was sucefully updated.
        /// </summary>
        public static string PutTemplateSuccess {
            get {
                return ResourceManager.GetString("PutTemplateSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You&apos;ve changed the default page..
        /// </summary>
        public static string SetContentDefaultOk {
            get {
                return ResourceManager.GetString("SetContentDefaultOk", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry but the URL you&apos;ve choosen is already taken by another page!.
        /// </summary>
        public static string SlugNotAvailable {
            get {
                return ResourceManager.GetString("SlugNotAvailable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Template is now public!.
        /// </summary>
        public static string TemplatePublicSuccess {
            get {
                return ResourceManager.GetString("TemplatePublicSuccess", resourceCulture);
            }
        }
    }
}
