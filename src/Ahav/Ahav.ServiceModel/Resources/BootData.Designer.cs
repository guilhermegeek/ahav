﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18408
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ahav.ServiceModel.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class BootData {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal BootData() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Ahav.ServiceModel.Resources.BootData", typeof(BootData).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;div class=&quot;container&quot;&gt;
        ///
        ///        &lt;div class=&quot;row&quot;&gt;
        ///
        ///            &lt;div class=&quot;col-lg-12&quot;&gt;
        ///                &lt;h1 class=&quot;page-header&quot;&gt;About
        ///                    &lt;small&gt;It&apos;s Nice to Meet You!&lt;/small&gt;
        ///                &lt;/h1&gt;
        ///                &lt;ol class=&quot;breadcrumb&quot;&gt;
        ///                    &lt;li&gt;&lt;a href=&quot;index.html&quot;&gt;Home&lt;/a&gt;
        ///                    &lt;/li&gt;
        ///                    &lt;li class=&quot;active&quot;&gt;About&lt;/li&gt;
        ///                &lt;/ol&gt;
        ///            &lt;/div&gt;
        ///
        ///        &lt;/div&gt;
        ///
        ///        &lt;div class=&quot;row&quot;&gt;
        ///
        ///            &lt;div class=&quot; [rest of string was truncated]&quot;;.
        /// </summary>
        public static string ContentAbout {
            get {
                return ResourceManager.GetString("ContentAbout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;div class=&quot;container&quot;&gt;
        ///
        ///        &lt;div class=&quot;row&quot;&gt;
        ///
        ///            &lt;div class=&quot;col-lg-12&quot;&gt;
        ///                &lt;h1 class=&quot;page-header&quot;&gt;FAQ
        ///                    &lt;small&gt;Frequently Asked Questions&lt;/small&gt;
        ///                &lt;/h1&gt;
        ///                &lt;ol class=&quot;breadcrumb&quot;&gt;
        ///                    &lt;li&gt;&lt;a href=&quot;index.html&quot;&gt;Home&lt;/a&gt;
        ///                    &lt;/li&gt;
        ///                    &lt;li class=&quot;active&quot;&gt;FAQ&lt;/li&gt;
        ///                &lt;/ol&gt;
        ///            &lt;/div&gt;
        ///
        ///        &lt;/div&gt;
        ///
        ///        &lt;div class=&quot;row&quot;&gt;
        ///
        ///            &lt;div class=&quot; [rest of string was truncated]&quot;;.
        /// </summary>
        public static string ContentFaq {
            get {
                return ResourceManager.GetString("ContentFaq", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;!-- Side Menu --&gt;
        ///    &lt;a id=&quot;menu-toggle&quot; href=&quot;#&quot; class=&quot;btn btn-primary btn-lg toggle&quot;&gt;&lt;i class=&quot;fa fa-bars&quot;&gt;&lt;/i&gt;&lt;/a&gt;
        ///    &lt;div id=&quot;sidebar-wrapper&quot;&gt;
        ///        &lt;ul class=&quot;sidebar-nav&quot;&gt;
        ///            &lt;a id=&quot;menu-close&quot; href=&quot;#&quot; class=&quot;btn btn-default btn-lg pull-right toggle&quot;&gt;&lt;i class=&quot;fa fa-times&quot;&gt;&lt;/i&gt;&lt;/a&gt;
        ///            &lt;li class=&quot;sidebar-brand&quot;&gt;&lt;a href=&quot;http://startbootstrap.com&quot;&gt;Start Bootstrap&lt;/a&gt;
        ///            &lt;/li&gt;
        ///            &lt;li&gt;&lt;a  ui-sref=&quot;content({path: &apos;about&apos;})&quot;&gt;About&lt;/a&gt;
        ///            &lt;/li&gt;
        ///   [rest of string was truncated]&quot;;.
        /// </summary>
        public static string Tpl1_Body {
            get {
                return ResourceManager.GetString("Tpl1_Body", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to /* Global Styles */
        ///
        ///html,
        ///body {
        ///  height: 100%;
        ///  width: 100%;
        ///}
        ///
        ///.vert-text {
        ///  display: table-cell;
        ///  vertical-align: middle;
        ///  text-align: center;
        ///}
        ///
        ///.vert-text h1 {
        ///  padding: 0;
        ///  margin: 0;
        ///  font-size: 4.5em;
        ///  font-weight: 700;
        ///}
        ///
        ////* Side Menu */
        ///
        ///#sidebar-wrapper {
        ///  margin-right: -250px;
        ///  right: 0;
        ///  width: 250px;
        ///  background: #000;
        ///  position: fixed;
        ///  height: 100%;
        ///  overflow-y: auto;
        ///  z-index: 1000;
        ///  -webkit-transition: all 0.4s ease 0s;
        ///  -moz-transition: [rest of string was truncated]&quot;;.
        /// </summary>
        public static string Tpl1_Css {
            get {
                return ResourceManager.GetString("Tpl1_Css", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Stylish Portfolio.
        /// </summary>
        public static string Tpl1_Name {
            get {
                return ResourceManager.GetString("Tpl1_Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;nav class=&quot;navbar navbar-custom navbar-fixed-top&quot; role=&quot;navigation&quot;&gt;
        ///        &lt;div class=&quot;container&quot;&gt;
        ///            &lt;div class=&quot;navbar-header page-scroll&quot;&gt;
        ///                &lt;button type=&quot;button&quot; class=&quot;navbar-toggle&quot; data-toggle=&quot;collapse&quot; data-target=&quot;.navbar-main-collapse&quot;&gt;
        ///                    &lt;i class=&quot;fa fa-bars&quot;&gt;&lt;/i&gt;
        ///                &lt;/button&gt;
        ///                &lt;a class=&quot;navbar-brand&quot; href=&quot;#page-top&quot;&gt;
        ///                    &lt;i class=&quot;fa fa-play-circle&quot;&gt;&lt;/i&gt;  &lt;span class=&quot;light&quot;&gt;Start&lt;/span&gt; Bootstrap
        ///  [rest of string was truncated]&quot;;.
        /// </summary>
        public static string Tpl2_Body {
            get {
                return ResourceManager.GetString("Tpl2_Body", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to body {
        ///    width: 100%;
        ///    height: 100%;
        ///    font-family: Lora,&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;
        ///    color: #fff;
        ///    background-color: #000;
        ///}
        ///
        ///html {
        ///    width: 100%;
        ///    height: 100%;
        ///}
        ///
        ///h1,
        ///h2,
        ///h3,
        ///h4,
        ///h5,
        ///h6 {
        ///    margin: 0 0 35px;
        ///    text-transform: uppercase;
        ///    font-family: Montserrat,&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;
        ///    font-weight: 700;
        ///    letter-spacing: 1px;
        ///}
        ///
        ///p {
        ///    margin: 0 0 25px;
        ///    font-size: 18px;
        ///    line-height: 1.5;
        ///}
        ///
        ///@medi [rest of string was truncated]&quot;;.
        /// </summary>
        public static string Tpl2_Css {
            get {
                return ResourceManager.GetString("Tpl2_Css", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to // Variables
        ///
        ///@primary: #28c3ab;
        ///@dark: #000;
        ///@light: #fff;
        ///
        ///// LESS
        ///
        ///body {
        ///    width: 100%;
        ///    height: 100%;
        ///    font-family: &quot;Lora&quot;,&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;
        ///    color: @light;
        ///    background-color: @dark;
        ///}
        ///
        ///html {
        ///    width: 100%;
        ///    height: 100%;
        ///}
        ///
        ///// Typhography 
        ///
        ///h1,
        ///h2,
        ///h3,
        ///h4,
        ///h5,
        ///h6 {
        ///    margin: 0 0 35px;
        ///    text-transform: uppercase;
        ///    font-family: &quot;Montserrat&quot;,&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;
        ///    font-weight: 700;
        ///    letter [rest of string was truncated]&quot;;.
        /// </summary>
        public static string Tpl2_Less {
            get {
                return ResourceManager.GetString("Tpl2_Less", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gray Scale.
        /// </summary>
        public static string Tpl2_Name {
            get {
                return ResourceManager.GetString("Tpl2_Name", resourceCulture);
            }
        }
    }
}
