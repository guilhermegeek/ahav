﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ahav.ServiceModel.Errors
{
    public static class RegisterErrors
    {
        public const string InvalidConfirmationToken = "InvalidToken";
        public const string EmailAlreadyInUse = "EmailInUse";
    }
}
