﻿using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class GetContentDefault : IReturn<GetContentDefaultResponse>
    {
        public ObjectId ContentId { get; set; }
    }
    public class GetContentDefaultResponse : IHasResponseStatus
    {
        public ContentDto Content { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
