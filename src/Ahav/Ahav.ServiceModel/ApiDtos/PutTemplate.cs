﻿using System.Collections.Generic;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class PutTemplate : IReturn<PutTemplateResponse>
    {
        public ObjectId TemplateId { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Body { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public string Javascript { get; set; }
        public string Css { get; set; }
        public string Less { get; set; }
        public ObjectId ThumbnailId { get; set; }
    }
    public class PutTemplateResponse : IHasResponseStatus, IHasResponseModal
    {
        public ContentTemplateDto Template { get; set; }
        public ResponseClient Modal { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
