﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class PutBlogTemplate : IReturn<PutBlogTemplateResponse>
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Body { get; set; }
        public Dictionary<string, string> BodyViewArticle { get; set; }
        public Dictionary<string, string> BodyViewIndex { get; set; }
        public Dictionary<string, string> Metadata { get; set; } 
        public bool Public { get; set; }
    }
    public class PutBlogTemplateResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
