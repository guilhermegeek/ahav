﻿using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class GetSupportContact : IReturn<GetSupportContactResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class GetSupportContactResponse : IHasResponseStatus
    {
        public SupportContactDto Contact { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
