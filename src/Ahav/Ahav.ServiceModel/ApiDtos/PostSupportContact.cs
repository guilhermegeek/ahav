﻿using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Interfaces;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class PostSupportContact : IReturn<PostSupportContactResponse>
    {
        public string Name { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
    }
    public class PostSupportContactResponse : IHasResponseStatus, IHasResponseModal
    {
        public SupportContactDto Contact { get; set; }
        public ResponseClient Modal { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }

}
