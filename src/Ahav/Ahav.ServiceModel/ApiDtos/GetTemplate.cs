﻿using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class GetTemplate : IReturn<GetTemplateResponse>
    {
        public ObjectId TemplateId { get; set; }
    }
    public class GetTemplateResponse : IHasResponseStatus
    {
        public ContentTemplateDto Template { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
