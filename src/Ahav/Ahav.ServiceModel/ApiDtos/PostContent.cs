﻿using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class PostContent : IReturn<PostContentResponse>
    {
        public string Slug { get; set; }
        public ObjectId Template { get; set; }
    }
    public class PostContentResponse : IHasResponseStatus, IHasResponseModal
    {
        public ContentDto Content { get; set; }
        public string ContentUri { get; set; }
        public ResponseClient Modal { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
