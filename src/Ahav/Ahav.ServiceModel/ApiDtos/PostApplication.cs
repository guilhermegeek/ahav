﻿using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class PostApplication : IReturn<PostApplicationResponse>
    {
        public string Name { get; set; }
        public AhavFeature[] Features { get; set; }
        public string Description { get; set; }
        public ObjectId PackageId { get; set; }
        public string Domain { get; set; }
    }
    public class PostApplicationResponse : IHasResponseStatus, IHasResponseModal
    {
        public AhavProjectDto Project { get; set; }
        public ResponseClient Modal { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
