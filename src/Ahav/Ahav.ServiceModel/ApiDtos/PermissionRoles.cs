﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;


namespace Ahav.ServiceModel.ApiDtos
{
    public class PermissionRoles : IReturn<PermissionRolesResponse>
    {
        public ObjectId UserId { get; set; }
        public string Role { get; set; }
    }

    public class PermissionRolesResponse : IHasResponseStatus
    {
        public IList<string> Roles { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }

}
