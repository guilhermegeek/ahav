﻿using System.Collections.Generic;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Interfaces;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class PostPlace : IReturn<PostPlaceResponse>
    {
        public PlaceType PlaceType { get; set; }
        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
    public class PostPlaceResponse : IHasResponseStatus, IHasResponseModal
    {
        public PlaceDto Place { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
