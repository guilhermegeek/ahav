﻿using System.Collections.Generic;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    /// <summary>
    /// Request DTO for /content-slug requests
    /// </summary>
    [FallbackRoute("/{Path*}")]
    public class GetContentView : IReturn<GetContentViewResponse>
    {
        public string Path { get; set; }
    }
    public class GetContentViewResponse : IHasResponseStatus
    {
        public string Scope { get; set; }
        public ContentDto Content { get; set; }
        public ContentTemplateDto Template { get; set; }
        public Dictionary<string, string> MetaTags { get; set; }
        public ObjectId? VideoInit { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        /// <summary>
        /// If true, the Content Template is the same as the Template sented in request
        /// </summary>
        public bool ReuseTemplate { get; set; }
        /// <summary>
        /// If the Content has a Template, renders the Template with the body of Content
        /// If no Template, ist the same as Content Body
        /// </summary>
        public string RenderBody { get; set; }
        public Gslide Gslide { get; set; }
    }

}
