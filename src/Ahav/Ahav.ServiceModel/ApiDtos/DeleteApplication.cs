﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class DeleteApplication : IReturn<DeleteApplicationResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class DeleteApplicationResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
