﻿using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class PostValidateContentSlug : IReturn<PostValidateContentSlugResponse>
    {
        public string Slug { get; set; }
        public ObjectId? ContentId { get; set; }
    }
    public class PostValidateContentSlugResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseClient Modal { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
