﻿using System.Collections.Generic;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class PostContentFile : IReturn<PostContentFileResponse>
    {
        public ObjectId ContentId { get; set; }
    }
    public class PostContentFileResponse : IHasResponseStatus, IHasResponseModal
    {
        public PostContentFileResponse()
        {
            this.Medias = new List<MediaDto>();
        }
        public IList<MediaDto> Medias { get; set; }
        public string File { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
