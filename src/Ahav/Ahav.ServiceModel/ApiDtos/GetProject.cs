﻿using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class GetProject : IReturn<GetProjectResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class GetProjectResponse : IHasResponseStatus
    {
        public AhavProjectDto Project { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
