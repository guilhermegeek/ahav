﻿using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class GetContent : IReturn<GetContentResponse>
    {
        public ObjectId ContentId { get; set; }
    }
    public class GetContentResponse : IHasResponseStatus
    {
        public ContentDto Content { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
