﻿using System.Collections.Generic;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class PutContent : IReturn<PutContentResponse>
    {
        public PutContent()
        {
            this.Files = new List<ObjectId>();
        }
        public ObjectId ContentId { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public string Javascript { get; set; }
        public Dictionary<string, string> Body { get; set; }
        /// <summary>
        /// Slug
        /// </summary>
        public string Slug { get; set; }
        public ObjectId Template { get; set; }
        public IList<ObjectId> Files { get; set; }
        public string Css { get; set; }
    }
    public class PutContentResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseClient Modal { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
