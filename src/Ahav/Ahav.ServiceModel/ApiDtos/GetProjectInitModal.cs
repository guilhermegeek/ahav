﻿using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class GetProjectInitModal : IReturn<GetProjectInitModalResponse>
    {
        public ObjectId ProjectId { get; set; }
    }
    public class GetProjectInitModalResponse : IHasResponseStatus
    {
        public MediaDto Video { get; set; }
        public bool HasVideo { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
