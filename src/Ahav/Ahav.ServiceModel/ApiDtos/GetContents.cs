﻿using System.Collections.Generic;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class GetContents : IReturn<GetContentsResponse>
    {
        public GetContents()
        {
            this.Take = 40;
        }
        public ObjectId ProjectId { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
    }
    public class GetContentsResponse : IHasResponseStatus
    {
        public IList<ContentInfo> Contents { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
