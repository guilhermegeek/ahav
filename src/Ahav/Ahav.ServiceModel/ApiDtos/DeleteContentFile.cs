﻿using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class DeleteContentFile : IReturn<DeleteContentFileResponse>
    {
        public ObjectId ContentId { get; set; }
        public ObjectId FileId { get; set; }
    }
    public class DeleteContentFileResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
