﻿using System.Collections.Generic;
using Ahav.ServiceModel.Dtos;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class GetAttachments : IReturn<GetAttachmentsResponse>
    {

    }
    public class GetAttachmentsResponse : IHasResponseStatus
    {
        public GetAttachmentsResponse()
        {
            this.Files = new List<MediaDto>();
        }
        public IList<MediaDto> Files { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
