﻿using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    /// <summary>
    /// Basic Registration
    /// </summary>
    public class PostRegisterBasic : IReturn<PostRegisterBasicResponse>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Redirect { get; set; }
    }
    public class PostRegisterBasicResponse : IHasResponseStatus, IHasResponseModal
    {
        public ObjectId UserId { get; set; }
        public string ConfirmationToken { get; set; }
        public string ReferrerUrl { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
