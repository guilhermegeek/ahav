﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.Auth;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    /// <summary>
    /// Saves a OAuth for an Application
    /// </summary>
    public class PostApplicationOAuthProvider : IReturn<PostApplicationOAuthProviderResponse>
    {
        public UserOAuthProvider OAuthAppsProviders { get; set; }
        public string ClientId { get; set; }
        public string Secret { get; set; }
    }
    public class PostApplicationOAuthProviderResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
