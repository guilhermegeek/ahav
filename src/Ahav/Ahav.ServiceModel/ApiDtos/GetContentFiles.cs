﻿using System.Collections.Generic;
using Ahav.Interfaces;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class GetContentFiles : IReturn<GetContentFilesResponse>, IPagedResult
    {
        public GetContentFiles()
        {
            this.Take = 20;
        }
        public ObjectId ContentId { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
        public ContentSortBy SortBy { get; set; }
    }
    public class GetContentFilesResponse : IHasResponseStatus
    {
        public GetContentFilesResponse()
        {
            this.Files = new List<string>();
            this.Medias = new List<MediaDto>();
        }
        public IList<string> Files { get; set; }
        public IList<MediaDto> Medias { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
