﻿using System.Collections.Generic;
using Ahav.ServiceModel.Dtos;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class GetAhavPackagesAvailable : IReturn<GetAhavPackagesAvailableResponse>
    {
        public int Skip { get; set; }
        public int Take { get; set; }
    }
    public class GetAhavPackagesAvailableResponse : IHasResponseStatus
    {
        public GetAhavPackagesAvailableResponse()
        {
            this.Packages = new List<AhavPackageInfo>();
        }
        public IList<AhavPackageInfo> Packages { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
