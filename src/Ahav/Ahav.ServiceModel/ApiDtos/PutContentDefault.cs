﻿using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class PutContentDefault : IReturn<PutContentDefaultResponse>
    {
        public ObjectId ContentId { get; set; }
    }
    public class PutContentDefaultResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
