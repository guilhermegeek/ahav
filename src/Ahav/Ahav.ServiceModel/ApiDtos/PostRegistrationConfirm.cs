﻿using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class PostRegistrationConfirm : IReturn<PostRegistrationConfirmResponse>
    {
        public ObjectId UserId { get; set; }
        public string Token { get; set; }
    }
    public class PostRegistrationConfirmResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public bool IsConfirmed { get; set; }
    }
}
