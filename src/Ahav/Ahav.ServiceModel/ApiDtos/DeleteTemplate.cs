﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    /// <summary>
    /// Deletes a Template
    /// In case the template has active contents using him, IdForContents is used to the migration
    /// </summary>
    public class DeleteTemplate : IReturn<DeleteTemplateResponse>
    {
        /// <summary>
        /// Template Id
        /// </summary>
        public ObjectId TemplateId { get; set; }
        /// <summary>
        /// New Template Id for Contents who use this template (if any)
        /// </summary>
        public ObjectId IdMigration { get; set; }
    }
    public class DeleteTemplateResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseClient Modal { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
