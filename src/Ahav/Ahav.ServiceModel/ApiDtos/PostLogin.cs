﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class PostLogin : IReturn<LoginResponse>
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool Persist { get; set; }
    }
    public class LoginResponse : IHasResponseStatus
    {
        public AhavAuth Result { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public string Email { get; set; }
        public AuthApi Auth { get; set; }
    }
}
