﻿using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class DeleteContent : IReturn<DeleteContentResponse>
    {
        public ObjectId ContentId { get; set; }
    }
    public class DeleteContentResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseClient Modal { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
