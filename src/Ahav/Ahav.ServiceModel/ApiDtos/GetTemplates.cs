﻿using System.Collections.Generic;
using Ahav.Interfaces;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class GetTemplates : IReturn<GetTemplatesResponse>, IPagedResult
    {
        public ObjectId[] Ids { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
    }
    public class GetTemplatesResponse : IHasResponseStatus
    {
        public GetTemplatesResponse()
        {
            this.Templates = new List<ContentTemplateInfo>();
        }
        public IList<ContentTemplateInfo> Templates { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
