﻿using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Interfaces;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class PostContentTemplate : IReturn<PostTemplateResponse>
    {
        public string Name { get; set; }
    }
    public class PostTemplateResponse : IHasResponseStatus, IHasResponseModal
    {
        public ContentTemplateInfo Template { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
