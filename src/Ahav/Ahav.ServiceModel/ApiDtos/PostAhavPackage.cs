﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Interfaces;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class PostAhavPackage : IReturn<PostAhavPackageResponse>
    {
        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Description { get; set; }
        public decimal Ammount { get; set; }
        /// <summary>
        /// Features wich will be enabled with this package
        /// </summary>
        public AhavFeature[] Features { get; set; }
        /// <summary>
        /// Duration in timespan of package
        /// </summary>
        public TimeSpan Duration { get; set; }
        /// <summary>
        /// Enables the package after his creation
        /// </summary>
        public bool Activate { get; set; }

        public DateTime Start { get; set; }
        public DateTime? End { get; set; }

    }
    public class PostAhavPackageResponse : IHasResponseStatus, IHasResponseModal
    {
        public AhavPackage Package { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
