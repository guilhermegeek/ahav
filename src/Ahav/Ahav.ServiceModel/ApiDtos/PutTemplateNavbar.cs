﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class PutTemplateNavbar : IReturn<PutContentTemplateNavbarResponse>
    {
        public ObjectId TemplateId { get; set; }
        public NavbarPutViewModel NavbarPut { get; set; }
    }
    public class PutContentTemplateNavbarResponse : IHasResponseStatus, IHasResponseModal
    {
        public NavbarPutViewModel NavbarPut { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
