﻿using System.Collections.Generic;
using Ahav.Interfaces;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class GetProjects : IPagedResult, IReturn<GetProjectsResponse>
    {
        public ObjectId[] ProjectsIds { get; set; }
        public ObjectId[] OwnersIds { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
    }
    public class GetProjectsResponse : IHasResponseStatus
    {
        public IList<AhavProjectInfo> Projects { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
