﻿using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class DeleteProjectInitModal : IReturn<DeleteProjectInitModalResponse>
    {

    }
    public class DeleteProjectInitModalResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
