﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class PutTemplateViewPage : IReturn<PutTemplateViewPageResponse>
    {
        public ObjectId Id { get;set; }
        public TemplateViewType ViewType { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Culture { get; set; }
    }
    public class PutTemplateViewPageResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
