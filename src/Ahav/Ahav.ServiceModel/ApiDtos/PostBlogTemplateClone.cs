﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    /// <summary>
    /// Clones a existing Blog Template
    /// </summary>
    public class PostBlogTemplateClone : IReturn<PostBlogTemplateCloneResponse>
    {
        public ObjectId BlogTemplateId { get; set; }
        public bool Public { get; set; }
    }
    public class PostBlogTemplateCloneResponse : IHasResponseStatus, IHasResponseModal
    {
        /// <summary>
        /// New Id
        /// </summary>
        public ObjectId BlogTemplateId { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
