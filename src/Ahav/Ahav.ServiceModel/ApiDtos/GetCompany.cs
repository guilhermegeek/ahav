﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace Ahav.ServiceModel.ApiDtos
{
    public class GetCompany : IReturn<GetCompanyResponse>
    {
        public ObjectId Id { get; set; }
    }
    public class GetCompanyResponse : IHasResponseStatus
    {
        public CompanyDto Company { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
