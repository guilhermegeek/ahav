﻿namespace Ahav.ServiceModel.Domain
{
    /// <summary>
    /// A delivery method is a standardized procedure for transferring the product or service to the destination of fulfilment chosen by the customer. Delivery methods are characterized by the means of transportation used, and by the organization or group that is the contracting party for the sending organization or person.
    /// </summary>
    public enum DeliveryMethod
    {
        DirectDownload,
        Freight,
        Mail,
        OwnFleet,
        PickUp,
        Dhl,
        FederalExpress,
        Ups
    }
}
