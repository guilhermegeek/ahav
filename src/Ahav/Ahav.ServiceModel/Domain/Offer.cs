﻿using System;

namespace Ahav.ServiceModel.Domain
{
    /// <summary>
    /// An offer to sell an item—for example, an offer to sell a product, the DVD of a movie, or tickets to an event.
    /// </summary>
    /// http://schema.org/Offer
    public class Offer
    {
        public int Quantity { get; set; }
        /// <summary>
        /// Paymments methods available for offer
        /// </summary>
        public PaymentMethod[] PaymentMethods { get; set; }
        public ItemAvailability Availability { get; set; }
        public DateTime AvailabilityStarts { get; set; }
        public DateTime AvailabilityEnds { get; set; }
        public DeliveryMethod[] DeliveryMehods { get; set; }
        /// <summary>
        /// The ISO 3166-1 (ISO 3166-1 alpha-2) or ISO 3166-2 code, or the GeoShape for the geo-political region(s) for which the offer or delivery charge specification is valid. 
        /// </summary>
        public string EligibleRegion { get; set; }
    }
}
