﻿namespace Ahav.ServiceModel.Domain
{
    public enum ContentSortBy
    {
        None = 0, IdAsc = 1, IdDesc = 2, NameAsc = 3, NameDesc = 3
    }
}
