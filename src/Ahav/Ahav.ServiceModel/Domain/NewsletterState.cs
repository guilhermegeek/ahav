﻿namespace Ahav.ServiceModel.Domain
{
    public enum NewsletterState
    {
        Draft = 0,
        WaitingForApprovement = 1,
        Rejected = 2,
        Started = 3,
        Finished = 4
    }
}
