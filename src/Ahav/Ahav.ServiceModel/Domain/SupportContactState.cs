﻿namespace Ahav.ServiceModel.Domain
{
    public enum SupportContactState
    {
        NotReaded = 0, Replied = 1, Closed = 2
    }
}
