﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ahav.ServiceModel.Domain
{
    public enum PlaceType
    {
        LocalBusiness, 
        GeoCache, 
        TouristAttraction,
        LandmarksOrHistoricalBuildings,
        // A landform or physical feature.  Landform elements include:
        // mountains, plains, lakes, rivers, seascape and oceanic waterbody interface features such as bays, peninsulas, seas and so forth, including sub-aqueous terrain features such as submersed mountain ranges, volcanoes, and the great ocean basins.
        Landform,
        CivicStructure,
        AdministrativeArea,
        Residence
    }
}
