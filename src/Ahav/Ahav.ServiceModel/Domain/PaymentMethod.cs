﻿namespace Ahav.ServiceModel.Domain
{
    public enum PaymentMethod
    {
        ByInvoice,
        PayPal,
        BankTransferInAdvance,
        Cash,
        CheckInAdvance,
        CashOnDelivery,
        DiretDebit,
        GoogleCheckout,
        PaySwarm,
        AmericanExpress,
        DinersClub,
        Discover,
        Jcb,
        MasterCard,
        Visa
    }
}
