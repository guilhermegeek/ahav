﻿namespace Ahav.ServiceModel.Domain
{
    /// <summary>
    /// Price
    /// </summary>
    public class Pricing
    {
        /// <summary>
        /// The default price, actual for all users
        /// </summary>
        public decimal List { get; set; }
        /// <summary>
        /// Price available for retail
        /// </summary>
        public decimal Retail { get; set; }
        public decimal Savings { get; set; }
        public decimal PctSavings { get; set; }
    }
}
