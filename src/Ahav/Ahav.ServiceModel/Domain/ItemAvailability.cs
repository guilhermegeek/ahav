﻿namespace Ahav.ServiceModel.Domain
{
    /// <summary>
    /// Product availablity options
    /// </summary>
    public enum ItemAvailability
    {
        // Indicates that the item for sale has been discontinued.
        Discontinued,
        // Indicates that the item for sale is in stock.
        InStock,
        // Indicates that the item for sale is available only in brick-and-mortar stores.
        InStoreOnly,
        // Indicates that the item has limited availability.
        LimitedAvailability,
        // Indicates that the item for sale is available only online.
        OnlineOnly,
        // Indicates that the item for sale is out of stock.
        OutOfStock,
        // Indicates that the item for sale is available for pre-order.
        PreOrder,
        // Indicates that the item has sold out.
        SoldOut
    }
}
