﻿namespace Ahav.ServiceModel.Domain
{
    public enum AccountState
    {
        NotConfirmed = 1,
        Confirmed = 2
    }
}
