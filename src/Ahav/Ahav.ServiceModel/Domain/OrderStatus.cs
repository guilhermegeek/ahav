﻿namespace Ahav.ServiceModel.Domain
{
    /// <summary>
    /// Indicates the current order status
    /// </summary>
    public enum OrderStatus
    {
        OrderCancelled,
        OrderDelivered,
        OrderInTransit,
        OrderPaymentDue,
        OrderPickupAvailable,
        OrderProblem,
        OrderProcessing,
        OrderReturned
    }
}
