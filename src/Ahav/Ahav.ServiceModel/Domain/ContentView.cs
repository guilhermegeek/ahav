﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain
{
    public class ContentView
    {
        public ObjectId Id { get; set; }
        public bool ReuseTemplate { get; set; }
        public string RenderBody { get; set; }
        public Dictionary<string, string> MetaTags { get; set; }
    }
}
