﻿namespace Ahav.ServiceModel.Domain
{
    public enum OfferItemCondition
    {
        DamagedCondition,
        NewCondition,
        RefurbishedCondition,
        UsedCondition
    }
}
