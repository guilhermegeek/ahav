﻿using System.Collections.Generic;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain.Entities
{
    public class NewsletterTemplate
    {
        public ObjectId Id { get; set; }
        public Dictionary<string, string> Body { get; set; }
        public Dictionary<string, string> Subject { get; set; }
    }
}
