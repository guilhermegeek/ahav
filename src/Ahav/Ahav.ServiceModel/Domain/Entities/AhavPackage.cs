﻿using System;
using System.Collections.Generic;
using Ahav.ServiceModel.Common;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain.Entities
{
    /// <summary>
    /// This DTO contains the prices for feature in Ahav
    /// </summary>
    public class AhavPackage
    {
        public ObjectId Id { get; set; }
        public DateTime Start { get; set; }
        public DateTime? End { get; set; }
        /// <summary>
        /// 15 days for trial 0 euros, 1 month 1 euro, 12 months 10 euro,s etc
        /// </summary>
        public Dictionary<long, decimal> DaysPrice { get; set; }
        public bool IsActive { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Description { get; set; }
        public decimal Ammount { get; set; }
        public AhavFeature[] Features { get; set; }
        public int Dimensions { get; set; }
    }
}
