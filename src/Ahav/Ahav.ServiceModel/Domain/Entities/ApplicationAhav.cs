﻿using System;
using System.Collections.Generic;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Social.Domain;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain.Entities
{
    /// <summary>
    /// Project 
    /// </summary>
    /// Each project has one domain and unique social ids providers
    /// Acts as a Website container
    public class ApplicationAhav
    {
        public ApplicationAhav()
        {
            Coupons = new List<ObjectId>();
            Templates = new ObjectId[0];
            Contents = new ObjectId[0];
            Packages = new ObjectId[0];
            Features = new AhavFeature[0];
            Blogs = new ObjectId[0];
            BlogTemplates = new ObjectId[0];
            Articles = new ObjectId[0];
            Attachments = new ObjectId[0];
            SocialProiderAppsIds =  new Dictionary<SocialProvider, string>();
        }
        /// <summary>
        /// Id
        /// </summary>
        public ObjectId Id { get; set; }
        /// <summary>
        /// Ids of applications on facebook, google, etc
        /// </summary>
        public Dictionary<SocialProvider, string> SocialProiderAppsIds { get; set; }
        
        /// <summary>
        /// Account that owns the Project
        /// </summary>
        public ObjectId OwnerId { get; set; }
        /// <summary>
        /// Project name, single person name, company group name, etc
        /// </summary>
        public string Name { get; set; }

        public string Description { get; set; }
        /// <summary>
        /// Domain associated with the account
        /// </summary>
        public string Domain { get; set; }
        /// <summary>
        /// If the account don't have any domain, this is the route
        /// </summary>
        public string RoutePath { get; set; }
        /// <summary>
        /// Features
        /// </summary>
        public ObjectId[] Packages { get; set; }
        /// <summary>
        /// Features enabled for this project (sum of all Packages)
        /// </summary>
        public AhavFeature[] Features { get; set; }
        public DateTime ExpiresAt { get; set; }
        public AhavProjectState ProjectState { get; set; }

        // Ecommerce
        public List<ObjectId> Coupons { get; set; }
        // Content
        public ObjectId[] Templates { get; set; }
        public ObjectId[] Contents { get; set; }
        public ObjectId[] Blogs { get; set; }
        public ObjectId[] Articles { get; set; }
        public ObjectId[] BlogTemplates { get; set; }
        public ObjectId[] Attachments { get; set; }
        /// <summary>
        /// Open auths for applications like Paypal, Facebook, Email providers, etc
        /// </summary>
        public Dictionary<OAuthAppsProviders, OpenAuth> OpenAuths { get; set; }
    }
    public enum OAuthAppsProviders
    {
        Paypal, Facebook, MailChimp
    }
    public class OpenAuth
    {
        public string ClientId { get; set; }
        public string Secret { get; set; }
    }
}
