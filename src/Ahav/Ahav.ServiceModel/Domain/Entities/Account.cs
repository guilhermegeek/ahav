﻿namespace Ahav.ServiceModel.Domain.Entities
{
    
    /// <summary>
    /// CMS Customer Account
    /// </summary>
    //public class AccountAhav
    //{
    //    /// <summary>
    //    /// Account Id, the same as User Id
    //    /// </summary>
    //    public ObjectId Id { get; set; }
    //    /// <summary>
    //    /// Email account
    //    /// </summary>
    //    public string PrimaryEmail { get; set; }
    //    /// <summary>
    //    /// If true, account is for a person like a freelancer
    //    /// If false, account is a company group, 2 freelancers project, etc
    //    /// </summary>
    //    public bool Singular { get; set; }
    //    /// <summary>
    //    /// Complete name of the person or company
    //    /// </summary>
    //    public string EntityName { get; set; }
    //    /// <summary>
    //    /// Complete address of person or company
    //    /// </summary>
    //    public string EntityAddress { get; set; }
    //    /// <summary>
    //    /// Active and inactive projects
    //    /// </summary>
    //    public ObjectId[] Projects { get; set; }
    //    /// <summary>
    //    /// Package orders
    //    /// </summary>
    //    public AhavPackageOrder[] Orders { get; set; }
    //}

    /// <summary>
    /// An order is equal to a aquicition
    /// </summary>
   
    public static class AccountRole
    {
        public const string Visitor = "visitor";
        public const string Administrator = "admin";
        public const string NewsAuthor = "newsauthor";
        public const string NewsPublisher = "newspublisher";
        public const string Gui = "gui";
    }    
}
