﻿using System.Collections.Generic;
using Ahav.Interfaces;
using Ahav.ServiceModel.Common;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain.Entities
{
    /// <summary>
    /// Interface for Place
    /// </summary>
    public interface IPlace
    {
        ObjectId Id { get; set; }
        string Name { get; set; }
        Dictionary<string, string> Title { get; set; }
        Dictionary<string, string> Description { get; set; }
        string Address { get; set; }
        double[] Location { get; set; }
        OpeningHoursSpecification[] OpeningHours { get; set; }
    }

    /// <summary>
    /// Entities that have a somewhat fixed, physical extension.
    /// </summary>
    public abstract class Place : IThink, IPlace
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Description { get; set; }
        public string Address { get; set; }
        public double[] Location { get; set; }
        public OpeningHoursSpecification[] OpeningHours { get; set; }
    }
}
