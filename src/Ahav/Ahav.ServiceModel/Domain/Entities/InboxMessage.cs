﻿namespace Ahav.ServiceModel.Domain.Entities
{
    /// <summary>
    /// Stored sorted by <userid, word, messageId>
    /// </summary>
    public class InboxMessage
    {
        /// <summary>
        /// Row Key
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// Column
        /// </summary>
        public string Word { get; set; }
        /// <summary>
        /// Version
        /// </summary>
        public string MessageId { get; set; }
    }
}
