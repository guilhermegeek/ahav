﻿using System;
using Ahav.Interfaces;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain.Entities
{
    /// <summary>
    /// An order is a confirmation of a transaction (a receipt), which can contain multiple line items, each represented by an Offer that has been accepted by the customer.
    /// </summary>
    /// http://schema.org/Order
    public class Order
    {
        /// <summary>
        /// Order Id
        /// </summary>
        public ObjectId Id { get; set; }
        /// <summary>
        /// Order offers (products)
        /// </summary>
        public OrderOffer[] Offers { get; set; }
        /// <summary>
        /// Party placing the order.
        /// Organization or Person
        /// </summary>
        public ICreator Customer { get; set; }
        /// <summary>
        /// The party taking the order (e.g. Amazon.com is a merchant for many sellers).
        /// </summary>
        public ICreator Merchant { get; set; }
        /// <summary>
        /// The current status of the order.
        /// </summary>
        public OrderStatus Status { get; set; }
        /// <summary>
        /// The date that payment is due.
        /// </summary>
        public DateTime PaymentDue { get; set; }
        /// <summary>
        /// The name of the credit card or other method of payment for the order.
        /// </summary>
        public PaymentMethod PaymentMethod { get; set; }
        public DeliveryMethod DeliveryMethod { get; set; }
        public string PaymentMethodId { get; set; }
    }
}
