﻿using System.Collections.Generic;
using Ahav.ServiceModel.Enums;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain.Entities
{
    public class Media
    {
        public ObjectId Id { get; set; }
        public string ThumbnailUrl { get; set; }
        public MediaType MediaType { get; set; }
        public string Extension { get; set; }
        public ObjectId OwnerId { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Title { get; set; }
    }
}
