﻿using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain.Entities
{
    public class SupportContact
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public SupportContactState State { get; set; }
    }
}
