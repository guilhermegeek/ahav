﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain.Entities
{
    public class Company
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public ObjectId[] EmployeIds { get; set; }
    }
}
