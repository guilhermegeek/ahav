﻿using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain.Entities
{
    public abstract class GetComments
    {
        public ObjectId Id { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
    }
    public abstract class GetCommentsResponse
    {
        public NComments Result { get; set; }
    }
    public abstract class AddComment
    {
        public ObjectId Id { get; set; }
        public string Comment { get; set; }
    }
    public abstract class AddCommentResponse
    {
        public NComment Result { get; set; }
    }
}
