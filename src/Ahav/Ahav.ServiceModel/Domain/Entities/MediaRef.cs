﻿using System.Collections.Generic;
using Ahav.ServiceModel.Enums;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain.Entities
{
    public class MediaRef
    {
        public ObjectId Id { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public MediaType MediaType { get; set; }
    }
}
