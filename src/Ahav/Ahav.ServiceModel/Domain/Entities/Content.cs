﻿using System.Collections.Generic;
using Ahav.Interfaces;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain.Entities
{
    public class Content : IHasComments, IHasAttachments
    {
        public Content()
        {
            this.Files = new ObjectId[0];
            this.Comments=new List<NComment>();
        }
        public ObjectId Id { get; set; }
        public ObjectId Owner { get; set; }
        public ObjectId ProjectId { get; set; }
        public ObjectId[] Files { get; set; }
        public string Slug { get; set; }
        /// <summary>
        /// Title element for <html>
        /// </summary>
        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Body { get; set; }
        /// <summary>
        /// Description is a summary, used in social networks, newsletters, etc
        /// </summary>
        public Dictionary<string, string> Description { get; set; }
        public string Javascript { get; set; }
        public ObjectId Template { get; set; }
        public ObjectId Thumbnail { get; set; }
        public IList<NComment> Comments { get; set; }
        public long CommentsCount { get; set; }

        public string Css { get; set; }
    }
}
