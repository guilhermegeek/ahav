﻿using System.Collections.Generic;
using Ahav.ServiceModel.Common;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain.Entities
{
    public class AhavProjectPermissions
    {
        public ObjectId Id { get; set; }
        public Dictionary<AhavFeature, ObjectId[]> Permissions { get; set; }
    }
}
