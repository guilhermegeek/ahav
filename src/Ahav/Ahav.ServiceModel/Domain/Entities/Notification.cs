﻿using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain.Entities
{
    public class Notification
    {
        public long Id { get; set; }
        public ObjectId UserId { get; set; }
    }
}
