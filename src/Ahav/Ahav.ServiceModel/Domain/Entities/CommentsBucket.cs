﻿using System.Collections.Generic;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain.Entities
{
    public class CommentsBucket
    {
        public CommentsBucket()
        {
            this.Comments=new List<NComment>();
        }
        public ObjectId RelativeDocumentId { get; set; }
        public int Index { get; set; }
        public IList<NComment> Comments { get; set; }
        public int Count { get; set; }
    }
}
