﻿using System;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain.Entities
{
    public class AhavPackageOrder
    {
        /// <summary>
        /// Features 
        /// </summary>
        public ObjectId[] Packages { get; set; }
        /// <summary>
        /// The date when the features will ne enabled
        /// </summary>
        public TimeSpan StartTime { get; set; }
        /// <summary>
        /// The date when the features will be disabled
        /// </summary>
        public DateTime EndTime { get; set; }
        /// <summary>
        /// Ammount
        /// </summary>
        public decimal Ammount { get; set; }
        /// <summary>
        /// If false, the order is a new order
        /// </summary>
        public bool Renew { get; set; }
        /// <summary>
        /// Order State
        /// </summary>
        public AhavPackageOrderState State { get; set; }
    }
}