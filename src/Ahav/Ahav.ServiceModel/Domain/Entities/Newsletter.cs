﻿using System.Collections.Generic;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain.Entities
{
    public class Newsletter
    {
        public Newsletter()
        {
            this.Files = new ObjectId[0];
        }
        public ObjectId Id { get; set; }
        public Dictionary<string, string> Subject { get; set; }
        public Dictionary<string, string> Content { get; set; }
        public bool IsHtml { get; set; }
        public NewsletterState State { get; set; }
        public string Name { get; set; }
        public ObjectId[] Files { get; set; }
    }
}
