﻿using System.Collections.Generic;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain.Entities
{
    public enum MailCommon
    {
        ConfirmAccount, RecoverPassword
    }
    public class Mail
    {
        public ObjectId Id { get; set; }
        public Dictionary<string, string> Subject { get; set; }
        public Dictionary<string, string> Content { get; set; }
        public bool IsHtml { get; set; }
        public string[] MailRecipients { get; set; }
    }
    /// <summary>
    /// Temporarily saves the email, until is sented
    /// </summary>
    public class MailQueue
    {
        public string Email { get; set; }
        public string[] Parameters { get; set; }
    }
    public class MailDto
    {
        public ObjectId Id { get; set; }
        public string Subject { get; set; }
        public bool IsHtml { get; set; }
        public string Content { get; set; }
    }
}
