﻿using System;
using System.Collections.Generic;
using Ahav.ServiceModel.Dtos;

namespace Ahav.ServiceModel.Domain.Entities
{
    public class NCommentAuthor
    {
        public string Name { get; set; }
        public string Uri { get; set; }
    }
    /// <summary>
    /// Wherever some property name of this dto change, repository of comments should updated!
    /// </summary>
    public class NComment
    {
        public NComment()
        {
            this.Snapshots=new List<NCommentSnapshot>();
            this.Replies=new List<NComment>();
        }
        public long Id { get; set; }
        public AuthorInfo User { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Comment { get; set; }
        public IList<NCommentSnapshot> Snapshots { get; set; } 
        public IList<NComment>  Replies { get; set; } 
    }
    public class NCommentSnapshot
    {
        public string Message { get; set; }
        public DateTime ModifiedAt { get; set; }
    }
    public class NComments
    {
        public NComments()
        {
            Comments = new List<NComment>();
        }
        public IList<NComment> Comments { get; set; }
        public long CommentsCount { get; set; }
    }
}
