﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain.Entities
{
    public class AuthVisitor
    {
        public string Token { get; set; }
        public string TempId { get; set; }
    }
    /// <summary>
    /// Sended to webapps
    /// </summary>
    public class AuthApi
    {
        public ObjectId Id { get; set; }
        public string Key { get; set; }
    }
    [DataContract]
    public class AhavAuth
    {
        public AhavAuth()
        {
            this.Roles = new List<string>();
        }
        [DataMember(Order = 0)]
        public string Token { get; set; }

        [DataMember(Order = 1)]
        public DateTime TokenExpires { get; set; }

        [DataMember(Order = 2)]
        public IList<string> Roles { get; set; }

        [DataMember(Order = 3)]
        public string[] SessionsIds { get; set; }

        [DataMember(Order = 4)]
        public ObjectId UserId { get; set; }
    }
}
