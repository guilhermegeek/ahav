﻿using System.Collections.Generic;
using Ahav.Interfaces;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain.Entities
{
    public class LocalBusiness : IThink, IPlace
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Description { get; set; }
        public string Address { get; set; }
        public double[] Location { get; set; }
        public OpeningHoursSpecification[] OpeningHours { get; set; }
        /// <summary>
        /// The larger organization that this local business is a branch of, if any.
        /// </summary>
        public CreatorOrganization Organization {get; set; }
        public LocalBusinessType BusinessType { get; set; }
    }
}
