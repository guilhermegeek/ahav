﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain.Entities
{
    public class LogEntity
    {
        public ObjectId Id { get; set; }
        public string Message { get; set; }
    }
}
