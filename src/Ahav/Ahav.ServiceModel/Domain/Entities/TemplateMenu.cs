﻿namespace Ahav.ServiceModel.Domain.Entities
{
    public class TemplateMenu
    {
        public string Href { get; set; }
        public HrefTarget Target { get; set; }
    }
    public enum HrefTarget
    {
        Blank, None
    }
}
