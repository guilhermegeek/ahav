﻿using System.Collections.Generic;
using Ahav.Interfaces;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain.Entities
{
    public enum TemplateType
    {
        Content, BlogIndex, BlogArticle
    }
    public class Template : IHasAttachments
    {
        public Template()
        {
            Files = new ObjectId[0];
        }
        public ObjectId Id { get; set; }
        public TemplateType TemplateType { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Body { get; set; }
        public string Less { get; set; }
        public ObjectId[] Files { get; set; }
        public ObjectId FavIcon { get; set; }
        public ObjectId ThumbnailId { get; set; }
        /// <summary>
        /// If any, the client use the images to apply Backstretch to Body (all device port)
        /// </summary>
        public string[] BodyBackStretchImagesUrl { get; set; }
        // Culture, Menu
        public Navbar Menu { get; set; }

        public IList<TemplateTheme> Themes { get; set; }
        public Dictionary<FontSelector, string> Fonts { get; set; }
        /// <summary>
        /// Views 
        /// </summary>
        public Dictionary<ViewsTypes, Dictionary<string,string>> Views { get; set; } 

    }
    public enum ViewsTypes { BlogArticleView, BlogHomeView, StoreProductView, StoreHomeView}
    
    public enum Color
    {
        Red = 0, Blue = 1
    }
    public class TemplateTheme
    {
        public string Name { get; set; }
        public Dictionary<Color, string> Colors { get; set; }
    }
    public class Navbar
    {
        public string Culture { get; set; }
        public IList<NavbarMenu> Menus { get; set; }
    }
    public class NavbarMenu
    {
        public IList<NavbarMenu> Menus { get; set; } 
        public string Name { get; set; }
        public string Href { get; set; }
    }
    /// <summary>
    /// Collection storing the Id of each content that uses the template
    /// Only templates created by the user or replicated from available templates will store this
    /// </summary>
    public class ContentTemplateIds
    {
        public ContentTemplateIds()
        {
            this.ContentId=new ObjectId[0];
        }
        public ObjectId TemplateId { get; set; }
        public ObjectId[] ContentId { get; set; }
    }
    
}
