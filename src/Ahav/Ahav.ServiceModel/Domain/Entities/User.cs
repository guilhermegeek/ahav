﻿using System;
using System.Collections.Generic;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Social.Domain;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public class User
    {
        public User()
        {
            Mobiles = new List<string>();
            Websites = new List<string>();
            PublicEmail = new List<string>();
            Social = new List<UserSocial>();
            Projects = new ObjectId[0];
        }
        public ObjectId Id { get; set; }
        public AccountState AccountState { get; set; }
        public DateTime Birthday { get; set; }
        public string PrimaryEmail { get; set; }
        public string DisplayName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public IList<string> Mobiles { get; set; }
        public IList<string> Websites { get; set; }
        public IList<string> PublicEmail { get; set; }
        public IList<UserSocial> Social { get; set; }
        public ObjectId[] Projects { get; set; }
        
        // Ecommerce
        public ObjectId[] OrdersCompleted { get; set; }
    }
}
