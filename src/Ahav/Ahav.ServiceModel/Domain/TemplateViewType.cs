﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ahav.ServiceModel.Domain
{
    public enum TemplateViewType
    {
        BlogIndex, 
        BlogArticle,
        StoreIndex,
        StoreProduct
    }
}
