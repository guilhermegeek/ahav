﻿using MongoDB.Bson;

namespace Ahav.ServiceModel.Domain
{
    public class OrderOffer
    {
        /// <summary>
        /// Product Id
        /// </summary>
        public ObjectId Id { get; set; }
        public int Quantity { get; set; }
        public Pricing Pricing { get; set; }
        public string Title { get; set; }
    }
}
