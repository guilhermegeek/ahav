﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ahav.ServiceModel.Domain
{
    public enum LocalBusinessType
    {
        AnimalShelter,
        AutomotiveBusiness,
        ChildCare,
        DryCleaningOrLaundry,
        EmergencyService,
        EmploymentAgency,
        EntertainmentBusiness,
        FinancialService,
        FoodEstablishment,
        GovernmentOffice,
        HealthAndBeautyBusiness,
        HomeAndConstructionBusiness,
        InternetCafe,
        Library,
        LodgingBusiness,
        MedicalOrganization,
        ProfessionalService,
        RadioStation,
        RealEstateAgent,
        RecyclingCenter,
        SelfStorage,
        ShoppingCenter,
        SportsActivityLocation,
        Store,
        TelevisionStation,
        TouristInformationCenter,
        TravelAgency
    }
}
