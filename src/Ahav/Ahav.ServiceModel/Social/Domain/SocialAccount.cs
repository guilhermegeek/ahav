﻿using System;

namespace Ahav.ServiceModel.Social.Domain
{
    public class SocialAccount
    {
        public object Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
    }
}
