using System;
using System.Collections.Generic;
using MongoDB.Bson;

namespace Ahav.ServiceModel.Social.Domain
{
    public class UserSocial
    {
        public SocialProvider Provider { get; set; }
        public object ProviderId { get; set; }
        public string Token { get; set; }
        public DateTime ExpiresAt { get; set; }
    }
    //public class ShareRef
    //{
    //    public SocialProvider Provider { get; set; }
    //    public DateTime CreatedAt { get; set; }
    //    public string Content { get; set; }
    //    public string AbsoluteUri { get; set; }
    //}
    public enum SocialProvider
    {
        Facebook, Google, Instagram, Twitter
    }
    }
