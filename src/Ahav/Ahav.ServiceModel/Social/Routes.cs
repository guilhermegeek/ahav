﻿namespace Ahav.ServiceModel.Social
{
    public static class SocialRoute
    {
        public const string RegisterSocial = "/api/social/register";
        public const string PostSocialStream = "/api/social/stream";
        public const string PostSocialLike = "/api/social/like";
    }
}
