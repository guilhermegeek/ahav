﻿using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Interfaces;
using Ahav.ServiceModel.Social.Domain;
using MongoDB.Bson;
using ServiceStack;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;


namespace Ahav.ServiceModel.Social
{
    public class PostFacebookTab : IReturn<PostFacebookTabResponse>
    {
        public string SubDomain { get; set; }
        public ObjectId ContentId { get; set; }
    }
    public class PostFacebookTabResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
    public class GetFacebookTab : IReturn<GetFacebookTabResponse>
    {
        public string SubDomain { get; set; }
    }
    public class GetFacebookTabResponse : IHasResponseStatus
    {
        public string RenderBody { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class RegisterSocial : IReturn<RegisterSocialResponse>
    {
        /// <summary>
        /// Facebook Id, Google Id, etc
        /// </summary>
        public object Id { get; set; }
        public string Token { get; set; }
        public SocialProvider Provider { get; set; }
    }
    public class RegisterSocialResponse : IHasResponseStatus, IHasResponseModal
    {
        public ObjectId UserId { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
    public class PostSocialLike : IReturn<PostSocialLikeResponse>
    {
        public object ObjectId { get; set; }
        public SocialProvider Provider { get; set; }
    }
    public class PostSocialLikeResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
    public class PostSocialAuthenticate : IReturn<PostSocialAuthenticateResponse>
    {
        public string Token { get; set; }
        public SocialProvider Provider { get; set; }
    }
    public class PostSocialAuthenticateResponse : IHasResponseStatus
    {
        public AuthApi Auth { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
    /// <summary>
    /// Publishes a new post on social networks.
    /// </summary>
    public class PostSocialStream : IReturn<PostSocialStreamResponse>
    {
        public ObjectId UserId { get; set; }
        /// <summary>
        /// Absolute uri o
        /// </summary>
        public string AbsoluteUrl { get; set; }
        public string Message { get; set; }
    }
    public class PostSocialStreamResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseClient Modal { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }

}
