﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ahav.ServiceModel.Enums
{
    public enum MediaObjType
    {
        Attachment = 0, 
        Content = 1, 
        Template = 2, 
        Article = 3
    }
}
