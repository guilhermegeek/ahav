﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ahav.ServiceModel.Enums
{
    public enum MediaType
    {
        All = 1, Other = 2, Image = 3, Video = 4
    }
}
