﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Ahav.Infrastructure.Extensions;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;
using ServiceStack.Text;

namespace Ahav.Infrastructure
{
    public static class ServiceStackExtensions
    {
        public static string QueryStringValue(this IHttpRequest request, string parameter)
        {
            return !string.IsNullOrEmpty(request.QueryString[parameter]) ? request.QueryString[parameter] : string.Empty;
        }
        public static string GetCulture()
        {
            return Thread.CurrentThread.CurrentCulture.ToString();

        }

        public static bool IsAjax(this IHttpRequest request)
        {
            return !String.IsNullOrEmpty(request.Headers["X-Requested-With"])
                && request.Headers["X-Requested-With"] == "XMLHttpRequest";
        }
        //public static bool IsAjax(this IHttpRequestContext request)
        //{
        //    return !String.IsNullOrEmpty(request.GetHeader("X-Requested-With"))
        //           && request.GetHeader("X-Requested-With") == "XMLHttpRequest";
        //}
        public static void SetProjectId(this IHttpRequest req, ObjectId projectId)
        {
            req.Items[ProjectExtensions.HttpItemProject] = JsonSerializer.SerializeToString(projectId);
        }
        /// <summary>
        /// Reads the request project Id from Http Items
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public static ObjectId GetProjectId(this IHttpRequest req)
        {
            return req.Items.ContainsKey(ProjectExtensions.HttpItemProject) &&
                   !string.IsNullOrEmpty(req.Items[ProjectExtensions.HttpItemProject].ToString())
                ? JsonSerializer.DeserializeFromString<ObjectId>(req.Items[ProjectExtensions.HttpItemProject].ToString())
                : ObjectId.Empty;
        }
       

        public static ResponseStatus AddError(this ResponseStatus responseStatus, string errorCode, string errorMessage,
            string errorField = null)
        {
            if (responseStatus.Errors == null)
                responseStatus.Errors = new List<ResponseError>();
            responseStatus.Errors.Add(new ResponseError
            {
                ErrorCode = errorCode,
                Message = errorMessage,
                FieldName = errorField ?? ""
            });
            return responseStatus;
        }
        public static bool HasProject(this IHttpRequest request)
        {
            return !request.GetProjectId().IsEmpty();
        }
        // MongoDB
        public static bool IsEmpty(this ObjectId id)
        {
            return id == ObjectId.Empty;
        }

        private static readonly byte[] copyBuf = new byte[0x1000];
        public static void CopyStream(this Stream instance, Stream target)
        {
            int bytesRead = 0;
            int bufSize = copyBuf.Length;

            while ((bytesRead = instance.Read(copyBuf, 0, bufSize)) > 0)
            {
                target.Write(copyBuf, 0, bytesRead);
            }
        }
    }

}
