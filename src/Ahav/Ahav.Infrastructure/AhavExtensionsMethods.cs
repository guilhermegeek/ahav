﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Ahav.Infrastructure.Extensions;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.Common.Web;
using ServiceStack.ServiceClient.Web;
using ServiceStack.ServiceHost;
using ServiceStack.Text;

namespace Ahav.Infrastructure
{
    /// <summary>
    /// Holds main statics methods for Ahav
    /// </summary>
    public static class AhavExtensionsMethods
    {
        public static bool IsAuthenticated(this IHttpRequest req)
        {
            return req.Items.ContainsKey(AccountExtensions.ReqItemAuth)
                   &&
                   JsonSerializer.DeserializeFromString<AhavAuth>(req.Items[AccountExtensions.ReqItemAuth].ToString()) !=
                   null
                   &&
                   JsonSerializer.DeserializeFromString<AhavAuth>(req.Items[AccountExtensions.ReqItemAuth].ToString()).
                       UserId != ObjectId.Empty;
        }

        public static bool IsAdmin(this IHttpRequest req)
        {
            return req.Items.ContainsKey(AccountExtensions.ReqItemRoles)
                   &&
                   JsonSerializer.DeserializeFromString<string[]>(req.Items[AccountExtensions.ReqItemRoles].ToString()).
                       Contains(AccountRole.Administrator);
        }

        public static AhavAuth GetAuth(this IHttpRequest req)
        {
            if (!req.Items.ContainsKey(AccountExtensions.ReqItemAuth))
                //throw HttpError.Unauthorized("Not authorized");
                return null;

            var auth = JsonSerializer.DeserializeFromString<AhavAuth>(req.Items[AccountExtensions.ReqItemAuth].ToString());
            if (auth == null)
                //throw HttpError.Unauthorized("Not authorized");
                return null;
            return auth;
        }


        /// <summary>
        /// Reads the credentials from request headers, query parameters, etc
        /// </summary>
        /// <param name="req"></param>
        /// <returns>Null if not authenticated</returns>
        public static AuthApi GetAuthorization(this IHttpRequest req)
        {
            ObjectId userId; // used as out on parses

            // header
            if (!string.IsNullOrEmpty(req.Headers[AccountExtensions.ClientAuthId])
                && !string.IsNullOrEmpty(req.Headers[AccountExtensions.ClientAuthKey])
                && ObjectId.TryParse(req.Headers[AccountExtensions.ClientAuthId], out userId))
            {
                return new AuthApi { Id = userId, Key = req.Headers[AccountExtensions.ClientAuthKey] };
            }

            // query parameters
            if (!string.IsNullOrEmpty(req.GetParam(AccountExtensions.ClientAuthId)) &&
                !string.IsNullOrEmpty(req.GetParam(AccountExtensions.ClientAuthKey))
                && ObjectId.TryParse(req.GetParam(AccountExtensions.ClientAuthId), out userId))
            {
                return new AuthApi { Id = userId, Key = req.GetParam(AccountExtensions.ClientAuthKey) };
            }

            // cookie
            if (req.Cookies.ContainsKey(AccountExtensions.ClientCookieAuth))
            {
                var auth = JsonSerializer.DeserializeFromString<AuthApi>(req.Cookies[AccountExtensions.ClientCookieAuth].Value);
                if (auth != null)
                {
                    return new AuthApi {Id = auth.Id, Key = auth.Key};
                }
            }
            return null;
        }
        /// <summary>
        /// Return the user id for the current request
        /// Throws Unauthorized exception if request is not authorized
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public static ObjectId GetUserId(this IHttpRequest req)
        {
            if (!req.Items.ContainsKey(AccountExtensions.ReqItemAuth))
                throw HttpError.Unauthorized("Unauthorized");

            AhavAuth auth = JsonSerializer.DeserializeFromString<AhavAuth>(req.Items[AccountExtensions.ReqItemAuth].ToString());
            if (auth == null || auth.UserId == ObjectId.Empty)
                throw HttpError.Unauthorized(string.Format("Auth is null in the Request. Http Item: {0}",
                                                           req.Items[AccountExtensions.ReqItemAuth]));

            return auth.UserId;
        }

        /// <summary>
        /// Sets credentials for JsonServiceClient (first remove existing credentials, then adds it)
        /// </summary>
        /// <param name="client"></param>
        /// <param name="id"></param>
        /// <param name="token"></param>
        public static void SetAuth(this JsonServiceClient client, ObjectId id, string token)
        {
            client.Headers.Remove(AccountExtensions.ClientAuthId);
            client.Headers.Remove(AccountExtensions.ClientAuthKey);
            client.Headers.Add(AccountExtensions.ClientAuthId, id.ToString());
            client.Headers.Add(AccountExtensions.ClientAuthKey, token);
        }
        /// <summary>
        /// Saves the credentials information for current request
        /// </summary>
        /// <param name="req"></param>
        /// <param name="auth"></param>
        public static void SetAuth(this IHttpRequest req, AhavAuth auth)
        {
            req.Items[AccountExtensions.ReqItemAuth] = JsonSerializer.SerializeToString(auth);
            req.Items[AccountExtensions.AngularAuth] =
                JsonSerializer.SerializeToString(new AuthApi {Id = auth.UserId, Key = auth.Token});
        }
        /// <summary>
        /// Saves the credentials information and roles permissions for current request
        /// </summary>
        /// <param name="req"></param>
        /// <param name="auth"></param>
        /// <param name="roles"></param>
        public static void SetAuth(this IHttpRequest req, AhavAuth auth, string[] roles)
        {
            req.Items[AccountExtensions.ReqItemRoles] = JsonSerializer.SerializeToString(roles);
            req.SetAuth(auth);
        }

        /// <summary>
        /// Updates the auth credentials returned from Response (header and cookie)
        /// </summary>
        /// <param name="res"></param>
        /// <param name="auth"></param>
        public static void UpdateAuth(this IHttpResponse res, AhavAuth auth)
        {
            var authResponse = new AuthApi {Id = auth.UserId, Key = auth.Token};
            // if cookie expira, verifica se ainda não foi renovado e manda
            var cookie = new Cookie
                             {
                                 Name = AccountExtensions.ClientCookieAuth,
                                 Value =
                                     JsonSerializer.SerializeToString(new AuthApi {Id = auth.UserId, Key = auth.Token}),
                                 Expires = DateTime.Now.AddDays(31),
                                 Path = "/"
                             };
            res.Cookies.AddCookie(cookie);
            res.AddHeader(AccountExtensions.HeaderAuth, JsonSerializer.SerializeToString(authResponse));
        }

        public static UserInfo ToInfo(this User entity)
        {
            return new UserInfo
                       {
                           Id = entity.Id,
                           DisplayName = entity.DisplayName
                       };
        }

        public static UserDto ToDto(this User user)
        {
            return new UserDto
                       {
                           AccountState = user.AccountState,
                           Id = user.Id,
                           Email = user.PrimaryEmail
                       };
        }
    }
}