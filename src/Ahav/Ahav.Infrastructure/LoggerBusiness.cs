﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Infrastructure;
using Ahav.Interfaces;
using Ahav.ServiceModel.Domain.Entities;

namespace Ahav.ServiceInterface.Business
{
    public class LoggerBusiness : ILogger
    {
        public void Error(string message)
        {
            var coll = MongoDbHelper.Instance.Database.GetCollection<LogEntity>(typeof (LogEntity).Name);
            coll.Insert(new LogEntity {Message = message});
        }
    }
}
