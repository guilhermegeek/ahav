﻿using MongoDB.Driver;
using ServiceStack.Configuration;

namespace Ahav.Infrastructure
{
    public class MongoDbHelper
    {
        private static MongoDbHelper _currentInstance;
        public static MongoDbHelper Instance
        {
            get
            {
                if (_currentInstance == null)
                {
                    _currentInstance = new MongoDbHelper();
                }
                return _currentInstance;
            }
        }
        public MongoDatabase Database
        {
            get
            {
                var appSettings = new AppSettings();
                var client = new MongoClient(appSettings.GetString("MongoDbHost"));
                var server = client.GetServer();

                var db = server.GetDatabase(appSettings.GetString("MongoDbDatabase"), SafeMode.True);
                return db;
            }
        }
    }
}
