﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using ServiceStack.Redis;

namespace Ahav.Infrastructure
{
    public abstract class Repository<T>
    {
        public MongoCollection collection;
        public IRedisClientsManager RedisManager { get; set; }
        public IRedisClient RedisClient { get; set; }
        
        private readonly string _collectionName = typeof (T).Name;
        public const string RedisCollectionComments = "{0}::comment_id";

        public Repository()
        {
            this.collection = MongoDbHelper.Instance.Database.GetCollection(_collectionName);
        }
        public IEnumerable<T> GetAll()
        {
            return collection.FindAllAs<T>();
        }
        public T Get(ObjectId entityId)
        {
            return collection.FindOneByIdAs<T>(entityId);
        }
        public T FindOne(IMongoQuery query)
        {
            return collection.FindOneAs<T>(query);
        }
        public IEnumerable<T> Find<T>(int skip, int take, IMongoQuery[] queries, IMongoFields fields = null, IMongoSortBy sort = null)
        {
            var querieBuilder = new QueryBuilder<T>();
            var news = collection.FindAs<T>(querieBuilder.And(queries))
                .SetFields(fields);
            return news;
        }
        public IEnumerable<T> FindAll<T>(int skip, int take, IMongoFields fields = null, IMongoSortBy sort = null)
        {
            return collection.FindAllAs<T>().SetFields(fields).SetSkip(skip).SetLimit(take);
        }
        public IEnumerable<T> FindAll<T>(int skip, int take, IMongoQuery queries, IMongoFields fields = null, IMongoSortBy sort = null)
        {
            return collection.FindAs<T>(queries).SetFields(fields).SetSkip(skip).SetLimit(take);
        }
        public IEnumerable<T> FindAll<T>(int skip, int take, IMongoQuery[] queries, IMongoFields fields = null, IMongoSortBy sort = null)
        {
            return collection.FindAs<T>(Query.And(queries)).SetFields(fields).SetSkip(skip).SetLimit(take);
        }
        public WriteConcernResult Save(ObjectId id, IMongoUpdate[] updateValues)
        {
            IMongoUpdate update = Update.Combine(updateValues);
            var query = Query.EQ("_id", id);
            return collection.Update(query, update);
        }
        public WriteConcernResult Save(ObjectId id, IMongoUpdate update)
        {
            var query = Query.EQ("_id", id);
            return collection.Update(query, update);
        }
        public WriteConcernResult Save(IMongoQuery query, IMongoUpdate update)
        {
            return collection.Update(query, update);
        }
        public WriteConcernResult Create(T entity)
        {
            return collection.Save(entity);
        }
        public T CreateEntity(T entity)
        {
            var res = collection.Save(entity);
            return entity;
        }

        public NComment SaveComment(ObjectId id, NComment comment)
        {
            using(var client = RedisManager.GetClient())
            {
                comment.Id = client.IncrementValue(string.Format(RedisCollectionComments, _collectionName));
                collection.Update(Query.EQ("_id", id),
                                         Update<NComments>
                                             .Push(x => x.Comments, comment)
                                             .Inc(x => x.CommentsCount, 1)
                );
                return comment;
            }
        }
        public NComment SaveComment(ObjectId id, string message)
        {
            return SaveComment(id, new NComment
            {
                Comment = message,
                CreatedAt = DateTime.Now
            });
        }
        public WriteConcernResult EditComment(ObjectId articleId, long commentId, string newMessage)
        {
            using(var client = RedisManager.GetClient())
            {

                var nComments = collection.FindOneAs<NComments>(Query.And(
                    Query.EQ("_id", articleId),
                    Query.EQ("Comments.$.id", commentId)));
                if(nComments == null)
                    return null;// warn user that the post has already been deleted
                var comment = nComments.Comments.First(x => x.Id == commentId);
                throw new NotImplementedException("CODE THIS!");
                //return collection.Update(Query.And(Query.EQ("_id", articleId), Query.EQ("Comments.id", commentId)),
                //                  Update.Set("Comments.$.message", newMessage)
                //                      .Push("Comments.$.snapshots", comment.ToSnapshot().ToBson()));
            }
        }
        public WriteConcernResult DeleteComment(ObjectId id, long commentId)
        {
            return collection.Update(Query.EQ("_id", id),
                                     Update<NComments>
                                     .Pull(x=>x.Comments, builder => builder.EQ(x => x.Id, commentId))
                                     .Inc(x => x.CommentsCount, -1));
        }
      
        
        public NComments GetNComments(ObjectId id, int skip, int take)
        {
            return collection.FindOneByIdAs<NComments>(id);
            return collection.FindAs<NComments>(Query.EQ("_id", id))
                .SetFields(Fields<NComments>.Include(x => x.Comments).Include(x => x.CommentsCount))
                .FirstOrDefault();
         
        }
        public WriteConcernResult Delete(ObjectId id)
        {
            return collection.Remove(Query.EQ("_id", id));
        }
   }
}