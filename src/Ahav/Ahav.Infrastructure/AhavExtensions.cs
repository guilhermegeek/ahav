﻿using System.Collections.Generic;
using System.Net;
using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Resources;
using ServiceStack.Common;
using ServiceStack.FluentValidation.Results;
using ServiceStack.ServiceInterface.Validation;
using ServiceStack.Validation;
using ServiceStack.FluentValidation;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;
using ServiceStack.Text;

namespace Ahav.Infrastructure
{
    public static class AhavExtensions
    {
        public const string HttpInitItem = "httpi";
        /// <summary>
        /// Internal Http item 
        /// </summary>
        public const string HttpItemIsBaseApp = "baseapp";
        /// <summary>
        /// For requests without pathinfo, it's redirected to this value
        /// This should be restricted for slugs
        /// </summary>
        public const string InternalDefaultPageRedirect = "internal-default-redirect";

        /// <summary>
        /// This flag is resolved at pre request filter. Here we just check the HttpItem 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public static bool IsBaseApp(this IHttpRequest req)
        {
            if (req.Items[HttpItemIsBaseApp] == null)
                return false;

            return JsonSerializer.DeserializeFromString<bool>(req.Items[HttpItemIsBaseApp].ToString());
        }

        public static ResponseClient Success(this ResponseClient client, string message, string title = "",
            ModalType modalType = ModalType.Normal)
        {
            client = new ResponseClient
                     {
                         Message = message,
                         Title = !string.IsNullOrEmpty(title) ? title : "Success",
                         Type = modalType
                     };
            return client;
        }

        public static ResponseClient Success(string message, string title = "", ModalType modalType = ModalType.Normal)
        {
            return new ResponseClient
                   {
                       Message = message,
                       Title = !string.IsNullOrEmpty(title) ? title : "Success",
                       Type = modalType
                   };
        }

        public static ResponseStatus InvalidOperation(string message = "", string errorCode = "internalservererror",
            object[] erros = null)
        {
            if (string.IsNullOrEmpty(message))
            {
                message = "Sorry but an internal error ocurred";
            }
            return new ResponseStatus
                   {
                       ErrorCode = errorCode,
                       Errors = new List<ResponseError> {new ResponseError {ErrorCode = errorCode, Message = message}},
                       Message = "Invalid"
                   };
        }

        public static ResponseStatus DataInternalError(string msg = "Internal error in Data Layer")
        {
            return new ResponseStatus
                   {
                       ErrorCode = "Error",
                       Message = msg
                   };
        }

        public static ResponseStatus DataNotFound(string msg = "Not Found")
        {
            return new ResponseStatus
                   {
                       ErrorCode = "NotFound",
                       Message = msg
                   };
        }

        

        public static ResponseStatus Unauthorized()
        {
            return new ResponseStatus
                   {
                       ErrorCode = "unauthorized",
                       Message = BaseResources.DontHavePermissions
                   };
        }

        public static ResponseStatus InternalServerError()
        {
            return new ResponseStatus
                   {
                       ErrorCode = ErrorBase.InternalServerError,
                       Message = "Internal server error detected"
                   };
        }

        public static void InternalServerError(ref ResponseStatus responseStatus, ref IHttpResponse response)
        {
            responseStatus = InternalServerError();
            response.StatusCode = (int) HttpStatusCode.InternalServerError;
        }

        /// <summary>
        /// Transforms a validation result into a ResponseStatus
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public static ResponseStatus ResultToResponseStatus(this ValidationResult result, string message="The request is invalid")
        {
            var error = result.ToErrorResult();
            return error.ToResponseStatus();
        }
    }

    public static class ErrorBase
    {
        public const string InternalServerError = "internalservererror";
        public const string DontHavePermission = "donthavepermission";
    }
}