﻿using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using ServiceStack.Configuration;

namespace Ahav.Infrastructure.Extensions
{
    public static class MediaExtensions
    {
        public static MediaDto ToDto(this Media entity)
        {
            var path = new AppSettings().Get("FilesUploadUri", @"\files\");
            return new MediaDto
            {
                Id = entity.Id,
                Extension = entity.Extension,
                Name = entity.Name
            };
        }
        public static MediaRef ToRef(this Media entity)
        {
            return new MediaRef
            {
                Id = entity.Id,
                MediaType = entity.MediaType
            };
        }
        public static MediaRef ToRef(this MediaDto entity)
        {
            return new MediaRef
            {
                Id = entity.Id,
                MediaType = entity.MediaType
            };
        }
    }
}
