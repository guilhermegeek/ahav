﻿using System;
using System.Net;
using System.Linq;
using Ahav.ServiceModel.Domain;
using MongoDB.Bson;
using ServiceStack.Common.Web;
using ServiceStack.ServiceClient.Web;
using ServiceStack.ServiceHost;
using ServiceStack.Text;

namespace Ahav.Infrastructure.Extensions
{
    public static class AccountError
    {
        public const string EmailInUse = "emailinuse";
    }
    public static class AccountExtensions
    {
        public const string UserId = "uid";
        public const string HeaderAuth = "mauth";
        public const string ReqItemAuth = "iauth";
        public const string ReqItemRoles = "iroles";
        public const string AngularAuth = "angauth";
        public const string ClientAuthId = "auth-id";
        public const string ClientAuthKey = "auth-key";
        public const string ClientCookieAuth = "auth";
        public const string HttpItemDash = "dash";
        public const string ProjectFeatures = "project::{0}::feature";

        
        
       
    }
}
