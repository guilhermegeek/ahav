﻿using System.Linq;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using Ahav.ServiceModel.Operations;

namespace Ahav.Infrastructure.Extensions
{
    public static class SupportAExtensions
    {
        public static SupportTicketDto ToDto(this SupportTicket ticket)
        {
            if (ticket == null) return null;
            return new SupportTicketDto
            {
                Id = ticket.Id,
                Title = ticket.Title,
                Content = ticket.Content,
                Priority = ticket.Priority,
                State = ticket.State
            };
        }
        public static SupportTicketInfo ToInfo(this SupportTicket ticket)
        {
            return new SupportTicketInfo
            {
                Id = ticket.Id,
                LastComment = ticket.Comments.Any() ? ticket.Comments.Last() : null,
            };
        }
        public static SupportContactDto ToDto(this SupportContact entity)
        {
            return new SupportContactDto
                       {
                           Id = entity.Id,
                           Name = entity.Name,
                           Email = entity.Email,
                           Contact = entity.Contact,
                           Subject = entity.Subject,
                           Message = entity.Message,
                           State = entity.State
                       };
        }
    }
}
