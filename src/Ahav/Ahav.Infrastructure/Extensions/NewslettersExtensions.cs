﻿using System.Linq;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;

namespace Ahav.Infrastructure.Extensions
{
    public static class NewslettersExtensions
    {
        public static NewsletterDto ToDto(this Newsletter newsletter)
        {
            var culture = ServiceStackExtensions.GetCulture();
            return new NewsletterDto
                       {
                           Id = newsletter.Id,
                           Subject = newsletter.Subject.Any(x => x.Key == culture)
                                         ? newsletter.Subject.First(x => x.Key == culture).Value
                                         : newsletter.Subject.FirstOrDefault().Value,
                           Content = newsletter.Content.Any(x => x.Key == culture)
                                         ? newsletter.Content.First(x => x.Key == culture).Value
                                         : newsletter.Content.FirstOrDefault().Value,
                           IsHtml = newsletter.IsHtml,
                           Url = string.Format("/api/newsletter/{0}", newsletter.Id),
                           Name = newsletter.Name
                       };
        }
        public static NewsletterInfo ToInfo(this Newsletter newsletter)
        {
            var culture = ServiceStackExtensions.GetCulture();
            return new NewsletterInfo
                       {
                           Id = newsletter.Id,
                           State = newsletter.State,
                           Subject = newsletter.Subject.Any(x => x.Key == culture)
                           ? newsletter.Subject.First(x => x.Key == culture).Value
                           : newsletter.Subject.FirstOrDefault().Value,
                           Url = string.Format("/api/newsletter/{0}", newsletter.Id),
                           Name = newsletter.Name
                       };
        }
    }
}
