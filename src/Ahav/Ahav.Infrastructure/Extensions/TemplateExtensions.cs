﻿using System.Linq;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;

namespace Ahav.Infrastructure.Extensions
{
    public static class TemplateExtensions
    {
        /// <summary>
        /// Wrapper for Template and Content rendering. 
        /// Inside this markup it's rendered the ContentBody
        /// </summary>
        public const string HtmlDivRenderId = @"<div id=""templateContent"">content</div>";
        /// <summary>
        /// Meta tags parsed to template
        /// </summary>
        public const string MetaTags = "metatags";
        public const string TemplateUiView = "<div data-ui-view></div>";
        /// <summary>
        /// Http Item to store the Template source for view rendering 
        /// </summary>
        public const string HttpItemTemplateSrc = "template_src";
        public static ContentTemplateInfo ToInfo(this Template entity)
        {
            return new ContentTemplateInfo
            {
                Id = entity.Id,
                Name = entity.Name
            };
        }
        public static ContentTemplateDto ToDto(this Template entity)
        {
            var culture = ServiceStackExtensions.GetCulture();
            return new ContentTemplateDto
            {
                Id = entity.Id,
                Name = entity.Name,
                Title = entity.Title == null
                            ? null
                            : entity.Title.Any(x => x.Key == culture)
                                  ? entity.Title.First(x => x.Key == culture).Value
                                  : entity.Title.First().Value,
                Body = entity.Body.Any(x => x.Key == culture)
                           ? entity.Body.First(x => x.Key == culture).Value
                           : entity.Body.First().Value,
                Less = entity.Less,
                Files = entity.Files
            };
        }
    }
}
