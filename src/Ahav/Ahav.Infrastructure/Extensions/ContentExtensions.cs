﻿using System.Linq;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;

namespace Ahav.Infrastructure.Extensions
{
    public static class ContentError
    {
        public const string SlugInUse = "slugInUse";
    }
    public static class ContentExtensions
    {
        public const string ContentThumbnail = "/static/content/{0}/thumbnail";
        public static ContentDto ToDto(this Content entity)
        {
            var culture = ServiceStackExtensions.GetCulture();
            var res = new ContentDto
                       {
                           Id = entity.Id,
                           Title = entity.Title.Any(x => x.Key == culture)
                                       ? entity.Title.First(x => x.Key == culture).Value
                                       : entity.Title.FirstOrDefault().Value,
                           Body = entity.Body.Any(x => x.Key == culture)
                                      ? entity.Body.First(x => x.Key == culture).Value
                                      : entity.Body.FirstOrDefault().Value,
                           Javascript = entity.Javascript,
                           Slug = entity.Slug,
                           Css = entity.Css,
                           Thumbnail = string.Format(ContentThumbnail, entity.Id),
                           ProjectId = entity.ProjectId
                       };
            
                res.TemplateId = entity.Template;
            res.Description = entity.Description == null || !entity.Description.Any()
                                  ? res.Title
                                  : entity.Description.Any(x => x.Key == culture)
                                        ? entity.Description.FirstOrDefault(x => x.Key == culture).Value
                                        : entity.Description.FirstOrDefault().Value;
            return res;

        }

        public static ContentInfo ToInfo(this Content entity)
        {
            var culture = ServiceStackExtensions.GetCulture();
            return new ContentInfo
                       {
                           Id = entity.Id,
                           Title = entity.Title.Any(x => x.Key == culture)
                                       ? entity.Title.First(x => x.Key == culture).Value
                                       : entity.Title.FirstOrDefault().Value,
                           Slug = entity.Slug
                       };
        }
        
    }
}
