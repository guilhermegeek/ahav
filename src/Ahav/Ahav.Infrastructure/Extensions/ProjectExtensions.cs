﻿using System.Linq;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using Ahav.ServiceModel.Dtos;
using MongoDB.Bson;
using ServiceStack.ServiceClient.Web;

namespace Ahav.Infrastructure.Extensions
{
    public static class ProjectError
    {
        public const string NotAvailable = "notavailable";
        public const string AppDomainAlreadyTaken = "appalreadyinuse";
    }
    public  static class ProjectExtensions
    {
        public const string HttpHeaderProjectId = "project";
        public const string HttpItemProject = "project";
        public const string HttpQueryProjectId = "project";
        public const string RedisFeatures = "p::{0}::features";

        public static void SetProjectCredentials(this JsonServiceClient client, ObjectId projectId)
        {
            client.Headers.Remove(HttpHeaderProjectId);
            client.Headers.Add(HttpHeaderProjectId, projectId.ToString());
        }
        public static AhavProjectDto ToDto(this ApplicationAhav entity)
        {
            return new AhavProjectDto
                       {
                           Id = entity.Id,
                           Name = entity.Name,
                           Description = entity.Description,
                           Domain = entity.Domain,
                           Attachments = entity.Attachments,
                           ProjectState = entity.ProjectState
                       };
        }
        public static AhavProjectInfo ToInfo(this ApplicationAhav entity)
        {
            return new AhavProjectInfo
                       {
                           Id = entity.Id,
                           Name = entity.Name
                       };
        }
        public static AhavPackageInfo ToInfo(this AhavPackage entity)
        {
            var culture = ServiceStackExtensions.GetCulture();
            return new AhavPackageInfo
                       {
                           Id = entity.Id,
                           Ammount = entity.Ammount,
                           Title = entity.Title.Any(x => x.Key == culture)
                                       ? entity.Title.First(x => x.Key == culture).Value
                                       : entity.Title.First().Value,
                           Description = entity.Description.Any(x => x.Key == culture)
                                             ? entity.Description.First(x => x.Key == culture).Value
                                             : entity.Description.First().Value,
                           End = entity.End,
                           Features = entity.Features
                       };
        }
    }
}
