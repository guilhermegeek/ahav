﻿namespace Ahav.Infrastructure.Extensions
{
    public static class SlugExtensions
    {
        public static string GetSlug(string path)
        {
            bool isHtml = path.Contains(".html");
            if(isHtml) path = path.Replace(".html", "").ToLower();
            return path;
        }
    }
}
