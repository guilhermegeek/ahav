﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace Carrers.ServiceModel
{
    /// <summary>
    /// DTO used in JobOffert for Company
    /// </summary>
    public class CompanyDataRed
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
    }
}
