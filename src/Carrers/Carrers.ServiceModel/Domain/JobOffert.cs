﻿using MongoDB.Bson;

namespace Carrers.ServiceModel.Domain
{
    public class JobOffert
    {
        public ObjectId Id { get; set; }
        public string Title { get; set; }
        public CompanyDataRed Company { get; set; }
        // City, Country (for remotely)
        public string City { get; set; }
        public bool WorkingRemotely { get; set; }
        public bool OfferRelocation { get; set; }
        public string Description { get; set; }
        public string SkillsAndRequirements { get; set; }
        public string[] Tags { get; set; }
        public bool AllowComments { get; set; }
    }

}
