﻿using Ahav.ServiceInterface.Data;
using City.ServiceModel.Operations;
using ServiceStack.ServiceInterface;

namespace City.Services
{
    public class CityService : Service
    {
        public ApplicationAhavRepository ProjectRepo { get; set; }

        /// <summary>
        /// Creates a new City Project. 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public PostCityResponse Post(PostCity request)
        {
            var response = new PostCityResponse();
            ProjectRepo.AddCity(request.Name, request.Domain);
            return response;
        }
    }
}
