﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using City.ServiceModel.ApiDtos;
using ServiceStack.Common.Web;
using ServiceStack.ServiceInterface;

namespace City.Services
{
    public class CityViewsService : Service
    {
        public HttpResult Get(ViewCityHome request)
        {
            var response = new ViewCityHomeResponse();

            return new HttpResult(response)
            {
                Template = "LayoutCity",
                View = "CityHome"
            };
        }
    }
}
