﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ahav.ServiceModel;
using City.ServiceModel;
using City.ServiceModel.ApiDtos;
using City.ServiceModel.Operations;
using City.Services;
using ServiceStack.Configuration;
using ServiceStack.Redis;
using ServiceStack.ServiceInterface;
using ServiceStack.WebHost.Endpoints;

namespace City
{
    public class CityPlugin : IPlugin
    {
        public void Register(IAppHost appHost)
        {
            
            appHost.RegisterService<CityViewsService>();
            
            appHost.Routes
                .Add<ViewCityHome>(CityRoute.GetHomeView, ApplyTo.Get);
            
            appHost.PreRequestFilters.Add((req, res) =>
                                              {
                                                  using (var redis = req.TryResolve<IRedisClientsManager>().GetClient())
                                                  {
                                                      Uri uri = new Uri(req.AbsoluteUri);
                                                      bool isCity = redis.SetContainsItem("city::domains", uri.Host);

                                                      if (!isCity)
                                                          return;
                                                  }

                                              });
        }
    }
}
