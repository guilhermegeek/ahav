﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace City.ServiceModel.ApiDtos
{
    /// <summary>
    /// DTO for request of sports home page /sports
    /// </summary>
    public class GetSportsView : IReturn<GetSportsViewResponse>
    {

    }
    public class GetSportsViewResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}