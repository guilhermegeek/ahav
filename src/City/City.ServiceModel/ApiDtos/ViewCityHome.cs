﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace City.ServiceModel.ApiDtos
{
    /// <summary>
    /// Home page City
    /// </summary>
    public class ViewCityHome : IReturn<ViewCityHomeResponse>
    {
        
    }
    public class ViewCityHomeResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
