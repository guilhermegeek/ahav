﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace City.ServiceModel
{
    public class CityAppModel
    {
        /// <summary>
        /// City application Id
        /// </summary>
        public ObjectId AppId { get; set; }
        /// <summary>
        /// Authenticated user id
        /// </summary>
        public ObjectId UserId { get; set; }
        /// <summary>
        /// Auth token
        /// </summary>
        public string Token { get; set; }
    }
}
