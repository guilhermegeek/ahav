﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace City.ServiceModel.Enums
{
    public enum RouteDificulty
    {
        Beginner, Easy, Normal
    }
}
