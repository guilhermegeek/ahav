﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace City.ServiceModel.Dtos
{
    public class StuvLineDto
    {
        public ObjectId Id { get; set; }
        /// <summary>
        /// Line code. Could be 1 or 1-a, etc
        /// </summary>
        public string Code { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
    }
}
