﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson;

namespace City.ServiceModel.Domain
{
    public class Route
    {
        public Route()
        {
            this.Medias = new MediaRef[0];
            this.Categories=new RouteCategoryRef[0];
            this.Places=new Dictionary<decimal, RoutePlace>();
        }
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Title { get; set; }
        /// <summary>
        /// Description of Route, the main information
        /// </summary>
        public Dictionary<string, string> Description { get; set; }
        /// <summary>
        /// Key - Distance from the previous place (first is zero)
        /// Value - RoutePlace dto
        /// </summary>
        public Dictionary<decimal, RoutePlace> Places { get; set; }
        /// <summary>
        /// DTOs for Route Categories
        /// </summary>
        public RouteCategoryRef[] Categories { get; set; }

        /// <summary>
        /// Total distance of places 
        /// </summary>
        public decimal Distance { get; set; }

        public MediaRef[] Medias { get; set; }
    }
}
