﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace City.ServiceModel.Domain
{
    /// <summary>
    /// Collection Route Category
    /// </summary>
    public class RouteCategory
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Body { get; set; }
        public ObjectId[] RouteIds { get; set; }
    }
}
