﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace City.ServiceModel.Domain
{
    /// <summary>
    /// Route Place is a embed document, copied from Place
    /// </summary>
    public class RoutePlace
    {
        public ObjectId RouteId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public Dictionary<string, string> Title { get; set; }
        /// <summary>
        /// Place description
        /// </summary>
        public Dictionary<string, string> Description { get; set; }
    }
}
