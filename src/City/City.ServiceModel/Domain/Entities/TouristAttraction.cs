﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.Interfaces;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain.Entities;
using MongoDB.Bson;

namespace City.ServiceModel.Domain.Entities
{
    public class TouristAttraction : IThink, IPlace 
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Description { get; set; }
        public string Address { get; set; }
        public double[] Location { get; set; }
        public OpeningHoursSpecification[] OpeningHours { get; set; }
    }
}
