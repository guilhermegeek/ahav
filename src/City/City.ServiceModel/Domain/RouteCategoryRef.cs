﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace City.ServiceModel.Domain
{
    /// <summary>
    /// This DTO is a copy of Route Category, used as an embed document 
    /// </summary>
    public class RouteCategoryRef
    {
        public ObjectId Id { get; set; }
        public Dictionary<string, string> Title { get; set; }
    }
}
