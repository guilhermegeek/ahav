﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace City.ServiceModel.Domain
{
    /// <summary>
    /// Sports leagues
    /// </summary>
    public class SportLeague
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public ObjectId CityAppId { get; set; }
    }
}
