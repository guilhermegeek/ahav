﻿using MongoDB.Bson;

namespace City.ServiceModel.Domain
{
    public class StuvLine
    {
        public ObjectId Id { get; set; }
        /// <summary>
        /// Line code. Could be 1 or 1-a, etc
        /// </summary>
        public string Code { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
    }
}
