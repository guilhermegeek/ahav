﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace City.ServiceModel.Operations
{
    public class PostCity : IReturn<PostCityResponse>
    {
        public string Name { get; set; }
        public string Domain { get; set; }
    }
    public class PostCityResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
