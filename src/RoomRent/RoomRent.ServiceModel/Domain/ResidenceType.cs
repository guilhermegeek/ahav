﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomRent.ServiceModel.Domain
{
    public enum ResidenceType
    {
        ApartamentComplex,
        GatedResidenceCommunity,
        SingleFamilyResidence
    }
}
