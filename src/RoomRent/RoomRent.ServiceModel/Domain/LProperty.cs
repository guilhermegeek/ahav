﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace RoomRent.ServiceModel.Domain
{
    public class LProperty
    {
        public ObjectId Id { get; set; }
        public ObjectId OwnerId { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Description { get; set; }
        public string Address { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public HouseFeature[] Features { get; set; }
        /// <summary>
        /// Who is suitable for this room?
        /// </summary>
        public RoomSuitableTo[] SuitableTo { get; set; }
        /// <summary>
        /// Contains the bills the tenant will have to pay
        /// </summary>
        public HouseBill[] BillsAssigned { get; set; }

        public ObjectId[] RoomsIds { get; set; }
    }
}
