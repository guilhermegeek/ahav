﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomRent.ServiceModel
{
    public enum RoomSuitableTo
    {
        ErasmusStudents = 1,
        DomesticStudents = 2,
        InternationalStudents = 3,
        OldPeople = 4
    }
}
