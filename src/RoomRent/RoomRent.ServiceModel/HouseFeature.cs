﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomRent.ServiceModel
{
    public enum HouseFeature
    {
        Furnished=1, 
        WashingMachine=2, 
        Wifi=3, 
        SmokingAllowed=4, 
        CleanService = 5, 
        EquippedKitchen = 6, 
        CableTv = 7,
        // Bathroom inside the room
        EnSuiteBathroom = 8,
        DoubleBed = 9,
        Parking = 10,
        Gym = 11,
        BikeStorage = 12,
        Pool = 13
    }
}
