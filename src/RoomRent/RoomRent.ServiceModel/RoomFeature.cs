﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomRent.ServiceModel
{
    public enum RoomFeature
    {
        Wifi = 1,
        Deskchair = 2,
        BedLinen = 3,
        Desk = 4,
        Wardrobe = 5,
        Window = 6
    }
}
