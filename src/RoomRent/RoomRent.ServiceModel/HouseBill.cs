﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomRent.ServiceModel
{
    public enum HouseBill
    {
        Internet = 1,
        Gaz = 2,
        Light = 3,
        Water = 4
    }
}
