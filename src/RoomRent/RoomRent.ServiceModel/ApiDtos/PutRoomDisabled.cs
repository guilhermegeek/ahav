﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace RoomRent.ServiceModel.ApiDtos
{
    /// <summary>
    /// Disable a active room, it's no more for rent
    /// </summary>
    public class PutRoomDisabled : IReturn<PutRoomDisabledResponse>
    {
        public ObjectId RoomId { get; set; }
    }
    public class PutRoomDisabledResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
