using System.Collections.Generic;
using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace RoomRent.ServiceModel.ApiDtos
{
    public class PutProperty : IReturn<PutPropertyResponse>
    {
        public ObjectId HouseId { get; set; }
        public string Name { get; set; }
        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Description { get; set; }
        public string Address { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public HouseFeature[] Features { get; set; }
        /// <summary>
        /// Who is suitable for this room?
        /// </summary>
        public RoomSuitableTo[] SuitableTo { get; set; }
        /// <summary>
        /// Contains the bills the tenant will have to pay
        /// </summary>
        public HouseBill[] BillsAssigned { get; set; }

    }
    public class PutPropertyResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}