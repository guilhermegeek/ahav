﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace RoomRent.ServiceModel.ApiDtos
{
    /// <summary>
    /// Publishes a room and make his visible, available for rent
    /// </summary>
    public class PutRoomForRent : IReturn<PutRoomForRentResponse>
    {
        public ObjectId RoomId { get; set; }
    }
    public class PutRoomForRentResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
