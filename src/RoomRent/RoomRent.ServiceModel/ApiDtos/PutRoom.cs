﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Domain;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace RoomRent.ServiceModel.ApiDtos
{
    /// <summary>
    /// Update Room. Property Id is not updated
    /// </summary>
    public class PutRoom : IReturn<PutRoomResponse>
    {
        public ObjectId RoomId { get; set; }

        public DateTime AvailableFrom { get; set; }
        public int AreaSquareMeter { get; set; }

        public Dictionary<string, string> Title { get; set; }
        public Dictionary<string, string> Description { get; set; }

    }
    public class PutRoomResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
