﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ahav.ServiceModel;
using Ahav.ServiceModel.Common;
using Ahav.ServiceModel.Interfaces;
using MongoDB.Bson;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace RoomRent.ServiceModel.ApiDtos
{
    /// <summary>
    /// Creates a new Room. Room isn't visible (must be published first)
    /// Associates room with the Property
    /// </summary>
    public class PostRoom : IReturn<PostRoomResponse>
    {
        /// <summary>
        /// House wich rooms pertences
        /// </summary>
        public ObjectId PropertyId { get; set; }
        /// <summary>
        /// Internal Name
        /// </summary>
        public string Name { get; set; }
    }
    public class PostRoomResponse : IHasResponseStatus, IHasResponseModal
    {
        public ResponseStatus ResponseStatus { get; set; }
        public ResponseClient Modal { get; set; }
    }
}
