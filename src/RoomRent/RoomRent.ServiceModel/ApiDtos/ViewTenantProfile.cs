﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace RoomRent.ServiceModel.ApiDtos
{
    public class ViewTenantProfile : IReturn<ViewTenantProfileResponse>
    {

    }
    public class ViewTenantProfileResponse : IHasResponseStatus
    {
        /// <summary>
        /// Total rooms user is following
        /// </summary>
        public int RoomsFavouriteCount { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
