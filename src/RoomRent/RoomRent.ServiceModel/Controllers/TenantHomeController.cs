﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace RoomRent.ServiceModel.Controllers
{
    public class TenantHomeController : IReturn<TenantHomeControllerResponse>
    {
        
    }
    public class TenantHomeControllerResponse : IHasResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
