﻿using ServiceStack.ServiceInterface;

namespace RoomRent.ServiceInterface.Controllers
{
    /// <summary>
    /// Main service for pages of Room Rent like about, faq, etc
    /// </summary>
    public class RoomRentController : Service
    {
    }
}
