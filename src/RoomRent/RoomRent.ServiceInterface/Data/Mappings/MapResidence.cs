﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization;
using RoomRent.ServiceModel.Domain.Entities;

namespace RoomRent.ServiceInterface.Data.Mappings
{
    public static class MapResidence
    {
        public static void Register(){
            if (!BsonClassMap.IsClassMapRegistered(typeof (Residence)))
            {
                BsonClassMap.RegisterClassMap<Residence>(m =>
                                                         {
                                                             m.MapProperty(x => x.ResidenceType);
                                                         });
            }
        }
    }
}
