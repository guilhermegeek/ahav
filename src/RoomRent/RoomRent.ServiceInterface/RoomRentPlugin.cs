﻿using System;
using Ahav.ServiceInterface;
using RoomRent.ServiceInterface.Controllers;
using RoomRent.ServiceInterface.Data.Mappings;
using RoomRent.ServiceInterface.Services;
using ServiceStack.WebHost.Endpoints;

namespace RoomRent.ServiceInterface
{
    public class RoomRentPlugin : IPlugin
    {
        public void Register(IAppHost appHost)
        {
            var container = appHost.GetContainer();

            // Enforce 
            if(!appHost.Plugins.Contains(new AhavPlugin()))
                throw new Exception("Ahav plugin not added to app host");


            // API and Controllers depencies
            appHost.RegisterService<LandlordService>();
            appHost.RegisterService<LandlordController>();
            appHost.RegisterService<TenantService>();
            appHost.RegisterService<TenantController>();
            appHost.RegisterService<RoomService>();
            appHost.RegisterService<RoomRentController>();
            appHost.RegisterService<PropertyService>();

            // MongoDB Mappings
            MapResidence.Register();


            appHost.PreRequestFilters.Add((request, response) =>
                                              {
                                                  

                                              });

        }
    }
}
