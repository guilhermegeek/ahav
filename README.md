# Ahav
Platafform built on top of ServiceStack and running on mono (3.x) supporting .NET 4.5
Ahav is a base project, used by others like City or RoomRent also published here. 
Frontend is designed to run on a web browser or a cross mobile application like PhoneGap. Offline support for database is mostly done with localstorage (both abble for web and mobile). LESS code is pre compiled but you can use a less nodejs module for that.
For data storage, i'm using MongoDB for almost everything, HBase for message/realtime non-block system built with Python Tornado (from FB team), Redis for data that requires fast access (like tokens).
SEO friendly with www.schema.org in mind when designed the platafform. All searchs on Google should have author, product, etc meta tags.
Ahav base project has main services like email, page content or templates to be consumed by third projects.

* Templates - Each template contains multiples template pages (ie: content, product, article, etc). I'm now working on a editor similar to Wix (aiming for better of couse) supporting Bootstrap themes with layout editor for rows, mobile versions, etc
* Contents - Contents are regular pages. Like article and product pages, Content pages can contain AngularJS directives
* Store - Ecommerce features like product catalog, iventory, sales management, etc. UI Web @TODO
* Magazine - Blog like feature that can handle a tech blog, photo blog, news blog (ie: City), etc
* Social Content - All content from web sites, ecommerces, cities apps can be shared with social networks. Facebook tabs supported ASAP :)
* Invoice - Invoice management. This services is consumed by City and Ahav (ie: package order, even a free or trial)

The above features are used by others projects.

# City 
Eacy city represents a Ahav Application but with more features. The template isn't dynamic like regular Ahav Applications
Examples like x-lisbon.pt, x-viseu.pt, etc

* Routes - Tourism routes with tips and advices, media attached. (next: augmented reality implemented)
* League sports - Enables to manager N sports leagues, with N teams, matches, etc
* RoomRent - Portal for Tenants and Landlords. Landlord can manage tasks like maitainence, monthly automatic invoices, etc. Augmented reality is also aplyied to Tenants search rooms 
* Carrers - Potal for job post/seek - employe/company

# RoomRent 
Room rent managing landlord and tenant, similar to Uniplaces but with a lot more features :)

* Invoice - Create automatic invoices for tenants. Charge extra bills like Internet, TV, etc
* Maintenance Request - Tenant can request a maintenance for a broken toillet or TV not working

# Market
Online market using Store and main Ahav plugins like contents.
Similar to OLX and others service, but allowing: buy, sell, trade and auction.
Like City, Market is oriented to each City and a global frontend allowing national and international searchs.

* Product Catalog -  Available under html and PDF (using the same template)
* Auctions Management 
* Seek Product - watch a product for certains parameters

## Notes
* All this applications run under the same AppHost, under a nginx proxy with fastcgi. Saving $ with mono (ideally i can have 50 cities applications running under the same server, under a single "website" service)
* Almost every code on server side is tested with test driven development pattern. The same for AngularJS code like services, controllers and directives
* Benchmarks: I haven't done any massive test like 10000 concurrent connections on realtime but as soon as i get a decent server i'll work on this. For now all tests are passing with reasonable results, <500ms and even <50ms

# Copying
Ahav source code is available under GNU Affero General Public License, see LICENCE.txt in the source. Alternative commercial licensing will soon be available.